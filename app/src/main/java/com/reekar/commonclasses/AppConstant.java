package com.reekar.commonclasses;


public class AppConstant {
//  public static final String BASE_URL = "http://192.168.1.199/reekar/api/api/v1/";   /*--Arvind sir --*/
//    public static final String BASE_URL = "http://192.168.1.97/reecar/api/v1/";         /*-- Suhail sir --*/

//  public static final String BASE_URL = "http://103.25.128.66/~demo/reekar/api/v1/";
//  public static final String BASE_URL = "http://13.235.135.178/api/v1/"; //live aws main url
  public static final String BASE_URL = "https://www.reekar.com/api/v1/"; //live aws main url


    //-------------URL for static content ---------------------------------
//    public  static final String BASE_URL_STATIC="http://192.168.1.97/reecar/static/content/";
//  public  static final String BASE_URL_STATIC="http://13.235.135.178/static/content/";  // live url
    public  static final String BASE_URL_STATIC="https:/reekar.com/static/content/";  // live url
    public  static final String URL_POLICY="privacy-policy?lang=";
    public  static final String URL_TERMS="terms-and-condition?lang=";


    static final boolean BOOL_LOG = true;
    static final boolean BOOL_SNACKBAR = true;
    static final boolean BOOL_TOAST = true;

    public  static final String DASHBOARD = "dashboard";
    public  static final String LIST = "list";
    public  static final String BOOK = "book";
    public  static final String LOGIN = "login";
    public  static final String MAIN_FRAGMENT = "main_fragment";
    public  static final String BOOKCAR_FRAGMENT = "main_fragment";
    public  static final String LISTCAR_FRAGMENT = "main_fragment";
    public  static final String PAYMENT_FRAGMENT = "payment_fragment";
    public  static final String CONTACT_FRAGMENT = "contact_fragment";
    public  static final String PROFILE_FRAGMENT = "profile_fragment";
    public  static final String MYLISTED_FRAGMENT = "mylisted_fragment";
    public  static final String MYBOOKED_CAR_FRAGMENT = "mylisted_fragment";
    public  static final String LISTVIEW_FRAGMENT = "listview_fragment";
    public  static final String MAPVIEW_FRAGMENT = "mapview_fragment";
    public  static final String MY_ORDERS_FRAGMENT = "my_orders_fragment";
    public  static final String LOGIN_FRAGMENT = "login_fragment";

    public  static final String PAYACTIVITY = "pay_activity";
    public  static final String BANKACTIVITY = "bank_activity";
    public  static final String CC_CARD_ACTIVITY = "cc_card_activity";
    public  static final String PAYPAL_ACTIVITY = "paypal_activity";
    public  static final String VERIFY_ACTIVITY = "verify_activity";
    public  static final String FORGOT_ACTIVITY = "forgot_activity";
    public  static final String CHANGE_PASS_ACTIVITY = "change_pass_activity";
    public  static final String OTP_ACTIVITY = "otp_activity";
    public  static final String CAR_DETAIL_ACTIVITY = "car_details_activity";
    public  static final String CAR_AVAILABILITY_ACTIVITY = "car_availability_activity";
    public  static final String PAYMENT_SUCCESSFUL_ACTIVITY = "payment_successful_activity";
    public  static final String LISTED_CAR_DETAILS_ACTIVITY = "listed_car_details_activity";
    public  static final String FOR_EDIT = "for_edit";
    public  static final String WHO_WE_ARE = "who_we_are";
    public  static final String TERMS = "terms";
    public  static final String POLICY = "policy";
    public  static final String SIGN_UP_ACTIVITY = "sign_up_activity";

    public static final  String ACCESS_CODE = "accesscode";
    public static final String IS_LOGIN = "is_login";
    public static final String IS_SKIP = "is_skip";

    public static final String SAVE_LOGIN_EMAIL = "save_login_email";
    public static final String SAVE_LOGIN_PASS = "save_login_pass";

    public static final String REG_TOKEN = "reg_token";
    public static final String DEVICE_TOKEN = "device_token";
    public static final int SUCCESS_CODE_201 = 201;
    public static final int SUCCESS_CODE = 200;
    public static final int VALIDATION_ERROR_CODE = 422;
    public static final int UNAUTHORIZED_CODE = 401;
    public static final int NOT_FOUND_CODE = 404;
    public static final int REQUEST_CODE_GPS = 202;
    public static final String FROM = "from";

    public static final String DEVICE_TYPE = "android";
    public static final String USER_EMAIL ="user_email";
    public static final String PASSWORD ="password";
    public static final String USER_NAME ="user_name";
    public static final String FIRST_NAME ="first_name";
    public static final String LAST_NAME ="last_name";
    public static final String USER_IMAGE ="user_image";
    public static final String USER_NUMBER ="user_number";
    public static final String USER_ID ="user_id";
    public static final String COUNTRY_CODE ="country_code";
    public static final String ADDRESS ="address" ;
    public static final String IMAGE_URI ="image_uri" ;

    public static final String DATA ="data" ;
    public static final String BRAND ="brand" ;
    public static final String BRAND_ID ="brand_id" ;

    public static final String CAR_MODEL ="Model" ;
    public static final String CAR_SEATS ="Seats" ;
    public static final String CAR_BRAND ="Brand" ;
    public static final String CAR_MFG_YEAR ="Year" ;
    public static final String CAR_FUEL_TYPE ="Engine Type" ;
    public static final String CAR_TRANSMISSION_TYPE ="Transmission" ;
    public static final String CAR_COLOR ="Color" ;
    public static final String VEHICLE_TYPE ="Vehicle Type" ;
    public static final String MILEAGE ="Mileage" ;
    public static final String CHASSIS_NUMBER ="Chassis Number" ;
    public static final String IS_LICENSED ="is_licensed" ;

    public static final String CAR_ID ="car_id" ;
    public static final String CAR_BRAND_ID ="Brand_id" ;
    public static final String CAR_FUEL_TYPE_ID ="engine_type_id" ;
    public static final String CAR_MODEL_ID ="model_id" ;
    public static final String CAR_TRANSMISSION_TYPE_ID ="transmission_id" ;
    public static final String CAR_COLOR_ID ="color_id" ;
    public static final String VEHICLE_TYPE_ID ="vehicle_id" ;
    public static final String SEAT_ID ="seat_id" ;



    public static final String PAYMENT_TYPE ="payment_type";
    public static final String IBAN ="IBAN";
    public static final String CC ="CC";
    public static final String PAYPAL ="Paypal";
    public static final String CHEKOUT ="Checkout";

    public static final String PAYMENT_DETAILS ="paymentDetails";
    public static final String PAYMENT_AMOUNT ="paymentAmount";


    //--- file type---
    public static final String DELETE_DOC_TYPE ="car_docs";
    public static final String DELETE_IMAGE_TYPE ="car_image";

    public static final String PDF ="application/pdf";
    public static final String MS_WORD ="application/msword";
    public static final String MS_WORD_DOC ="application/doc";
    public static final String IMAGE_JPEG ="image/jpeg";
    public static final String IMAGE_JPG ="image/jpg";
    public static final String IMAGE_PNG ="image/png";
    public static final String IMAGE_TYPE ="image/*";

    public static final String LONGITUDE ="longitude";
    public static final String LATITUDE ="latitude";
    public static final String PICKUP_TIME ="pickup_time";
    public static final String DROP_TIME ="drop_time";
    public static final String PICKUP_DATE ="pickup_date";
    public static final String DROP_DATE ="drop_date";
    public static final String USD ="USD";
    public static final String EURO ="EURO";
    public static final String PPD_LTH ="PPD_LTH";
    public static final String PPD_HTL ="PPD_HTL";
    public static final String TP_LTH ="TP_LTH";
    public static final String TP_HTL ="TP_HTL";
    public static final String ID ="id";
    public static final String AC ="ac";
    public static final String GPS ="gps";
    public static final String IPOD_INTERFACE ="ipod_interface";
    public static final String SUNROOF ="sunroof";
    public static final String CHILD_SEAT ="child_seat";
    public static final String ELECTRIC_WINDOW ="electric_windows";
    public static final String HEATED_SEAT ="heated_seat";
    public static final String GAUGE ="prm_gauge";
    public static final String ABS ="abs";
    public static final String TRACTION_CONTROL ="traction_control";
    public static final String AUDIO_SYSTEM ="audio_system";
    public static final String PANORMA_ROOF ="panorma_roof";
    public static final String CRUISE_CONTROL ="cruise_control";
    public static final String CAR_LIST ="car_list";


    public static final String[] FEATURES_KEY = {AC,GPS,IPOD_INTERFACE,SUNROOF,CHILD_SEAT,ELECTRIC_WINDOW,HEATED_SEAT
            ,PANORMA_ROOF,GAUGE,ABS,TRACTION_CONTROL,AUDIO_SYSTEM,CRUISE_CONTROL};

    public static final String AED ="AED";
    public static final String BOOKING_FROM_DATE ="booking_from_date";
    public static final String BOOKING_FROM_TIME ="booking_from_time";
    public static final String BOOKING_TO_DATE ="booking_to_date";
    public static final String BOOKING_TO_TIME ="booking_to_time";
    public static final String CURRENCY ="currency";
    public static final String TOTAL_AMOUNT ="total_amount";
    public static final String PRICE_PER_DAY ="price_per_day";
    public static final String SECURITY_AMOUNT ="security_amount";
    public static final String TAX_AMOUNT ="tax_amount";
    public static final String GRAND_TOTAL ="grand_total";
    public static final String GRAND_TOTAL_USD ="grand_total_usd";
    public static final String BILLING_FIRST_NAME ="billing_first_name";
    public static final String BILLING_LAST_NAME ="billing_last_name";
    public static final String BILLING_EMAIL ="billing_email";
    public static final String BILLING_MOBILE ="billing_mobile";
    public static final String BILLING_ALTERNATIVE_MOBILE ="billing_alternative_mobile";
    public static final String BILLING_ADDRESS ="billing_address";
    public static final String PICKUP_LOCATION ="pickup_location";
    public static final String CURRENT_LANGUAGE ="current_language";
    public static final String CURRENT_CURRENCY ="USER-LOCATION-CURRENCY";
    public static final String CURRENT_CURRENCY_SYMBOL="CURRENT_CURRENCY_SYMBOL";

}

