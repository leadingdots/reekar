package com.reekar.commonclasses;

import android.content.Context;

import com.akanksha.commonclassutil.PreferencesManager;
import com.google.common.reflect.TypeToken;
import com.google.gson.Gson;
import com.reekar.models.CurrencyModel;

import java.util.ArrayList;
import java.util.List;

public class CurrencyJson {

    private static String currency="[{\"key\":\"USD\",\"symbol\":\"$\"},{\"key\":\"AED\",\"symbol\":\"AED\"},{\"key\":\"ARS\",\"symbol\":\"$\"},{\"key\":\"AUD\",\"symbol\":\"$\"},{\"key\":\"BGN\",\"symbol\":\"Лв.\"},{\"key\":\"BRL\",\"symbol\":\"R$\"},{\"key\":\"BSD\",\"symbol\":\"B$\"},{\"key\":\"CAD\",\"symbol\":\"$\"},{\"key\":\"CHF\",\"symbol\":\"Fr.\"},{\"key\":\"CLP\",\"symbol\":\"$\"},{\"key\":\"CNY\",\"symbol\":\"¥\"},{\"key\":\"COP\",\"symbol\":\"$\"},{\"key\":\"CZK\",\"symbol\":\"Kč\"},{\"key\":\"DKK\",\"symbol\":\"Kr.\"},{\"key\":\"DOP\",\"symbol\":\"$\"},{\"key\":\"EGP\",\"symbol\":\"E£\"},{\"key\":\"EUR\",\"symbol\":\"€\"},{\"key\":\"FJD\",\"symbol\":\"FJ$\"},{\"key\":\"GBP\",\"symbol\":\"£\"},{\"key\":\"GTQ\",\"symbol\":\"Q\"},{\"key\":\"HKD\",\"symbol\":\"$\"},{\"key\":\"HRK\",\"symbol\":\"kn\"},{\"key\":\"IDR\",\"symbol\":\"Rp\"},{\"key\":\"ILS\",\"symbol\":\"₪\"},{\"key\":\"INR\",\"symbol\":\"₹\"},{\"key\":\"ISK\",\"symbol\":\"kr\"},{\"key\":\"JPY\",\"symbol\":\"¥\"},{\"key\":\"KRW\",\"symbol\":\"₩\"},{\"key\":\"KZT\",\"symbol\":\"₸\"},{\"key\":\"MXN\",\"symbol\":\"$\"},{\"key\":\"MYR\",\"symbol\":\"RM\"},{\"key\":\"NOK\",\"symbol\":\"kr\"},{\"key\":\"NZD\",\"symbol\":\"$\"},{\"key\":\"PAB\",\"symbol\":\"B/.\"},{\"key\":\"PEN\",\"symbol\":\"S/\"},{\"key\":\"PHP\",\"symbol\":\"₱\"},{\"key\":\"PKR\",\"symbol\":\"₨\"},{\"key\":\"PLN\",\"symbol\":\"zł\"},{\"key\":\"PYG\",\"symbol\":\"₲\"},{\"key\":\"RON\",\"symbol\":\"lei\"},{\"key\":\"RUB\",\"symbol\":\"\u20BD\"},{\"key\":\"SAR\",\"symbol\":\"SAR\"},{\"key\":\"SEK\",\"symbol\":\"kr\"},{\"key\":\"SGD\",\"symbol\":\"$\"},{\"key\":\"THB\",\"symbol\":\"฿ \"},{\"key\":\"TRY\",\"symbol\":\"₺\"},{\"key\":\"TWD\",\"symbol\":\"$\"},{\"key\":\"UAH\",\"symbol\":\"₴\"},{\"key\":\"UYU\",\"symbol\":\"$\"},{\"key\":\"ZAR\",\"symbol\":\"R\"}]";

    public static String updateCurrency(Context context) {
        String symbol="";
        ArrayList<CurrencyModel> arlCurrency =new ArrayList<>();
        arlCurrency = new Gson().fromJson(currency, new TypeToken<List<CurrencyModel>>() {}.getType());
        for (CurrencyModel currencyModel : arlCurrency) {
            if (currencyModel.getKey().equals(PreferencesManager.Companion.getStringPreferences(context,AppConstant.CURRENT_CURRENCY))) {
                symbol=currencyModel.getSymbol();
                break;
            }

        }
        return symbol;
    }
}
