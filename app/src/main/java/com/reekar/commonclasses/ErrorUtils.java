package com.reekar.commonclasses;

import android.content.Context;

import com.reekar.apiservices.APIClient;
import com.reekar.models.ApiResponse;

import java.io.IOException;
import java.lang.annotation.Annotation;

import okhttp3.ResponseBody;
import retrofit2.Converter;
import retrofit2.Response;


public class ErrorUtils {
    public static ApiResponse parseError(Response<?> response, Context mContext) {

        Converter<ResponseBody, ApiResponse> converter = APIClient.getClient(mContext)
                .responseBodyConverter(ApiResponse.class, new Annotation[0]);

        ApiResponse error;

        try {
            error = converter.convert(response.errorBody());
        } catch (IOException e) {
            return new ApiResponse();
        }

        return error;
    }
}
