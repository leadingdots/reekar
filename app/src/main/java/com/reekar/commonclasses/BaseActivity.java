package com.reekar.commonclasses;

import android.os.Bundle;
import android.view.View;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import com.akanksha.commonclassutil.PreferencesManager;
import com.reekar.translation.ChangeLanguage;

public abstract class BaseActivity extends AppCompatActivity implements View.OnClickListener {

    public abstract void setToolbar();
    public abstract void setListener();


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);


    }
}
