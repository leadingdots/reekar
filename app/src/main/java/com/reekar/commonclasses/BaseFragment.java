package com.reekar.commonclasses;

import android.view.View;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;

import com.google.android.gms.common.ConnectionResult;

public abstract class BaseFragment extends Fragment implements View.OnClickListener {

    public static boolean isGpsEnabled;

    public abstract void setToolbar();
    public abstract void setListener();

//    public abstract void onConnectionFailed(@NonNull ConnectionResult connectionResult);
}
