package com.reekar.commonclasses;

import android.app.Activity;
import android.app.ActivityManager;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.Signature;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.graphics.drawable.AnimationDrawable;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.LayerDrawable;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.wifi.WifiInfo;
import android.net.wifi.WifiManager;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.format.Formatter;
import android.text.style.ForegroundColorSpan;
import android.util.Base64;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.webkit.MimeTypeMap;
import android.widget.CompoundButton;
import android.widget.RatingBar;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AlertDialog;
import androidx.core.content.ContextCompat;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentActivity;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;
import androidx.recyclerview.widget.LinearLayoutManager;

import com.akanksha.commonclassutil.LogUtil;
import com.akanksha.commonclassutil.PreferencesManager;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.reekar.R;
import com.reekar.activity.MainActivity;
import com.reekar.adapter.FilterAdapter;
import com.reekar.adapter.FilterListItemAdapter;
import com.reekar.databinding.FilterDialogLayoutBinding;
import com.reekar.databinding.SortByDialogLayoutBinding;
import com.reekar.databinding.TakePictureDailogBinding;
import com.reekar.interfaces.OkCancelInterface;
import com.reekar.interfaces.OnFilterClick;
import com.reekar.interfaces.OnLocationChangeInterface;
import com.reekar.interfaces.OnSortItemClick;
import com.reekar.interfaces.RecyclerviewItemClickInterface;
import com.reekar.location.GPSTrackerNew;
import com.reekar.models.CurrencyDataModel;
import com.reekar.models.DataModel;
import com.reekar.models.FilterModel;

import java.io.IOException;
import java.lang.reflect.Type;
import java.net.Inet4Address;
import java.net.Inet6Address;
import java.net.InetAddress;
import java.net.NetworkInterface;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Currency;
import java.util.Date;
import java.util.Enumeration;
import java.util.List;
import java.util.Locale;

import static android.content.Context.WIFI_SERVICE;


public class CommonUtils {

    private static ProgressDialog dialog;
    static AnimationDrawable animationDrawable;
    private static Dialog dialogAmin;
    private static AlertDialog progressDialog;
    private static FilterListItemAdapter filterListItemAdapter;
    private static List<DataModel> subfilter = new ArrayList<>();
    private static GPSTrackerNew gpsTracker;
    private static Locale locale;
    private static  String countryCode="";

    public static void setFragment(Fragment fragment, boolean removeStack, FragmentActivity activity,
                                   int mContainer, String tag) {
//        BaseActivity.fragment = fragment;
        FragmentManager fragmentManager = activity.getSupportFragmentManager();
        FragmentTransaction ftTransaction = fragmentManager.beginTransaction();


        if (removeStack) {
            if (tag != null) {
                fragmentManager.popBackStack(tag, FragmentManager.POP_BACK_STACK_INCLUSIVE);
                ftTransaction.add(mContainer, fragment, tag);
            } else
                ftTransaction.add(mContainer, fragment);
        } else {
            for (int i = 0; i < fragmentManager.getBackStackEntryCount(); ++i) {
                fragmentManager.popBackStack();
            }
            if (tag != null)
                ftTransaction.add(mContainer, fragment, tag);
            else
                ftTransaction.add(mContainer, fragment);
        }
//        if (tag != AppConstant.HOME && tag != "main"&& tag != AppConstant.SELECT_LOCATION && tag != AppConstant.PAIR_DEVICE) {
//            clearAllFrag(activity);
        ftTransaction.addToBackStack(tag);


        ftTransaction.commit();
//        fragmentManager.executePendingTransactions();

    }


    public static void keyBoardHide(Activity context){

        /*InputMethodManager imm = (InputMethodManager) context.getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(editText.getWindowToken(), InputMethodManager.HIDE_IMPLICIT_ONLY); */
        try {
            InputMethodManager inputMethodManager = (InputMethodManager) context.getSystemService(Activity.INPUT_METHOD_SERVICE);
            if (context.getCurrentFocus() != null) {
                inputMethodManager.hideSoftInputFromWindow(context.getCurrentFocus().getWindowToken(), InputMethodManager.HIDE_NOT_ALWAYS);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }


    }
    public static void hideKeyBoard(Activity context){


        try {
            InputMethodManager imm = (InputMethodManager) context.getSystemService(Activity.INPUT_METHOD_SERVICE);
//            context.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN);
//            imm.toggleSoftInput(InputMethodManager.HIDE_IMPLICIT_ONLY, 0);
            imm.hideSoftInputFromWindow(context.getWindow().getDecorView().getWindowToken(), 0);
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    public static void ratingBar(Context context, RatingBar ratingBar){

        LayerDrawable stars = (LayerDrawable) ratingBar.getProgressDrawable();
        stars.getDrawable(2).setColorFilter(ContextCompat.getColor(context, R.color.black), PorterDuff.Mode.SRC_ATOP);

    }

    public static void showProgressDialog(Context mContext) {
        hideProgressDialog();
        dialog = new ProgressDialog(mContext, R.style.AppCompatAlertDialogStyle);
        dialog.setCancelable(false);

        dialog.setMessage(mContext.getString(R.string.please_wait));

        dialog.show();

    }
    public static void hideProgressDialog() {
        try {
            if (dialog != null)
                dialog.dismiss();
            if (progressDialog != null)
                progressDialog.dismiss();

            if (dialogAmin != null)
                dialogAmin.dismiss();
            if (animationDrawable != null)
                animationDrawable.stop();

        } catch (Exception e){
            e.printStackTrace();
        }
    }
    public static void showAlertDailog(Context context, String title, String message, String ok, String cancel, final OkCancelInterface okCancelInterface){

        AlertDialog.Builder builder = new AlertDialog.Builder(context,R.style.DatePickerStyle);

        builder.setTitle(title);
        builder.setMessage(message);
        builder.setCancelable(false);
        builder.setNegativeButton(cancel, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                okCancelInterface.onCancelClick();
                dialog.dismiss();

            }
        });

        builder.setPositiveButton(ok, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                okCancelInterface.onOkClick();
                dialog.dismiss();
            }
        });
        builder.create().show();

    }

    public static void showImageDailoge(final Context context, final String tempImage,boolean isDocument){

        TakePictureDailogBinding takePictureDailogBinding;
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        //layout inflate custom
        View dialogView = LayoutInflater.from(context).inflate(R.layout.take_picture_dailog, null);
        builder.setView(dialogView);
        takePictureDailogBinding = DataBindingUtil.bind(dialogView);
        final AlertDialog imagedialog = builder.create();
        takePictureDailogBinding.tvCamera.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                imagedialog.dismiss();
                TakePictureUtils.takePicture((Activity) context,tempImage);
            }
        });
        takePictureDailogBinding.tvGallery.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                imagedialog.dismiss();
                TakePictureUtils.openGallery((Activity) context);
            }
        });
        takePictureDailogBinding.tvCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                imagedialog.dismiss();
            }
        });
        takePictureDailogBinding.tvDocuments.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                imagedialog.dismiss();
                TakePictureUtils.openPdf((Activity) context);
            }
        });

        if (isDocument){
            takePictureDailogBinding.isdocView.setVisibility(View.VISIBLE);
            takePictureDailogBinding.tvDocuments.setVisibility(View.VISIBLE);
        }
        else {
            takePictureDailogBinding.isdocView.setVisibility(View.GONE);
            takePictureDailogBinding.tvDocuments.setVisibility(View.GONE);
        }
        builder.setCancelable(false);


        imagedialog.show();
        imagedialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        imagedialog.getWindow().setGravity(Gravity.BOTTOM);

    }

    public static void showFilterDailoge(final Context context, final ArrayList<FilterModel> filterlist,
                                         final OnFilterClick onFilterClick){

        subfilter.clear();
        subfilter.addAll(filterlist.get(0).getData()); // sublist of filter to show 0 position always open

        FilterDialogLayoutBinding filterDialogLayoutBinding;
        AlertDialog.Builder builder = new AlertDialog.Builder(context,R.style.AppTheme_NoActionBar);
        //layout inflate custom
        View dialogView = LayoutInflater.from(context).inflate(R.layout.filter_dialog_layout, null);
        builder.setView(dialogView);
        filterDialogLayoutBinding = DataBindingUtil.bind(dialogView);
        final AlertDialog dialog = builder.create();

        filterDialogLayoutBinding.filterToolbar.tvToolbarBarName.setText(R.string.filter);
        filterDialogLayoutBinding.filterToolbar.custToolBckBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });
        FilterAdapter filterAdapter=new FilterAdapter(context,filterlist, new RecyclerviewItemClickInterface() {
            @Override
            public void onItemClick(int position) {
                subfilter.clear();
                if (filterlist.get(position).getData()!=null)//getting sublist & check sublist not coming null
                    subfilter.addAll(filterlist.get(position).getData()); //getting sublist according to position
                filterListItemAdapter.notifyDataSetChanged();

            }
        });
        filterDialogLayoutBinding.firstFilterRecyclerview.setLayoutManager(new LinearLayoutManager(context));
        filterDialogLayoutBinding.firstFilterRecyclerview.setAdapter(filterAdapter);


        filterListItemAdapter=new FilterListItemAdapter(context,subfilter);
        filterDialogLayoutBinding.secondFilterRecyclerview.setLayoutManager(new LinearLayoutManager(context));
        filterDialogLayoutBinding.secondFilterRecyclerview.setAdapter(filterListItemAdapter);
        filterDialogLayoutBinding.applyFilterTv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
                onFilterClick.onItemClick(filterlist);

            }
        });

        builder.setCancelable(false);
        dialog.show();
//        imagedialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        if (dialog.getWindow() != null) {
            dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.WHITE));
//            dialog.getWindow().setGravity(Gravity.BOTTOM);
            dialog.getWindow().setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);
        }
    }

    public static void showSortByDialog(final Context context, String sort, final OnSortItemClick onSortItemClick){

        SortByDialogLayoutBinding binding;
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        //layout inflate custom
        View dialogView = LayoutInflater.from(context).inflate(R.layout.sort_by_dialog_layout, null);
        builder.setView(dialogView);
        binding = DataBindingUtil.bind(dialogView);
        final AlertDialog dialog = builder.create();


        switch (sort){
            case AppConstant.PPD_LTH:
                binding.rbPricePerDayLowToHigh.setChecked(true);
                break;
            case AppConstant.PPD_HTL:
                binding.rbPricePerDayHighToLow.setChecked(true);
                break;
            case AppConstant.TP_HTL:
                binding.rbTotalPriceHighToLow.setChecked(true);
                break;
            case AppConstant.TP_LTH:
                binding.rbTotalPriceLowToHigh.setChecked(true);
                break;
            default:
                binding.rbClosestToYou.setChecked(true);
        }

        binding.rbClosestToYou.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                if (b)
                    onSortItemClick.onItemClick("");
                dialog.dismiss();
            }
        });

        binding.rbPricePerDayLowToHigh.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                if (b)
                    onSortItemClick.onItemClick(AppConstant.PPD_LTH);
                dialog.dismiss();
            }
        });
        binding.rbPricePerDayHighToLow.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                if (b)
                    onSortItemClick.onItemClick(AppConstant.PPD_HTL);
                dialog.dismiss();
            }
        });

        binding.rbTotalPriceHighToLow.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                if (b)
                    onSortItemClick.onItemClick(AppConstant.TP_HTL);
                dialog.dismiss();
            }
        });

        binding.rbTotalPriceLowToHigh.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                if (b)
                    onSortItemClick.onItemClick(AppConstant.TP_LTH);
                dialog.dismiss();
            }
        });

        builder.setCancelable(true);
        dialog.show();
        if (dialog.getWindow() != null) {
            dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.WHITE));
            dialog.getWindow().setGravity(Gravity.BOTTOM);
            dialog.getWindow().setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        }
    }

    public static Toast showToast(Context context, String message) {
        Toast toast = Toast.makeText(context, message, Toast.LENGTH_SHORT);
        if (AppConstant.BOOL_TOAST) {
            toast.show();
        }
        return toast;

    }

    public static void spannableTextColor(Context context, String text, int color, int start, int end, TextView textView){

        final SpannableString spanableString = new SpannableString(text);
        spanableString.setSpan(new ForegroundColorSpan(ContextCompat.getColor(context,color)),
                start, end, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
        textView.setText(spanableString);

    }

    public static void navigateToLogin(Context context){
        clearSharedPrefrence(context);
        Intent intent = new Intent(context, MainActivity.class);
        context.startActivity(intent);
        ((Activity)context).finish();
    }

    public static void clearSharedPrefrence(Context context){

        PreferencesManager.Companion.clearPreference(context,AppConstant.IS_LOGIN);
//        PreferencesManager.Companion.clearPreference(context,AppConstant.DEVICE_TOKEN);
        PreferencesManager.Companion.clearPreference(context,AppConstant.FIRST_NAME);
        PreferencesManager.Companion.clearPreference(context,AppConstant.LAST_NAME);
        PreferencesManager.Companion.clearPreference(context,AppConstant.USER_EMAIL);
        PreferencesManager.Companion.clearPreference(context,AppConstant.USER_NUMBER);
        PreferencesManager.Companion.clearPreference(context,AppConstant.USER_IMAGE);
        PreferencesManager.Companion.clearPreference(context,AppConstant.IS_SKIP);

    }
    public static String changeDateFormat(String date,String inputFormat,String outputFormat){
        try {
            SimpleDateFormat spf=new SimpleDateFormat(inputFormat);
            Date newDate=spf.parse(date);
            spf= new SimpleDateFormat(outputFormat);
            date = spf.format(newDate);

            return date;
        }
        catch (ParseException e){
            e.printStackTrace();
        }
        return "";
    }

    public static String fetchAddress(Context context, double lat, double lng) {
        Geocoder geocoder;
        List<Address> addresses;
        geocoder = new Geocoder(context, Locale.getDefault());

        try {
            addresses = geocoder.getFromLocation(lat, lng, 1); // Here 1 represent max location result to returned, by documents it recommended 1 to 5

            return addresses.get(0).getAddressLine(0);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return "";
    }
    public static String fetchCountry(Context context, double lat, double lng) {
        Geocoder geocoder;
        List<Address> addresses;
        geocoder = new Geocoder(context, Locale.getDefault());

        try {
            addresses = geocoder.getFromLocation(lat, lng, 1); // Here 1 represent max location result to returned, by documents it recommended 1 to 5

            return addresses.get(0).getCountryName();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return "";
    }
    public static boolean isNetworkAvailable(Context context) {
        ConnectivityManager connectivityManager
                = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        assert connectivityManager != null;
        NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
        return activeNetworkInfo != null && activeNetworkInfo.isConnected();
    }
    public static void printHashKey(Context pContext) {
        try {
            PackageInfo info = pContext.getPackageManager().getPackageInfo(pContext.getPackageName(), PackageManager.GET_SIGNATURES);
            for (Signature signature : info.signatures) {
                MessageDigest md = MessageDigest.getInstance("SHA");
                md.update(signature.toByteArray());
                String hashKey = new String(Base64.encode(md.digest(), 0));
                Log.i( "Hash Key: " , hashKey);
            }
        } catch (NoSuchAlgorithmException e) {
//            Log.e(TAG, "printHashKey()", e);
        } catch (Exception e) {
//            Log.e(TAG, "printHashKey()", e);
        }
    }

    public static String getMimeType(String url) {
        String type = null;
        String extension = MimeTypeMap.getFileExtensionFromUrl(url);
        if (extension != null) {
            type = MimeTypeMap.getSingleton().getMimeTypeFromExtension(extension);
        }
        return type;
    }

    public static boolean isAppOnForeground(Context context) {
        ActivityManager activityManager = (ActivityManager) context.getSystemService(Context.ACTIVITY_SERVICE);
        List<ActivityManager.RunningAppProcessInfo> appProcesses = activityManager.getRunningAppProcesses();
        String packageName = "com.reekar";
        for (ActivityManager.RunningAppProcessInfo appProcess : appProcesses) {
            if (appProcess.importance == ActivityManager.RunningAppProcessInfo.IMPORTANCE_FOREGROUND && appProcess.processName == packageName) {
                return true;
            }
        }
        return false;
    }

    public static String getCurrentCurrency(final Context context){
        Locale current = context.getResources().getConfiguration().locale;
        gpsTracker = new GPSTrackerNew(context, new OnLocationChangeInterface() {
            @Override
        public void onLocationChanged(Location location) {
            Geocoder geocoder;
            List<Address> addresses;
            geocoder = new Geocoder(context, Locale.getDefault());

            try {
                addresses = geocoder.getFromLocation(location.getLatitude(), location.getLongitude(), 1); // Here 1 represent max location result to returned, by documents it recommended 1 to 5
                locale= addresses.get(0).getLocale();

                if (locale!=null) {
                    countryCode= Currency.getInstance(locale).getCurrencyCode();
                }
                if (countryCode.isEmpty()) {
                    countryCode = "USD";
                }
                saveCurrency(context,countryCode);

            } catch (IOException e) {
                e.printStackTrace();
            }
            gpsTracker.stopUsingGPS();
        }
    });

        //-------- by default pick durrency header ----------
        if (locale!=null) {
            countryCode= Currency.getInstance(locale).getCurrencyCode();
        }
        if (countryCode.isEmpty()) {
            countryCode = "USD";
        }
        saveCurrency(context,countryCode);
        return countryCode;
    }

    private static void saveCurrency(Context context, String countryCode) {
        PreferencesManager.Companion.saveStringPreferences(context,AppConstant.CURRENT_CURRENCY,  countryCode);
    }

    private void fetchCurrency(Context context, String country, String json) {
        String currency="";

        Type type = new TypeToken<CurrencyDataModel>() {}.getType();

        CurrencyDataModel currencyData = new Gson().fromJson(json, type);
        for (CurrencyDataModel.CountryModel model : currencyData.getCountry()){
            if(country.equalsIgnoreCase(model.getCountryName())){
                currency = model.getCurrencyCode();
                break;
            }
        }

        LogUtil.Companion.errorLog("currency",currency);
        PreferencesManager.Companion.saveStringPreferences(context,AppConstant.CURRENT_CURRENCY,currency);

    }

        public static String getMyIp(Context context){
            WifiManager wifiMgr = (WifiManager) context.getSystemService(WIFI_SERVICE);
            WifiInfo wifiInfo = wifiMgr.getConnectionInfo();
            int ip = wifiInfo.getIpAddress();
            return  Formatter.formatIpAddress(ip);
        }

    public static String getMobileIPAddress() {
        try {
            List<NetworkInterface> interfaces = Collections.list(NetworkInterface.getNetworkInterfaces());
            for (NetworkInterface intf : interfaces) {
                List<InetAddress> addrs = Collections.list(intf.getInetAddresses());
                for (InetAddress addr : addrs) {
                    if (!addr.isLoopbackAddress()) {
                        return  addr.getHostAddress();
                    }
                }
            }
        } catch (Exception ex) { } // for now eat exceptions
        return "";
    }


    public static String getDeviceipMobileData(){
        try {
            for (java.util.Enumeration<java.net.NetworkInterface> en = java.net.NetworkInterface.getNetworkInterfaces(); en.hasMoreElements();) {
                java.net.NetworkInterface networkinterface = en.nextElement();
                for (java.util.Enumeration<java.net.InetAddress> enumIpAddr = networkinterface.getInetAddresses(); enumIpAddr.hasMoreElements();) {
                    java.net.InetAddress inetAddress = enumIpAddr.nextElement();
                    if (!inetAddress.isLoopbackAddress()) {
                        return inetAddress.getHostAddress().toString();
                    }
                }
            }
        } catch (Exception ex) {
            Log.e("Current IP", ex.toString());
        }
        return "";
    }


    public static String getIPAddress(boolean useIPv4) {
        try {
            List<NetworkInterface> interfaces = Collections.list(NetworkInterface.getNetworkInterfaces());
            for (NetworkInterface intf : interfaces) {
                List<InetAddress> addrs = Collections.list(intf.getInetAddresses());
                for (InetAddress addr : addrs) {
                    if (!addr.isLoopbackAddress()) {
                        String sAddr = addr.getHostAddress();
                        //boolean isIPv4 = InetAddressUtils.isIPv4Address(sAddr);
                        boolean isIPv4 = sAddr.indexOf(':')<0;

                        if (useIPv4) {
                            if (isIPv4)
                                return sAddr;
                        } else {
                            if (!isIPv4) {
                                int delim = sAddr.indexOf('%'); // drop ip6 zone suffix
                                return delim<0 ? sAddr.toUpperCase() : sAddr.substring(0, delim).toLowerCase();
                            }
                        }
                    }
                }
            }
        } catch (Exception ex) { } // for now eat exceptions
        return "";
    }

    public static String getIpv4() {
        try {
            for (Enumeration<NetworkInterface> en = NetworkInterface
                    .getNetworkInterfaces(); en.hasMoreElements(); ) {
                NetworkInterface intf = en.nextElement();
                for (Enumeration<InetAddress> enumIpAddr = intf
                        .getInetAddresses(); enumIpAddr.hasMoreElements(); ) {
                    InetAddress inetAddress = enumIpAddr.nextElement();
                    System.out.println("ip1--:" + inetAddress);
                    System.out.println("ip2:" + inetAddress.getHostAddress());

                    //return inetAddress.getHostAddress();
                    if (!inetAddress.isLoopbackAddress()/* && inetAddress instanceof Inet4Address*/) {
                        String ipaddress = inetAddress.getHostAddress().toString();
                        return ipaddress;
                    }

                }
            }
        } catch (Exception ex) {
            Log.e("IP Address", ex.toString());
        }
        return null;
    }

    public static String getLocalIpV6() {
        try {
            for (Enumeration<NetworkInterface> en = NetworkInterface
                    .getNetworkInterfaces(); en.hasMoreElements(); ) {
                NetworkInterface intf = en.nextElement();
                for (Enumeration<InetAddress> enumIpAddr = intf
                        .getInetAddresses(); enumIpAddr.hasMoreElements(); ) {
                    InetAddress inetAddress = enumIpAddr.nextElement();
                    System.out.println("ip1--:" + inetAddress);
                    System.out.println("ip2--:" + inetAddress.getHostAddress());

                    if (!inetAddress.isLoopbackAddress() && inetAddress instanceof Inet6Address) {
                        String ipaddress = inetAddress.getHostAddress().toString();
                        return ipaddress;
                    }


                }
            }
        } catch (Exception ex) {
            Log.e("IP Address", ex.toString());
        }
        return null;
    }

    public static String getIPAddress1(boolean useIPv4) {
        try {
            List<NetworkInterface> interfaces = Collections.list(NetworkInterface.getNetworkInterfaces());
            for (NetworkInterface intf : interfaces) {
                List<InetAddress> addrs = Collections.list(intf.getInetAddresses());
                for (InetAddress addr : addrs) {
                    if (!addr.isLoopbackAddress()) {
                        String sAddr = addr.getHostAddress();
//boolean isIPv4 = InetAddressUtils.isIPv4Address(sAddr);
                        boolean isIPv4 = sAddr.indexOf(':')<0;

                        if (useIPv4) {
                            if (isIPv4)
                                return sAddr;
                        } else {
                            if (!isIPv4) {
                                int delim = sAddr.indexOf('%'); // drop ip6 zone suffix
                                return delim<0 ? sAddr.toUpperCase() : sAddr.substring(0, delim).toUpperCase();
                            }
                        }
                    }
                }
            }
        } catch (Exception ignored) { } // for now eat exceptions
        return "";
    }
}