package com.reekar.commonclasses;

import android.app.DatePickerDialog;
import android.app.TimePickerDialog;
import android.content.Context;
import android.util.Log;
import android.widget.DatePicker;
import android.widget.TextView;
import android.widget.TimePicker;

import com.akanksha.commonclassutil.LogUtil;
import com.reekar.R;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.TimeZone;

/**
 * Author:    Akanksha Paul
 * Created:   22/1/20
 **/
public class TimeUtil {

    private final static String SERVER_DATE_FORMAT = "yyyy-MM-dd HH:mm:ss";
    public static void datePicker(Context context, final TextView textView, final String dateFormat){
        final Calendar cal = Calendar.getInstance();
        int year = cal.get(Calendar.YEAR);
        int month = cal.get(Calendar.MONTH);
        int day = cal.get(Calendar.DAY_OF_MONTH);

        DatePickerDialog datePickerDialog = new DatePickerDialog(context, R.style.DatePickerStyle,
                new DatePickerDialog.OnDateSetListener() {
                    @Override
                    public void onDateSet(DatePicker datePicker,int year, int month, int day) {
                        cal.set(year,month,day);
                            SimpleDateFormat format = new SimpleDateFormat(dateFormat);
                        String dateString = format.format(cal.getTime());
                        textView.setText(dateString);
                    }
                },year,month,day);

        datePickerDialog.getDatePicker().setMinDate(System.currentTimeMillis() - 1000);
        datePickerDialog.show();

    }

    public static void timePicker(Context context, final TextView textView, final String dateFormat){
        final Calendar calendar = Calendar.getInstance();

        final int hour = calendar.get(Calendar.HOUR_OF_DAY);
        final int minute = calendar.get(Calendar.MINUTE);
        final int second = calendar.get(Calendar.SECOND);


        TimePickerDialog dialog = new TimePickerDialog(context, R.style.DatePickerStyle, new TimePickerDialog.OnTimeSetListener() {
            @Override
            public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
//                Calendar calendar = Calendar.getInstance();
                calendar.set(Calendar.HOUR_OF_DAY, hourOfDay);
                calendar.set(Calendar.MINUTE, minute);
                SimpleDateFormat format = new SimpleDateFormat(dateFormat);
                String dateString = format.format(calendar.getTime());
                textView.setText(dateString);

            }
        },hour, minute, false);

        dialog.show();

    }

    public static String UtcToLocal(String serverDate, String format, Context context) {
        try {
            SimpleDateFormat originalFormat = new SimpleDateFormat(SERVER_DATE_FORMAT, context.getResources().
                    getConfiguration().locale);
            originalFormat.setTimeZone(TimeZone.getTimeZone("UTC"));
            SimpleDateFormat targetFormat = new SimpleDateFormat(format);
            targetFormat.setTimeZone(TimeZone.getDefault());
            Date date = originalFormat.parse(serverDate);
            String formattedDate = targetFormat.format(date);

            LogUtil.Companion.errorLog("date",formattedDate);
            return formattedDate;
        } catch (Exception ex) {
            ex.printStackTrace();
            return "";
        }


    }

    public static String localDateToGMT(String dateString,String format,Context context){
        try {
            SimpleDateFormat originalFormat = new SimpleDateFormat(format);
            originalFormat.setTimeZone(TimeZone.getDefault());
            SimpleDateFormat targetFormat = new SimpleDateFormat(SERVER_DATE_FORMAT);
            targetFormat.setTimeZone(TimeZone.getTimeZone("UTC"));
            Date date = originalFormat.parse(dateString);

            return targetFormat.format(date);
        } catch (Exception ex) {
            ex.printStackTrace();
            return "";
        }
    }

    public static boolean CheckDates(String d1,String d2){
        SimpleDateFormat dfDate = new SimpleDateFormat("dd/MM/yyyy");

        boolean b = false;
        try {
            if(dfDate.parse(d1).before(dfDate.parse(d2)))
            {
                b = true;//If start date is before end date
            }
            else if(dfDate.parse(d1).equals(dfDate.parse(d2)))
            {
                b = true;//If two dates are equal
            }
            else
            {
//                b = false; //If start date is after the end date
            }
        } catch (ParseException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        return b;
    }

    public static boolean isTimeGreater(String pdate, String ptime,String ddate, String dtime) {
        Date currentTime, pickedTime,dropTime;
        SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy hh:mm a");
        String currentTimeString = sdf.format(new Date());
        boolean b=false;
        try {
            currentTime = sdf.parse(currentTimeString);
            pickedTime = sdf.parse(pdate + " " + ptime);
            dropTime = sdf.parse(ddate + " " + dtime);


            if (pickedTime.compareTo(currentTime) >=0) {
                b= true;
            }
            if (pickedTime.before(dropTime)) {
                b= true;
            }
            else {
                b= false;
            }
        } catch (ParseException e) {
            e.printStackTrace();
        }

        return b;
    }

    public static boolean isPastTime(String pdate, String ptime) {
        Date currentTime, pickedTime;
        SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy hh:mm a");
        String currentTimeString = sdf.format(new Date());

        try {
            currentTime = sdf.parse(currentTimeString);
            pickedTime = sdf.parse(pdate + " " + ptime);


            if (pickedTime.compareTo(currentTime) >=0) {
                return true;
            }
            else {
                return false;
            }
        } catch (ParseException e) {
            e.printStackTrace();
        }

        return true;
    }

    public static boolean onTimeSet(TextView view, int hourOfDay, int minute) {
        Calendar datetime = Calendar.getInstance();
        Calendar c = Calendar.getInstance();
        datetime.set(Calendar.HOUR_OF_DAY, hourOfDay);
        datetime.set(Calendar.MINUTE, minute);
        if(datetime.getTimeInMillis() > c.getTimeInMillis()){
//            it's after current
            return true;
        }else{
            return false;
//            it's before current'
        }
    }




    public static List<Integer> getYearData() {
        List<Integer> arlYear = new ArrayList<>();
        int currentYear = Calendar.getInstance().get(Calendar.YEAR);

        int lastYear = currentYear - 70;

        LogUtil.Companion.errorLog("year", String.valueOf(currentYear));
        LogUtil.Companion.errorLog("lastYear", String.valueOf(lastYear));

        for (int i=currentYear;i>=lastYear;i--){
            arlYear.add(i);
        }

        return arlYear;
    }

    public static void timeZone(){
        Calendar cal = Calendar.getInstance();
        TimeZone tz = cal.getTimeZone();
        Log.d("Time zone","="+tz.getDisplayName());
    }
}
