package com.reekar.activity;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.view.View;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;

import com.akanksha.commonclassutil.CheckPermission;
import com.akanksha.commonclassutil.PreferencesManager;
import com.akanksha.commonclassutil.ToastUtil;
import com.bumptech.glide.Glide;
import com.reekar.R;
import com.reekar.apiservices.ApiUtils;
import com.reekar.commonclasses.AppConstant;
import com.reekar.commonclasses.BaseActivity;
import com.reekar.commonclasses.CommonUtils;
import com.reekar.commonclasses.ErrorUtils;
import com.reekar.commonclasses.TakePictureUtils;
import com.reekar.cropimage.CropImage;
import com.reekar.databinding.ActivityProfileEditBinding;
import com.reekar.interfaces.ApiResponseInterface;
import com.reekar.models.ApiResponse;
import com.reekar.models.ErrorModel;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.HashMap;
import java.util.Map;

import okhttp3.MediaType;
import okhttp3.RequestBody;
import retrofit2.Response;

public class ProfileEditActivity extends BaseActivity {


    private ActivityProfileEditBinding profileEditBinding;
    private Context context;

    private String tempImage="tempImage";

    private String uri;
    private File file;

    private String path;

    Intent CropIntent ;
    DisplayMetrics displayMetrics ;
    int width, height;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        profileEditBinding= DataBindingUtil.setContentView(this,R.layout.activity_profile_edit);
        context=this;

        getIntentData();
        setToolbar();
        setListener();
    }

    @Override
    public void setToolbar() {
        profileEditBinding.profileEditToolbar.tvToolbarBarName.setText(R.string.edit_profile);
    }

    @Override
    public void setListener() {
        profileEditBinding.profileEditToolbar.custToolBckBtn.setOnClickListener(this);
        profileEditBinding.submit.setOnClickListener(this);
        profileEditBinding.profileImage.setOnClickListener(this);

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){

            case R.id.cust_tool_bck_btn:{
                finish();
                break;
            }
            case R.id.submit:{
                if (CommonUtils.isNetworkAvailable(context))
                callApiUpdateProfile();
                else
                    ToastUtil.Companion.showShortToast(context,getString(R.string.failed_internet));
//                carryData();
                break;
            }
            case R.id.profile_image:{
                tempImage = tempImage+System.currentTimeMillis();
                checkPermission();
                break;
            }

        }
    }

    private void carryData() {
        Intent intent = new Intent();
        intent.putExtra(AppConstant.FIRST_NAME, profileEditBinding.firstName.getText().toString());
        intent.putExtra(AppConstant.LAST_NAME, profileEditBinding.lastName.getText().toString());
        intent.putExtra(AppConstant.USER_NUMBER, profileEditBinding.mobileNumber.getText().toString());
        intent.putExtra(AppConstant.ADDRESS, profileEditBinding.address.getText().toString());
        intent.putExtra(AppConstant.COUNTRY_CODE, profileEditBinding.countryCodePicker.getSelectedCountryCodeWithPlus());
        intent.putExtra(AppConstant.IMAGE_URI,uri);
        setResult(RESULT_OK, intent);
        finish();
    }

   private void getIntentData(){
       Intent intent = getIntent();
       if (intent!=null) {
           String fname = intent.getStringExtra(AppConstant.FIRST_NAME);
           String lname = intent.getStringExtra(AppConstant.LAST_NAME);
           String address = intent.getStringExtra(AppConstant.ADDRESS);
           String number = intent.getStringExtra(AppConstant.USER_NUMBER);
           if (intent.getStringExtra(AppConstant.COUNTRY_CODE)!=null) {
               String ccode = intent.getStringExtra(AppConstant.COUNTRY_CODE).replace("+", "");
               if (ccode!=null && !ccode.isEmpty())
                   profileEditBinding.countryCodePicker.setCountryForPhoneCode(Integer.parseInt(ccode));
           }

           String image =intent.getStringExtra(AppConstant.IMAGE_URI);

           if (fname!=null)
           profileEditBinding.firstName.setText(fname);
           if (lname!=null)
           profileEditBinding.lastName.setText(lname);
           if (address!=null)
           profileEditBinding.address.setText(address);

           if (number!=null)
           profileEditBinding.mobileNumber.setText(number);
//           if (uri!=null)
//           profileEditBinding.profileImage.setImageResource(image);
           Glide.with(context)
                   .load(image)
                   .placeholder(R.drawable.ic_user)
                   .into(profileEditBinding.profileImage);
       }

   }
   private void callApiUpdateProfile(){
        CommonUtils.showProgressDialog(context);

//       ApiRequest apiRequest = new ApiRequest();
       Map <String, RequestBody> map=new HashMap<>();
       map.put("Authorization",ApiUtils.toRequestBody(PreferencesManager.Companion.getStringPreferences(context,AppConstant.ACCESS_CODE)));
       map.put("first_name",ApiUtils.toRequestBody(profileEditBinding.firstName.getText().toString()));
       map.put("last_name",ApiUtils.toRequestBody(profileEditBinding.lastName.getText().toString()));
       map.put("mobile",ApiUtils.toRequestBody(profileEditBinding.mobileNumber.getText().toString()));
       map.put("country_code",ApiUtils.toRequestBody(profileEditBinding.countryCodePicker.getSelectedCountryCodeWithPlus()));
       map.put("address",ApiUtils.toRequestBody(profileEditBinding.address.getText().toString()));

       if (file!=null){
           RequestBody filebody=RequestBody.create(MediaType.parse("image/jpg"),file);
           map.put("image\"; filename=\"" + file.getName(),filebody);
       }
       ApiUtils.callApiUpdateProfile(context, map, new ApiResponseInterface() {
           @Override
           public void onResponse(Response<ApiResponse> response) {
               CommonUtils.hideProgressDialog();
               if (response.code()==AppConstant.SUCCESS_CODE){
                   if (response.body()!=null) {
                       String msg= response.body().getMessage();
                       if (response.body().getImage()!=null)
                       uri =response.body().getImage();

                       PreferencesManager.Companion.saveStringPreferences(context,AppConstant.USER_IMAGE,uri);
                       ToastUtil.Companion.showShortToast(context, msg);
                       carryData();
                   }


               }else if(response.code()==AppConstant.VALIDATION_ERROR_CODE){
                   parseError(response);
               }
               else {
                   ToastUtil.Companion.showShortToast(context,getString(R.string.something_went_wrong));
               }

           }

           @Override
           public void onFailure() {
               CommonUtils.hideProgressDialog();
               ToastUtil.Companion.showShortToast(context,getString(R.string.something_went_wrong));
           }
       });
   }
    public void parseError(Response<ApiResponse> response){

        if (response.errorBody()!=null){
            try {
                ApiResponse error = ErrorUtils.parseError(response,context);
                if (error!=null && error.getError()!=null){
                    showError(error.getError());
                }
            }
            catch (Exception e){

            }
        }
    }
    private void showError(ErrorModel error) {
        if (error.getFirstName()!=null){
            profileEditBinding.firstName.setError(error.getFirstName().get(0));
        }
        if (error.getMobile()!=null){
            profileEditBinding.mobileNumber.setError(error.getMobile().get(0));
        }
        if (error.getAddress()!=null){
            profileEditBinding.address.setError(error.getAddress().get(0));
        }
        if (error.getImage()!=null){
            ToastUtil.Companion.showShortToast(context,error.getImage().get(0));
        }
    }

/*    private void callApiUpdateProfilePicture(){
        CommonUtils.showProgressDialog(context);

        ApiRequest apiRequest = new ApiRequest();
        apiRequest.setDevice_token(PreferencesManager.Companion.getStringPreferences(context,AppConstant.ACCESS_CODE));

        ApiUtils.callApiUpdateProfile(context, apiRequest, new ApiResponseInterface() {
            @Override
            public void onResponse(Response<ApiResponse> response) {
                CommonUtils.hideProgressDialog();
                if (response.code()==AppConstant.SUCCESS_CODE){
                    String msg= response.body().getMessage();
                    ToastUtil.Companion.showShortToast(context,msg);
                }
            }

            @Override
            public void onFailure() {

            }
        });
    }*/

    private void checkPermission(){
        if (CheckPermission.Companion.checkIsMarshMallowVersion()){
            if (CheckPermission.Companion.checkCameraStoragePermission(context)){
                CommonUtils.showImageDailoge(context,tempImage,false);
            }
            else {
                CheckPermission.Companion.requestCameraStoragePermission((Activity) context);
            }
        }
        else {
            CommonUtils.showImageDailoge(context,tempImage,false);
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);

        switch (requestCode){

            case CheckPermission.REQUEST_CODE_CAMERA_STORAGE_PERMISSION:{
                if (grantResults.length>0 && grantResults[0]== PackageManager.PERMISSION_GRANTED){
                    CommonUtils.showImageDailoge(context,tempImage,false);
                }
                break;
            }
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode==Activity.RESULT_OK)
            switch (requestCode){
                case TakePictureUtils.TAKE_PICTURE:
                    TakePictureUtils.startCropImage((Activity) context,tempImage+".jpg",500,500);
//                    setImage(data,requestCode);
                    break;
                case TakePictureUtils.PICK_GALLERY: {

                    try {
                        InputStream inputStream = getContentResolver().openInputStream(data.getData());
                        FileOutputStream fileOutputStream = new FileOutputStream(new File(getExternalFilesDir("temp"),tempImage+".jpg"));
                        TakePictureUtils.copyStream(inputStream,fileOutputStream);
                        fileOutputStream.close();
                        inputStream.close();

                    } catch (FileNotFoundException e) {
                        e.printStackTrace();
                    }
                    catch (IOException e) {
                        e.printStackTrace();
                    }
                    TakePictureUtils.startCropImage((Activity) context,tempImage+".jpg",500,500);
//                    setImage(data,requestCode);
                    break;
                }
                case TakePictureUtils.CROP_FROM_CAMERA:{
                    path=null;
                    if (data!=null){
                        path=data.getStringExtra(CropImage.IMAGE_PATH);
                        if (path!=null)
                       file =new File(path);

                        if (path == null) {
                            return;
                        }
                        Glide.with(context)
                                .load(file)
                                .placeholder(R.drawable.ic_user)
                                .into(profileEditBinding.profileImage);
                    }

                }
            }
    }

//    private void setImage(Intent data, int requestCode) {
//        file =new File(getExternalFilesDir("temp"),tempImage+".jpg");
//        uri= FileProvider.getUriForFile(context,context.getApplicationContext().getPackageName()+".provider",file);
//        if (requestCode==TakePictureUtils.PICK_GALLERY){
////            profileEditBinding.profileImage.setImage
//            Glide.with(context)
//                    .load(file)
//                    .placeholder(R.drawable.ic_user)
//                    .into(profileEditBinding.profileImage);
//        }
//        else if (requestCode==TAKE_PICTURE){
////            profileEditBinding.profileImage.setImageURI(uri);
//            Glide.with(context)
//                    .load(uri)
//                    .placeholder(R.drawable.ic_user)
//                    .into(profileEditBinding.profileImage);
//        }
//
//    }


}
