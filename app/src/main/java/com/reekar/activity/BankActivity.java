package com.reekar.activity;

import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;

import android.content.Context;
import android.os.Bundle;
import android.view.View;

import com.reekar.R;
import com.reekar.apiservices.ApiUtils;
import com.reekar.commonclasses.AppConstant;
import com.reekar.commonclasses.BaseActivity;
import com.reekar.commonclasses.CommonUtils;
import com.reekar.commonclasses.ErrorUtils;
import com.reekar.databinding.ActivityBankBinding;
import com.reekar.interfaces.ApiResponseInterface;
import com.reekar.models.ApiRequest;
import com.reekar.models.ApiResponse;
import com.reekar.models.ErrorModel;

import retrofit2.Response;

public class BankActivity extends BaseActivity {

    private ActivityBankBinding bankBinding;

    private Context mContaxt;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_bank);
        mContaxt=this;
        bankBinding = DataBindingUtil.setContentView(this,R.layout.activity_bank);


        setToolbar();
        setListener();
    }

    @Override
    public void setToolbar() {
        bankBinding.ibanToolbar.tvToolbarBarName.setText(R.string.iban);
    }

    @Override
    public void setListener() {
        bankBinding.ibanToolbar.custToolBckBtn.setOnClickListener(this);
        bankBinding.submitBankDetailsBtn.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {

        switch (v.getId()){

            case R.id.cust_tool_bck_btn:{

                finish();
                break;
            }
            case R.id.submit_bank_details_btn:{
                finish();
//                callApiPaymentMethodSave();
                break;
            }
        }
    }

    public void callApiPaymentMethodSave(){
        CommonUtils.showProgressDialog(mContaxt);

        ApiRequest apiRequest = new ApiRequest();
        apiRequest.setDevice_token(AppConstant.DEVICE_TOKEN);
        apiRequest.setType(AppConstant.IBAN);
        apiRequest.setFirst_name(bankBinding.firstName.getText().toString().trim());
        apiRequest.setLast_name(bankBinding.lastName.getText().toString().trim());
        apiRequest.setIban(bankBinding.iban.getText().toString());
        apiRequest.setBank_name(bankBinding.bankName.getText().toString());
        apiRequest.setBank_swift_code(bankBinding.bankSwiftCode.getText().toString());
        apiRequest.setMobile(bankBinding.phoneNumber.getText().toString());

        ApiUtils.callApiSavePaymentMethod(mContaxt, apiRequest, new ApiResponseInterface() {
            @Override
            public void onResponse(Response<ApiResponse> response) {
                CommonUtils.hideProgressDialog();
                if (response.code()==AppConstant.SUCCESS_CODE){

                    CommonUtils.showToast(mContaxt,response.body().getMessage());
                    finish();
                }
                else if (response.code()==AppConstant.VALIDATION_ERROR_CODE){
                    parseError(response);
                }
                else {
                    CommonUtils.showToast(mContaxt,getString(R.string.something_went_wrong));
                }
            }

            @Override
            public void onFailure() {
                CommonUtils.hideProgressDialog();
            }
        });
    }

    private void parseError(Response<ApiResponse> response) {
        if (response.errorBody()!=null){
            try {
                ApiResponse error = ErrorUtils.parseError(response,mContaxt);
                if (error!=null && error.getError()!=null){
                    showError(error.getError());
                }
            }
            catch (Exception e){

            }
        }
    }

    private void showError(ErrorModel error) {
        if (error.getFirstName()!=null){
            bankBinding.firstName.setError(error.getFirstName().get(0));
        }
        if (error.getLastName()!=null){
            bankBinding.lastName.setError(error.getLastName().get(0));
        }
        if (error.getMobile()!=null){
            bankBinding.phoneNumber.setError(error.getMobile().get(0));
        }

    }
}
