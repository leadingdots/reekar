package com.reekar.activity;

import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;

import com.akanksha.commonclassutil.PreferencesManager;
import com.akanksha.commonclassutil.ToastUtil;
import com.reekar.R;
import com.reekar.apiservices.ApiUtils;
import com.reekar.commonclasses.AppConstant;
import com.reekar.commonclasses.BaseActivity;
import com.reekar.commonclasses.BaseFragment;
import com.reekar.commonclasses.CommonUtils;
import com.reekar.commonclasses.ErrorUtils;
import com.reekar.databinding.ActivityForgotPasswordBinding;
import com.reekar.interfaces.ApiResponseInterface;
import com.reekar.models.ApiRequest;
import com.reekar.models.ApiResponse;
import com.reekar.models.ErrorModel;
import com.reekar.translation.ChangeLanguage;

import retrofit2.Response;

public class ForgotPasswordActivity extends BaseActivity {

    private ActivityForgotPasswordBinding forgotPasswordBinding;

    private Context mContext;

    private String forgotemail;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mContext=this;
        forgotPasswordBinding = DataBindingUtil.setContentView(this,R.layout.activity_forgot_password);

        setToolbar();
        setListener();
    }

    @Override
    public void setToolbar() {
        forgotPasswordBinding.forgotPassToolbar.tvToolbarBarName.setText(R.string.forgot_pass);
    }

    @Override
    public void setListener() {
        forgotPasswordBinding.forgotPassToolbar.custToolBckBtn.setOnClickListener(this);
        forgotPasswordBinding.sendOtpForgotpass.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {

        switch (v.getId()){

            case R.id.cust_tool_bck_btn:{
                finish();
                break;
            }
            case R.id.send_otp_forgotpass:{
                validation();
                break;
            }
        }
    }
    private void validation(){

        forgotemail = forgotPasswordBinding.verifyEmailForgotpass.getText().toString();
        String emailPattern = "[a-zA-Z0-9._-]+@[a-z]+\\.+[a-z]+";

        if (forgotemail.isEmpty()) {
            forgotPasswordBinding.verifyEmailForgotpass.setError(getString(R.string.please_enter_email));
            forgotPasswordBinding.verifyEmailForgotpass.requestFocus();
        }
        else if (!forgotemail.matches(emailPattern)) {
            forgotPasswordBinding.verifyEmailForgotpass.setError(getString(R.string.please_enter_correct_email));
            forgotPasswordBinding.verifyEmailForgotpass.requestFocus();
        }
        else {
            if (CommonUtils.isNetworkAvailable(mContext))
            callApiForgotPass();
            else
                ToastUtil.Companion.showShortToast(mContext,getString(R.string.failed_internet));
        }
    }
    private void callApiForgotPass() {
        CommonUtils.showProgressDialog(mContext);

        ApiRequest apiRequest = new ApiRequest();
        apiRequest.setEmail(forgotPasswordBinding.verifyEmailForgotpass.getText().toString());

        ApiUtils.callApiResendOtp(mContext, apiRequest, new ApiResponseInterface() {
            @Override
            public void onResponse(Response<ApiResponse> response) {
                CommonUtils.hideProgressDialog();
                if (response.code()== AppConstant.SUCCESS_CODE){

                    CommonUtils.showToast(mContext, ChangeLanguage.translate(mContext,response.body().getMessage()));
                    Intent intent=new Intent(mContext,OtpActivity.class);
                    intent.putExtra(AppConstant.USER_EMAIL,forgotPasswordBinding.verifyEmailForgotpass.getText().toString());
                    intent.putExtra(AppConstant.FROM,AppConstant.FORGOT_ACTIVITY);
                    startActivity(intent);
                }
                else if (response.code()==AppConstant.VALIDATION_ERROR_CODE){
                    parseError(response);
                }
                else if (response.code()==AppConstant.SUCCESS_CODE_201){
                    CommonUtils.showToast(mContext,ChangeLanguage.translate(mContext,response.body().getMessage()));
                }
                else {
                    CommonUtils.showToast(mContext,getString(R.string.something_went_wrong));
                }
            }

            @Override
            public void onFailure() {
                CommonUtils.hideProgressDialog();
            }
        });
    }

    public void parseError(Response<ApiResponse> response){

        if (response.errorBody()!=null){
            try {
                ApiResponse error = ErrorUtils.parseError(response,mContext);
                if (error!=null && error.getError()!=null){
                    showError(error.getError());
                }
            }
            catch (Exception e){

            }
        }
    }

    private void showError(ErrorModel error) {
        if (error.getEmail()!=null){
            forgotPasswordBinding.verifyEmailForgotpass.setError(ChangeLanguage.translate(mContext,error.getEmail().get(0)));
        }

    }
}
