package com.reekar.activity;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.renderscript.ScriptGroup;
import android.view.View;

import com.akanksha.commonclassutil.ToastUtil;
import com.braintreepayments.api.BraintreeFragment;
import com.braintreepayments.api.exceptions.InvalidArgumentException;
import com.braintreepayments.api.interfaces.PaymentMethodNonceCreatedListener;
import com.braintreepayments.api.models.PaymentMethodNonce;
import com.paypal.android.sdk.payments.PayPalConfiguration;
import com.paypal.android.sdk.payments.PayPalPayment;
import com.paypal.android.sdk.payments.PayPalService;
import com.paypal.android.sdk.payments.PaymentActivity;
import com.paypal.android.sdk.payments.PaymentConfirmation;
import com.reekar.R;
import com.reekar.apiservices.ApiUtils;
import com.reekar.commonclasses.AppConstant;
import com.reekar.commonclasses.BaseActivity;
import com.reekar.commonclasses.BaseFragment;
import com.reekar.commonclasses.CommonUtils;
import com.reekar.commonclasses.TimeUtil;
import com.reekar.databinding.ActivityPayMethodBinding;
import com.reekar.interfaces.ApiResponseInterface;
import com.reekar.models.ApiRequest;
import com.reekar.models.ApiResponse;
import com.reekar.paymentmethod.PayPalConfig;

import org.json.JSONException;
import org.json.JSONObject;

import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;
import java.util.TimeZone;

import retrofit2.Response;

public class PayMethodActivity extends BaseActivity {

    private ActivityPayMethodBinding payMethodBinding;

    private Context context;

    private String bookingId;

    private String carId;
    private String mPickDate;
    private String mDropDate;
    private String mPickuLocation;

    private int taxAmt =0;
    private String totalAmt;
    private String fname;
    private String lname;
    private String ccode;
    private String phoneNum;
    private String alternativeNum;
    private String address;
    private double pricePerDay;
    private double securityAmt;
    private double grandTotalAmt;
    private String email;
    private String paymentType;
    private double grandTotalUSD;

    private String txtId;
    private String bankId;
    private String txtStatus;
    private String txtPayment;

    /*-- paypal --*/
    public static final int PAYPAL_REQUEST_CODE = 123;

    private static PayPalConfiguration payPalConfiguration =new PayPalConfiguration()
            .environment(PayPalConfiguration.ENVIRONMENT_PRODUCTION)
            .clientId(PayPalConfig.PAYPAL_CLIENT_ID);

    private String amount="";

    //-----braintree-------------
    BraintreeFragment mBraintreeFragment;
    String mAuthorization;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        payMethodBinding = DataBindingUtil.setContentView(this,R.layout.activity_pay_method);
        context=this;



        getIntentData();

        setToolbar();
        setListener();
    }

    private void getIntentData() {
        Intent intent =getIntent();
        if (intent!=null){
            if (intent.getStringExtra(AppConstant.BOOKING_FROM_DATE)!=null){
                mPickDate=intent.getStringExtra(AppConstant.BOOKING_FROM_DATE);
            }
            if (intent.getStringExtra(AppConstant.BOOKING_TO_DATE)!=null){
                mDropDate=intent.getStringExtra(AppConstant.BOOKING_TO_DATE);
            }
            if (intent.getStringExtra(AppConstant.PICKUP_LOCATION)!=null){
                mPickuLocation=intent.getStringExtra(AppConstant.PICKUP_LOCATION);
            }
            if (intent.getStringExtra(AppConstant.CAR_ID)!=null){
                carId=intent.getStringExtra(AppConstant.CAR_ID);
            }
            if (intent.getStringExtra(AppConstant.BILLING_FIRST_NAME)!=null){
                fname=intent.getStringExtra(AppConstant.BILLING_FIRST_NAME);
            }
            if (intent.getStringExtra(AppConstant.BILLING_LAST_NAME)!=null){
                lname=intent.getStringExtra(AppConstant.BILLING_LAST_NAME);
            }
            if (intent.getStringExtra(AppConstant.BILLING_MOBILE)!=null){
                phoneNum=intent.getStringExtra(AppConstant.BILLING_MOBILE);
            }
            if (intent.getStringExtra(AppConstant.BILLING_ALTERNATIVE_MOBILE)!=null){
                alternativeNum=intent.getStringExtra(AppConstant.BILLING_ALTERNATIVE_MOBILE);
            }
            if (intent.getStringExtra(AppConstant.BILLING_ADDRESS)!=null){
                address=intent.getStringExtra(AppConstant.BILLING_ADDRESS);
            }
//            if (intent.getStringExtra(AppConstant.PRICE_PER_DAY)!=null){
                pricePerDay= intent.getDoubleExtra(AppConstant.PRICE_PER_DAY,0);
//            }
//            if (intent.getStringExtra(AppConstant.SECURITY_AMOUNT)!=null){
                securityAmt= intent.getDoubleExtra(AppConstant.SECURITY_AMOUNT,0);
//            }
            if (intent.getStringExtra(AppConstant.BILLING_EMAIL)!=null){
                email=intent.getStringExtra(AppConstant.BILLING_EMAIL);
            }
            if (intent.getStringExtra(AppConstant.ID)!=null){
                bookingId=intent.getStringExtra(AppConstant.ID);
            }
            grandTotalAmt=intent.getDoubleExtra(AppConstant.GRAND_TOTAL,0);
            grandTotalUSD=intent.getDoubleExtra(AppConstant.GRAND_TOTAL_USD,0);
        }
    }

    @Override
    public void setToolbar() {
        payMethodBinding.payMethodActivityToolbar.tvToolbarBarName.setText(R.string.payment_method);

    }

    @Override
    public void setListener() {
        payMethodBinding.payMethodActivityToolbar.custToolBckBtn.setOnClickListener(this);
        payMethodBinding.circlePaypal.setOnClickListener(this);
        payMethodBinding.circleIban.setOnClickListener(this);
        payMethodBinding.imgCheckoutPay.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.cust_tool_bck_btn:{
                finish();
                break;
            }
            case R.id.circle_paypal:{
                sendDataToPayPal();
//                Intent intent = new Intent(context,PaypalActivity.class);
//                intent.putExtra(AppConstant.GRAND_TOTAL,grandTotalAmt);
//                startActivityForResult(intent,2);
//                finishAffinity();
                break;
            }

            case R.id.img_checkout_pay:{
               // paymentType =AppConstant.CHEKOUT;
              //  callApiSuccessOrderApi();
                sendData();
//                Intent intent = new Intent(context,StripePaymentActivity.class);
//                startActivity(intent);
//                finishAffinity();
                break;
            }
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        stopService(new Intent(context,PayPalService.class));
    }

    private void processPayment(){
        PayPalPayment payPalPayment=new PayPalPayment(new BigDecimal(String.valueOf(grandTotalUSD)),"USD",getString(R.string.pay_to_reeker),
                PayPalPayment.PAYMENT_INTENT_SALE);

        Intent intent =new Intent(context, PaymentActivity.class);
        intent.putExtra(PayPalService.EXTRA_PAYPAL_CONFIGURATION,payPalConfiguration);
        intent.putExtra(PaymentActivity.EXTRA_PAYMENT,payPalPayment);
        startActivityForResult(intent,PAYPAL_REQUEST_CODE);

//        Intent intent = new Intent(context, PayPalService.class);
//        intent.putExtra(PayPalService.EXTRA_PAYPAL_CONFIGURATION, payPalConfiguration);
//        startService(intent);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode==PAYPAL_REQUEST_CODE){

            if (resultCode==RESULT_OK){
                PaymentConfirmation paymentConfirmation =data.getParcelableExtra(PaymentActivity.EXTRA_RESULT_CONFIRMATION);
                if (paymentConfirmation!=null){

                    txtId = paymentConfirmation.getProofOfPayment().getPaymentId();
//                    txtId = paymentConfirmation.getProofOfPayment().getTransactionId();
                    if (CommonUtils.isNetworkAvailable(context))
                    callApiSuccessOrderApi();
                    else
                        ToastUtil.Companion.showShortToast(context,getString(R.string.failed_internet));

//                        Intent intent =new Intent();
//                        intent.putExtra(AppConstant.PAYMENT_DETAILS,paymentDetails);
//                        finish();
//                        startActivity(new Intent(context, PaymentDetailsActivity.class)
//                        .putExtra(AppConstant.PAYMENT_DETAILS,paymentDetails)
//                        .putExtra(AppConstant.PAYMENT_AMOUNT,amount)     //setResult(RESULT_OK,intent);
//                        );
                }
            }
            else if (requestCode==RESULT_CANCELED){
                ToastUtil.Companion.showShortToast(context,getString(R.string.cancel));
            }

        }

        if (resultCode==PaymentActivity.RESULT_EXTRAS_INVALID){
            ToastUtil.Companion.showShortToast(context,getString(R.string.invalid_));
        }

    }


    private void getDetails(JSONObject response) {
        try {
            txtId=response.getString("id");
            callApiSuccessOrderApi();
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    private void callApiSuccessOrderApi(){
        CommonUtils.showProgressDialog(context);

        ApiRequest apiRequest =new ApiRequest();
        apiRequest.setBookingId(bookingId);
        apiRequest.setPaymentMethod(paymentType);
        apiRequest.setBankReferenceId("");
        apiRequest.setTransactionId(txtId);
        apiRequest.setTimeZone(TimeZone.getDefault().getID());
        ApiUtils.callSuccessOrderApi(context, apiRequest, new ApiResponseInterface() {
            @Override
            public void onResponse(Response<ApiResponse> response) {
                CommonUtils.hideProgressDialog();
                if (response.code()== AppConstant.SUCCESS_CODE){
                    if (response.body()!=null){

                        if (response.body().getMessage()!=null)
                            ToastUtil.Companion.showShortToast(context,response.body().getMessage());

                        Intent intent = new Intent(context,PaymentSuccessfulActivity.class);
                        intent.putExtra(AppConstant.ID,response.body().getOrderId());
                        startActivity(intent);
                        finishAffinity();
                    }
                }
                else if (response.code()==AppConstant.VALIDATION_ERROR_CODE){

                }
                else if (response.code()==AppConstant.UNAUTHORIZED_CODE){
                    ToastUtil.Companion.showShortToast(context,getString(R.string.something_went_wrong));
                }
                else {
                    ToastUtil.Companion.showShortToast(context,getString(R.string.something_went_wrong));
                }
            }
            @Override
            public void onFailure() {
                CommonUtils.hideProgressDialog();
                ToastUtil.Companion.showShortToast(context,getString(R.string.something_went_wrong));
            }
        });
    }
    private void sendData(){
        Intent intent = new Intent(context,StripePaymentActivity.class);
        intent.putExtra(AppConstant.ID,bookingId);
        intent.putExtra(AppConstant.CAR_ID,carId);
        intent.putExtra(AppConstant.BOOKING_FROM_DATE,mPickDate);
        intent.putExtra(AppConstant.BOOKING_TO_DATE,mDropDate);
        intent.putExtra(AppConstant.PICKUP_LOCATION,mPickuLocation);
        intent.putExtra(AppConstant.BILLING_FIRST_NAME,fname);
        intent.putExtra(AppConstant.BILLING_LAST_NAME,lname);
        intent.putExtra(AppConstant.BILLING_MOBILE,phoneNum);
        intent.putExtra(AppConstant.BILLING_ALTERNATIVE_MOBILE,alternativeNum);
        intent.putExtra(AppConstant.BILLING_ADDRESS,address);
        intent.putExtra(AppConstant.PRICE_PER_DAY,pricePerDay);
        intent.putExtra(AppConstant.SECURITY_AMOUNT,securityAmt);
        intent.putExtra(AppConstant.BILLING_EMAIL,email);
        intent.putExtra(AppConstant.GRAND_TOTAL,grandTotalAmt);
        intent.putExtra(AppConstant.GRAND_TOTAL_USD,grandTotalUSD);
        startActivity(intent);
    }

    private void sendDataToPayPal(){
        Intent intent = new Intent(context,PaypalBraintreeActivity.class);
        intent.putExtra(AppConstant.ID,bookingId);
        intent.putExtra(AppConstant.CAR_ID,carId);
        intent.putExtra(AppConstant.BOOKING_FROM_DATE,mPickDate);
        intent.putExtra(AppConstant.BOOKING_TO_DATE,mDropDate);
        intent.putExtra(AppConstant.PICKUP_LOCATION,mPickuLocation);
        intent.putExtra(AppConstant.BILLING_FIRST_NAME,fname);
        intent.putExtra(AppConstant.BILLING_LAST_NAME,lname);
        intent.putExtra(AppConstant.BILLING_MOBILE,phoneNum);
        intent.putExtra(AppConstant.BILLING_ALTERNATIVE_MOBILE,alternativeNum);
        intent.putExtra(AppConstant.BILLING_ADDRESS,address);
        intent.putExtra(AppConstant.PRICE_PER_DAY,pricePerDay);
        intent.putExtra(AppConstant.SECURITY_AMOUNT,securityAmt);
        intent.putExtra(AppConstant.BILLING_EMAIL,email);
        intent.putExtra(AppConstant.GRAND_TOTAL,grandTotalAmt);
        intent.putExtra(AppConstant.GRAND_TOTAL_USD,grandTotalUSD);
        startActivity(intent);
    }
/*    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode==2){
            if (resultCode==RESULT_OK){
                if (data!=null){
                    try {
                        JSONObject jsonObject=new JSONObject(data.getStringExtra(AppConstant.PAYMENT_DETAILS));
                        getDetails(jsonObject.getJSONObject("response"));
                    } catch (Exception e) {
                        e.printStackTrace();
                    }

                }
            }
        }
    }*/

   /* private void callPlaceOrderApi(){
        CommonUtils.showProgressDialog(context);
        int dayCount=1;
        SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy hh:mm a");
        Date Date1 = null,Date2 = null;
        try{
            Date1 = sdf.parse(mPickDate);
            Date2 = sdf.parse(mDropDate);

            dayCount = (int) ((Date2.getTime() - Date1.getTime())/(24*60*60*1000));

        }catch(Exception e)
        {
            e.printStackTrace();
        }

        double totalAmount= pricePerDay*dayCount;
        double grandTotal= totalAmount+securityAmt+taxAmt;

        ApiRequest apiRequest = new ApiRequest();
        apiRequest.setCarId(carId);
        apiRequest.setBookingFromDate(mPickDate);
        apiRequest.setBookingToDate(mDropDate);
        apiRequest.setCurrency(CurrencyJson.updateCurrency(context));
        apiRequest.setTotalAmount(String.valueOf(totalAmount));
        apiRequest.setSecurityAmount(String.valueOf(securityAmt));
        apiRequest.setTaxAmount(String.valueOf(taxAmt));
        apiRequest.setGrandTotal(String.valueOf(grandTotal));
        apiRequest.setBillingFirstName(fname);
        apiRequest.setBillingLastName(lname);
        apiRequest.setBillingEmail(email);
        apiRequest.setBillingMobile(phoneNum);
        apiRequest.setBillingAlternateMobile(alternativeNum);
        apiRequest.setBillingAddress(address);
        apiRequest.setPickupLocation(mPickuLocation);

        ApiUtils.callPlaceOrderApi(context, apiRequest, new ApiResponseInterface() {
            @Override
            public void onResponse(Response<ApiResponse> response) {
                CommonUtils.hideProgressDialog();
                if (response.code()== AppConstant.SUCCESS_CODE){
                    if (response.body()!=null){

                        if (response.body().getMessage()!=null)
                        ToastUtil.Companion.showShortToast(context,response.body().getMessage());
                        if (response.body().getBookingId()!=null)
                        bookingId=response.body().getBookingId();

                        Intent intent = new Intent(context,PaymentSuccessfulActivity.class);
                        intent.putExtra(AppConstant.ID,bookingId);
                        startActivity(intent);
                        finishAffinity();
                    }

                }
                else if (response.code()==AppConstant.VALIDATION_ERROR_CODE){

                }
                else if (response.code()==AppConstant.UNAUTHORIZED_CODE){
                    ToastUtil.Companion.showShortToast(context,getString(R.string.something_went_wrong));

                }
                else {
                    ToastUtil.Companion.showShortToast(context,getString(R.string.something_went_wrong));

                }
            }

            @Override
            public void onFailure() {
                CommonUtils.hideProgressDialog();
                ToastUtil.Companion.showShortToast(context,getString(R.string.something_went_wrong));
            }
        });
    }*/
}
