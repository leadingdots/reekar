package com.reekar.activity;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.IntentSender;
import android.content.pm.PackageManager;
import android.graphics.Rect;
import android.os.Bundle;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.widget.Toolbar;
import androidx.core.view.GravityCompat;
import androidx.databinding.DataBindingUtil;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.navigation.ui.AppBarConfiguration;

import com.akanksha.commonclassutil.CheckPermission;
import com.akanksha.commonclassutil.PreferencesManager;
import com.akanksha.commonclassutil.ToastUtil;
import com.bumptech.glide.Glide;
import com.google.android.material.snackbar.Snackbar;
import com.google.android.play.core.appupdate.AppUpdateInfo;
import com.google.android.play.core.appupdate.AppUpdateManager;
import com.google.android.play.core.appupdate.AppUpdateManagerFactory;
import com.google.android.play.core.install.InstallState;
import com.google.android.play.core.install.InstallStateUpdatedListener;
import com.google.android.play.core.install.model.AppUpdateType;
import com.google.android.play.core.install.model.InstallStatus;
import com.google.android.play.core.install.model.UpdateAvailability;
import com.google.android.play.core.tasks.OnSuccessListener;
import com.google.android.play.core.tasks.Task;
import com.reekar.R;
import com.reekar.apiservices.ApiUtils;
import com.reekar.commonclasses.AppConstant;
import com.reekar.commonclasses.BaseActivity;
import com.reekar.commonclasses.CommonUtils;
import com.reekar.databinding.ActivityMainBinding;
import com.reekar.fragment.ContactFragment;
import com.reekar.fragment.DashboardFragment;
import com.reekar.fragment.ListCarFragment;
import com.reekar.fragment.LoginFragment;
import com.reekar.fragment.MainFragment;
import com.reekar.fragment.MyBookedCarFragment;
import com.reekar.fragment.MyListingCarFragment;
import com.reekar.fragment.MyOrdersFragment;
import com.reekar.fragment.PaymentOptionsFragment;
import com.reekar.fragment.ProfileFragment;
import com.reekar.interfaces.ApiResponseInterface;
import com.reekar.interfaces.OkCancelInterface;
import com.reekar.models.ApiRequest;
import com.reekar.models.ApiResponse;
import com.reekar.translation.ChangeLanguage;

import retrofit2.Response;

public class MainActivity extends BaseActivity {

    public ActivityMainBinding mainBinding;
    private Context mContext;

    private AppBarConfiguration mAppBarConfiguration;
    private ImageView navOpenerBtn;
    DrawerLayout drawer;
    private View view;

    private Boolean login;
//    private Boolean skip;
    private String skip;
    private ImageView imageViewToolbar;
    private TextView textViewToolbar;
    private String activityfrom;
    private String firstname;
    private String lastname;

    private static final int REQ_CODE_VERSION_UPDATE = 530;
    private AppUpdateManager appUpdateManager;
    private InstallStateUpdatedListener installStateUpdatedListener;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        mContext =this;
        mainBinding = DataBindingUtil.setContentView(this,R.layout.activity_main);

        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        /*CommonUtils.showAlertDailog(mContext, "Currency",
                PreferencesManager.Companion.getStringPreferences(mContext,AppConstant.CURRENT_CURRENCY), getString(R.string.ok),
                getString(R.string.canle),
                new OkCancelInterface() {
                    @Override
                    public void onOkClick() {

                    }

                    @Override
                    public void onCancelClick() {

                    }
                });*/
        imageViewToolbar = findViewById(R.id.logo_nav);
        textViewToolbar = findViewById(R.id.toolbar_textview);
//      toolbar.setNavigationIcon(R.drawable.ic_menu_black_24dp);

        drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        navOpenerBtn = (ImageView) findViewById(R.id.nav_btn);




        checkForAppUpdate();
        getIntentdata();
        setToolbar();
        openDashboard();
        setListener();
//        permissionLocation();

    }

    private void getIntentdata(){
        Intent intent = getIntent();
        if (intent!=null) {
            if (intent.getStringExtra(AppConstant.FROM)!=null)
                activityfrom = intent.getStringExtra(AppConstant.FROM);
            if (intent.getStringExtra(AppConstant.IS_SKIP)!=null)
                skip=intent.getStringExtra(AppConstant.IS_SKIP);
        }
    }

    private void openDrawer() {
        DrawerLayout navDrawer = findViewById(R.id.drawer_layout);
        if(!navDrawer.isDrawerOpen(GravityCompat.START))
            navDrawer.openDrawer(GravityCompat.START);
        else
            navDrawer.closeDrawer(GravityCompat.END);
    }

    public void openDashboard(){
//        skip=PreferencesManager.Companion.getBooleanPreferences(mContext,AppConstant.IS_SKIP);
        login  = PreferencesManager.Companion.getBooleanPreferences(mContext,AppConstant.IS_LOGIN);

        if (!login){
            if (skip!=null) {
                if (skip.equals(AppConstant.IS_SKIP)) {
                    CommonUtils.setFragment(new DashboardFragment(), false, this, R.id.container_mainn, AppConstant.DASHBOARD);
//            PreferencesManager.Companion.clearPreference(mContext,AppConstant.IS_SKIP);
                }
            }
            else {
                CommonUtils.setFragment(new MainFragment(), false, this, R.id.container_mainn, AppConstant.MAIN_FRAGMENT);
            }
        }
        else {
            setProfile();
            CommonUtils.setFragment(new MainFragment(), false, this, R.id.container_mainn, AppConstant.MAIN_FRAGMENT);
        }
        if (activityfrom != null) {
            if (activityfrom.equals(AppConstant.VERIFY_ACTIVITY) && login) {
                CommonUtils.setFragment(new PaymentOptionsFragment(), true, this, R.id.container_mainn, AppConstant.PAYMENT_FRAGMENT);
            }
            if (activityfrom.equals(AppConstant.CAR_DETAIL_ACTIVITY)) {
                Bundle bundle = new Bundle();
                bundle.putString(AppConstant.FROM,activityfrom);
                Fragment fragment = new LoginFragment();
                fragment.setArguments(bundle);

                this.getSupportFragmentManager().popBackStack(AppConstant.DASHBOARD, FragmentManager.POP_BACK_STACK_INCLUSIVE);
                CommonUtils.setFragment(fragment, true, this, R.id.container_mainn, AppConstant.LOGIN_FRAGMENT);
            }
                if (activityfrom.equals(AppConstant.CAR_AVAILABILITY_ACTIVITY )&& login) {
//                this.getSupportFragmentManager().popBackStack(AppConstant.DASHBOARD, FragmentManager.POP_BACK_STACK_INCLUSIVE);
                CommonUtils.setFragment(new MyListingCarFragment(), true, this, R.id.container_mainn, AppConstant.MYLISTED_FRAGMENT);
            }
//                if (activityfrom.equals(AppConstant.PAYMENT_SUCCESSFUL_ACTIVITY )&& login) {
//                this.getSupportFragmentManager().popBackStack(AppConstant.DASHBOARD, FragmentManager.POP_BACK_STACK_INCLUSIVE);
//                CommonUtils.setFragment(new MyBookedCarFragment(), true, this, R.id.container_mainn, AppConstant.MYLISTED_FRAGMENT);
//            }
        }

    }

    private void setProfile(){
        firstname = PreferencesManager.Companion.getStringPreferences(mContext,AppConstant.FIRST_NAME);
        lastname = PreferencesManager.Companion.getStringPreferences(mContext,AppConstant.LAST_NAME);

        mainBinding.sideNavMenu.myProfileLayout.setVisibility(View.VISIBLE);
        mainBinding.sideNavMenu.navLogoutLayout.setVisibility(View.VISIBLE);
        mainBinding.sideNavMenu.gettingStartLayout.setVisibility(View.GONE);
        mainBinding.sideNavMenu.navHeaderMain.profileImage.setVisibility(View.VISIBLE);
        mainBinding.sideNavMenu.navHeaderMain.headerUsername.setVisibility(View.VISIBLE);
        if (lastname!=null) {
            mainBinding.sideNavMenu.navHeaderMain.headerUsername.setText(firstname + " " + lastname);
        }
        else {
            mainBinding.sideNavMenu.navHeaderMain.headerUsername.setText(firstname);
        }
        mainBinding.sideNavMenu.navHeaderMain.imageViewGlobal.setVisibility(View.GONE);

        Glide.with(mContext)
                .load(PreferencesManager.Companion.getStringPreferences(mContext,AppConstant.USER_IMAGE))
                .placeholder(R.drawable.ic_user)
                .into(mainBinding.sideNavMenu.navHeaderMain.ivUser);
    }

    @Override
    public void setToolbar() {

    }

    @Override
    public void setListener() {
        navOpenerBtn.setOnClickListener(this);
        mainBinding.sideNavMenu.navHome.setOnClickListener(this);
        mainBinding.sideNavMenu.navHowWork.setOnClickListener(this);
        mainBinding.sideNavMenu.navLogin.setOnClickListener(this);
        mainBinding.sideNavMenu.navBookcar.setOnClickListener(this);
        mainBinding.sideNavMenu.navListcar.setOnClickListener(this);
        mainBinding.sideNavMenu.navMyinfo.setOnClickListener(this);
        mainBinding.sideNavMenu.navMypayment.setOnClickListener(this);
        mainBinding.sideNavMenu.navMyBookedCar.setOnClickListener(this);
        mainBinding.sideNavMenu.navMylistedCar.setOnClickListener(this);
        mainBinding.sideNavMenu.navMyOrderList.setOnClickListener(this);
        mainBinding.sideNavMenu.navBookcarLogged.setOnClickListener(this);
        mainBinding.sideNavMenu.navListLogged.setOnClickListener(this);
        mainBinding.sideNavMenu.navWhoWe.setOnClickListener(this);
        mainBinding.sideNavMenu.navTerms.setOnClickListener(this);
        mainBinding.sideNavMenu.navPolicy.setOnClickListener(this);
        mainBinding.sideNavMenu.navContactUs.setOnClickListener(this);
        mainBinding.sideNavMenu.navLogout.setOnClickListener(this);
        mainBinding.sideNavMenu.navHeaderMain.ivEdit.setOnClickListener(this);

//        mainBinding.sideNavMenu.navHeaderMain
    }

    @Override
    public void onClick(View v) {

        switch (v.getId()){

            case R.id.nav_btn:{
                CommonUtils.hideKeyBoard((Activity) mContext);
                openDrawer();
                break;
            }
            case R.id.iv_edit:{
                drawer.closeDrawer(GravityCompat.START);
                CommonUtils.setFragment(new ProfileFragment(), true, this, R.id.container_mainn, AppConstant.PROFILE_FRAGMENT);
                break;
            }
            case R.id.nav_home:{
                drawer.closeDrawer(GravityCompat.START);
                if (!login) {
//                    if (skip){
//                        CommonUtils.setFragment(new DashboardFragment(), false, this, R.id.container_mainn, AppConstant.DASHBOARD);
//                    }
//                    else {
                        CommonUtils.setFragment(new MainFragment(), false, this, R.id.container_mainn, AppConstant.MAIN_FRAGMENT);
//                    }
                }
                else {
                    CommonUtils.setFragment(new MainFragment(), false, this, R.id.container_mainn, AppConstant.MAIN_FRAGMENT);
                }
                break;
            }
            case R.id.nav_how_work:{
                drawer.closeDrawer(GravityCompat.START);
                Intent intent = new Intent(mContext,HowItWorksActivity.class);
                intent.putExtra(AppConstant.FROM,AppConstant.WHO_WE_ARE);
                startActivity(intent);
                break;
            }
            case R.id.nav_login:{
                drawer.closeDrawer(GravityCompat.START);
                CommonUtils.setFragment(new LoginFragment(), true, this, R.id.container_mainn, AppConstant.LOGIN_FRAGMENT);
//                if (skip){
//                    CommonUtils.setFragment(new LoginFragment(), true, this, R.id.container_mainn, AppConstant.LOGIN_FRAGMENT);
//                }
                break;
            }
            case R.id.nav_bookcar:{
                drawer.closeDrawer(GravityCompat.START);
//                if (!skip){
//                    CommonUtils.setFragment(new BookCarFragment(), true, this, R.id.container_mainn, AppConstant.BOOKCAR_FRAGMENT);
//                }
//                else {
//                    CommonUtils.setFragment(new DashboardFragment(), false, this, R.id.container_mainn, AppConstant.DASHBOARD);
//                }
                CommonUtils.setFragment(new DashboardFragment(), false, this, R.id.container_mainn, AppConstant.DASHBOARD);

                break;
            }
            case R.id.nav_listcar:{
                drawer.closeDrawer(GravityCompat.START);
//                if (!skip)
                   CommonUtils.setFragment(new ListCarFragment(), true, this, R.id.container_mainn, AppConstant.LISTVIEW_FRAGMENT);
//                else
//                    CommonUtils.setFragment(new ListCarFragment(), true, this, R.id.container_mainn, AppConstant.LISTVIEW_FRAGMENT);
                break;
            }
            case R.id.nav_myinfo:{
                drawer.closeDrawer(GravityCompat.START);
                CommonUtils.setFragment(new ProfileFragment(), true, this, R.id.container_mainn, AppConstant.PROFILE_FRAGMENT);
                break;
            }
            case R.id.nav_mypayment:{
                drawer.closeDrawer(GravityCompat.START);
                CommonUtils.setFragment(new PaymentOptionsFragment(), true, this, R.id.container_mainn, AppConstant.PAYMENT_FRAGMENT);
                break;
            }case R.id.nav_my_booked_car:{
                drawer.closeDrawer(GravityCompat.START);
                CommonUtils.setFragment(new MyBookedCarFragment(), true, this, R.id.container_mainn, AppConstant.MYBOOKED_CAR_FRAGMENT);
//                Intent intent =new Intent(mContext,MyBookedCarActivity.class);
//                startActivity(intent);
                break;
            }
            case R.id.nav_mylisted_car:{
                drawer.closeDrawer(GravityCompat.START);
                CommonUtils.setFragment(new MyListingCarFragment(), true, this, R.id.container_mainn, AppConstant.MYLISTED_FRAGMENT);
                break;
            }
            case R.id.nav_my_order_list:{
                drawer.closeDrawer(GravityCompat.START);
//                Intent intent =new Intent(mContext,MyOrderListActivity.class);
//                startActivity(intent);
                CommonUtils.setFragment(new MyOrdersFragment(), true, this, R.id.container_mainn, AppConstant.MY_ORDERS_FRAGMENT);

                break;
            }
            case R.id.nav_bookcar_logged:{
                drawer.closeDrawer(GravityCompat.START);
                CommonUtils.setFragment(new DashboardFragment(), false, this, R.id.container_mainn, AppConstant.DASHBOARD);
                break;
            }case R.id.nav_list_logged:{
                drawer.closeDrawer(GravityCompat.START);
                Intent intent =new Intent(mContext,CarInfromationActivity.class);
                startActivity(intent);
                break;
            }case R.id.nav_who_we:{
                drawer.closeDrawer(GravityCompat.START);
                Intent intent = new Intent(mContext,TermsConditionsActivity.class);
                intent.putExtra(AppConstant.FROM,AppConstant.WHO_WE_ARE);
                startActivity(intent);
                break;
            }case R.id.nav_terms:{
                drawer.closeDrawer(GravityCompat.START);
                Intent intent = new Intent(mContext,TermsConditionsActivity.class);
                intent.putExtra(AppConstant.FROM,AppConstant.TERMS);
                startActivity(intent);
                break;
            }case R.id.nav_policy:{
                drawer.closeDrawer(GravityCompat.START);
                Intent intent = new Intent(mContext,TermsConditionsActivity.class);
                intent.putExtra(AppConstant.FROM,AppConstant.POLICY);
                startActivity(intent);
                break;
            }
            case R.id.nav_contact_us:{
                drawer.closeDrawer(GravityCompat.START);
                CommonUtils.setFragment(new ContactFragment(), true, this, R.id.container_mainn, AppConstant.CONTACT_FRAGMENT);
                break;
            }
            case R.id.nav_logout:{
                drawer.closeDrawer(GravityCompat.START);
                CommonUtils.showAlertDailog(mContext, getString(R.string.logout), getString(R.string.logout_message), getString(R.string.ok), getString(R.string.canle),
                        new OkCancelInterface() {
                    @Override
                    public void onOkClick() {
                        if (CommonUtils.isNetworkAvailable(mContext))
                        callApiLogout();
                        else
                            ToastUtil.Companion.showShortToast(mContext,getString(R.string.failed_internet));
                    }

                    @Override
                    public void onCancelClick() {

                    }
                });

                break;
            }

        }

    }

    private void callApiLogout(){
        CommonUtils.showProgressDialog(mContext);

        ApiRequest apiRequest =new ApiRequest();
        apiRequest.setDevice_token(PreferencesManager.Companion.getStringPreferences(mContext,AppConstant.DEVICE_TOKEN));
        ApiUtils.callApiLogout(mContext, apiRequest, new ApiResponseInterface() {
            @Override
            public void onResponse(Response<ApiResponse> response) {
                CommonUtils.hideProgressDialog();

                if (response.code()==AppConstant.SUCCESS_CODE){
                    if (response.body()!=null)
                        CommonUtils.showToast(mContext, ChangeLanguage.translate(mContext,response.body().getMessage()));
                    CommonUtils.clearSharedPrefrence(mContext);

                    Intent intent = new Intent(mContext,MainActivity.class);
                    startActivity(intent);
                    finish();
                }
                else if (response.code()==AppConstant.UNAUTHORIZED_CODE){
                        CommonUtils.clearSharedPrefrence(mContext);
                        Intent intent = new Intent(mContext, MainActivity.class);
                        startActivity(intent);
                        finish();
                }
                else {
                    CommonUtils.showToast(mContext,getString(R.string.something_went_wrong));
                }
            }

            @Override
            public void onFailure() {
                CommonUtils.hideProgressDialog();
            }
        });
    }
    @Override
    protected void onResume() {
        super.onResume();
        if (PreferencesManager.Companion.getBooleanPreferences(mContext,AppConstant.IS_LOGIN)){
            setProfile();
        }
//        permissionLocation();
        checkNewAppVersionState();
    }

    @Override
    protected void onRestart() {
        super.onRestart();
//        permissionLocation();
    }

    @Override
    protected void onDestroy() {
        unregisterInstallStateUpdListener();
        super.onDestroy();
    }

//    @Override
//    public void onActivityResult(int requestCode, final int resultCode, Intent intent) {
//        super.onActivityResult(requestCode, resultCode, intent);
//
//        switch (requestCode) {
//
//            case REQ_CODE_VERSION_UPDATE:
//                if (resultCode != RESULT_OK) { //RESULT_OK / RESULT_CANCELED / RESULT_IN_APP_UPDATE_FAILED
//                    //L.d("Update flow failed! Result code: " + resultCode);
//                    // If the update is cancelled or fails,
//                    // you can request to start the update again.
//                    unregisterInstallStateUpdListener();
//                }
//
//                break;
//        }
//    }

    private void checkForAppUpdate() {
        // Creates instance of the manager.
        appUpdateManager = AppUpdateManagerFactory.create(getApplicationContext());

        // Returns an intent object that you use to check for an update.
        Task<AppUpdateInfo> appUpdateInfoTask = appUpdateManager.getAppUpdateInfo();

        // Create a listener to track request state updates.
        installStateUpdatedListener = new InstallStateUpdatedListener() {
            @Override
            public void onStateUpdate(InstallState installState) {
                // Show module progress, log state, or install the update.
                if (installState.installStatus() == InstallStatus.DOWNLOADED){
                    popupSnackbarForCompleteUpdateAndUnregister();

                }
                    // After the update is downloaded, show a notification
                    // and request user confirmation to restart the app.
            }
        };

        // Checks that the platform will allow the specified type of update.
        appUpdateInfoTask.addOnSuccessListener(new OnSuccessListener<AppUpdateInfo>() {
            @Override
            public void onSuccess(AppUpdateInfo appUpdateInfo) {
                if (appUpdateInfo.updateAvailability() == UpdateAvailability.UPDATE_AVAILABLE) {
                    // Request the update.
                    if (appUpdateInfo.isUpdateTypeAllowed(AppUpdateType.FLEXIBLE)) {

                        // Before starting an update, register a listener for updates.
                        appUpdateManager.registerListener(installStateUpdatedListener);
                        // Start an update.
                        MainActivity.this.startAppUpdateFlexible(appUpdateInfo);
                    } else if (appUpdateInfo.isUpdateTypeAllowed(AppUpdateType.IMMEDIATE)) {
                        // Start an update.
                        MainActivity.this.startAppUpdateImmediate(appUpdateInfo);
                    }
                }
            }
        });
    }

    private void startAppUpdateImmediate(AppUpdateInfo appUpdateInfo) {
        try {
            appUpdateManager.startUpdateFlowForResult(
                    appUpdateInfo,
                    AppUpdateType.IMMEDIATE,
                    // The current activity making the update request.
                    this,
                    // Include a request code to later monitor this update request.
                    MainActivity.REQ_CODE_VERSION_UPDATE);
        } catch (IntentSender.SendIntentException e) {
            e.printStackTrace();
        }
    }

    private void startAppUpdateFlexible(AppUpdateInfo appUpdateInfo) {
        try {
            appUpdateManager.startUpdateFlowForResult(
                    appUpdateInfo,
                    AppUpdateType.FLEXIBLE,
                    // The current activity making the update request.
                    this,
                    // Include a request code to later monitor this update request.
                    MainActivity.REQ_CODE_VERSION_UPDATE);
        } catch (IntentSender.SendIntentException e) {
            e.printStackTrace();
            unregisterInstallStateUpdListener();
        }
    }

    /**
     * Displays the snackbar notification and call to action.
     * Needed only for Flexible app update
     */
    private void popupSnackbarForCompleteUpdateAndUnregister() {
        Snackbar snackbar = Snackbar.make(drawer,getString(R.string.update_downloaded), Snackbar.LENGTH_INDEFINITE);
        snackbar.setAction("Restart", new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                appUpdateManager.completeUpdate();
            }
        });
        snackbar.setActionTextColor(getResources().getColor(R.color.colorAccent));
        snackbar.show();

        unregisterInstallStateUpdListener();
    }

    /**
     * Checks that the update is not stalled during 'onResume()'.
     * However, you should execute this check at all app entry points.
     */
    private void checkNewAppVersionState() {
        appUpdateManager
                .getAppUpdateInfo()
                .addOnSuccessListener(
                        new OnSuccessListener<AppUpdateInfo>() {
                            @Override
                            public void onSuccess(AppUpdateInfo appUpdateInfo) {
                                //FLEXIBLE:
                                // If the update is downloaded but not installed,
                                // notify the user to complete the update.
                                if (appUpdateInfo.installStatus() == InstallStatus.DOWNLOADED) {
                                popupSnackbarForCompleteUpdateAndUnregister();
                                }

                                //IMMEDIATE:
                                if (appUpdateInfo.updateAvailability()
                                        == UpdateAvailability.DEVELOPER_TRIGGERED_UPDATE_IN_PROGRESS) {
                                    // If an in-app update is already running, resume the update.
                                    MainActivity.this.startAppUpdateImmediate(appUpdateInfo);
                                }
                            }
                        });

    }

    /**
     * Needed only for FLEXIBLE update
     */
    private void unregisterInstallStateUpdListener() {
        if (appUpdateManager != null && installStateUpdatedListener != null)
            appUpdateManager.unregisterListener(installStateUpdatedListener);
    }

    /**
     * This method is used to handle back press
     */
    @Override
    public void onBackPressed() {
        if (drawer.isDrawerOpen(GravityCompat.START)){
            drawer.closeDrawer(GravityCompat.START);
        }
        else {

            if (getSupportFragmentManager().getBackStackEntryCount() >1) {
                getSupportFragmentManager().addOnBackStackChangedListener(getListener());

                super.onBackPressed();
            }
          /*  if (getSupportFragmentManager().getBackStackEntryCount()==1){
                new AlertDialog.Builder(this)
                        .setMessage("Are you sure you want to exit?")
                        .setCancelable(false)
                        .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                finish();
                            }
                        })
                        .setNegativeButton("No", null)
                        .show();
            }*/
            else
            {
                finish();
            }
        }
    }

    /**
     * This method is used to handle back press of fragment if exist
     * @return back stack listener
     */
    private FragmentManager.OnBackStackChangedListener getListener() {
        return new FragmentManager.OnBackStackChangedListener() {
            @SuppressWarnings("ConstantConditions")
            public void onBackStackChanged() {
                try {
                    FragmentManager manager = getSupportFragmentManager();

                    if (manager != null) {
                        if (getSupportFragmentManager().getBackStackEntryCount() != 0) {
                            String tag = getSupportFragmentManager().getBackStackEntryAt(getSupportFragmentManager().getBackStackEntryCount() - 1).getName();
                            if (getSupportFragmentManager().findFragmentByTag(tag) != null) {
                                getSupportFragmentManager().findFragmentByTag(tag).onResume();
                            }

                        } else {

                            getSupportFragmentManager().getFragments().get(getSupportFragmentManager().getBackStackEntryCount()).onResume();
                        }
                    }
                } catch (Exception e){
                    e.printStackTrace();
                }
            }
        };
    }
    public void setToolBar(boolean b){
        if (b) {
           mainBinding.appBarMain.toolbarLayout.setVisibility(View.VISIBLE);
            textViewToolbar.setVisibility(View.VISIBLE);
            textViewToolbar.setText("");

        }
        else{
            mainBinding.appBarMain.toolbarLayout.setVisibility(View.INVISIBLE);
        }

    }

    public void setToolBar(ImageView navBtn){
        mainBinding.appBarMain.toolbarLayout.setVisibility(View.GONE);
        navBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                CommonUtils.hideKeyBoard((Activity) mContext);
                openDrawer();
            }
        });
    }


    @Override
    public boolean dispatchTouchEvent(MotionEvent event) {
        if (event.getAction() == MotionEvent.ACTION_DOWN) {
            View v = getCurrentFocus();
            if ( v instanceof EditText) {
                Rect outRect = new Rect();
                v.getGlobalVisibleRect(outRect);
                if (!outRect.contains((int)event.getRawX(), (int)event.getRawY())) {
                    v.clearFocus();
                }
            }
        }
        return super.dispatchTouchEvent(event);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
       if (requestCode==REQ_CODE_VERSION_UPDATE){
                if (resultCode != RESULT_OK) { //RESULT_OK / RESULT_CANCELED / RESULT_IN_APP_UPDATE_FAILED
                    //L.d("Update flow failed! Result code: " + resultCode);
                    // If the update is cancelled or fails,
                    // you can request to start the update again.
                    unregisterInstallStateUpdListener();
                }

        }
        try {
            for (Fragment fragment : getSupportFragmentManager().getFragments()) {
                fragment.onActivityResult(requestCode, resultCode, data);
                Log.d("Activity", "ON RESULT CALLED");
            }
        } catch (Exception e) {
            Log.d("ERROR", e.toString());
        }

    }

    private void permissionLocation(){
        if (CheckPermission.Companion.checkIsMarshMallowVersion()){
            if (CheckPermission.Companion.checkLocationPermission(mContext)){
              PreferencesManager.Companion.saveStringPreferences(mContext,AppConstant.CURRENT_CURRENCY,  CommonUtils.getCurrentCurrency(mContext));
            }
            else
                CheckPermission.Companion.requestLocationPermission((Activity) mContext);
        }
        else {
            PreferencesManager.Companion.saveStringPreferences(mContext,AppConstant.CURRENT_CURRENCY,  CommonUtils.getCurrentCurrency(mContext));
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);

        switch (requestCode) {
            case CheckPermission.REQUEST_CODE_LOCATION_PERMISSION: {
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    PreferencesManager.Companion.saveStringPreferences(mContext,AppConstant.CURRENT_CURRENCY,  CommonUtils.getCurrentCurrency(mContext));

                }
            }
        }
    }
}
