package com.reekar.activity;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;

import com.akanksha.commonclassutil.ToastUtil;
import com.braintreepayments.api.BraintreeFragment;
import com.braintreepayments.api.DataCollector;
import com.braintreepayments.api.PayPal;
import com.braintreepayments.api.dropin.DropInActivity;
import com.braintreepayments.api.dropin.DropInResult;
import com.braintreepayments.api.exceptions.InvalidArgumentException;
import com.braintreepayments.api.interfaces.BraintreeErrorListener;
import com.braintreepayments.api.interfaces.BraintreeResponseListener;
import com.braintreepayments.api.interfaces.ConfigurationListener;
import com.braintreepayments.api.interfaces.PaymentMethodNonceCreatedListener;
import com.braintreepayments.api.models.Configuration;
import com.braintreepayments.api.models.PayPalRequest;
import com.braintreepayments.api.models.PaymentMethodNonce;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.reekar.R;
import com.reekar.apiservices.ApiUtils;
import com.reekar.commonclasses.AppConstant;
import com.reekar.commonclasses.CommonUtils;
import com.reekar.interfaces.ApiResponseInterface;
import com.reekar.models.ApiRequest;
import com.reekar.models.ApiResponse;
import com.reekar.models.PayPalModel;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.TimeZone;

import retrofit2.Response;

public class PaypalBraintreeActivity extends AppCompatActivity implements ConfigurationListener,
        PaymentMethodNonceCreatedListener, BraintreeErrorListener {

    private Context context;

    protected String mAuthorization;
    protected String mCustomerId;
    protected BraintreeFragment mBraintreeFragment;

    private String mDeviceData;
    private String strDeviceData;

    private String mPayerId;
    private String mClientMetadataId;
    private String mDescription;
    private String mNonce;

    private String bookingId;

    private String carId;
    private String mPickDate;
    private String mDropDate;
    private String mPickuLocation;

    private int taxAmt =0;
    private String totalAmt;
    private String fname;
    private String lname;
    private String email;
    private String phoneNum;
    private String alternativeNum;
    private String address;
    private double pricePerDay;
    private double securityAmt;
    private double grandTotalAmt;
    private String paymentType="Paypal";
    private double grandTotalUSD;

    private String txtId;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_paypal_braintree);
        context=this;

        getIntentData();
        callBraintreeApi();

    }
    private void getIntentData() {
        Intent intent =getIntent();
        if (intent!=null){
            if (intent.getStringExtra(AppConstant.BOOKING_FROM_DATE)!=null){
                mPickDate=intent.getStringExtra(AppConstant.BOOKING_FROM_DATE);
            }
            if (intent.getStringExtra(AppConstant.BOOKING_TO_DATE)!=null){
                mDropDate=intent.getStringExtra(AppConstant.BOOKING_TO_DATE);
            }
            if (intent.getStringExtra(AppConstant.PICKUP_LOCATION)!=null){
                mPickuLocation=intent.getStringExtra(AppConstant.PICKUP_LOCATION);
            }
            if (intent.getStringExtra(AppConstant.CAR_ID)!=null){
                carId=intent.getStringExtra(AppConstant.CAR_ID);
            }
            if (intent.getStringExtra(AppConstant.BILLING_FIRST_NAME)!=null){
                fname=intent.getStringExtra(AppConstant.BILLING_FIRST_NAME);
            }
            if (intent.getStringExtra(AppConstant.BILLING_LAST_NAME)!=null){
                lname=intent.getStringExtra(AppConstant.BILLING_LAST_NAME);
            }
            if (intent.getStringExtra(AppConstant.BILLING_MOBILE)!=null){
                phoneNum=intent.getStringExtra(AppConstant.BILLING_MOBILE);
            }
            if (intent.getStringExtra(AppConstant.BILLING_ALTERNATIVE_MOBILE)!=null){
                alternativeNum=intent.getStringExtra(AppConstant.BILLING_ALTERNATIVE_MOBILE);
            }
            if (intent.getStringExtra(AppConstant.BILLING_ADDRESS)!=null){
                address=intent.getStringExtra(AppConstant.BILLING_ADDRESS);
            }
//            if (intent.getStringExtra(AppConstant.PRICE_PER_DAY)!=null){
            pricePerDay= intent.getDoubleExtra(AppConstant.PRICE_PER_DAY,0);
//            }
//            if (intent.getStringExtra(AppConstant.SECURITY_AMOUNT)!=null){
            securityAmt= intent.getDoubleExtra(AppConstant.SECURITY_AMOUNT,0);
//            }
            if (intent.getStringExtra(AppConstant.BILLING_EMAIL)!=null){
                email=intent.getStringExtra(AppConstant.BILLING_EMAIL);
            }
            if (intent.getStringExtra(AppConstant.ID)!=null){
                bookingId=intent.getStringExtra(AppConstant.ID);
            }
            grandTotalAmt=intent.getDoubleExtra(AppConstant.GRAND_TOTAL,0);
            grandTotalUSD=intent.getDoubleExtra(AppConstant.GRAND_TOTAL_USD,0);
        }
    }

    private PayPalRequest getPayPalRequest(@Nullable String amount) {
        PayPalRequest request = new PayPalRequest(amount);///// ------------ye wala used --------------

        request.displayName(getString(R.string.app_name));///// ------------ye wala used --------------
        request.intent(PayPalRequest.INTENT_AUTHORIZE); ///// ------------ye wala used --------------

        return request;
    }
    @Override
    public void onPaymentMethodNonceCreated(PaymentMethodNonce paymentMethodNonce) {
//        Log.e("data",new Gson().toJson(paymentMethodNonce));
        PayPalModel payPalModel=new PayPalModel();
        PayPalModel payPalModel1 = new Gson().fromJson(new Gson().toJson(paymentMethodNonce),new TypeToken<PayPalModel>(){}.getType());

        if (payPalModel1.getmNonce()!=null)
        mNonce=payPalModel1.getmNonce();
        if (payPalModel1.getmPayerId()!=null)
        mPayerId=payPalModel1.getmPayerId();

        callApiBraintreePaypal();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == 2) {
            if (resultCode == RESULT_OK) {
                DropInResult result = data.getParcelableExtra(DropInResult.EXTRA_DROP_IN_RESULT);
                // use the result to update your UI and send the payment method nonce to your serve
            } else if (resultCode == RESULT_CANCELED) {
                // the user canceled
            } else {
                // handle errors here, an exception may be available in
                Exception error = (Exception) data.getSerializableExtra(DropInActivity.EXTRA_ERROR);
            }
        }
    }

    @Override
    public void onError(Exception error) {

    }
    @Override
    public void onConfigurationFetched(Configuration configuration) {
        DataCollector.collectDeviceData(mBraintreeFragment, new BraintreeResponseListener<String>() {
            @Override
            public void onResponse(String deviceData) {
                mDeviceData = deviceData;
//                Log.e("device data",mDeviceData);
                try {
                    JSONObject jresponse = new JSONObject(deviceData);
                     strDeviceData= jresponse.getString("correlation_id");
                     Log.e("device data",strDeviceData);


                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        });
    }

    private void callBraintreeApi(){
        CommonUtils.showProgressDialog(context);

        ApiUtils.callBraintreePaymentApi(context, new ApiResponseInterface() {
            @Override
            public void onResponse(Response<ApiResponse> response) {
                CommonUtils.hideProgressDialog();
                if (response.code()== AppConstant.SUCCESS_CODE){
                    if (response.body().getCsStripeKey()!=null){
                        mAuthorization=response.body().getCsStripeKey();
                        try {
                            mBraintreeFragment = BraintreeFragment.newInstance((AppCompatActivity) context, mAuthorization);
                        } catch (InvalidArgumentException e) {
                            e.printStackTrace();
                        }
                        PayPal.requestOneTimePayment(mBraintreeFragment, getPayPalRequest(String.valueOf(grandTotalUSD)));
                    }
                }
                else {
                    ToastUtil.Companion.showShortToast(context,getString(R.string.something_went_wrong));
                }
            }

            @Override
            public void onFailure() {
                ToastUtil.Companion.showShortToast(context,getString(R.string.something_went_wrong));
            }
        });
    }

    private void callApiBraintreePaypal(){

        ApiRequest apiRequest =new ApiRequest();
        apiRequest.setAmount(String.format("%.2f", grandTotalUSD));
        apiRequest.setPaymentMethodNonce(mNonce);
        apiRequest.setDeviceData(strDeviceData);

        ApiUtils.callBraintreePaypalPaymentApi(context, apiRequest, new ApiResponseInterface() {
            @Override
            public void onResponse(Response<ApiResponse> response) {
                if (response.code()==AppConstant.SUCCESS_CODE){
                    if (response.body().getResult().getTransaction().getId()!=null){
                        txtId=response.body().getResult().getTransaction().getId();
                        callApiSuccessOrderApi();
                    }
                }
                else {
                    ToastUtil.Companion.showShortToast(context,getString(R.string.something_went_wrong));
                }
            }

            @Override
            public void onFailure() {
                ToastUtil.Companion.showShortToast(context,getString(R.string.something_went_wrong));
            }
        });
    }
    private void callApiSuccessOrderApi(){
        CommonUtils.showProgressDialog(context);

        ApiRequest apiRequest =new ApiRequest();
        apiRequest.setBookingId(bookingId);
        apiRequest.setPaymentMethod(paymentType);
        apiRequest.setBankReferenceId("");
        apiRequest.setTransactionId(txtId);
        apiRequest.setTimeZone(TimeZone.getDefault().getID());
        ApiUtils.callSuccessOrderApi(context, apiRequest, new ApiResponseInterface() {
            @Override
            public void onResponse(Response<ApiResponse> response) {
                CommonUtils.hideProgressDialog();
                if (response.code()== AppConstant.SUCCESS_CODE){
                    if (response.body()!=null){

                        if (response.body().getMessage()!=null)
                            ToastUtil.Companion.showShortToast(context,response.body().getMessage());

                        Intent intent = new Intent(context,PaymentSuccessfulActivity.class);
                        intent.putExtra(AppConstant.ID,response.body().getOrderId());
                        startActivity(intent);
                        finishAffinity();
                    }
                }
                else if (response.code()==AppConstant.VALIDATION_ERROR_CODE){

                }
                else if (response.code()==AppConstant.UNAUTHORIZED_CODE){
                    ToastUtil.Companion.showShortToast(context,getString(R.string.something_went_wrong));
                }
                else {
                    ToastUtil.Companion.showShortToast(context,getString(R.string.something_went_wrong));
                }
            }
            @Override
            public void onFailure() {
                CommonUtils.hideProgressDialog();
                ToastUtil.Companion.showShortToast(context,getString(R.string.something_went_wrong));
            }
        });
    }
}
