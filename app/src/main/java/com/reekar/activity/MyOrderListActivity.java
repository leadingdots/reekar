package com.reekar.activity;

import android.content.Context;
import android.os.Bundle;
import android.view.View;

import androidx.databinding.DataBindingUtil;

import com.reekar.R;
import com.reekar.adapter.MyOrderListAdapter;
import com.reekar.commonclasses.BaseActivity;
import com.reekar.databinding.ActivityMyOrderListBinding;

public class MyOrderListActivity extends BaseActivity {

    private ActivityMyOrderListBinding myOrderListBinding;
    private Context context;

    private MyOrderListAdapter myOrderListAdapter;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
      myOrderListBinding= DataBindingUtil.setContentView(this,R.layout.activity_my_order_list);
      context=this;


       /* myOrderListAdapter = new MyOrderListAdapter(context, arlOrder);
        myOrderListBinding.myOrderlistRecyclerview.setHasFixedSize(true);
        myOrderListBinding.myOrderlistRecyclerview.setLayoutManager(new LinearLayoutManager(context));
        myOrderListBinding.myOrderlistRecyclerview.setAdapter(myOrderListAdapter);*/

      setToolbar();
      setListener();
    }

    @Override
    public void setToolbar() {
        myOrderListBinding.myoderToolbar.tvToolbarBarName.setText(R.string.my_order_list);
    }

    @Override
    public void setListener() {
        myOrderListBinding.myoderToolbar.custToolBckBtn.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){

            case R.id.cust_tool_bck_btn:{
                finish();
                break;
            }
        }
    }
}
