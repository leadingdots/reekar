package com.reekar.activity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;

import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.akanksha.commonclassutil.LogUtil;
import com.akanksha.commonclassutil.PreferencesManager;
import com.akanksha.commonclassutil.ToastUtil;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.reekar.R;
import com.reekar.adapter.CarRentingFeatureAdapter;
import com.reekar.apiservices.ApiUtils;
import com.reekar.commonclasses.AppConstant;
import com.reekar.commonclasses.BaseActivity;
import com.reekar.commonclasses.CommonUtils;
import com.reekar.commonclasses.ErrorUtils;
import com.reekar.databinding.ActivityOptionsFeatureBinding;
import com.reekar.interfaces.ApiResponseInterface;
import com.reekar.interfaces.RecyclerviewItemClickInterface;
import com.reekar.models.ApiResponse;
import com.reekar.models.CarModel;
import com.reekar.models.CurrencyDataModel;
import com.reekar.models.ErrorModel;
import com.reekar.models.FeatureModel;
import com.reekar.models.SaveCarInfoApiRequest;
import com.reekar.translation.ChangeLanguage;

import java.io.IOException;
import java.io.InputStream;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import retrofit2.Response;

public class OptionsFeatureActivity extends BaseActivity
{

    private ActivityOptionsFeatureBinding optionsFeatureBinding;
    private Context context;

    String[] spinerdata = { "USD", "EURO"};
    private List<FeatureModel> featurelist = new ArrayList<>();
    private String[] stringArray;
    private String carId;
    private String pricePerDay;
    private String totalPrice;
    private String securityPrice;

    private ApiResponse apiResponse = new ApiResponse();
    private int ac = 0;
    private int nav = 0;
    private int ipod = 0;
    private int sunroof = 0;
    private int childSeat = 0;
    private int electricWindow = 0;
    private int heatedSeat = 0;
    private int panormaRoof = 0;
    private int gauge = 0;
    private int abs = 0;
    private int tractionControl = 0;
    private int audioControl = 0;
    private int cruiseControl = 0;

    private String yearOfManufacture;
    private int brandId;
    private int modelId;
    private int fuelTypeId;
    private int transmissionId;
    private int colorId;
    private int vehicleTypeId;
    private int seatId;

    private int isLicensed;
    private String mileage;
    private String chassisNumber;
    private CarRentingFeatureAdapter carRentingFeatureAdapter;

    private Boolean forEdit;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        optionsFeatureBinding= DataBindingUtil.setContentView(this,R.layout.activity_options_feature);
        context=this;
        if (PreferencesManager.Companion.getStringPreferences(context,AppConstant.CURRENT_CURRENCY)!=null) {
            optionsFeatureBinding.pricePerDay.setHint(getString(R.string.price_per_day) + " " +"("+PreferencesManager.Companion.getStringPreferences(context, AppConstant.CURRENT_CURRENCY)+")");
            optionsFeatureBinding.tvSecurityAmt.setHint(getString(R.string.security_amount) + " "+"("+PreferencesManager.Companion.getStringPreferences(context, AppConstant.CURRENT_CURRENCY)+")");
        }
        else {
            optionsFeatureBinding.pricePerDay.setHint(getString(R.string.price_per_day) + " " +"(AED)");
            optionsFeatureBinding.tvSecurityAmt.setHint(getString(R.string.security_amount) + " " +"(AED)");
        }
//        featurelist= Arrays.asList(getResources().getStringArray(R.array.options_feature));

        /* -------Add string array ------------*/
        if (getResources().getStringArray(R.array.options_feature)!=null)
            stringArray=getResources().getStringArray(R.array.options_feature);

        List<String> arlFeatureKey = new ArrayList<>(Arrays.asList(AppConstant.FEATURES_KEY));

        int i=0;
        for (String s : stringArray){
            FeatureModel featureModel=new FeatureModel();
            featureModel.setName(s);
            featureModel.setKey(arlFeatureKey.get(i));
            featurelist.add(featureModel);
            i++;
        }
        /* for (int i = 0; i < stringArray.length; i++) {
            FeatureModel featureModel =new FeatureModel(stringArray[i]);
            featurelist.add(featureModel);
        }*/


        getIntentData();
//        openActivity();
        spinner();
        setAdapter(optionsFeatureBinding.recyclerview);
        setToolbar();
        setListener();
        setData();
    }

    private void spinner() {
        ArrayAdapter spinerArray = new ArrayAdapter(context,R.layout.custom_spiner_text,spinerdata);
        spinerArray.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        optionsFeatureBinding.spinner.setAdapter(spinerArray);
        optionsFeatureBinding.spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
    }

    private void setAdapter(RecyclerView carFeaturesRecyclerview) {
        carRentingFeatureAdapter =new CarRentingFeatureAdapter(context, featurelist, new RecyclerviewItemClickInterface() {
            @Override
            public void onItemClick(int position) {
//                pos=featurelist.get(position).getName();

            }
        });
        carFeaturesRecyclerview.setHasFixedSize(true);
        carFeaturesRecyclerview.setLayoutManager(new LinearLayoutManager(context));
        carFeaturesRecyclerview.setAdapter(carRentingFeatureAdapter);
    }
    @Override
    public void setToolbar() {
        optionsFeatureBinding.toolbar.tvToolbarBarName.setText(R.string.options_feature);
    }

    @Override
    public void setListener() {
        optionsFeatureBinding.toolbar.custToolBckBtn.setOnClickListener(this);
        optionsFeatureBinding.submit.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.cust_tool_bck_btn:{
                onBackPressed();
                break;
            }
            case R.id.submit:{
                validation();
//                Intent intent=new Intent(context,CarPhotosActivity.class);
//                startActivity(intent);
                break;
            }
            case R.id.spinner:{
                optionsFeatureBinding.spinner.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                    @Override
                    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                    }
                });
                break;
            }
        }
    }



    private void validation(){
        pricePerDay =optionsFeatureBinding.pricePerDay.getText().toString();
        totalPrice=optionsFeatureBinding.totalPrice.getText().toString();
        securityPrice=optionsFeatureBinding.tvSecurityAmt.getText().toString();

        if (pricePerDay.isEmpty()){
            optionsFeatureBinding.pricePerDay.requestFocus();
            optionsFeatureBinding.pricePerDay.setError(getString(R.string.enter_price_per_day));
        }
//        else if(totalPrice.isEmpty()) {
//            optionsFeatureBinding.totalPrice.requestFocus();
//            optionsFeatureBinding.totalPrice.setError(getString(R.string.enter_total_price));
//        }
        else if(securityPrice.isEmpty()) {
            optionsFeatureBinding.tvSecurityAmt.requestFocus();
            optionsFeatureBinding.tvSecurityAmt.setError(getString(R.string.enter_security_amount));
        }
        else {
            setFeatureData();
            if (CommonUtils.isNetworkAvailable(context))
            callApiSaveCarInfo();
            else
                ToastUtil.Companion.showShortToast(context,getString(R.string.failed_internet));
        }

    }

    private void getIntentData(){

        Intent intent =getIntent();
        if (intent!=null){
//            if (intent.getIntExtra(AppConstant.BRAND_ID)!=null)
            brandId=intent.getIntExtra(AppConstant.BRAND_ID,0);
//            if (intent.getIntExtra(AppConstant.CAR_MODEL_ID)!=null)
            modelId=intent.getIntExtra(AppConstant.CAR_MODEL_ID,0);
//            if (intent.getIntExtra(AppConstant.CAR_MFG_YEAR)!=null)
            yearOfManufacture=intent.getStringExtra(AppConstant.CAR_MFG_YEAR);
//            if (intent.getIntExtra(AppConstant.CAR_TRANSMISSION_TYPE_ID)!=null)
            fuelTypeId=intent.getIntExtra(AppConstant.CAR_FUEL_TYPE_ID,0);

            transmissionId=intent.getIntExtra(AppConstant.CAR_TRANSMISSION_TYPE_ID,0);

            colorId=intent.getIntExtra(AppConstant.CAR_COLOR_ID,0);

            vehicleTypeId=intent.getIntExtra(AppConstant.VEHICLE_TYPE_ID,0);

            seatId=intent.getIntExtra(AppConstant.SEAT_ID,0);

            if (intent.getStringExtra(AppConstant.MILEAGE)!=null)
            mileage=intent.getStringExtra(AppConstant.MILEAGE);
            if (intent.getStringExtra(AppConstant.CHASSIS_NUMBER)!=null)
            chassisNumber=intent.getStringExtra(AppConstant.CHASSIS_NUMBER);

            isLicensed=intent.getIntExtra(AppConstant.IS_LICENSED,0);

            if (getIntent().getStringExtra(AppConstant.DATA) != null){
                Type type = new TypeToken<ApiResponse>() {
                }.getType();

                apiResponse = new Gson().fromJson(getIntent().getStringExtra(AppConstant.DATA),type);
            }

            if (getIntent().getStringExtra(AppConstant.CAR_ID) != null)
                carId =getIntent().getStringExtra(AppConstant.CAR_ID);

        }
    }

private void setFeatureData(){
    for (FeatureModel feature : featurelist){
        if (feature!=null){
            if (feature.isChecked())
            switch (feature.getName()){
                case "Air Condition (AC)":
                    ac = 1;
                    break;
                case "Navigation Screen (GPS)":
                    nav = 1;
                    break;
                case "Ipod Interface":
                    ipod = 1;
                    break;
                case "Sunroof":
                    sunroof = 1;
                    break;
                case "Child Seat":
                    childSeat = 1;
                    break;
                case "Electric Windows":
                    electricWindow = 1;
                    break;
                case "Heated Seats":
                    heatedSeat = 1;
                    break;
                case "Panorma Roof":
                    panormaRoof = 1;
                    break;
                case "PRM Gauge":
                    gauge = 1;
                    break;
                case "ABS":
                    abs = 1;
                    break;
                case "Traction Control":
                    tractionControl = 1;
                    break;
                case "Audio System":
                    audioControl = 1;
                    break;
                case "Cruise Control":
                    cruiseControl = 1;
                    break;
            }
        }
    }
}
    private void callApiSaveCarInfo(){
        CommonUtils.showProgressDialog(context);
        SaveCarInfoApiRequest apiRequest =new SaveCarInfoApiRequest();
        apiRequest.setCarId(carId);
        apiRequest.setBrandId(brandId);
        apiRequest.setModelId(modelId);
        apiRequest.setEngineTypeId(fuelTypeId);
        apiRequest.setTransmissionId(transmissionId);
        apiRequest.setYear(yearOfManufacture);

        apiRequest.setColorId(colorId);
        apiRequest.setVehicleTypeId(vehicleTypeId);
        apiRequest.setSeatId(seatId);
        apiRequest.setAc(ac);
        apiRequest.setGps(nav);
        apiRequest.setIpodInterface(ipod);
        apiRequest.setSunroof(sunroof);
        apiRequest.setChildSeat(childSeat);
        apiRequest.setElectricWindows(electricWindow);
        apiRequest.setHeatedSeat(heatedSeat);
        apiRequest.setPanormaRoof(panormaRoof);
        apiRequest.setPrmGauge(gauge);
        apiRequest.setAbs(abs);
        apiRequest.setTractionControl(tractionControl);
        apiRequest.setAudioSystem(audioControl);

        apiRequest.setCruiseControl(cruiseControl);
        apiRequest.setMileage(mileage);
        apiRequest.setChassisNumber(chassisNumber);
        apiRequest.setLicensed(isLicensed);

//        apiRequest.setCurrency(optionsFeatureBinding.spinner.getSelectedItem().toString());
        apiRequest.setPricePerDay(optionsFeatureBinding.pricePerDay.getText().toString());
//        apiRequest.setTotalPrice(optionsFeatureBinding.totalPrice.getText().toString());
        apiRequest.setSecurityAmount(optionsFeatureBinding.tvSecurityAmt.getText().toString());

        ApiUtils.callSaveCarInfoApi(context, apiRequest, new ApiResponseInterface() {
            @Override
            public void onResponse(Response<ApiResponse> response) {
                CommonUtils.hideProgressDialog();
                if (response.code()== AppConstant.SUCCESS_CODE){
                    if (response.body()!=null){
                        if (response.body().getCarId()!=null)
                            carId=response.body().getCarId();
                        ToastUtil.Companion.showShortToast(context,response.body().getMessage());
                        Intent intent=new Intent(context,CarPhotosActivity.class);
                        intent.putExtra(AppConstant.CAR_ID,carId);
                        intent.putExtra(AppConstant.DATA,new Gson().toJson(apiResponse));
                        startActivity(intent);
                    }
                }
                else if (response.code()==AppConstant.UNAUTHORIZED_CODE){
                    CommonUtils.navigateToLogin(context);
                }
                else if (response.code()==AppConstant.VALIDATION_ERROR_CODE){
                    errorParse(response);
                }
                else {
                    CommonUtils.showToast(context,getString(R.string.something_went_wrong));
                }

            }

            @Override
            public void onFailure() {
                CommonUtils.hideProgressDialog();
                ToastUtil.Companion.showShortToast(context,getString(R.string.something_went_wrong));

            }
        });
    }
    private void errorParse(Response<ApiResponse> response) {
        if (response.errorBody()!=null){
            try {
                ApiResponse error = ErrorUtils.parseError(response, context);
                if (error!=null && error.getError()!=null){
                    showError(error.getError());
                }
            }
            catch (Exception e){

            }
        }
    }
    private void showError(ErrorModel error) {
        if (error.getBrandId()!=null){
            ToastUtil.Companion.showShortToast(context,error.getBrandId().get(0));
        }
        else if (error.getModelId()!=null){
            ToastUtil.Companion.showShortToast(context,error.getModelId().get(0));
        }
        else if (error.getVehicleTypeId()!=null){
            ToastUtil.Companion.showShortToast(context,error.getVehicleTypeId().get(0));
        }
        else if (error.getEngineTypeId()!=null){
            ToastUtil.Companion.showShortToast(context,error.getEngineTypeId().get(0));
        }else if (error.getTransmissionId()!=null){
            ToastUtil.Companion.showShortToast(context,error.getTransmissionId().get(0));
        }else if (error.getColorId()!=null){
            ToastUtil.Companion.showShortToast(context,error.getColorId().get(0));
        }else if (error.getSeatId()!=null){
            ToastUtil.Companion.showShortToast(context,error.getSeatId().get(0));
        }else if (error.getYear()!=null){
            ToastUtil.Companion.showShortToast(context,error.getYear().get(0));
        }else if (error.getPricePerDay()!=null){
            ToastUtil.Companion.showShortToast(context,error.getPricePerDay().get(0));
        }else if (error.getAc()!=null){
            ToastUtil.Companion.showShortToast(context,error.getAc().get(0));
        }else if (error.getGps()!=null){
            ToastUtil.Companion.showShortToast(context,error.getGps().get(0));
        }else if (error.getIpodInterface()!=null){
            ToastUtil.Companion.showShortToast(context,error.getIpodInterface().get(0));
        }else if (error.getSunroof()!=null){
            ToastUtil.Companion.showShortToast(context,error.getSunroof().get(0));
        }else if (error.getChildSeat()!=null){
            ToastUtil.Companion.showShortToast(context,error.getChildSeat().get(0));
        }else if (error.getElectricWindows()!=null){
            ToastUtil.Companion.showShortToast(context,error.getElectricWindows().get(0));
        }else if (error.getHeatedSeat()!=null){
            ToastUtil.Companion.showShortToast(context,error.getHeatedSeat().get(0));
        }else if (error.getPanormaRoof()!=null){
            ToastUtil.Companion.showShortToast(context,error.getPanormaRoof().get(0));
        }else if (error.getPrmGauge()!=null){
            ToastUtil.Companion.showShortToast(context,error.getPrmGauge().get(0));
        }else if (error.getAbs()!=null){
            ToastUtil.Companion.showShortToast(context,error.getAbs().get(0));
        }else if (error.getTractionControl()!=null){
            ToastUtil.Companion.showShortToast(context,error.getTractionControl().get(0));
        }else if (error.getAudioSystem()!=null){
            ToastUtil.Companion.showShortToast(context,error.getAudioSystem().get(0));
        }else if (error.getLicensed()!=null){
            ToastUtil.Companion.showShortToast(context,error.getLicensed().get(0));
        }else if (error.getCruiseControl()!=null){
            ToastUtil.Companion.showShortToast(context,error.getCruiseControl().get(0));
        }


    }

    private void setData(){
        if (apiResponse != null){
            if (apiResponse.getCar() != null){
                CarModel cars = apiResponse.getCar();
                if (cars.getPricePerDay() != null) {
                    optionsFeatureBinding.pricePerDay.setText(String.valueOf(cars.getPricePerDay()));
                }

                if (cars.getTotalPrice() != null)
                    optionsFeatureBinding.totalPrice.setText(String.valueOf(cars.getTotalPrice()));

                if (cars.getSecurityAmount() != null)
                    optionsFeatureBinding.tvSecurityAmt.setText(String.valueOf(cars.getSecurityAmount()));

                for (FeatureModel model :featurelist){
                    for (String features : cars.getFeatures()){
                        if (model.getKey().equals(features.toLowerCase())){
                            model.setChecked(true);
                        }
                    }
                }

                carRentingFeatureAdapter.notifyDataSetChanged();
            }
        }
    }

    @Override
    public void onBackPressed() {
        Intent intent =new Intent();
        intent.putExtra(AppConstant.CAR_ID,carId);
        setResult(RESULT_OK,intent);
        finish();
    }
}
