package com.reekar.activity;

import android.content.Context;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;

import androidx.databinding.DataBindingUtil;

import com.akanksha.commonclassutil.PreferencesManager;
import com.reekar.R;
import com.reekar.commonclasses.AppConstant;
import com.reekar.commonclasses.BaseActivity;
import com.reekar.databinding.ActivityLanguageBinding;
import com.reekar.translation.ChangeLanguage;
import com.reekar.translation.Language;

public class LanguageActivity extends BaseActivity {
    private Context context;
    private ActivityLanguageBinding binding;
    private String language = Language.ENGLISH;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this,R.layout.activity_language);
        context = this;


        getLastSelectedLanguage();
        setListener();

    }

    private void getLastSelectedLanguage() {
        String currentLanguage = PreferencesManager.Companion.getStringPreferences(context, AppConstant.CURRENT_LANGUAGE);
        language = currentLanguage;
        switch (currentLanguage){
            case Language.ENGLISH:
                showView(binding.ivEnglish);
                break;

            case Language.ARABIC:
                showView(binding.ivArabic);
                break;
        }
    }

    @Override
    public void setToolbar() {

    }

    @Override
    public void setListener() {
        binding.btnSubmit.setOnClickListener(this);
        binding.llEnglish.setOnClickListener(this);
        binding.llArabic.setOnClickListener(this);
    }



    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.btn_submit:
                changeLanguage(language);
                break;
            case R.id.ll_english:
                language = Language.ENGLISH;
                showView(binding.ivEnglish);
                break;
            case R.id.ll_arabic:
                language = Language.ARABIC;
                showView(binding.ivArabic);
                break;
        }
    }

    private void showView(ImageView imageView) {
        binding.ivArabic.setVisibility(View.INVISIBLE);
        binding.ivEnglish.setVisibility(View.INVISIBLE);

        imageView.setVisibility(View.VISIBLE);
    }

    private void changeLanguage(String language){
        ChangeLanguage.setLocale(context,language);
    }

}
