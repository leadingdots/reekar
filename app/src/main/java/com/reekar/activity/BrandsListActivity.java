package com.reekar.activity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;

import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.LinearLayoutManager;

import com.akanksha.commonclassutil.LogUtil;
import com.akanksha.commonclassutil.ToastUtil;
import com.google.gson.Gson;
import com.reekar.R;
import com.reekar.adapter.BrandsAdapter;
import com.reekar.apiservices.ApiUtils;
import com.reekar.commonclasses.AppConstant;
import com.reekar.commonclasses.BaseActivity;
import com.reekar.commonclasses.CommonUtils;
import com.reekar.commonclasses.TimeUtil;
import com.reekar.databinding.ActivityBrandsListBinding;
import com.reekar.interfaces.ApiResponseInterface;
import com.reekar.interfaces.RecyclerviewItemClickInterface;
import com.reekar.models.ApiResponse;
import com.reekar.models.DataModel;
import com.reekar.models.FilterModel;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Response;

public class BrandsListActivity extends BaseActivity {

    private ActivityBrandsListBinding brandsListBinding;
    private Context context;

    private BrandsAdapter brandsAdapter;
    private ArrayList<FilterModel> arlFilter = new ArrayList<>();
    private ArrayList<DataModel> arlData = new ArrayList<>();
    ArrayList<DataModel> filteredList = new ArrayList<>();;


    private String year;

    private String from;
    private int brandId;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        context=this;
        brandsListBinding = DataBindingUtil.setContentView(this, R.layout.activity_brands_list);

        getIntentdata();
        loadList();
        setBrandsAdapter();
        if (CommonUtils.isNetworkAvailable(context))
        callApiMasterData();
        else
            ToastUtil.Companion.showShortToast(context,getString(R.string.failed_internet));

        setToolbar();
        setListener();
    }

    @Override
    public void setToolbar() {

    }
    private void setBrandsAdapter() {
        arlFilter.clear();
        arlData.clear();
        brandsAdapter = new BrandsAdapter(context, arlData, new RecyclerviewItemClickInterface() {
            @Override
            public void onItemClick(int position) {
                DataModel varr = arlData.get(position);
                Intent intent = new Intent();

//                intent.putParcelableArrayListExtra(AppConstant.BRAND_ID, varr);

                if (from.equals(AppConstant.CAR_BRAND)) {
                    intent.putExtra(AppConstant.BRAND, filteredList.get(position).getName());
                    intent.putExtra(AppConstant.BRAND_ID, filteredList.get(position).getId());
                }
                if (from.equals(AppConstant.CAR_SEATS)) {
                    intent.putExtra(AppConstant.CAR_SEATS, filteredList.get(position).getName());
                    intent.putExtra(AppConstant.SEAT_ID, filteredList.get(position).getId());
                }
                if (from.equals(AppConstant.CAR_MODEL)) {
                    intent.putExtra(AppConstant.CAR_MODEL, filteredList.get(position).getName());
                    intent.putExtra(AppConstant.CAR_MODEL_ID, filteredList.get(position).getId());
                }
                if (from.equals(AppConstant.CAR_FUEL_TYPE)) {
                    intent.putExtra(AppConstant.CAR_FUEL_TYPE, filteredList.get(position).getName());
                    intent.putExtra(AppConstant.CAR_FUEL_TYPE_ID, filteredList.get(position).getId());
                }
                if (from.equals(AppConstant.CAR_TRANSMISSION_TYPE)) {
                    intent.putExtra(AppConstant.CAR_TRANSMISSION_TYPE, filteredList.get(position).getName());
                    intent.putExtra(AppConstant.CAR_TRANSMISSION_TYPE_ID, filteredList.get(position).getId());
                }
                if (from.equals(AppConstant.CAR_MFG_YEAR))
                    intent.putExtra(AppConstant.CAR_MFG_YEAR,filteredList.get(position).getName());

                if (from.equals(AppConstant.CAR_COLOR)) {
                    intent.putExtra(AppConstant.CAR_COLOR, filteredList.get(position).getName());
                    intent.putExtra(AppConstant.CAR_COLOR_ID, filteredList.get(position).getId());
                }
                if (from.equals(AppConstant.VEHICLE_TYPE)) {
                    intent.putExtra(AppConstant.VEHICLE_TYPE, filteredList.get(position).getName());
                    intent.putExtra(AppConstant.VEHICLE_TYPE_ID, filteredList.get(position).getId());
                }
                setResult(RESULT_OK, intent);
                finish();
            }
        });
        brandsListBinding.recyclerview.setHasFixedSize(true);
        brandsListBinding.recyclerview.setLayoutManager(new LinearLayoutManager(context));
        brandsListBinding.recyclerview.setAdapter(brandsAdapter);

    }
    private void getIntentdata() {
        Intent intent = getIntent();
        if (intent!= null) {
            if (intent.getStringExtra(AppConstant.FROM)!=null)
                from = intent.getStringExtra(AppConstant.FROM);
            brandId= intent.getIntExtra(AppConstant.BRAND_ID,0);
        }
    }
    @Override
    public void setListener() {
        brandsListBinding.toolbar.custToolBckBtn.setOnClickListener(this);
        brandsListBinding.etSearch.addTextChangedListener(new TextWatcher() {
            @Override
            public void afterTextChanged(Editable s) {
                // TODO Auto-generated method stub
            }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                // TODO Auto-generated method stub
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                filteredList.clear();
                filter(s.toString());
            }
        });

    }

    private void filter(String text) {


        for (DataModel item : arlData) {
            if (item.getName().toLowerCase().contains(text.toLowerCase())) {
                filteredList.add(item);
            }
        }
        brandsAdapter.filterList(filteredList);
        if (filteredList.size()>0)
            brandsListBinding.llNoData.llNoDataParent.setVisibility(View.GONE);
        else
            brandsListBinding.llNoData.llNoDataParent.setVisibility(View.VISIBLE);

    }


    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.cust_tool_bck_btn: {
                finish();
                break;
            }
        }
    }



    private void callApiMasterData() {
        CommonUtils.showProgressDialog(context);
        ApiUtils.callApiMasterData(context, new ApiResponseInterface() {
            @Override
            public void onResponse(Response<ApiResponse> response) {
                if (response.code()==AppConstant.SUCCESS_CODE)
                    arlFilter.clear();
                arlData.clear();
                filteredList.clear();
                if (response.body() != null && response.body().getFilter() != null && response.body().getFilter().size() >0){

                    arlFilter.addAll(response.body().getFilter());
                    addFilterData("Year", TimeUtil.getYearData()); // added to master list

                    for (FilterModel model : arlFilter) { // masterlist for loop
                        if (model.getData() != null && model.getData().size() > 0) {
                            if (from.equals(model.getName())) {
                                arlData.addAll(model.getData());

                                LogUtil.Companion.errorLog("arlData",new Gson().toJson(arlData));
                            }

                            /*-- this is for bring model according to brandId --- */
                            List<DataModel> arlModel= new ArrayList<>();
                            if (from.equals(AppConstant.CAR_MODEL)){
                                for (DataModel dataModel : arlData){

//                                     LogUtil.Companion.errorLog("brandId", String.valueOf(brandId));
                                    LogUtil.Companion.errorLog("dataModel.getBrandId()", String.valueOf(dataModel.getBrandId()));

                                    if (brandId == dataModel.getBrandId()) {
                                        arlModel.add(dataModel);
                                        LogUtil.Companion.errorLog("arlModel",new Gson().toJson(arlModel));
                                    }
                                }
                                if (arlModel.size()>0) {
                                    arlData.clear();
                                    arlData.addAll(arlModel);
                                }
                            }


                        }
                    }
                }

                if (arlData.size()>0)
                    brandsListBinding.llNoData.llNoDataParent.setVisibility(View.GONE);
                else
                    brandsListBinding.llNoData.llNoDataParent.setVisibility(View.VISIBLE);

                filteredList.addAll(arlData);
                brandsAdapter.notifyDataSetChanged();

                CommonUtils.hideProgressDialog();
            }

            @Override
            public void onFailure() {
                CommonUtils.hideProgressDialog();
            }
        });
    }

    private void addFilterData(String features, List<Integer> stringArray) {
        FilterModel model = new FilterModel();
        model.setName(features);

//        String[] features = getResources().getStringArray(R.array.options_feature);
        List<DataModel> arlFeatures = new ArrayList<>();
        for (int data : stringArray){
            DataModel dataModel = new DataModel();
            dataModel.setName(String.valueOf(data));

            arlFeatures.add(dataModel);
        }
        model.setData(arlFeatures);

        arlFilter.add(model);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }


    private void loadList(){

        if (from!=null){

            if (from.equals(AppConstant.CAR_BRAND)){
                brandsListBinding.tvTitleName.setText(getString(R.string.brand));
            }
            if (from.equals(AppConstant.CAR_MODEL)){
                brandsListBinding.tvTitleName.setText(getString(R.string.model));
            }
            if (from.equals(AppConstant.CAR_SEATS)){
                brandsListBinding.tvTitleName.setText(getString(R.string.seats));
            }
            if (from.equals(AppConstant.CAR_FUEL_TYPE)){
                brandsListBinding.tvTitleName.setText(getString(R.string.fuel_type));
            }
            if (from.equals(AppConstant.CAR_TRANSMISSION_TYPE)){
                brandsListBinding.tvTitleName.setText(getString(R.string.transmission_type));
            }
            if (from.equals(AppConstant.CAR_MFG_YEAR)){
                brandsListBinding.tvTitleName.setText(getString(R.string.year_of_manufacture));
            }
            if (from.equals(AppConstant.CAR_COLOR)){
                brandsListBinding.tvTitleName.setText(getString(R.string.vehicle_color));
            }
            if (from.equals(AppConstant.VEHICLE_TYPE)){
                brandsListBinding.tvTitleName.setText(getString(R.string.vehicle_type));
            }


        }
    }

}

