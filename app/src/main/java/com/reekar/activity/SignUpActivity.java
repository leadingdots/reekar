package com.reekar.activity;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.FragmentActivity;

import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.text.Editable;
import android.text.Html;
import android.text.SpannableString;
import android.text.TextPaint;
import android.text.TextWatcher;
import android.text.method.HideReturnsTransformationMethod;
import android.text.method.LinkMovementMethod;
import android.text.method.PasswordTransformationMethod;
import android.text.style.ClickableSpan;
import android.text.style.ForegroundColorSpan;
import android.text.style.URLSpan;
import android.text.style.UnderlineSpan;
import android.text.util.Linkify;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;

import android.view.inputmethod.EditorInfo;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.akanksha.commonclassutil.CheckValidation;
import com.akanksha.commonclassutil.PreferencesManager;
import com.akanksha.commonclassutil.ToastUtil;
import com.facebook.AccessToken;
import com.facebook.AccessTokenTracker;
import com.facebook.CallbackManager;

import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.FacebookSdk;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.facebook.Profile;
import com.facebook.ProfileTracker;
import com.facebook.appevents.AppEventsLogger;
import com.facebook.login.LoginManager;
import com.facebook.login.LoginResult;
import com.facebook.login.widget.LoginButton;
import com.google.android.gms.auth.api.Auth;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.auth.api.signin.GoogleSignInResult;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.hbb20.CountryCodePicker;
import com.reekar.R;
import com.reekar.apiservices.APIInterface;
import com.reekar.apiservices.ApiUtils;
import com.reekar.commonclasses.AppConstant;
import com.reekar.commonclasses.BaseActivity;
import com.reekar.commonclasses.CommonUtils;
import com.reekar.commonclasses.ErrorUtils;
import com.reekar.databinding.ActivitySignUpBinding;
import com.reekar.interfaces.ApiResponseInterface;
import com.reekar.models.ApiRequest;
import com.reekar.models.ApiResponse;
import com.reekar.models.ErrorModel;
import com.reekar.translation.ChangeLanguage;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.Arrays;

import retrofit2.Response;


public class SignUpActivity extends BaseActivity implements GoogleApiClient.OnConnectionFailedListener, View.OnFocusChangeListener {

    private Context context;
    private APIInterface mApiInterface;
    private String action ="wcfmvm_email_verification_code";
    private ActivitySignUpBinding signUpBinding;

    private Boolean passVisible=false;
    private GoogleApiClient googleApiClient;
    private static final int RC_SIGN_IN = 1;
    private static final int FB_SIGN_IN = 2;

    private Boolean emailAvailable = true;
    /*
     data sending through intent
     */
    private String fname;
    private String lname;
    private String email;
    private String accescode;
    private String ccode;
    private String mnumber;
    private String mNumber;
    private String cccode;
    private int hasMobile;

    private String pass;
    private String cpass;

    private EditText input;
    private CountryCodePicker ccp;

    private String onlineImage;
    private static final String EMAIL = "email";
    LinearLayout loginButton;
    private CallbackManager callbackManager;
//    private LoginButton loginButton;
    private AccessTokenTracker accessTokenTracker;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        context = this;
        signUpBinding = DataBindingUtil.setContentView(this,R.layout.activity_sign_up);




        privacyPolicy();
        emailCheck();
        facebookLogin();
        signInGoogle();
        setListener();
    }

    private void privacyPolicy() {
        SpannableString SpanString = new SpannableString(getString(R.string.terms_content));

        URLSpan teremsAndCondition = new URLSpan(getString(R.string.terms_content)) {
            @Override
            public void onClick(View textView) {
                Intent mIntent = new Intent(context, TermsConditionsActivity.class);
                mIntent.putExtra(AppConstant.FROM,AppConstant.TERMS );
                startActivity(mIntent);
            }

        };
        // Character starting from 31 - 47 is Terms and condition.
        // Character starting from 52 - 67 is privacy policy.
        URLSpan privacy = new URLSpan(getString(R.string.terms_content)) {
            @Override
            public void onClick(View textView) {
                Intent mIntent = new Intent(context, TermsConditionsActivity.class);
                mIntent.putExtra(AppConstant.FROM, AppConstant.POLICY);
                startActivity(mIntent);

            }
        };
        SpanString.setSpan(teremsAndCondition, 31, 47, 0);
        SpanString.setSpan(privacy, 52, 67, 0);
        SpanString.setSpan(new ForegroundColorSpan(Color.WHITE), 31, 47, 0);
        SpanString.setSpan(new ForegroundColorSpan(Color.WHITE), 52, 67, 0);
        SpanString.setSpan(new UnderlineSpan(), 31, 47, 0);
        SpanString.setSpan(new UnderlineSpan(), 52, 67, 0);

        signUpBinding.tvPrivacyPolicy.setMovementMethod(LinkMovementMethod.getInstance());
        signUpBinding.tvPrivacyPolicy.setText(SpanString, TextView.BufferType.SPANNABLE);
        signUpBinding.tvPrivacyPolicy.setSelected(true);

    }

    private void facebookLogin() {
        callbackManager = CallbackManager.Factory.create();
//        loginButton = findViewById(R.id.login_button);
        loginButton = findViewById(R.id.facebook_signin_btn);

//        loginButton.setReadPermissions(Arrays.asList("email", "public_profile"));

        LoginManager.getInstance().registerCallback(callbackManager, new FacebookCallback<LoginResult>() {
            String facebook_email;
            String name;
            String profilePicUrl;
            private Profile mProfile;
            @Override
            public void onSuccess(LoginResult loginResult) {
                AccessToken accessToken = loginResult.getAccessToken();
                useLoginInformation(accessToken);

            }
            @Override
            public void onCancel() {

            }
            @Override
            public void onError(FacebookException exception) {
                // App code
            }
        });

        // Defining the AccessTokenTracker
        accessTokenTracker = new AccessTokenTracker() {
            // This method is invoked everytime access token changes
            @Override
            protected void onCurrentAccessTokenChanged(AccessToken oldAccessToken, AccessToken currentAccessToken) {
                useLoginInformation(currentAccessToken);

            }
        };


    }
    private void useLoginInformation(AccessToken accessToken) {
        /**
         Creating the GraphRequest to fetch user details
           1st Param - AccessToken
           2nd Param - Callback (which will be invoked once the request is successful)
         **/
        GraphRequest request = GraphRequest.newMeRequest(accessToken, new GraphRequest.GraphJSONObjectCallback() {
            //On Completed is invoked once the GraphRequest is successful
            @Override
            public void onCompleted(JSONObject object, GraphResponse response) {
                try {
                    if (object!=null && response!=null) {
                        if (object.getString("name") != null)
                            fname = object.getString("name");
                        if (object.getString("email") != null)
                            email = object.getString("email");
                        if (object.getJSONObject("picture").getJSONObject("data").getString("url") != null)
                            onlineImage = object.getJSONObject("picture").getJSONObject("data").getString("url");
                   /* signUpBinding.firstName.setText(fname);
                    signUpBinding.emailEt.setText(email);*/
                        Log.e("fbimage", onlineImage);
                      if (CommonUtils.isNetworkAvailable(context))
                            callApiSocialLogin();
                        else
                            ToastUtil.Companion.showShortToast(context,getString(R.string.failed_internet));
                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        });
        // We set parameters to the GraphRequest using a Bundle.
        Bundle parameters = new Bundle();
        parameters.putString("fields", "id,name,email,picture.width(200)");
        request.setParameters(parameters);
        // Initiate the GraphRequest
        request.executeAsync();
    }

    private void alertBoxEditText() {
        AlertDialog.Builder alert = new AlertDialog.Builder(context);
        alert.setCancelable(false);
        LayoutInflater inflater = this.getLayoutInflater();
        View dialogView = inflater.inflate(R.layout.custom_dailog, null);

//        alert.setTitle("Add mobile number");
//        alert.setMessage("Please enter mobile number");

        // Set an EditText view to get user input
//        final EditText input = new EditText(getActivity());
        alert.setView(dialogView);

        input = (EditText) dialogView.findViewById(R.id.et_add_number);
        ccp = (CountryCodePicker) dialogView.findViewById(R.id.country_code_picker);
//        input.setText("");
        alert.setPositiveButton(getString(R.string.proceed), new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int whichButton) {
                // Do something with value!
                if (input.getText()!=null) {
                    cccode=ccp.getSelectedCountryCodeWithPlus().toString();
                    mNumber =input.getText().toString();
                    if (CommonUtils.isNetworkAvailable(context))
                        callApiSocialLogin();
                    else
                        ToastUtil.Companion.showShortToast(context,getString(R.string.failed_internet));

                }

            }
        });


        alert.show();
    }

    public void validation(){

        fname = signUpBinding.firstName.getText().toString();
        lname = signUpBinding.lastName.getText().toString();
        email = signUpBinding.emailEt.getText().toString();
        ccode = signUpBinding.countryCodePicker.getSelectedCountryCode();
        mnumber = signUpBinding.phoneNumber.getText().toString();
        pass = signUpBinding.passEt.getText().toString();
        cpass = signUpBinding.confrmPassEt.getText().toString();

        String emailPattern = "[a-zA-Z0-9._-]+@[a-z]+\\.+[a-z]+";

        if (fname.isEmpty()){
            signUpBinding.firstName.setError(getString(R.string.please_enter_first_name));
            signUpBinding.firstName.requestFocus();
        }
        else if (!CheckValidation.Companion.isValidName(fname)){
            signUpBinding.firstName.setError(getString(R.string.please_enter_valid_first_name));
            signUpBinding.firstName.requestFocus();
        }
/*        else if(lname.isEmpty()){
            signUpBinding.lastName.setError(getString(R.string.please_enter_last_name));
            signUpBinding.lastName.requestFocus();

        }
        else if(!CheckValidation.Companion.isValidName(lname)){
            signUpBinding.lastName.setError(getString(R.string.please_enter_valid_last_name));
            signUpBinding.lastName.requestFocus();

        }*/
        else if (email.isEmpty()){
            signUpBinding.emailEt.requestFocus();
            signUpBinding.emailEt.setError(getString(R.string.please_enter_email));
        }
        else if (!email.matches(emailPattern)){  // emailAvailable == false
            signUpBinding.emailEt.requestFocus();
            signUpBinding.emailEt.setError(getString(R.string.please_enter_correct_email));
/*            signUpBinding.checkEmail.setVisibility(View.VISIBLE);
            signUpBinding.checkEmail.setTextColor(getResources().getColor(R.color.red));
            signUpBinding.checkEmail.setText("Unavailable");*/

        }
        else if (mnumber.isEmpty()){
            signUpBinding.phoneNumber.setError(getString(R.string.please_enter_phonenumber));
            signUpBinding.phoneNumber.requestFocus();

        }
        else if (pass.isEmpty()){
            signUpBinding.passEt.setError(getString(R.string.please_enter_pass));
            signUpBinding.passEt.requestFocus();
        }
        else if (pass.length()<8){
            signUpBinding.passEt.setError(getString(R.string.please_enter_correct_pass2));
            signUpBinding.passEt.requestFocus();
        }
        else if (cpass.isEmpty()){
            signUpBinding.confrmPassEt.setError(getString(R.string.please_enter_confirm_pass));
            signUpBinding.confrmPassEt.requestFocus();

        }
        else if (!cpass.equals(pass)){
            signUpBinding.confrmPassEt.setError(getString(R.string.passwrd_doent_match));
            signUpBinding.confrmPassEt.requestFocus();

        }
        else {
/*        else if (email.matches(emailPattern)){
            callApiEmailCheck();

            if (emailAvailable){*/
           if (CommonUtils.isNetworkAvailable(context)){
               callRegisterApi();
           }
           else
               ToastUtil.Companion.showShortToast(context,getString(R.string.failed_internet));

//            }
        }

    }
    public void emailCheck(){
            signUpBinding.emailEt.setOnEditorActionListener(new EditText.OnEditorActionListener() {
                @Override
                public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {

                    email = signUpBinding.emailEt.getText().toString();
                    String emailPattern = "[a-zA-Z0-9._-]+@[a-z]+\\.+[a-z]+";

                    if (actionId == EditorInfo.IME_ACTION_NEXT) {
                        //do here your stuff
                        if (!email.matches(emailPattern)) {
                            signUpBinding.emailEt.setError(getString(R.string.please_enter_correct_email));
                            signUpBinding.emailEt.requestFocus();
                            signUpBinding.checkEmail.setVisibility(View.INVISIBLE);
                            emailAvailable = false;
                        }
                        else {
                            callApiEmailCheck();
                        }

                    }
                    return false;
                }
            });
    }

    @Override
    public void setToolbar() {

    }

    @Override
    public void setListener() {

        signUpBinding.backBtn.setOnClickListener(this);
        signUpBinding.nextBtn.setOnClickListener(this);

        signUpBinding.clearTextFirst.setOnClickListener(this);
        signUpBinding.clearTextLast.setOnClickListener(this);
        signUpBinding.clearTextEmail.setOnClickListener(this);
        signUpBinding.clearTextPhoneNumber.setOnClickListener(this);
        signUpBinding.clearTextPassword.setOnClickListener(this);
        signUpBinding.clearTextConfirmPassword.setOnClickListener(this);

        signUpBinding.passShow.setOnClickListener(this);
        signUpBinding.cnfrmPassShow.setOnClickListener(this);
        signUpBinding.phoneNumber.setOnFocusChangeListener(this);
        signUpBinding.googleSignInButton.setOnClickListener(this);
        signUpBinding.facebookSigninBtn.setOnClickListener(this);

        signUpBinding.tvPrivacyPolicy.setOnClickListener(this);
        signUpBinding.emailEt.addTextChangedListener(new TextWatcher() {
            @Override
            public void afterTextChanged(Editable s) {
                // TODO Auto-generated method stub
            }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                // TODO Auto-generated method stub
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                signUpBinding.checkEmail.setVisibility(View.INVISIBLE);
            }
        });

        signUpBinding.countryPicker.setOnCountryChangeListener(new CountryCodePicker.OnCountryChangeListener() {
            @Override
            public void onCountrySelected() {
                String nm =signUpBinding.countryPicker.getSelectedCountryCode();
                signUpBinding.countryCodePicker.setCountryForPhoneCode(Integer.parseInt(nm));
            }
        });

    }
    @Override
    public void onFocusChange(View view, boolean hasfocus) {
        switch(view.getId()){
            case R.id.phone_number:
                callApiEmailCheck();
                emailCheck();
                break;
        }
    }

    public void onDestroy() {
        super.onDestroy();
        LoginManager.getInstance().logOut();
        // We stop the tracking before destroying the activity
        accessTokenTracker.stopTracking();
    }

    @Override
    public void onClick(View v) {

        switch (v.getId()){

            case R.id.back_btn:{
                finish();
                break;
            }
            case R.id.next_btn:{
                validation();
                break;
            }
            case R.id.tv_privacy_policy:{
//

                break;
            }

            case R.id.clear_text_first:{
                signUpBinding.firstName.getText().clear();
                break;
            }
            case R.id.clear_text_last:{
                signUpBinding.lastName.getText().clear();
                break;
            }
            case R.id.clear_text_email:{
                signUpBinding.emailEt.getText().clear();
                signUpBinding.checkEmail.setVisibility(View.INVISIBLE);
                break;
            }
            case R.id.clear_text_phone_number:{
                signUpBinding.phoneNumber.getText().clear();

                break;
            }
            case R.id.clear_text_password:{
                signUpBinding.passEt.getText().clear();

                break;
            }
            case R.id.clear_text_confirm_password:{
                signUpBinding.confrmPassEt.getText().clear();
                break;
            }
            case R.id.pass_show:{

                if (passVisible == false){
                    signUpBinding.passEt.setTransformationMethod(HideReturnsTransformationMethod.getInstance());
                    signUpBinding.passEt.setSelection(signUpBinding.passEt.getText().length());
                    passVisible=true;
                }
                else{
                    signUpBinding.passEt.setTransformationMethod(PasswordTransformationMethod.getInstance());
                    signUpBinding.passEt.setSelection(signUpBinding.passEt.getText().length());
                    passVisible=false;
                }
                break;
            }
            case R.id.cnfrm_pass_show:{
                if (passVisible == false){
                    signUpBinding.confrmPassEt.setTransformationMethod(HideReturnsTransformationMethod.getInstance());
                    signUpBinding.confrmPassEt.setSelection(signUpBinding.confrmPassEt.getText().length());
                    passVisible=true;
                }
                else{
                    signUpBinding.confrmPassEt.setTransformationMethod(PasswordTransformationMethod.getInstance());
                    signUpBinding.confrmPassEt.setSelection(signUpBinding.confrmPassEt.getText().length());
                    passVisible=false;
                }
                break;
            }
            case R.id.google_sign_in_button:{
                Intent intent = Auth.GoogleSignInApi.getSignInIntent(googleApiClient);
                startActivityForResult(intent,RC_SIGN_IN);
                break;
            }
            case R.id.facebook_signin_btn:{
                LoginManager.getInstance().logInWithReadPermissions((Activity) context,Arrays.asList("email", "public_profile"));
                break;
            }
        }
    }


    private void callApiEmailCheck() {
        ApiRequest apiRequest = new ApiRequest();
        apiRequest.setEmail(signUpBinding.emailEt.getText().toString());

        ApiUtils.callApiEmailCheck(context, apiRequest, new ApiResponseInterface() {
            @Override
            public void onResponse(Response<ApiResponse> response) {
                if (response.code()==AppConstant.SUCCESS_CODE){
                    String msg = response.body().getMessage();
                    signUpBinding.checkEmail.setText(ChangeLanguage.translate(context,msg));
                    signUpBinding.checkEmail.setVisibility(View.VISIBLE);
                    signUpBinding.checkEmail.setTextColor(getResources().getColor(R.color.green));
                    emailAvailable=true;
                }
                else if (response.code()==AppConstant.SUCCESS_CODE_201){
                    emailAvailable=false;
                    signUpBinding.checkEmail.setVisibility(View.VISIBLE);
                    signUpBinding.checkEmail.setText(ChangeLanguage.translate(context,response.body().getMessage()));
                    signUpBinding.checkEmail.setTextColor(getResources().getColor(R.color.colorPrimary));
                }
            }

            @Override
            public void onFailure() {
                ToastUtil.Companion.showShortToast(context,getString(R.string.something_went_wrong));
            }
        });
    }


    @Override
    public void onPointerCaptureChanged(boolean hasCapture) {
    }
    /*
    API method or Sign up
     */
    public void callRegisterApi(){
        CommonUtils.showProgressDialog(context);

        ApiRequest apiRequest =new ApiRequest();
        apiRequest.setFirst_name(signUpBinding.firstName.getText().toString().trim());
        apiRequest.setLast_name(signUpBinding.lastName.getText().toString().trim());
        apiRequest.setEmail(signUpBinding.emailEt.getText().toString().trim());
        apiRequest.setCountry_code(signUpBinding.countryCodePicker.getSelectedCountryCodeWithPlus().toString());
        apiRequest.setMobile(signUpBinding.phoneNumber.getText().toString().trim());
        apiRequest.setPassword(signUpBinding.passEt.getText().toString());
        apiRequest.setPassword_confirmation(signUpBinding.confrmPassEt.getText().toString());
        apiRequest.setDevice_type(AppConstant.DEVICE_TYPE);
        apiRequest.setMyip(CommonUtils.getMobileIPAddress());


        ApiUtils.callRegisterApi(context, apiRequest, new ApiResponseInterface() {
            @Override
            public void onResponse(Response<ApiResponse> response) {
                CommonUtils.hideProgressDialog();
                if (response.code()==AppConstant.SUCCESS_CODE){
                    saveData(response);
                    String msg = response.body().getMessage();
                    CommonUtils.showToast(context,msg);
//                    PreferencesManager.Companion.saveStringPreferences(context,AppConstant.FIRST_NAME,signUpBinding.firstName.getText().toString());
//                    PreferencesManager.Companion.saveStringPreferences(context,AppConstant.LAST_NAME,signUpBinding.firstName.getText().toString());
                    signUpBinding.checkEmail.setText(getString(R.string.available));
                    signUpBinding.checkEmail.setVisibility(View.VISIBLE);
                    Intent intent = new Intent(context, OtpActivity.class);
                    intent.putExtra(AppConstant.USER_EMAIL,email);
                    startActivity(intent);
                }
                else if (response.code()==AppConstant.VALIDATION_ERROR_CODE){
                    errorParse(response);
                }
                else if (response.code()==AppConstant.SUCCESS_CODE_201){
                    errorParse(response);
                }
                else {
                    CommonUtils.showToast(context,getString(R.string.something_went_wrong));
                }
            }

            @Override
            public void onFailure() {
                CommonUtils.hideProgressDialog();
                ToastUtil.Companion.showShortToast(context,getString(R.string.something_went_wrong));

            }
        });
    }

    /*
      --- google sign in --
    */
    public void signInGoogle(){
        GoogleSignInOptions gso =  new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestEmail()
                .build();
        googleApiClient=new GoogleApiClient.Builder(context)
                .enableAutoManage((FragmentActivity) context, this)
                .addApi(Auth.GOOGLE_SIGN_IN_API,gso)
                .build();
    }
    private void handleSignInResult(GoogleSignInResult result){
        if(result.isSuccess()){
//            gotoProfile();
            if (result.getSignInAccount()!=null) {
                fname=result.getSignInAccount().getDisplayName();
                email= result.getSignInAccount().getEmail();

                if (result.getSignInAccount().getPhotoUrl()!=null){
                    onlineImage = result.getSignInAccount().getPhotoUrl().toString();
                    Log.e("gimage",onlineImage);
                }
               if (CommonUtils.isNetworkAvailable(context))
                callApiSocialLogin();
                else
                    ToastUtil.Companion.showShortToast(context,getString(R.string.failed_internet));
            }
//      PreferencesManager.Companion.saveBooleanPreferences(getActivity(), AppConstant.LOGINCHECK,true);
        }else{
            CommonUtils.showToast(context,"Sign in cancel");
        }

    }
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        callbackManager.onActivityResult(requestCode, resultCode, data);
        if(requestCode==RC_SIGN_IN){
            GoogleSignInResult result = Auth.GoogleSignInApi.getSignInResultFromIntent(data);
            handleSignInResult(result);
        }

    }
    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {
        CommonUtils.showToast(context,"Something went wrong");

    }

    private void callApiSocialLogin() {
        CommonUtils.showProgressDialog(context);
        ApiRequest apiRequest = new ApiRequest();
        apiRequest.setImage(onlineImage);
        apiRequest.setName(fname);
        apiRequest.setEmail(email);
        apiRequest.setCountry_code(cccode);
        apiRequest.setMobile(mNumber);
        apiRequest.setMyip(CommonUtils.getMobileIPAddress());

        apiRequest.setDevice_type(AppConstant.DEVICE_TYPE);
        apiRequest.setDevice_token(PreferencesManager.Companion.getStringPreferences(context, AppConstant.DEVICE_TOKEN));

        ApiUtils.callApiSocialLogin(context, apiRequest, new ApiResponseInterface() {
            @Override
            public void onResponse(Response<ApiResponse> response) {
                CommonUtils.hideProgressDialog();
                if (response.code() == AppConstant.SUCCESS_CODE) {
                    String msg = response.body().getMessage();
                    if (response.body() != null) {
                        if (response.body().getHasMobile()==0){
                            alertBoxEditText();
                        }
                        else {
                            saveData(response);
                            PreferencesManager.Companion.saveBooleanPreferences(context, AppConstant.IS_LOGIN, true);
                            Intent intent = new Intent(context, MainActivity.class);
                            startActivity(intent);
                            //CommonUtils.setFragment(new DashboardFragment(), false, getActivity(), R.id.container_main, AppConstant.DASHBOARD);

                            CommonUtils.showToast(context, ChangeLanguage.translate(context, msg));
                            ((Activity) context).finishAffinity();
                        }

                    }
                }
                else if (response.code() == AppConstant.VALIDATION_ERROR_CODE){
                    errorParse(response);

                }
                else if (response.code() == AppConstant.UNAUTHORIZED_CODE){
                    errorParse(response);
                }
                else if (response.code()==AppConstant.SUCCESS_CODE_201){
                    CommonUtils.showToast(context,response.body().getMessage());
                }
                else {
                    CommonUtils.showToast(context,getString(R.string.something_went_wrong));

                }
            }

            @Override
            public void onFailure() {
                CommonUtils.hideProgressDialog();
                ToastUtil.Companion.showShortToast(context,getString(R.string.something_went_wrong));

            }
        });
    }

    /**
     * This method is used to save data in shared preference
     */
    private void saveData(Response<ApiResponse> response) {
        if (response.body().getAccessCode() != null)
            PreferencesManager.Companion.saveStringPreferences(context,AppConstant.ACCESS_CODE,response.body().getAccessCode());
//        PreferencesManager.Companion.saveStringPreferences(context,AppConstant.USER_EMAIL,loginBinding.etEmailLogin.getText().toString());
        if (response.body().getFirstNaame() != null)
            PreferencesManager.Companion.saveStringPreferences(context,AppConstant.FIRST_NAME,response.body().getFirstNaame());
        if (response.body().getLastName() != null)
            PreferencesManager.Companion.saveStringPreferences(context,AppConstant.LAST_NAME,response.body().getLastName());
        if (response.body().getUserImage() != null)
            PreferencesManager.Companion.saveStringPreferences(context,AppConstant.USER_IMAGE,response.body().getUserImage());
//            PreferencesManager.Companion.saveStringPreferences(context,AppConstant.PASSWORD,loginBinding.etPassLogin.getText().toString());
//        if (response.body().getUserCurrency()!=null)
//            PreferencesManager.Companion.saveStringPreferences(context,AppConstant.CURRENT_CURRENCY,response.body().getUserCurrency());

    }
    private void errorParse(Response<ApiResponse> response) {
        if (response.errorBody()!=null){
            try {
                ApiResponse error = ErrorUtils.parseError(response, context);
                if (error!=null && error.getError()!=null){
                    showError(error.getError());
                }
            }
            catch (Exception e){

            }
        }
    }
    private void showError(ErrorModel error) {
        if (error.getFirstName()!=null){
            signUpBinding.firstName.setError(ChangeLanguage.translate(context,error.getFirstName().get(0)));
        }
        if (error.getLastName()!=null){
           signUpBinding.lastName.setError(ChangeLanguage.translate(context,error.getLastName().get(0)));
        }
        if (error.getEmail()!=null){
           //signUpBinding.emailEt.setError(error.getEmail().get(0));
           signUpBinding.checkEmail.setVisibility(View.VISIBLE);
           signUpBinding.checkEmail.setText(getString(R.string.unavailable));
           signUpBinding.checkEmail.setTextColor(getResources().getColor(R.color.colorPrimary));
        }
        if (error.getMobile()!=null){
           signUpBinding.phoneNumber.setError(ChangeLanguage.translate(context,error.getMobile().get(0)));
        }
        if (error.getPassword()!=null){
           signUpBinding.passEt.setError(ChangeLanguage.translate(context,error.getPassword().get(0)));
        }
        if (error.getPasswordConfirmation()!=null){
           signUpBinding.confrmPassEt.setError(ChangeLanguage.translate(context,error.getPasswordConfirmation().get(0)));
        }

    }

    @Override
    protected void onPause() {
        super.onPause();
        googleApiClient.stopAutoManage((FragmentActivity) context);
        googleApiClient.disconnect();
    }
}


