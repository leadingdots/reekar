package com.reekar.activity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.LinearLayout;

import androidx.core.content.ContextCompat;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;

import com.reekar.R;
import com.reekar.apiservices.ApiUtils;
import com.reekar.commonclasses.AppConstant;
import com.reekar.commonclasses.BaseActivity;
import com.reekar.commonclasses.CommonUtils;
import com.reekar.commonclasses.TimeUtil;
import com.reekar.databinding.ActivitySearchCarBinding;
import com.reekar.fragment.MapViewFragment;
import com.reekar.fragment.SearchCarListFragment;
import com.reekar.interfaces.ApiResponseInterface;
import com.reekar.models.ApiRequest;
import com.reekar.models.ApiResponse;
import com.reekar.models.CarModel;
import com.reekar.models.FilterModel;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Response;

public class SearchCarActivity extends BaseActivity {

    private ActivitySearchCarBinding searchCarBinding;
    private Context mContext;
    private String mAddress,pickupTime,pickupDate,dropTime,dropDate;
    private double lat,lng;

    private String from;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        searchCarBinding= DataBindingUtil.setContentView(this,R.layout.activity_search_car);
        mContext =this;

        getIntentData();
        openFragment();
        setToolbar();
        setListener();

    }

    @Override
    public void setToolbar() {

    }

    @Override
    public void setListener() {
        searchCarBinding.toolbarSearchCar.custToolBckBtn.setOnClickListener(this);
        searchCarBinding.listviewLlBtn.setOnClickListener(this);
        searchCarBinding.mapviewLlBtn.setOnClickListener(this);
    }

    @Override
    public void onBackPressed() {
        finish();
    }

    @Override
    protected void onResume() {
        super.onResume();
        //openFragment();
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){

            case R.id.cust_tool_bck_btn:
            {
                finish();
                break;
            }
            case R.id.listview_ll_btn:
            {
                Fragment fragment = new SearchCarListFragment();
//                setIntentData(fragment);
                searchCarBinding.filterBottomLayout.setVisibility(View.GONE);
                changeView(searchCarBinding.listviewLlBtn);
                setFrag(new SearchCarListFragment(),AppConstant.LISTVIEW_FRAGMENT);
//                /CommonUtils.setFragment(fragment, false, this, R.id.container_car_search, AppConstant.LISTVIEW_FRAGMENT);
                break;
            }
            case R.id.mapview_ll_btn:
            {
                Fragment fragment = new MapViewFragment();
//                setIntentData(fragment);
                searchCarBinding.filterBottomLayout.setVisibility(View.GONE);
                changeView(searchCarBinding.mapviewLlBtn);
                setFrag(new MapViewFragment(),AppConstant.MAPVIEW_FRAGMENT);
                //CommonUtils.setFragment(fragment, false, this, R.id.container_car_search, AppConstant.MAPVIEW_FRAGMENT);
                break;
            }
        }
    }


    public void getIntentData() {
        Intent intent = getIntent();
        if (intent != null) {
            if (intent.getStringExtra(AppConstant.ADDRESS) != null)
                mAddress = intent.getStringExtra(AppConstant.ADDRESS);
            lat = intent.getDoubleExtra(AppConstant.LATITUDE,0.0);
            lng = intent.getDoubleExtra(AppConstant.LONGITUDE,0.0);

            if (intent.getStringExtra(AppConstant.PICKUP_TIME) != null)
                pickupTime = intent.getStringExtra(AppConstant.PICKUP_TIME);
            if (intent.getStringExtra(AppConstant.DROP_TIME) != null)
                dropTime = intent.getStringExtra(AppConstant.DROP_TIME);
            if (intent.getStringExtra(AppConstant.PICKUP_DATE) != null)
                pickupDate = intent.getStringExtra(AppConstant.PICKUP_DATE);
            if (intent.getStringExtra(AppConstant.DROP_DATE) != null)
                dropDate = intent.getStringExtra(AppConstant.DROP_DATE);
            if (intent.getStringExtra(AppConstant.FROM)!=null)
                from=intent.getStringExtra(AppConstant.FROM);

        }

    }

    private void openFragment(){
        if (from!=null) {
            if (from.equals(AppConstant.MAIN_FRAGMENT)) {
                Fragment fragment = new MapViewFragment();

                searchCarBinding.filterBottomLayout.setVisibility(View.GONE);
                changeView(searchCarBinding.mapviewLlBtn);
                setFrag(new MapViewFragment(), AppConstant.MAPVIEW_FRAGMENT);
                //CommonUtils.setFragment(fragment, false, this, R.id.container_car_search, AppConstant.MAPVIEW_FRAGMENT);
            }
            if (from.equals(AppConstant.BOOKCAR_FRAGMENT)) {
                Fragment fragment = new MapViewFragment();

                searchCarBinding.filterBottomLayout.setVisibility(View.GONE);
                changeView(searchCarBinding.mapviewLlBtn);
                setFrag(new MapViewFragment(), AppConstant.MAPVIEW_FRAGMENT);
                //CommonUtils.setFragment(fragment, false, this, R.id.container_car_search, AppConstant.MAPVIEW_FRAGMENT);
            }
            if (from.equals(AppConstant.LISTCAR_FRAGMENT)) {
                Fragment fragment = new MapViewFragment();

                searchCarBinding.filterBottomLayout.setVisibility(View.GONE);
                changeView(searchCarBinding.mapviewLlBtn);
                setFrag(new MapViewFragment(), AppConstant.MAPVIEW_FRAGMENT);
                //CommonUtils.setFragment(fragment, false, this, R.id.container_car_search, AppConstant.MAPVIEW_FRAGMENT);
            }
            else {
                Fragment fragment = new SearchCarListFragment();
//            setIntentData(fragment);
                setFrag(new SearchCarListFragment(), AppConstant.LISTVIEW_FRAGMENT);
                searchCarBinding.filterBottomLayout.setVisibility(View.GONE);
            }
        } else {
            setFrag(new SearchCarListFragment(), AppConstant.LISTVIEW_FRAGMENT);
            searchCarBinding.filterBottomLayout.setVisibility(View.GONE);
        }
    }

    private void setFrag(Fragment fragment,String tag) {
        setIntentData(fragment);
        CommonUtils.setFragment(fragment, false, this, R.id.container_car_search,
                tag);

    }

    private void setIntentData(Fragment fragment) {
        Bundle bundle = new Bundle();
        bundle.putString(AppConstant.ADDRESS,mAddress);
        bundle.putString(AppConstant.PICKUP_TIME,pickupTime);
        bundle.putString(AppConstant.DROP_TIME,dropTime);
        bundle.putString(AppConstant.PICKUP_DATE,pickupDate);
        bundle.putString(AppConstant.DROP_DATE,dropDate);

        bundle.putDouble(AppConstant.LATITUDE,lat);
        bundle.putDouble(AppConstant.LONGITUDE,lng);

        fragment.setArguments(bundle);
    }

    private void changeView(LinearLayout linearLayout) {
        searchCarBinding.listviewLlBtn.setBackgroundColor(ContextCompat.getColor(mContext,R.color.darkGray));
        searchCarBinding.mapviewLlBtn.setBackgroundColor(ContextCompat.getColor(mContext,R.color.darkGray));
        linearLayout.setBackgroundColor(ContextCompat.getColor(mContext,R.color.colorPrimary));
    }
}
