package com.reekar.activity;

import androidx.databinding.DataBindingUtil;

import android.app.DatePickerDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;

import com.akanksha.commonclassutil.ToastUtil;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.reekar.R;
import com.reekar.apiservices.ApiUtils;
import com.reekar.commonclasses.AppConstant;
import com.reekar.commonclasses.BaseActivity;
import com.reekar.commonclasses.CommonUtils;
import com.reekar.commonclasses.ErrorUtils;
import com.reekar.commonclasses.TimeUtil;
import com.reekar.databinding.ActivityCarAvailabilityBinding;
import com.reekar.interfaces.ApiResponseInterface;
import com.reekar.models.ApiResponse;
import com.reekar.models.CarModel;
import com.reekar.models.ErrorModel;
import com.reekar.models.SaveCarInfoApiRequest;
import com.reekar.models.UploadFileModel;

import java.lang.reflect.Type;
import java.sql.Time;
import java.util.Calendar;

import retrofit2.Response;

public class CarAvailabilityActivity extends BaseActivity {

    private ActivityCarAvailabilityBinding carAvailabilityBinding;
    private Context context;
    private DatePickerDialog.OnDateSetListener mDateSetListener;

    private String dateFormat="dd/MM/yyyy";
    private String timeFormat="hh:mm a";

    private String carId;
    private String mPickDate,mDropDate,mPickTime,mDropTime;


    Calendar calendar = Calendar.getInstance();
    private int hour = calendar.get(Calendar.HOUR_OF_DAY);
    private int minute = calendar.get(Calendar.MINUTE);
    private int second = calendar.get(Calendar.SECOND);

    private ApiResponse apiResponse = new ApiResponse();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        carAvailabilityBinding = DataBindingUtil.setContentView(this,R.layout.activity_car_availability);
        context=this;

        getIntentData();
        setToolbar();
        setListener();
        setData();

    }

    private void getIntentData(){
        Intent intent =getIntent();
        if (intent!=null){
            carId =intent.getStringExtra(AppConstant.CAR_ID);

            if (getIntent().getStringExtra(AppConstant.DATA) != null){
                Type type = new TypeToken<ApiResponse>() {}.getType();
                apiResponse = new Gson().fromJson(getIntent().getStringExtra(AppConstant.DATA),type);
            }
        }
    }

    @Override
    public void setToolbar() {
        carAvailabilityBinding.carAvailableToolbar.tvToolbarBarName.setText(getString(R.string.car_availablity));
    }

    @Override
    public void setListener() {
        carAvailabilityBinding.carAvailableToolbar.custToolBckBtn.setOnClickListener(this);
        carAvailabilityBinding.carPickupTime.setOnClickListener(this);
        carAvailabilityBinding.carPickupDate.setOnClickListener(this);
        carAvailabilityBinding.carDropTime.setOnClickListener(this);
        carAvailabilityBinding.carDropDate.setOnClickListener(this);
        carAvailabilityBinding.submitBtnCaravailabl.setOnClickListener(this);
    }

    private void validation(){
        mPickDate = carAvailabilityBinding.carPickupDate.getText().toString();
        mDropDate=carAvailabilityBinding.carDropDate.getText().toString();
        mPickTime =carAvailabilityBinding.carPickupTime.getText().toString();
        mDropTime=carAvailabilityBinding.carDropTime.getText().toString();

       if (mPickTime.isEmpty())
            ToastUtil.Companion.showLongToast(context, getString(R.string.error_pickup_time));
       else if (mPickDate.isEmpty())
            ToastUtil.Companion.showLongToast(context, getString(R.string.error_pickup_date));
        else if (mDropTime.isEmpty())
            ToastUtil.Companion.showLongToast(context, getString(R.string.error_drop_time));
        else if (mDropDate.isEmpty())
            ToastUtil.Companion.showLongToast(context, getString(R.string.error_drop_date));
        else if (!TimeUtil.CheckDates(mPickDate,mDropDate))
            ToastUtil.Companion.showLongToast(context, getString(R.string.pick_date_should_be_greater));
        else if (mPickTime.equals(mDropTime) && mPickDate.equals(mDropDate))
            ToastUtil.Companion.showLongToast(context, getString(R.string.error_difffrent_times));

        else if (!TimeUtil.isTimeGreater(mPickDate,mPickTime,mDropDate,mDropTime))
            ToastUtil.Companion.showLongToast(context, getString(R.string.drop_time_should_be_greater));

        else {
            if (CommonUtils.isNetworkAvailable(context))
         callApiSaveAvailablity();
            else
                ToastUtil.Companion.showShortToast(context,getString(R.string.failed_internet));
        }

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){

            case R.id.cust_tool_bck_btn:{
                finish();
                break;
            }
            case R.id.car_pickup_date:{
                TimeUtil.datePicker(context,carAvailabilityBinding.carPickupDate,dateFormat);
                break;
            }
            case R.id.car_pickup_time:{
                TimeUtil.timePicker(context,carAvailabilityBinding.carPickupTime,timeFormat);
                break;
            }
            case R.id.car_drop_date:{
                TimeUtil.datePicker(context,carAvailabilityBinding.carDropDate,dateFormat);
                break;
            }
            case R.id.car_drop_time:{
                TimeUtil.timePicker(context,carAvailabilityBinding.carDropTime,timeFormat);
                break;
            }
            case R.id.submit_btn_caravailabl:{
                validation();
                break;
            }
        }
    }

    private void callApiSaveAvailablity(){
        CommonUtils.showProgressDialog(context);

        SaveCarInfoApiRequest apiRequest =new SaveCarInfoApiRequest();
        apiRequest.setCarId(carId);
        apiRequest.setAvailableFromDate(TimeUtil.localDateToGMT(mPickDate+" "+mPickTime,"dd/MM/yyyy hh:mm a",context));
        apiRequest.setAvailableToDate(TimeUtil.localDateToGMT(mDropDate+" "+mDropTime,"dd/MM/yyyy hh:mm a",context));

        ApiUtils.callSaveCarAvailableApi(context, apiRequest, new ApiResponseInterface() {
            @Override
            public void onResponse(Response<ApiResponse> response) {
                CommonUtils.hideProgressDialog();
                if (response.code()==AppConstant.SUCCESS_CODE){
                    if (response.body()!=null){
                        ToastUtil.Companion.showShortToast(context,response.body().getMessage());
                        Intent intent =new Intent(context,MainActivity.class);
                        intent.putExtra(AppConstant.FROM,AppConstant.CAR_AVAILABILITY_ACTIVITY);
                        startActivity(intent);
                        finishAffinity();
                    }
                }
                else if (response.code()==AppConstant.VALIDATION_ERROR_CODE){
                    parseError(response);
                }
                else {
                    ToastUtil.Companion.showShortToast(context,getString(R.string.something_went_wrong));
                }
            }

            @Override
            public void onFailure() {
                CommonUtils.hideProgressDialog();
                ToastUtil.Companion.showShortToast(context,getString(R.string.something_went_wrong));

            }
        });
    }

    public void parseError(Response<ApiResponse> response){

        if (response.errorBody()!=null){
            try {
                ApiResponse error = ErrorUtils.parseError(response,context);
                if (error!=null && error.getError()!=null){
                    showError(error.getError());
                }
            }
            catch (Exception e){

            }
        }
    }
    private void showError(ErrorModel error) {
        if (error.getAvailableFromDate()!=null){
            ToastUtil.Companion.showShortToast(context,error.getAvailableFromDate().get(0));
        }
        if (error.getAvailableToDate()!=null){
            ToastUtil.Companion.showShortToast(context,error.getAvailableToDate().get(0));

        }
        if (error.getAvailableFromTime()!=null){
            ToastUtil.Companion.showShortToast(context,error.getAvailableFromTime().get(0));

        }
        if (error.getAvailableToTime()!=null){
            ToastUtil.Companion.showShortToast(context,error.getAvailableToTime().get(0));

        }
    }

    private void setData(){
        if (apiResponse != null){
            if (apiResponse.getCar() != null){
                CarModel cars = apiResponse.getCar();
                    if (cars.getAvailableFromDate() != null) {
                        carAvailabilityBinding.carPickupDate.setText(TimeUtil.UtcToLocal(cars.getAvailableFromDate(),"dd/MM/yyyy",context));
                    }
                    if (cars.getAvailableToDate() != null) {
                        carAvailabilityBinding.carDropDate.setText(TimeUtil.UtcToLocal(cars.getAvailableToDate(),"dd/MM/yyyy",context));
                    }
                    if (cars.getAvailableFromDate() != null) {
                        carAvailabilityBinding.carPickupTime.setText(TimeUtil.UtcToLocal(cars.getAvailableFromDate(),"hh:mm a",context));
                    }
                    if (cars.getAvailableToDate() != null) {
                        carAvailabilityBinding.carDropTime.setText(TimeUtil.UtcToLocal(cars.getAvailableToDate(),"hh:mm a",context));
                    }
            }
        }
    }

}
