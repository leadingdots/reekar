package com.reekar.activity;

import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;

import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.text.Html;
import android.text.method.ScrollingMovementMethod;
import android.view.View;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.TextView;

import com.akanksha.commonclassutil.PreferencesManager;
import com.akanksha.commonclassutil.ToastUtil;
import com.reekar.R;
import com.reekar.apiservices.ApiUtils;
import com.reekar.commonclasses.AppConstant;
import com.reekar.commonclasses.BaseActivity;
import com.reekar.commonclasses.CommonUtils;
import com.reekar.databinding.ActivityTermsConditionsBinding;
import com.reekar.interfaces.ApiResponseInterface;
import com.reekar.models.ApiRequest;
import com.reekar.models.ApiResponse;

import retrofit2.Response;

public class TermsConditionsActivity extends BaseActivity {

    private ActivityTermsConditionsBinding termsConditionsBinding;

    private String key;
    private Context context;
    private String from;


    private String URL="http://192.168.1.97/reecar/static/content/";
//    private String URL="http://13.235.135.178/static/content/";  // live url
    private String URL_POLICY="privacy-policy";
    private String URL_TERMS="terms-and-condition";

//
/*    private String URL_POLICY="http://192.168.1.97/reecar/static/content/privacy-policy";
    private String URL_TERMS="http://192.168.1.97/reecar/static/content/terms-and-condition";*/
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        termsConditionsBinding= DataBindingUtil.setContentView(this,R.layout.activity_terms_conditions);
        context=this;

        getIntentData();
        setToolbar();
        setListener();
//        loadURL();
        if (CommonUtils.isNetworkAvailable(context)){
//            callApiPolicy();
        }
        else
            ToastUtil.Companion.showShortToast(context,getString(R.string.failed_internet));

    }



    private void  getIntentData(){
        Intent intent = getIntent();
        if (intent!=null) {
            if(intent.getStringExtra(AppConstant.FROM)!=null)
            from = intent.getStringExtra(AppConstant.FROM);
        }
    }

    private void openActivity(String url) {
            termsConditionsBinding.tvDummyText.setMovementMethod(new ScrollingMovementMethod());
        termsConditionsBinding.webview.getSettings().setLoadsImagesAutomatically(true);
        termsConditionsBinding.webview.getSettings().setJavaScriptEnabled(true);
        termsConditionsBinding.webview.setScrollBarStyle(View.SCROLLBARS_INSIDE_OVERLAY);
        termsConditionsBinding.webview.loadUrl(url);

    }

/*
    private void loadURL() {
        WebView theWebPage = new WebView(this);
        theWebPage.getSettings().setJavaScriptEnabled(true);
        theWebPage.getSettings().setPluginState(WebSettings.PluginState.ON);
        setContentView(theWebPage);
        theWebPage.loadUrl(url);
    }
*/

    @Override
    public void setToolbar() {
        String language = PreferencesManager.Companion.getStringPreferences(context, AppConstant.CURRENT_LANGUAGE);
        switch (from){
            case AppConstant.WHO_WE_ARE:{

                break;
            }
            case AppConstant.TERMS:{
//                key = "term_and_condition";
                termsConditionsBinding.toolbar.tvToolbarBarName.setText(R.string.terms);
                openActivity(AppConstant.BASE_URL_STATIC+AppConstant.URL_TERMS+language);
                break;
            }
            case AppConstant.POLICY:{
//                key = "privacy_policy";
                openActivity(AppConstant.BASE_URL_STATIC+AppConstant.URL_POLICY+language);
                termsConditionsBinding.toolbar.tvToolbarBarName.setText(R.string.our_policy);
                break;
            }

        }

    }


    @Override
    public void setListener() {
        termsConditionsBinding.toolbar.custToolBckBtn.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){

            case R.id.cust_tool_bck_btn:{
                finish();
                break;
            }
        }
    }

    private void callApiPolicy(){
        CommonUtils.showProgressDialog(context);

        ApiRequest apiRequest =new ApiRequest();
        apiRequest.setContentFor(key);
        ApiUtils.callStaticContentApi(context,apiRequest, new ApiResponseInterface() {
            @Override
            public void onResponse(Response<ApiResponse> response) {
                CommonUtils.hideProgressDialog();
                if (response.code()==AppConstant.SUCCESS_CODE){
                    if (response.body()!=null){
                        if (response.body().getContent()!=null) {
                            String content = response.body().getContent();
                            termsConditionsBinding.tvDummyText.setText(Html.fromHtml(content), TextView.BufferType.SPANNABLE);
                        }
                    }
                }
            }

            @Override
            public void onFailure() {
                CommonUtils.hideProgressDialog();
                ToastUtil.Companion.showShortToast(context,getString(R.string.something_went_wrong));
            }
        });
    }
}
