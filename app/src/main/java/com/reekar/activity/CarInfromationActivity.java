package com.reekar.activity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.RadioButton;

import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;

import com.akanksha.commonclassutil.ToastUtil;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.reekar.R;
import com.reekar.commonclasses.AppConstant;
import com.reekar.commonclasses.BaseActivity;
import com.reekar.databinding.ActivityCarInfromationBinding;
import com.reekar.models.ApiResponse;
import com.reekar.models.CarModel;

import java.lang.reflect.Type;

public class CarInfromationActivity extends BaseActivity {

    private ActivityCarInfromationBinding carInfromationBinding;

    private Context context;
    private String chassisNumber;
    private String mileage;

    private String from;

    //    private int carId;
    private int brandId;
    private int modelId;
    private int fuelTypeId;
    private int transmissionId;
    private int colorId;
    private int vehicleTypeId;
    private int seatId;
    private ApiResponse apiResponse = new ApiResponse();
    private String carId ="";

    private int isLicensed;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        carInfromationBinding = DataBindingUtil.setContentView(this,R.layout.activity_car_infromation);
        context=this;

        mGetIntent();
//        openActivity();
        setListener();
        setToolbar();

        setData();
    }

    private void mGetIntent() {
        if (getIntent() != null){
            if (getIntent().getStringExtra(AppConstant.DATA) != null){
                Type type = new TypeToken<ApiResponse>() {
                }.getType();
                apiResponse = new Gson().fromJson(getIntent().getStringExtra(AppConstant.DATA),type);

                if (getIntent().getStringExtra(AppConstant.CAR_ID) != null)
                    carId =getIntent().getStringExtra(AppConstant.CAR_ID);

                if (getIntent().getStringExtra(AppConstant.FROM)!=null)
                    from =getIntent().getStringExtra(AppConstant.FROM);

            }
        }
    }

    @Override
    public void setToolbar() {
        carInfromationBinding.carInfoToolbar.tvToolbarBarName.setText(getString(R.string.car_information));
    }

    private void setData(){
        if (apiResponse != null){
            if (apiResponse.getCar() != null){
                CarModel cars = apiResponse.getCar();
                if (cars.getBrandDetail() != null && cars.getBrandDetail().getName()!=null && cars.getBrandDetail().getDeletedAt()==null) {
                    carInfromationBinding.tvBrandName.setText(cars.getBrandDetail().getName());
                    carInfromationBinding.tvCarModel.setVisibility(View.VISIBLE);
                    brandId =cars.getBrandDetail().getId();
                }

                if (cars.getModelDetail() != null && cars.getModelDetail().getName()!=null&& cars.getModelDetail().getDeletedAt()==null) {
                    carInfromationBinding.tvCarModel.setText(cars.getModelDetail().getName());
                    modelId =cars.getModelDetail().getId();
                }
                if (cars.getYear() != null) {
                    carInfromationBinding.tvYearOfManufac.setText(cars.getYear());

                }
                if (cars.getEnginetypeDetail() != null && cars.getEnginetypeDetail().getName()!=null&& cars.getEnginetypeDetail().getDeletedAt()==null) {
                    carInfromationBinding.tvFuelType.setText(cars.getEnginetypeDetail().getName());
                    fuelTypeId =cars.getEnginetypeDetail().getId();

                }
                if (cars.getColorDetail() != null && cars.getColorDetail().getName()!=null&& cars.getColorDetail().getDeletedAt()==null) {
                    carInfromationBinding.tvColor.setText(cars.getColorDetail().getName());
                    colorId =cars.getColorDetail().getId();

                }

                if (cars.getVehicletypeDetail() != null && cars.getVehicletypeDetail().getName()!=null && cars.getVehicletypeDetail().getDeletedAt()==null) {
                    carInfromationBinding.tvVehicleType.setText(cars.getVehicletypeDetail().getName());
                    vehicleTypeId =cars.getVehicletypeDetail().getId();

                }

                if (cars.getSeatDetail() != null && cars.getSeatDetail().getName()!=null && cars.getSeatDetail().getDeletedAt()==null) {
                    carInfromationBinding.tvSeatCapicity.setText(cars.getSeatDetail().getName());
                    seatId =cars.getSeatDetail().getId();

                }

                if (cars.getTransmissionDetail() != null && cars.getTransmissionDetail().getName()!=null && cars.getTransmissionDetail().getDeletedAt()==null) {
                    carInfromationBinding.tvTransmissType.setText(cars.getTransmissionDetail().getName());
                    transmissionId =cars.getTransmissionDetail().getId();

                }
                if (cars.getMileage()!=null){
                    carInfromationBinding.etMileage.setText(cars.getMileage());
                    mileage=cars.getMileage().toString();
                }
                if (cars.getChassisNumber()!=null){
                    carInfromationBinding.etChassisNumber.setText(cars.getChassisNumber());
                    chassisNumber=cars.getChassisNumber().toString();

                }
                if (cars.getLicensed()==1){
                    carInfromationBinding.rbtnYes.setChecked(true);
                    carInfromationBinding.rbtnNo.setChecked(false);
                }
                if (cars.getLicensed()==0){
                    carInfromationBinding.rbtnYes.setChecked(false);
                    carInfromationBinding.rbtnNo.setChecked(true);
                }

            }
        }
    }

    @Override
    public void setListener() {
        carInfromationBinding.carInfoToolbar.custToolBckBtn.setOnClickListener(this);
        carInfromationBinding.submitCarInfoBtn.setOnClickListener(this);
        carInfromationBinding.tvBrandName.setOnClickListener(this);
        carInfromationBinding.tvYearOfManufac.setOnClickListener(this);
        carInfromationBinding.tvTransmissType.setOnClickListener(this);
        carInfromationBinding.tvSeatCapicity.setOnClickListener(this);
        carInfromationBinding.tvCarModel.setOnClickListener(this);
        carInfromationBinding.tvFuelType.setOnClickListener(this);
        carInfromationBinding.tvColor.setOnClickListener(this);
        carInfromationBinding.tvVehicleType.setOnClickListener(this);


    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){

            case R.id.cust_tool_bck_btn:{
                finish();
                break;
            }
            case R.id.tv_car_model: {
                startActivityForResult(new Intent(context, BrandsListActivity.class)
                        .putExtra(AppConstant.FROM, AppConstant.CAR_MODEL)
                        .putExtra(AppConstant.BRAND_ID,brandId),2);
                break;
            }
            case R.id.tv_seat_capicity: {
                startActivityForResult(new Intent(context, BrandsListActivity.class)
                        .putExtra(AppConstant.FROM, AppConstant.CAR_SEATS), 2);
                break;
            }
            case R.id.tv_fuel_type: {
                startActivityForResult(new Intent(context, BrandsListActivity.class)
                        .putExtra(AppConstant.FROM, AppConstant.CAR_FUEL_TYPE), 2);
                break;
            }
            case R.id.tv_transmiss_type: {
                startActivityForResult(new Intent(context, BrandsListActivity.class)
                        .putExtra(AppConstant.FROM, AppConstant.CAR_TRANSMISSION_TYPE), 2);
                break;
            }
            case R.id.tv_brand_name:{
                startActivityForResult(new Intent(context, BrandsListActivity.class)
                        .putExtra(AppConstant.FROM,AppConstant.CAR_BRAND),2);

                break;
            }
            case R.id.tv_year_of_manufac:{
                startActivityForResult(new Intent(context, BrandsListActivity.class)
                        .putExtra(AppConstant.FROM,AppConstant.CAR_MFG_YEAR),2);
                break;
            }
            case R.id.tv_color:{
                startActivityForResult(new Intent(context, BrandsListActivity.class)
                        .putExtra(AppConstant.FROM,AppConstant.CAR_COLOR),2);
                break;
            } case R.id.tv_vehicle_type:{
                startActivityForResult(new Intent(context, BrandsListActivity.class)
                        .putExtra(AppConstant.FROM,AppConstant.VEHICLE_TYPE),2);
                break;
            }
            case R.id.submit_car_info_btn:{
                int selectedId = carInfromationBinding.radioGroup.getCheckedRadioButtonId();
                RadioButton radioButton = (RadioButton) findViewById(selectedId);
                if (radioButton.isChecked()){
                    isLicensed=1;
                }
                validation();
//                Intent intent=new Intent(context,OptionsFeatureActivity.class);
//                startActivity(intent);
                break;
            }
        }
    }
    private void validation(){

        String brand = carInfromationBinding.tvBrandName.getText().toString();
        String model = carInfromationBinding.tvCarModel.getText().toString();
        String yearOfManufacture = carInfromationBinding.tvYearOfManufac.getText().toString();
        String fuelType = carInfromationBinding.tvFuelType.getText().toString();
        String seatCapicity = carInfromationBinding.tvSeatCapicity.getText().toString();
        String transmissionType = carInfromationBinding.tvTransmissType.getText().toString();
        String color = carInfromationBinding.tvColor.getText().toString();
        String vehicleType = carInfromationBinding.tvVehicleType.getText().toString();
        chassisNumber=carInfromationBinding.etChassisNumber.getText().toString();
        mileage=carInfromationBinding.etMileage.getText().toString();

        if (brand.isEmpty()){
            carInfromationBinding.tvCarModel.setVisibility(View.GONE);
            ToastUtil.Companion.showShortToast(context,getString(R.string.enter_brand_name));
        }
        else if (model.isEmpty()){
            ToastUtil.Companion.showShortToast(context,getString(R.string.enter_model_name));
        }
        else if(yearOfManufacture.isEmpty()){
            ToastUtil.Companion.showShortToast(context,getString(R.string.enter_mfg_year));
        }
        else if (fuelType.isEmpty()){
            ToastUtil.Companion.showShortToast(context,getString(R.string.enter_fuel_type));

        }
        else if (seatCapicity.isEmpty()){
            ToastUtil.Companion.showShortToast(context,getString(R.string.enter_seat_capcity));
        }
        else if (transmissionType.isEmpty()){
            ToastUtil.Companion.showShortToast(context,getString(R.string.enter_transmission));
        }
        else if (color.isEmpty()){
            ToastUtil.Companion.showShortToast(context,getString(R.string.enter_vehicle_color));
        }
        else if (vehicleType.isEmpty()){
            ToastUtil.Companion.showShortToast(context,getString(R.string.enter_vehicle_type));
        }
        else if (chassisNumber.isEmpty()){
            ToastUtil.Companion.showShortToast(context,getString(R.string.enter_vehicle_chassis_number));
        }
        else if (mileage.isEmpty()){
            ToastUtil.Companion.showShortToast(context,getString(R.string.enter_vehicle_mileage));
        }
        else {
            Intent intent=new Intent(context,OptionsFeatureActivity.class);
//            intent.putExtra(AppConstant.CAR_ID,carId);
            intent.putExtra(AppConstant.BRAND_ID,brandId);
            intent.putExtra(AppConstant.CAR_MODEL_ID,modelId);
            intent.putExtra(AppConstant.CAR_MFG_YEAR,carInfromationBinding.tvYearOfManufac.getText().toString());
            intent.putExtra(AppConstant.CAR_FUEL_TYPE_ID,fuelTypeId);
            intent.putExtra(AppConstant.CAR_SEATS,carInfromationBinding.tvSeatCapicity.getText().toString());
            intent.putExtra(AppConstant.CAR_TRANSMISSION_TYPE_ID,transmissionId);
            intent.putExtra(AppConstant.CAR_COLOR_ID,colorId);
            intent.putExtra(AppConstant.VEHICLE_TYPE_ID,vehicleTypeId);
            intent.putExtra(AppConstant.SEAT_ID,seatId);
            intent.putExtra(AppConstant.DATA,new Gson().toJson(apiResponse));
            intent.putExtra(AppConstant.CAR_ID,carId);
            intent.putExtra(AppConstant.CHASSIS_NUMBER,chassisNumber);
            intent.putExtra(AppConstant.MILEAGE,mileage);
            intent.putExtra(AppConstant.IS_LICENSED,isLicensed);
            startActivityForResult(intent,3);
        }

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode==3){
            if (data != null) {
                if (data.getStringExtra(AppConstant.CAR_ID)!=null)
                carId = data.getStringExtra(AppConstant.CAR_ID);
            }
        }
        if (requestCode==2){
            if (data != null) {
//            String brandId = data.getStringExtra(AppConstant.BRAND_ID);
                String brands = data.getStringExtra(AppConstant.BRAND);
                String model = data.getStringExtra(AppConstant.CAR_MODEL);
                String mfgYear = data.getStringExtra(AppConstant.CAR_MFG_YEAR);
                String fuelType = data.getStringExtra(AppConstant.CAR_FUEL_TYPE);
                String seats = data.getStringExtra(AppConstant.CAR_SEATS);
                String transmission = data.getStringExtra(AppConstant.CAR_TRANSMISSION_TYPE);
                String color = data.getStringExtra(AppConstant.CAR_COLOR);
                String vehicleType = data.getStringExtra(AppConstant.VEHICLE_TYPE);

                if (data.getIntExtra(AppConstant.BRAND_ID, 0) > 0){
                    if (data.getIntExtra(AppConstant.BRAND_ID, 0) != brandId)
                        carInfromationBinding.tvCarModel.setText("");
                brandId = data.getIntExtra(AppConstant.BRAND_ID, 0);
                }
//                ArrayList<DataModel> test = getIntent().getParcelableArrayListExtra(AppConstant.BRAND_ID);
//                LogUtil.Companion.errorLog("brandId", String.valueOf(brandId));
                if (data.getIntExtra(AppConstant.CAR_MODEL_ID, 0) > 0)
                modelId = data.getIntExtra(AppConstant.CAR_MODEL_ID,0);
                if (data.getIntExtra(AppConstant.CAR_FUEL_TYPE_ID, 0) > 0)
                    fuelTypeId = data.getIntExtra(AppConstant.CAR_FUEL_TYPE_ID,0);
                if (data.getIntExtra(AppConstant.CAR_TRANSMISSION_TYPE_ID, 0) > 0)
                    transmissionId = data.getIntExtra(AppConstant.CAR_TRANSMISSION_TYPE_ID,0);
                if (data.getIntExtra(AppConstant.CAR_COLOR_ID, 0) > 0)
                    colorId= data.getIntExtra(AppConstant.CAR_COLOR_ID,0);
                if (data.getIntExtra(AppConstant.VEHICLE_TYPE_ID, 0) > 0)
                    vehicleTypeId= data.getIntExtra(AppConstant.VEHICLE_TYPE_ID,0);
                if (data.getIntExtra(AppConstant.SEAT_ID, 0) > 0)
                    seatId= data.getIntExtra(AppConstant.SEAT_ID,0);

                if (brands != null) {
                    carInfromationBinding.tvBrandName.setText(brands);
                    carInfromationBinding.tvCarModel.setVisibility(View.VISIBLE);
                }
                if (carInfromationBinding.tvBrandName.getText().toString().isEmpty()){
                    carInfromationBinding.tvCarModel.setVisibility(View.GONE);
                }
                if (model != null)
                    carInfromationBinding.tvCarModel.setText(model);
                if (mfgYear != null)
                    carInfromationBinding.tvYearOfManufac.setText(mfgYear);
                if (fuelType != null)
                    carInfromationBinding.tvFuelType.setText(fuelType);
                if (seats != null)
                    carInfromationBinding.tvSeatCapicity.setText(seats);
                if (transmission != null)
                    carInfromationBinding.tvTransmissType.setText(transmission);
                if (color != null)
                    carInfromationBinding.tvColor.setText(color);
                if (vehicleType != null)
                    carInfromationBinding.tvVehicleType.setText(vehicleType);
            }
        }
    }
}
