package com.reekar.activity;

import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.LinearLayoutManager;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;

import com.akanksha.commonclassutil.PreferencesManager;
import com.akanksha.commonclassutil.ToastUtil;
import com.bumptech.glide.Glide;
import com.reekar.R;
import com.reekar.adapter.MyListingCarAdapter;
import com.reekar.apiservices.ApiUtils;
import com.reekar.commonclasses.AppConstant;
import com.reekar.commonclasses.BaseActivity;
import com.reekar.commonclasses.CommonUtils;
import com.reekar.commonclasses.CurrencyJson;
import com.reekar.databinding.ActivityMyBookedCarDetailsBinding;
import com.reekar.interfaces.ApiResponseInterface;
import com.reekar.models.ApiRequest;
import com.reekar.models.ApiResponse;
import com.reekar.models.OrderModel;

import retrofit2.Response;

public class MyBookedCarDetailsActivity extends BaseActivity {

    private ActivityMyBookedCarDetailsBinding myBookedCarDetailsBinding;
    private Context context;

    private String mId;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        myBookedCarDetailsBinding= DataBindingUtil.setContentView(this,R.layout.activity_my_booked_car_details);
        context=this;

        getIntentata();
        setToolbar();
        setListener();
        if (CommonUtils.isNetworkAvailable(context)){
            callBookedCarDetailApi();
        }
        else
            ToastUtil.Companion.showShortToast(context,getString(R.string.failed_internet));
    }

    private void getIntentata() {
        Intent intent =getIntent();
        if (intent!=null){
            if (intent.getStringExtra(AppConstant.ID)!=null)
                mId = intent.getStringExtra(AppConstant.ID);
        }
    }

    @Override
    public void setToolbar() {
        myBookedCarDetailsBinding.toolbar.tvToolbarBarName.setText(R.string.booked_car_details);
    }

    @Override
    public void setListener() {
        myBookedCarDetailsBinding.toolbar.custToolBckBtn.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){

            case R.id.cust_tool_bck_btn:{
                finish();
                break;
            }
        }
    }

    private void callBookedCarDetailApi(){
        CommonUtils.showProgressDialog(context);

        ApiRequest apiRequest = new ApiRequest();
        apiRequest.setOrderId(mId);

        ApiUtils.callOrderDetailApi(context, apiRequest, new ApiResponseInterface() {
            @Override
            public void onResponse(Response<ApiResponse> response) {
                CommonUtils.hideProgressDialog();
                switch (response.code()) {
                    case AppConstant.SUCCESS_CODE:

                        if (response.body() != null && response.body().getOrder() != null)
                            setData(response.body().getOrder());
                        break;
                    case AppConstant.UNAUTHORIZED_CODE:
                        CommonUtils.navigateToLogin(context);
                    default:
                        CommonUtils.showToast(context, getString(R.string.something_went_wrong));
                }
            }

            @Override
            public void onFailure() {
                CommonUtils.hideProgressDialog();
                CommonUtils.showToast(context,getString(R.string.something_went_wrong));
            }
        });
    }

    private void setData(OrderModel order) {
        String currency = PreferencesManager.Companion.getStringPreferences(context,AppConstant.CURRENT_CURRENCY_SYMBOL);
        if (currency.isEmpty()){
            currency=PreferencesManager.Companion.getStringPreferences(context,AppConstant.CURRENT_CURRENCY);
        }

        myBookedCarDetailsBinding.llParent.setVisibility(View.VISIBLE);
        if (order.getBookingId() != null)
            myBookedCarDetailsBinding.tvBookingId.setText(order.getBookingId());

        if (order.getPickupLocation() != null)
            myBookedCarDetailsBinding.tvCarPickupLocation.setText(order.getBillingAddress());

        if (order.getBillingFirstName() != null && order.getBillingLastName()!=null) {
            myBookedCarDetailsBinding.tvOwnerName.setText(order.getBillingFirstName() + " " + order.getBillingLastName());
        }
        else {
            myBookedCarDetailsBinding.tvOwnerName.setText(order.getBillingFirstName());
        }
        if (order.getContactNo() != null)
            myBookedCarDetailsBinding.tvOwnerNumber.setText(order.getBillingMobile());

        if (order.getBrand() != null)
            myBookedCarDetailsBinding.tvCar.setText(order.getBrand()+" "+order.getModel());


        myBookedCarDetailsBinding.tvTotalAmount.setText(currency+" "+order.getGrandTotal());
        myBookedCarDetailsBinding.tvTotalRent.setText(currency+" "+order.getTotalAmount());
        myBookedCarDetailsBinding.tvTax.setText(currency+" "+order.getTaxAmount());
        myBookedCarDetailsBinding.tvSecurityAmount.setText(currency+" "+order.getSecurityAmount());

        Glide.with(context).load(order.getImage()).placeholder(R.mipmap.placeholder_car).into(myBookedCarDetailsBinding.ivCar);

    }

}
