package com.reekar.activity;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.DownloadManager;
import android.content.Context;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.view.View;

import androidx.core.app.ActivityCompat;
import androidx.databinding.DataBindingUtil;

import com.akanksha.commonclassutil.PreferencesManager;
import com.akanksha.commonclassutil.ToastUtil;
import com.bumptech.glide.Glide;
import com.reekar.R;
import com.reekar.apiservices.ApiUtils;
import com.reekar.commonclasses.AppConstant;
import com.reekar.commonclasses.BaseActivity;
import com.reekar.commonclasses.CommonUtils;
import com.reekar.databinding.ActivityMyOrderDetailsBinding;
import com.reekar.interfaces.ApiResponseInterface;
import com.reekar.models.ApiRequest;
import com.reekar.models.ApiResponse;
import com.reekar.models.OrderModel;

import retrofit2.Response;

public class MyOrderDetailsActivity extends BaseActivity
{

    private ActivityMyOrderDetailsBinding binding;
    private Context context;
    private String mId = "";
    private String invoice;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this,R.layout.activity_my_order_details);
        context=this;

        mGetIntent();
        setToolbar();
        setListener();

        if (CommonUtils.isNetworkAvailable(context)){
            callOrderDetailApi();
        }
        else
            ToastUtil.Companion.showShortToast(context,getString(R.string.failed_internet));
    }

    private void mGetIntent() {
        if (getIntent() != null){
            if (getIntent().getStringExtra(AppConstant.ID) != null){
                mId = getIntent().getStringExtra(AppConstant.ID);
            }
        }
    }

    @Override
    public void setToolbar() {
        binding.orderDetailToolbar.tvToolbarBarName.setText(R.string.my_order);
    }

    @Override
    public void setListener() {
        binding.orderDetailToolbar.custToolBckBtn.setOnClickListener(this);
        binding.btnDownloadInvoice.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.cust_tool_bck_btn:{
                finish();
                break;
            }
            case R.id.btn_download_invoice:{
                downloadInvoice();
                break;
            }
        }
    }

    private void callOrderDetailApi(){
        CommonUtils.showProgressDialog(context);

        ApiRequest apiRequest = new ApiRequest();
        apiRequest.setOrderId(mId);

        ApiUtils.callOrderDetailApi(context, apiRequest, new ApiResponseInterface() {
            @Override
            public void onResponse(Response<ApiResponse> response) {
                CommonUtils.hideProgressDialog();
                switch (response.code()) {
                    case AppConstant.SUCCESS_CODE:

                        if (response.body() != null && response.body().getOrder() != null)
                            setData(response.body().getOrder());
                        break;
                    case AppConstant.UNAUTHORIZED_CODE:
                        CommonUtils.navigateToLogin(context);
                    default:
                        CommonUtils.showToast(context, getString(R.string.something_went_wrong));
                }
            }

            @Override
            public void onFailure() {
                CommonUtils.hideProgressDialog();
                CommonUtils.showToast(context,getString(R.string.something_went_wrong));
            }
        });
    }

    @SuppressLint("SetTextI18n")
    private void setData(OrderModel order) {

        String currency = PreferencesManager.Companion.getStringPreferences(context,AppConstant.CURRENT_CURRENCY_SYMBOL);
        if (currency.isEmpty()){
            currency=PreferencesManager.Companion.getStringPreferences(context,AppConstant.CURRENT_CURRENCY);
        }


        binding.llParent.setVisibility(View.VISIBLE);
        if (order.getBookingId() != null)
            binding.tvBookingId.setText(order.getBookingId());

        if (order.getPickupLocation() != null)
            binding.tvCarPickupLocation.setText(order.getPickupLocation());

        if (order.getOwnerName() != null)
            binding.tvOwnerName.setText(order.getOwnerName());

        if (order.getContactNo() != null)
            binding.tvOwnerNumber.setText(order.getContactNo());

        if (order.getBrand() != null)
            binding.tvCar.setText(order.getBrand()+" "+order.getModel());

        if (order.getInvoice()!=null)
            invoice=order.getInvoice();
        binding.tvTotalAmount.setText(currency+" "+order.getGrandTotal());
        binding.tvTotalRent.setText(currency+" "+order.getTotalAmount());
        binding.tvTax.setText(currency+" "+order.getTaxAmount());
        binding.tvSecurityAmount.setText(currency+" "+order.getSecurityAmount());

        Glide.with(context).load(order.getImage()).placeholder(R.mipmap.placeholder_car).into(binding.ivCar);

    }

    private void downloadInvoice(){
        if(ActivityCompat.checkSelfPermission(this, Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED || ActivityCompat.checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED){
            // this will request for permission when user has not granted permission for the app
            ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE}, 1);
        }
        else{
            //Download Script
            DownloadManager downloadManager = (DownloadManager) getSystemService(Context.DOWNLOAD_SERVICE);
            Uri uri = Uri.parse(invoice);
            DownloadManager.Request request = new DownloadManager.Request(uri);
            request.setVisibleInDownloadsUi(true);
            request.setNotificationVisibility(DownloadManager.Request.VISIBILITY_VISIBLE_NOTIFY_COMPLETED);
            request.setDestinationInExternalPublicDir(Environment.DIRECTORY_DOWNLOADS, uri.getLastPathSegment());
            downloadManager.enqueue(request);


            ToastUtil.Companion.showShortToast(context,getString(R.string.file_download_started));
        }

    }
}
