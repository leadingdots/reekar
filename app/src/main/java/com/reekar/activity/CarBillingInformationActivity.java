package com.reekar.activity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;

import androidx.databinding.DataBindingUtil;

import com.akanksha.commonclassutil.CheckValidation;
import com.akanksha.commonclassutil.ToastUtil;
import com.reekar.R;
import com.reekar.apiservices.ApiUtils;
import com.reekar.commonclasses.AppConstant;
import com.reekar.commonclasses.BaseActivity;
import com.reekar.commonclasses.CommonUtils;
import com.reekar.commonclasses.CurrencyJson;
import com.reekar.commonclasses.ErrorUtils;
import com.reekar.commonclasses.TimeUtil;
import com.reekar.databinding.ActivityCarBillingInformationBinding;
import com.reekar.interfaces.ApiResponseInterface;
import com.reekar.models.ApiRequest;
import com.reekar.models.ApiResponse;
import com.reekar.models.ErrorModel;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;

import retrofit2.Response;

public class CarBillingInformationActivity extends BaseActivity {

    private ActivityCarBillingInformationBinding carBillingInformationBinding;
    private Context context;


    private String mPickDate;
    private String mPickTime;
    private String mDropDate;
    private String mDropTime;
    private String mPickuLocation;

    private String profileId;
    private String carId;
    private String email;
    private String fname;
    private String lname;
    private String ccode;
    private String phoneNum;
    private String address;
    private double pricePerDay;
    private double securityAmt;
    private int taxAmt =0;
    private String alternativeNum;
    private String bookingId;

    private double grandTotal;

    private double grandTotalUSD;
    double totalAmount;
//    private int dayCount;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        carBillingInformationBinding= DataBindingUtil.setContentView(this,R.layout.activity_car_billing_information);
        context=this;

        getIntentData();
        setToolbar();
        setListener();

        if (CommonUtils.isNetworkAvailable(context)){
            callApiProfile();
        }
        else
            ToastUtil.Companion.showShortToast(context,getString(R.string.failed_internet));
    }

    private void getIntentData() {
        Intent intent =getIntent();
        if (intent!=null){
            if (intent.getStringExtra(AppConstant.CAR_ID)!=null){
                carId=intent.getStringExtra(AppConstant.CAR_ID);
            }
           if (intent.getStringExtra(AppConstant.BOOKING_FROM_DATE)!=null){
               mPickDate=intent.getStringExtra(AppConstant.BOOKING_FROM_DATE);
           }
           if (intent.getStringExtra(AppConstant.BOOKING_FROM_TIME)!=null){
               mPickTime=intent.getStringExtra(AppConstant.BOOKING_FROM_TIME);
           }
           if (intent.getStringExtra(AppConstant.BOOKING_TO_DATE)!=null){
               mDropDate=intent.getStringExtra(AppConstant.BOOKING_TO_DATE);
           }
           if (intent.getStringExtra(AppConstant.BOOKING_TO_TIME)!=null){
               mDropTime=intent.getStringExtra(AppConstant.BOOKING_TO_TIME);
           }
           if (intent.getStringExtra(AppConstant.PICKUP_LOCATION)!=null){
               mPickuLocation=intent.getStringExtra(AppConstant.PICKUP_LOCATION);
           }
//           if (intent.getStringExtra(AppConstant.PRICE_PER_DAY)!=null){
               pricePerDay = intent.getDoubleExtra(AppConstant.PRICE_PER_DAY,0);
//           }
//           if (intent.getStringExtra(AppConstant.SECURITY_AMOUNT)!=null){
               securityAmt=intent.getDoubleExtra(AppConstant.SECURITY_AMOUNT,0);
//           }
        }
    }

    @Override
    public void setToolbar() {
        carBillingInformationBinding.toolbar.tvToolbarBarName.setText(R.string.booking_infomation);
    }

    @Override
    public void setListener() {
        carBillingInformationBinding.toolbar.custToolBckBtn.setOnClickListener(this);
        carBillingInformationBinding.submit.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){

            case R.id.cust_tool_bck_btn:{
                finish();
                break;
            }
            case R.id.submit:{
                validation();
                break;
            }
        }
    }

    private void callApiProfile(){
        CommonUtils.showProgressDialog(context);

        ApiRequest apiRequest =new ApiRequest();

        ApiUtils.callApiProfile(context, apiRequest, new ApiResponseInterface() {
            @Override
            public void onResponse(Response<ApiResponse> response) {
                CommonUtils.hideProgressDialog();
                if (response!=null)
                    if (response.code()==AppConstant.SUCCESS_CODE){
                        if (response.body()!=null){

                            carBillingInformationBinding.parentLl.setVisibility(View.VISIBLE);

                            if (response.body().getProfile().getId()!=null)
                                profileId = response.body().getProfile().getId().toString();
                            if (response.body().getProfile().getEmail()!=null)
                                email = response.body().getProfile().getEmail();
                            if (response.body().getProfile().getFirstName()!=null)
                                fname = response.body().getProfile().getFirstName();
                            if (response.body().getProfile().getLastName()!=null)
                                lname = response.body().getProfile().getLastName();
                            if (response.body().getProfile().getCountryCode()!=null) {
                                ccode = response.body().getProfile().getCountryCode();
                                ccode=ccode.replace("+","");
                            }
                            if (response.body().getProfile().getMobile()!=null)
                                phoneNum = response.body().getProfile().getMobile();
                            if (response.body().getProfile().getAddress()!=null)
                                address = response.body().getProfile().getAddress().toString();

                            if (fname!=null)
                            carBillingInformationBinding.tvFirstName.setText(fname);
                            if (lname!=null)
                            carBillingInformationBinding.tvLastName.setText(lname);
                            if (ccode!=null && !ccode.isEmpty())
                            carBillingInformationBinding.countryCodePicker.setCountryForPhoneCode(Integer.parseInt(ccode));
                            if (phoneNum!=null)
                            carBillingInformationBinding.tvMobileNumber.setText(phoneNum);
                            if (email!=null)
                            carBillingInformationBinding.tvEmail.setText(email);
                            if (address!=null)
                            carBillingInformationBinding.tvAddress.setText(address);

                        }
                    }
                    else if (response.code()==AppConstant.UNAUTHORIZED_CODE){
                        CommonUtils.navigateToLogin(context);
                    }
                    else {
                        ToastUtil.Companion.showShortToast(context,getString(R.string.something_went_wrong));
                    }

            }

            @Override
            public void onFailure() {
                CommonUtils.hideProgressDialog();
                ToastUtil.Companion.showShortToast(context,getString(R.string.something_went_wrong));
            }
        });
    }

    private void callPlaceOrderApi(){
        CommonUtils.showProgressDialog(context);
        SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy hh:mm a");
        int dayCount=1;
        Date Date1 = null,Date2 = null;

        try{

            Date1 = sdf.parse(mPickDate +" "+mPickTime);
            Date2 = sdf.parse(mDropDate +" "+mDropTime);

//            dayCount = (int) Math.ceil((Date2.getDate() - Date1.getDate())/(1000 * 60 * 60 * 24));
            dayCount = (Date2.getDate() - Date1.getDate());
            dayCount=dayCount+1;

            Log.e("daycount", String.valueOf(dayCount));
        }catch(Exception e)
        {
            e.printStackTrace();
        }
        totalAmount = pricePerDay * dayCount;
        grandTotal = totalAmount + securityAmt + taxAmt;

        ApiRequest apiRequest = new ApiRequest();
        apiRequest.setCarId(carId);
        apiRequest.setBookingFromDate(TimeUtil.localDateToGMT(mPickDate+" "+mPickTime,"dd/MM/yyyy hh:mm a",context));
        apiRequest.setBookingToDate(TimeUtil.localDateToGMT(mDropDate+" "+mDropTime,"dd/MM/yyyy hh:mm a",context));
        apiRequest.setCurrency(CurrencyJson.updateCurrency(context));
        apiRequest.setTotalAmount(String.valueOf(totalAmount));
        apiRequest.setSecurityAmount(String.valueOf(securityAmt));
        apiRequest.setTaxAmount(String.valueOf(taxAmt));
        apiRequest.setGrandTotal(String.valueOf(grandTotal));
        apiRequest.setBillingFirstName(carBillingInformationBinding.tvFirstName.getText().toString());
        apiRequest.setBillingLastName(carBillingInformationBinding.tvLastName.getText().toString());
        apiRequest.setBillingEmail(carBillingInformationBinding.tvEmail.getText().toString());
        apiRequest.setBillingMobile(carBillingInformationBinding.countryCodePicker.getSelectedCountryCodeWithPlus()+carBillingInformationBinding.tvMobileNumber.getText().toString());
        if (!carBillingInformationBinding.tvAlternateNumber.getText().toString().isEmpty())
        apiRequest.setBillingAlternateMobile(carBillingInformationBinding.countryCodePicker.getSelectedCountryCodeWithPlus()+carBillingInformationBinding.tvAlternateNumber.getText().toString());
        apiRequest.setBillingAddress(carBillingInformationBinding.tvAddress.getText().toString());
        apiRequest.setPickupLocation(mPickuLocation);

        ApiUtils.callPlaceOrderApi(context, apiRequest, new ApiResponseInterface() {
            @Override
            public void onResponse(Response<ApiResponse> response) {
                CommonUtils.hideProgressDialog();
                if (response.code()== AppConstant.SUCCESS_CODE){
                    if (response.body()!=null){

                        if (response.body().getMessage()!=null)
                            ToastUtil.Companion.showShortToast(context,response.body().getMessage());
                        if (response.body().getBookingId()!=null)
                            bookingId=response.body().getBookingId();
                            grandTotalUSD=response.body().getGrandTotalUsd();
                            sendData();
                    }

                }
                else if (response.code()==AppConstant.VALIDATION_ERROR_CODE){
                    parseError(response);
                }
                else if (response.code()==AppConstant.UNAUTHORIZED_CODE){
                    ToastUtil.Companion.showShortToast(context,getString(R.string.something_went_wrong));

                }
                else {
                    ToastUtil.Companion.showShortToast(context,getString(R.string.something_went_wrong));

                }
            }

            @Override
            public void onFailure() {
                CommonUtils.hideProgressDialog();
                ToastUtil.Companion.showShortToast(context,getString(R.string.something_went_wrong));
            }
        });
    }
    public void parseError(Response<ApiResponse> response){

        if (response.errorBody()!=null){
            try {
                ApiResponse error = ErrorUtils.parseError(response,context);
                if (error!=null && error.getError()!=null){
                    showError(error.getError());
                }
            }
            catch (Exception e){

            }
        }
    }
    private void showError(ErrorModel error) {
        if (error.getBillingFirstName()!=null){
            carBillingInformationBinding.tvFirstName.setError(error.getBillingFirstName().get(0));
        }
        if (error.getBillingMobile()!=null){
            carBillingInformationBinding.tvMobileNumber.setError(error.getBillingMobile().get(0));
        }
        if (error.getBillingAddress()!=null) {
            carBillingInformationBinding.tvAddress.setError(error.getBillingAddress().get(0));
        }
        if (error.getBillingEmail()!=null) {
            carBillingInformationBinding.tvEmail.setError(error.getBillingEmail().get(0));
        }
        if (error.getBillingLastName()!=null) {
            carBillingInformationBinding.tvLastName.setError(error.getBillingLastName().get(0));
        }
    }

    private void sendData(){
        Intent intent = new Intent(context,UploadIdentityActivity.class);
        intent.putExtra(AppConstant.ID,bookingId);
        intent.putExtra(AppConstant.CAR_ID,carId);
        intent.putExtra(AppConstant.BOOKING_FROM_DATE,mPickDate);
        intent.putExtra(AppConstant.BOOKING_FROM_TIME,mPickTime);
        intent.putExtra(AppConstant.BOOKING_TO_DATE,mDropDate);
        intent.putExtra(AppConstant.BOOKING_TO_TIME,mDropTime);
        intent.putExtra(AppConstant.PICKUP_LOCATION,mPickuLocation);
        intent.putExtra(AppConstant.BILLING_FIRST_NAME,carBillingInformationBinding.tvFirstName.getText().toString());
        intent.putExtra(AppConstant.BILLING_LAST_NAME,carBillingInformationBinding.tvLastName.getText().toString());
        intent.putExtra(AppConstant.BILLING_MOBILE,carBillingInformationBinding.countryCodePicker.getSelectedCountryCodeWithPlus()
                +carBillingInformationBinding.tvMobileNumber.getText().toString());
        if (!carBillingInformationBinding.tvAlternateNumber.getText().toString().isEmpty())
            intent.putExtra(AppConstant.BILLING_ALTERNATIVE_MOBILE,carBillingInformationBinding.countryCodePicker.getSelectedCountryCodeWithPlus()
                    +carBillingInformationBinding.tvAlternateNumber.getText().toString());
        intent.putExtra(AppConstant.BILLING_ADDRESS,carBillingInformationBinding.tvAddress.getText().toString());
        intent.putExtra(AppConstant.PRICE_PER_DAY,pricePerDay);
        intent.putExtra(AppConstant.SECURITY_AMOUNT,securityAmt);
        intent.putExtra(AppConstant.BILLING_EMAIL,email);
        intent.putExtra(AppConstant.GRAND_TOTAL,grandTotal);
        intent.putExtra(AppConstant.GRAND_TOTAL_USD,grandTotalUSD);
        startActivity(intent);

    }


    private void validation(){

        fname=carBillingInformationBinding.tvFirstName.getText().toString();
        lname=carBillingInformationBinding.tvLastName.getText().toString();
        phoneNum=carBillingInformationBinding.tvMobileNumber.getText().toString();
        email=carBillingInformationBinding.tvEmail.getText().toString();
        address=carBillingInformationBinding.tvAddress.getText().toString();


        if (TextUtils.isEmpty(fname)){
            carBillingInformationBinding.tvFirstName.setError(getString(R.string.please_enter_first_name));
        }
        else if (!CheckValidation.Companion.isValidName(fname)){
            carBillingInformationBinding.tvFirstName.setError(getString(R.string.please_enter_valid_first_name));
        }
        else if (!TextUtils.isEmpty(lname) && !CheckValidation.Companion.isValidName(lname)){
            carBillingInformationBinding.tvLastName.setError(getString(R.string.please_enter_valid_last_name));
        }
        else if (TextUtils.isEmpty(phoneNum)){
            carBillingInformationBinding.tvMobileNumber.setError(getString(R.string.please_enter_phonenumber));
        }

        else if(!CheckValidation.Companion.isValidMobileNumber(phoneNum)){
            carBillingInformationBinding.tvMobileNumber.setError(getString(R.string.please_enter_correct_phonenumber));
        }
        else if (email.isEmpty()){
            carBillingInformationBinding.tvEmail.setError(getString(R.string.please_enter_email));
        }
        else if (!CheckValidation.Companion.isValidEmail(email)){
            carBillingInformationBinding.tvEmail.setError(getString(R.string.please_enter_correct_email));
        }
        else if (address.isEmpty()){
            carBillingInformationBinding.tvAddress.setError(getString(R.string.billing_address_error));
        }
        else {
            if (CommonUtils.isNetworkAvailable(context))
            callPlaceOrderApi();
            else
                ToastUtil.Companion.showShortToast(context,getString(R.string.failed_internet));
        }

    }



}
