package com.reekar.activity;

import android.content.Context;
import android.os.Bundle;
import android.view.View;

import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.LinearLayoutManager;

import com.reekar.R;
import com.reekar.adapter.MyBookedCarAdapter;
import com.reekar.commonclasses.BaseActivity;
import com.reekar.databinding.ActivityMyBookedCarBinding;

public class MyBookedCarActivity extends BaseActivity {

    private ActivityMyBookedCarBinding myBookedCarBinding;
    private MyBookedCarAdapter myBookedCarAdapter;
    private Context context;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        myBookedCarBinding= DataBindingUtil.setContentView(this,R.layout.activity_my_booked_car);
        context=this;

        setRecyclerViewAdapter();
        setToolbar();
        setListener();
    }

    private void setRecyclerViewAdapter() {
//        myBookedCarAdapter = new MyBookedCarAdapter(context);
//        myBookedCarBinding.recyclerMyBookedCar.setHasFixedSize(true);
//        myBookedCarBinding.recyclerMyBookedCar.setLayoutManager(new LinearLayoutManager(context));
//        myBookedCarBinding.recyclerMyBookedCar.setAdapter(myBookedCarAdapter);

    }

    @Override
    public void setToolbar() {
        myBookedCarBinding.toolbar.tvToolbarBarName.setText(R.string.my_booked_car);
    }

    @Override
    public void setListener() {
        myBookedCarBinding.toolbar.custToolBckBtn.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){

            case R.id.cust_tool_bck_btn:{
                finish();
                break;
            }
        }
    }
}
