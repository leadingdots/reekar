package com.reekar.activity;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;

import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.GridLayoutManager;

import com.akanksha.commonclassutil.PreferencesManager;
import com.akanksha.commonclassutil.ToastUtil;
import com.reekar.R;
import com.reekar.adapter.CarDetailsFeatureAdapter;
import com.reekar.adapter.CarFeatureAdapter;
import com.reekar.adapter.ImageViewPagerAdapter;
import com.reekar.apiservices.ApiUtils;
import com.reekar.commonclasses.AppConstant;
import com.reekar.commonclasses.BaseActivity;
import com.reekar.commonclasses.CommonUtils;
import com.reekar.databinding.ActivityCarDetailsBinding;
import com.reekar.interfaces.ApiResponseInterface;
import com.reekar.models.ApiRequest;
import com.reekar.models.ApiResponse;
import com.reekar.models.CarFeatureModel;
import com.reekar.models.CarModel;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import retrofit2.Response;

public class CarDetailsActivity extends BaseActivity{

    private ActivityCarDetailsBinding carDetailsBinding;
    private Context mContext;
    private ImageViewPagerAdapter imageViewPagerAdapter;
    private List<String> arlCarImage = new ArrayList<>();
    private List<CarFeatureModel> arlCarFeature = new ArrayList<>();

    private CarDetailsFeatureAdapter carDetailsFeatureAdapter;
    private boolean login=false;
    private CarFeatureAdapter carFeatureAdapter;
    private List<String> arlFeature = new ArrayList<>();

    private String mId;
    private String mPickDate;
    private String mPickTime;
    private String mDropDate;
    private String mDropTime;
    private String mAdress;
    private double pricePerDay;
    private double securityAmt;


    private int taxAmt =0;
    private double grandTotal;
    double totalAmount;
    private double grandTotalUSD;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        carDetailsBinding= DataBindingUtil.setContentView(this,R.layout.activity_car_details);
        mContext=this;

        login  = PreferencesManager.Companion.getBooleanPreferences(mContext,AppConstant.IS_LOGIN);

        mGetIntent();
        setAdapter();
        setToolbar();
        setListener();

        if (CommonUtils.isNetworkAvailable(mContext))
            callCarDetailsApi();
        else
            ToastUtil.Companion.showShortToast(mContext,getString(R.string.failed_internet));
    }

    private void mGetIntent() {
        if (getIntent() != null){
            if (getIntent().getStringExtra(AppConstant.CAR_ID) != null){
                mId = getIntent().getStringExtra(AppConstant.CAR_ID);
            }
            if (getIntent().getStringExtra(AppConstant.BOOKING_FROM_DATE) != null) {
                mPickDate = getIntent().getStringExtra(AppConstant.BOOKING_FROM_DATE);
            }
            if (getIntent().getStringExtra(AppConstant.BOOKING_FROM_TIME)!=null){
                mPickTime=getIntent().getStringExtra(AppConstant.BOOKING_FROM_TIME);
            }

            if (getIntent().getStringExtra(AppConstant.BOOKING_TO_DATE) != null) {
                mDropDate = getIntent().getStringExtra(AppConstant.BOOKING_TO_DATE);
            }
            if (getIntent().getStringExtra(AppConstant.BOOKING_TO_TIME)!=null){
                mDropTime=getIntent().getStringExtra(AppConstant.BOOKING_TO_TIME);
            }
            if (getIntent().getStringExtra(AppConstant.PICKUP_LOCATION) != null) {
                mAdress = getIntent().getStringExtra(AppConstant.PICKUP_LOCATION);
            }
        }
    }

    private void setAdapter() {
        imageViewPagerAdapter = new ImageViewPagerAdapter(mContext,arlCarImage, AppConstant.CAR_DETAIL_ACTIVITY);
        carDetailsBinding.viewpagerCarDetails.setAdapter(imageViewPagerAdapter);
        carDetailsBinding.carDetailsDotsIndicator.setupWithViewPager(carDetailsBinding.viewpagerCarDetails);

        carDetailsFeatureAdapter=new CarDetailsFeatureAdapter(mContext,arlCarFeature);
        carDetailsBinding.carDetailFeaturesRecyclerview.setHasFixedSize(true);
        carDetailsBinding.carDetailFeaturesRecyclerview.setLayoutManager(new GridLayoutManager(mContext,2));
        carDetailsBinding.carDetailFeaturesRecyclerview.setAdapter(carDetailsFeatureAdapter);
        carDetailsBinding.carDetailFeaturesRecyclerview.setNestedScrollingEnabled(false);

        carFeatureAdapter =new CarFeatureAdapter(mContext,arlFeature,AppConstant.CAR_DETAIL_ACTIVITY);
        carDetailsBinding.rvCarFeatures.setHasFixedSize(true);
        carDetailsBinding.rvCarFeatures.setLayoutManager(new GridLayoutManager(mContext,4));
        carDetailsBinding.rvCarFeatures.setAdapter(carFeatureAdapter);
        carDetailsBinding.rvCarFeatures.setNestedScrollingEnabled(false);
    }

    @Override
    public void setToolbar() {
        carDetailsBinding.carDetailsToolbar.tvToolbarBarName.setText(R.string.car_information);
    }

    @Override
    public void setListener() {
        carDetailsBinding.carDetailsToolbar.custToolBckBtn.setOnClickListener(this);
        carDetailsBinding.submit.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){

            case R.id.cust_tool_bck_btn:{
                finish();
                break;
            }
            case R.id.submit:{
                if (!PreferencesManager.Companion.getBooleanPreferences(mContext,AppConstant.IS_LOGIN)){
                    Intent intent = new Intent(mContext,MainActivity.class);
                    intent.putExtra(AppConstant.FROM,AppConstant.CAR_DETAIL_ACTIVITY);
                    intent.putExtra(AppConstant.CAR_ID,mId);
                    startActivity(intent);
                }
                else {
                    Intent intent = new Intent(mContext, CarBillingInformationActivity.class);
                    intent.putExtra(AppConstant.CAR_ID,mId);
                    intent.putExtra(AppConstant.BOOKING_FROM_DATE,mPickDate);
                    intent.putExtra(AppConstant.BOOKING_TO_DATE,mDropDate);
                    intent.putExtra(AppConstant.BOOKING_FROM_TIME,mPickTime);
                    intent.putExtra(AppConstant.BOOKING_TO_TIME,mDropTime);
                    intent.putExtra(AppConstant.PICKUP_LOCATION,mAdress);
                    intent.putExtra(AppConstant.PRICE_PER_DAY,pricePerDay);
                    intent.putExtra(AppConstant.SECURITY_AMOUNT,securityAmt);
                    startActivity(intent);
                }
                break;
            }
        }
    }

    private void callCarDetailsApi(){
        CommonUtils.showProgressDialog(mContext);

        ApiRequest apiRequest = new ApiRequest();
        apiRequest.setCarId(mId);

        ApiUtils.callSearchCarDetailsApi(mContext, apiRequest, new ApiResponseInterface() {
            @Override
            public void onResponse(Response<ApiResponse> response) {
                CommonUtils.hideProgressDialog();

                switch (response.code()){
                    case AppConstant.SUCCESS_CODE:
                        if (response.body() != null){
                            if (response.body().getCar() != null){
                                setData(response.body().getCar());

                            }
                        }
                        break;
                    case AppConstant.UNAUTHORIZED_CODE:
                        CommonUtils.navigateToLogin(mContext);
                    default:
                        CommonUtils.showToast(mContext,getString(R.string.something_went_wrong));
                }
            }

            @Override
            public void onFailure() {
                CommonUtils.hideProgressDialog();
                CommonUtils.showToast(mContext,getString(R.string.something_went_wrong));
            }
        });
    }

    @SuppressLint("SetTextI18n")
    private void setData(CarModel cars) {
        carDetailsBinding.llParent.setVisibility(View.VISIBLE);
        if (cars.getBrand() != null) {
            String carName = cars.getBrand() + " " + cars.getModel();
//            carDetailsBinding.tvBrand.setText(cars.getBrand());

            CommonUtils.spannableTextColor(mContext, carName, R.color.colorPrimary, cars.getBrand().length(),
                    carName.length(), carDetailsBinding.tvBrand);
        }
//        String currency = CurrencyJson.updateCurrency(mContext) +" ";
//        if (cars.getCurrency() != null) {
//            currency=cars.getCurrency();
////        }
//        else
//            currency="AED";
        String currency = PreferencesManager.Companion.getStringPreferences(mContext,AppConstant.CURRENT_CURRENCY_SYMBOL);
        if (currency.isEmpty()){
            currency=PreferencesManager.Companion.getStringPreferences(mContext,AppConstant.CURRENT_CURRENCY);
        }


        if (cars.getPricePerDay() != null){
            carDetailsBinding.tvPriceDay.setText(currency+" " + cars.getPricePerDay());
            pricePerDay=cars.getPricePerDay();
        } else
            carDetailsBinding.tvPriceDay.setText(currency+" 0");

        if (cars.getSecurityAmount() != null) {
            carDetailsBinding.tvSecurityAmount.setText(currency+" " +cars.getSecurityAmount());
            securityAmt=cars.getSecurityAmount();
        } else
            carDetailsBinding.tvSecurityAmount.setText(currency+" 0");
        SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy hh:mm a");
        int dayCount=1;
        Date Date1 = null,Date2 = null;

        try{

            Date1 = sdf.parse(mPickDate +" "+mPickTime);
            Date2 = sdf.parse(mDropDate +" "+mDropTime);

//            dayCount = (int) Math.ceil((Date2.getDate() - Date1.getDate())/(1000 * 60 * 60 * 24));
            dayCount = (Date2.getDate() - Date1.getDate());
            dayCount=dayCount+1;

            Log.e("daycount", String.valueOf(dayCount));
        }catch(Exception e)
        {
            e.printStackTrace();
        }
        totalAmount = pricePerDay * dayCount;
        grandTotal = totalAmount + securityAmt + taxAmt;
        carDetailsBinding.tvTotalAmount.setText(currency+" "+String.format("%.2f", grandTotal));
       /* if (cars.getModel() != null)
            carDetailsBinding.tvModel.setText(cars.getModel());*/

        if (cars.getImages() != null && cars.getImages().size()>0){
            arlCarImage.clear();
            arlCarImage.addAll(cars.getImages());
            imageViewPagerAdapter.notifyDataSetChanged();
        }

        arlCarFeature.clear();

        if (cars.getVehicletypeDetail() != null &&cars.getVehicletypeDetail().getName()!=null)
            setFeatureData(cars.getVehicletypeDetail().getName(),getString(R.string.vehicle_type),R.mipmap.car_type);

        if (cars.getEnginetypeDetail() != null && cars.getEnginetypeDetail().getName()!=null)
            setFeatureData(cars.getEnginetypeDetail().getName(),getString(R.string.engine_type),R.mipmap.engine);

        if (cars.getSeatDetail() != null && cars.getSeatDetail().getName()!=null)
            setFeatureData(cars.getSeatDetail().getName(),getString(R.string.seats)+" Persons",R.mipmap.seat_capacity);

        if (cars.getColorDetail() != null && cars.getColorDetail().getName()!=null)
            setFeatureData(cars.getColorDetail().getName(),getString(R.string.vehicle_color),R.mipmap.car_color);
        if (cars.getTransmissionDetail() != null && cars.getTransmissionDetail().getName()!=null)
            setFeatureData(cars.getTransmissionDetail().getName(),getString(R.string.transmission),R.mipmap.transmission);


        carDetailsFeatureAdapter.notifyDataSetChanged();

        if (cars.getFeatures() != null && cars.getFeatures().size()>0){

            arlFeature.addAll(cars.getFeatures());
            carFeatureAdapter.notifyDataSetChanged();
        }

    }

    private void setFeatureData(String detail, String name, int image) {
        CarFeatureModel carFeatureModel = new CarFeatureModel();
        carFeatureModel.setName(name);
        carFeatureModel.setDetail(detail);
        carFeatureModel.setImage(image);
        arlCarFeature.add(carFeatureModel);
    }
}
