package com.reekar.activity;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.view.Gravity;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;

import androidx.annotation.NonNull;
import androidx.core.content.ContextCompat;
import androidx.databinding.DataBindingUtil;

import com.akanksha.commonclassutil.CheckPermission;
import com.akanksha.commonclassutil.ToastUtil;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.libraries.places.api.Places;
import com.google.android.libraries.places.api.model.Place;
import com.google.android.libraries.places.widget.AutocompleteSupportFragment;
import com.google.android.libraries.places.widget.listener.PlaceSelectionListener;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.reekar.R;
import com.reekar.apiservices.ApiUtils;
import com.reekar.commonclasses.AppConstant;
import com.reekar.commonclasses.BaseActivity;
import com.reekar.commonclasses.CommonUtils;
import com.reekar.databinding.ActivityCarRentalLocationBinding;
import com.reekar.interfaces.ApiResponseInterface;
import com.reekar.interfaces.OnLocationChangeInterface;
import com.reekar.location.GPSTrackerNew;
import com.reekar.models.ApiResponse;
import com.reekar.models.CarModel;
import com.reekar.models.SaveCarInfoApiRequest;

import java.lang.reflect.Type;
import java.util.Arrays;
import java.util.Objects;

import retrofit2.Response;

public class CarRentalLocationActivity extends BaseActivity implements OnMapReadyCallback{

    private ActivityCarRentalLocationBinding carRentalLocationBinding;
    private GoogleMap mMap;
    int AUTOCOMPLETE_REQUEST_CODE = 1;

    EditText etPlace;

    private LocationManager locationManager;
    private LocationListener locationListener;
    String provider;

    private Context context;

    private String carId;
    private LatLng latLng;

    private GPSTrackerNew gpsTracker;
    private double lat =0.0;
    private double lng =0.0;

    private ApiResponse apiResponse = new ApiResponse();
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        carRentalLocationBinding= DataBindingUtil.setContentView(this,R.layout.activity_car_rental_location);
        context=this;


        getIntentData();
        setPlaceAutocomplete();
        setToolbar();
        setListener();
    }


    private void getIntentData(){
        Intent intent =getIntent();
        if (intent!=null){
            carId =intent.getStringExtra(AppConstant.CAR_ID);

            if (getIntent().getStringExtra(AppConstant.DATA) != null){
                Type type = new TypeToken<ApiResponse>() {}.getType();
                apiResponse = new Gson().fromJson(getIntent().getStringExtra(AppConstant.DATA),type);
            }


        }
    }
    @Override
    public void setToolbar() {
        carRentalLocationBinding.carrentalLocToolbar.tvToolbarBarName.setText(R.string.car_rental_location);
    }

    @Override
    public void setListener() {
        carRentalLocationBinding.carrentalLocToolbar.custToolBckBtn.setOnClickListener(this);
        carRentalLocationBinding.correctBtn.setOnClickListener(this);

    }
    @Override
    public void onClick(View v) {
        switch (v.getId()){

            case R.id.cust_tool_bck_btn:{
                finish();
                break;
            }
            case R.id.correct_btn:{
                if (CommonUtils.isNetworkAvailable(context))
                callApiSaveCarLocation();
                else
                    ToastUtil.Companion.showShortToast(context,getString(R.string.failed_internet));

//                validation();
//                Intent intent= new Intent(context,CarAvailabilityActivity.class);
//                startActivity(intent);
                break;
            }

        }
    }
    //    private void validation(){
//
//        String locs=etPlace.getText().toString();
//
//        if (locs.isEmpty()){
//            carRentalLocationBinding.correctBtn.setVisibility(View.GONE);
//        }
//        else {
//            carRentalLocationBinding.correctBtn.setVisibility(View.VISIBLE);
//        }
//    }
    private void setPlaceAutocomplete() {
        Places.initialize(getApplicationContext(),getString(R.string.google_maps_key));
        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);


        //===========map external code ===========
        AutocompleteSupportFragment autocompleteFragment = (AutocompleteSupportFragment)
                getSupportFragmentManager().findFragmentById(R.id.autocomplete_fragment);
        ImageView searchIcon = (ImageView)((LinearLayout)autocompleteFragment.getView()).getChildAt(0);
        /*        autocompleteFragment.getView().setBackgroundColor(ContextCompat.getColor(context,R.color.white));*/
        searchIcon.setColorFilter(ContextCompat.getColor(context,R.color.white));
        etPlace =((EditText)autocompleteFragment.getView().findViewById(R.id.places_autocomplete_search_input));
        etPlace.setTextColor(ContextCompat.getColor(context,R.color.white));
        etPlace.setHint("Enter Location");
        etPlace.setTextSize(15.0f);
        etPlace.setTextColor(ContextCompat.getColor(context,R.color.white));
        etPlace.setGravity(Gravity.CENTER_VERTICAL);

        /*  ==returning lat long also to google =====*/
        autocompleteFragment.setPlaceFields(Arrays.asList(Place.Field.ID,Place.Field.NAME,Place.Field.LAT_LNG));
        autocompleteFragment.setOnPlaceSelectedListener(new PlaceSelectionListener() {
            @Override
            public void onPlaceSelected(Place place) {
                setMarker(place.getLatLng());
                zoomToLocation(place.getLatLng());
            }

            @Override
            public void onError(Status status) {

            }
        });
    }

    private void setCurrentLocation(){
        mMap.setMyLocationEnabled(true);
        gpsTracker =new GPSTrackerNew(context, new OnLocationChangeInterface() {
            @Override
            public void onLocationChanged(Location location) {

                latLng =new LatLng(location.getLatitude(),location.getLongitude());
                latLng =new LatLng(location.getLatitude(),location.getLongitude());

                if (latLng==null){
                    carRentalLocationBinding.correctBtn.setVisibility(View.GONE);
                }
                else {
                    carRentalLocationBinding.correctBtn.setVisibility(View.VISIBLE);
                }

                setMarker(latLng);
                zoomToLocation(latLng);
                gpsTracker.stopUsingGPS();

            }
        });
    }

    private void zoomToLocation(LatLng latLng) {
        this.latLng = latLng;
        CameraPosition cameraPosition =new CameraPosition.Builder().target(
                new LatLng(latLng.latitude,latLng.longitude))
                .zoom(10f)
                .build();
        mMap.animateCamera(CameraUpdateFactory.newCameraPosition(cameraPosition));
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;
        mMap.setOnMapLoadedCallback(new GoogleMap.OnMapLoadedCallback() {
            @Override
            public void onMapLoaded() {
                if (CheckPermission.Companion.checkIsMarshMallowVersion()){
                    if (CheckPermission.Companion.checkLocationPermission(context)){
                        setCurrentLocation();
                    } else
                        CheckPermission.Companion.requestLocationPermission((Activity) context);
                } else
                    setCurrentLocation();
            }
        });
        setMapClickListener();

    }

    private void setMapClickListener() {
        mMap.setOnMapClickListener(new GoogleMap.OnMapClickListener() {
            @Override
            public void onMapClick(LatLng latLng) {
                setMarker(latLng);
                zoomToLocation(latLng);
            }
        });
    }



    public void setMarker(LatLng latLng) {
        mMap.clear();
        mMap.addMarker(new MarkerOptions()
                .position(latLng)
                .icon(BitmapDescriptorFactory
                        .fromResource(R.mipmap.icons_map_view_midium)));

        String address= CommonUtils.fetchAddress(context,latLng.latitude,latLng.longitude);

        AutocompleteSupportFragment autocompleteFragment = (AutocompleteSupportFragment)
                getSupportFragmentManager().findFragmentById(R.id.autocomplete_fragment);
        assert autocompleteFragment != null;
        ((EditText) Objects.requireNonNull(autocompleteFragment.getView()).findViewById(R.id.places_autocomplete_search_input)).setText(address);

        if (gpsTracker!=null)
        gpsTracker.stopUsingGPS();
        if (latLng==null){
            carRentalLocationBinding.correctBtn.setVisibility(View.GONE);
        }
        else {
            carRentalLocationBinding.correctBtn.setVisibility(View.VISIBLE);
        }

    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);

        switch (requestCode){

            case CheckPermission.REQUEST_CODE_LOCATION_PERMISSION:{
                if (grantResults.length>0 && grantResults[0]== PackageManager.PERMISSION_GRANTED){
                    setCurrentLocation();
                }
                break;
            }
        }
    }

    private void callApiSaveCarLocation(){
        CommonUtils.showProgressDialog(context);

        SaveCarInfoApiRequest apiRequest=new SaveCarInfoApiRequest();
        apiRequest.setCarId(carId);
        apiRequest.setCarLocation(etPlace.getText().toString());
        apiRequest.setCarLocationLat(String.valueOf(latLng.latitude));
        apiRequest.setCarLocationLong(String.valueOf(latLng.longitude));

        ApiUtils.callSaveCarLocationApi(context, apiRequest, new ApiResponseInterface() {
            @Override
            public void onResponse(Response<ApiResponse> response) {
                CommonUtils.hideProgressDialog();
                if (response.code()==AppConstant.SUCCESS_CODE){

                    if (response.body()!=null){
                        ToastUtil.Companion.showShortToast(context,response.body().getMessage());
                        Intent intent =new Intent(context,CarAvailabilityActivity.class);
                        intent.putExtra(AppConstant.CAR_ID,carId);
                        intent.putExtra(AppConstant.DATA,new Gson().toJson(apiResponse));
                        startActivity(intent);
                    }
                }
            }

            @Override
            public void onFailure() {
                CommonUtils.hideProgressDialog();
            }
        });
    }

    private void setData(){
        if (apiResponse != null){
            if (apiResponse.getCar() != null){
                CarModel cars = apiResponse.getCar();
                if (cars.getCarLocationLat() != null && cars.getCarLocationLong()!=null) {

                    mMap.clear();
                    mMap.addMarker(new MarkerOptions()
                            .position(latLng)
                            .icon(BitmapDescriptorFactory
                                    .fromResource(R.mipmap.icons_map_view_midium)));

                    String address= CommonUtils.fetchAddress(context,latLng.latitude,latLng.longitude);

                    AutocompleteSupportFragment autocompleteFragment = (AutocompleteSupportFragment)
                            getSupportFragmentManager().findFragmentById(R.id.autocomplete_fragment);
                    ((EditText)autocompleteFragment.getView().findViewById(R.id.places_autocomplete_search_input)).setText(address);
                }
            }
        }
    }

}
