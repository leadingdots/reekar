package com.reekar.activity;

import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.DhcpInfo;
import android.net.NetworkInfo;
import android.net.wifi.WifiManager;
import android.os.Bundle;
import android.os.Handler;
import android.text.TextUtils;
import android.util.Log;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import com.akanksha.commonclassutil.LogUtil;
import com.akanksha.commonclassutil.PreferencesManager;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.FirebaseApp;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.InstanceIdResult;
import com.google.firebase.messaging.FirebaseMessaging;
import com.reekar.R;
import com.reekar.apiservices.ApiUtils;
import com.reekar.commonclasses.AppConstant;
import com.reekar.commonclasses.CommonUtils;
import com.reekar.interfaces.ApiResponseInterface;
import com.reekar.models.ApiRequest;
import com.reekar.models.ApiResponse;

import java.math.BigInteger;
import java.net.InetAddress;
import java.net.NetworkInterface;
import java.net.UnknownHostException;
import java.nio.ByteOrder;
import java.util.Collections;
import java.util.List;
import java.util.Objects;

import retrofit2.Response;

public class SplashActivity extends AppCompatActivity {

    private Context mContext;

    private String IPaddress;
    private String myIp;
    Boolean IPValue;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);
        mContext = this;

        CommonUtils.printHashKey(mContext);
//        Log.e("currency",CommonUtils.getCurrentCurrency(mContext));

//        if (TextUtils.isEmpty(PreferencesManager.Companion.getStringPreferences(mContext,AppConstant.CURRENT_LANGUAGE)))
            /*PreferencesManager.Companion.saveStringPreferences(mContext,AppConstant.CURRENT_LANGUAGE, Language.ENGLISH);

        ChangeLanguage.changeLocale(getApplicationContext(),
                PreferencesManager.Companion.getStringPreferences(getApplicationContext(), AppConstant.CURRENT_LANGUAGE));*/
        if (TextUtils.isEmpty(PreferencesManager.Companion.getStringPreferences(mContext,
                AppConstant.DEVICE_TOKEN)))
            generateFcmToken();

        /** Duration of wait **/
        final int SPLASH_DISPLAY_LENGTH = 2000;

        /* New Handler to start the Menu-Activity
         * and close this Splash-Screen after some seconds.*/
        new Handler().postDelayed(new Runnable(){
            @Override
            public void run() {
                /* Create an Intent that will start the Menu-Activity. */
                Intent mainIntent = new Intent(SplashActivity.this, MainActivity.class);
                SplashActivity.this.startActivity(mainIntent);
                SplashActivity.this.finish();
            }
        }, SPLASH_DISPLAY_LENGTH);

        NetwordDetect();
        callApiCurrency();
//        ToastUtil.Companion.showLongToast(this,myIp);


    }

    private void callApiCurrency() {
        PreferencesManager.Companion.saveStringPreferences(SplashActivity.this,AppConstant.CURRENT_CURRENCY,"AED");
        PreferencesManager.Companion.saveStringPreferences(SplashActivity.this,AppConstant.CURRENT_CURRENCY_SYMBOL,"AED");

        ApiRequest apiRequest=new ApiRequest();
//        apiRequest.setMyip("188.247.75.182");
        apiRequest.setMyip(myIp);

        ApiUtils.callCurrencyApi(SplashActivity.this,apiRequest, new ApiResponseInterface() {
            @Override
            public void onResponse(Response<ApiResponse> response) {
                if (response.code()==AppConstant.SUCCESS_CODE){
                    if (response.body().getUserCurrency()!=null)
                        PreferencesManager.Companion.saveStringPreferences(SplashActivity.this,AppConstant.CURRENT_CURRENCY,response.body().getUserCurrency());
                    if (response.body().getUserCurrencySymbol()!=null)
                        PreferencesManager.Companion.saveStringPreferences(SplashActivity.this,AppConstant.CURRENT_CURRENCY_SYMBOL,response.body().getUserCurrencySymbol());

                }
                else {
                    //Toast.makeText(SplashActivity.this,getString(R.string.something_went_wrong),Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure() {
                //Toast.makeText(SplashActivity.this,getString(R.string.something_went_wrong),Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void generateFcmToken() {
        FirebaseApp.initializeApp(mContext);
        FirebaseInstanceId.getInstance().getInstanceId()
                .addOnCompleteListener(new OnCompleteListener<InstanceIdResult>() {
                    @Override
                    public void onComplete(@NonNull Task<InstanceIdResult> task) {
                        if (task.isSuccessful()) {
                            String token = Objects.requireNonNull(task.getResult()).getToken();
                            PreferencesManager.Companion.saveStringPreferences(mContext, AppConstant.DEVICE_TOKEN,token);
                        }
                    }
                });

        FirebaseMessaging.getInstance().subscribeToTopic("reekar_all")
                .addOnCompleteListener(new OnCompleteListener<Void>() {
                    @Override
                    public void onComplete(@NonNull Task<Void> task) {

                    }
                });
        LogUtil.Companion.errorLog("token",PreferencesManager.Companion.getStringPreferences(mContext, AppConstant.DEVICE_TOKEN));
    }

    //Check the internet connection.
    private void NetwordDetect() {

        boolean WIFI = false;

        boolean MOBILE = false;

        ConnectivityManager CM = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);

        NetworkInfo[] networkInfo = CM.getAllNetworkInfo();

        for (NetworkInfo netInfo : networkInfo) {

            if (netInfo.getTypeName().equalsIgnoreCase("WIFI"))

                if (netInfo.isConnected())

                    WIFI = true;

            if (netInfo.getTypeName().equalsIgnoreCase("MOBILE"))

                if (netInfo.isConnected())

                    MOBILE = true;
        }

        if(WIFI == true)

        {
//            IPaddress = GetDeviceipWiFiData();
            IPaddress = CommonUtils.getIPAddress1(false);
//            textview.setText(IPaddress);
            myIp=IPaddress;


        }

        if(MOBILE == true)
        {

//            IPaddress = GetDeviceipMobileData();
            IPaddress = CommonUtils.getIPAddress1(false);
            myIp=IPaddress;
//            textview.setText(IPaddress);

        }

    }

    public String GetDeviceipMobileData(){
        try {
            List<NetworkInterface> interfaces = Collections.list(NetworkInterface.getNetworkInterfaces());
            for (NetworkInterface intf : interfaces) {
                List<InetAddress> addrs = Collections.list(intf.getInetAddresses());
                for (InetAddress addr : addrs) {
                    if (!addr.isLoopbackAddress()) {
                        return  addr.getHostAddress();
                    }
                }
            }
        } catch (Exception ex) { } // for now eat exceptions
        return "";
    }

    public String GetDeviceipWiFiData()
    {
        final WifiManager manager = (WifiManager) mContext.getSystemService(WIFI_SERVICE);
        final DhcpInfo dhcp = manager.getDhcpInfo();
        int ipAddress = dhcp.gateway;
        ipAddress = (ByteOrder.nativeOrder().equals(ByteOrder.LITTLE_ENDIAN)) ?
                Integer.reverseBytes(ipAddress) : ipAddress;
        byte[] ipAddressByte = BigInteger.valueOf(ipAddress).toByteArray();
        try {
            InetAddress myAddr = InetAddress.getByAddress(ipAddressByte);
            return myAddr.getHostAddress();
        } catch (UnknownHostException e) {
            // TODO Auto-generated catch block
            Log.e("Wifi Class", "Error getting Hotspot IP address ", e);
        }
        return "null";

    }

}
