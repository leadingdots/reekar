package com.reekar.activity;

import androidx.core.content.ContextCompat;
import androidx.databinding.DataBindingUtil;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.Bundle;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.style.ForegroundColorSpan;
import android.view.View;

import com.akanksha.commonclassutil.PreferencesManager;
import com.reekar.R;
import com.reekar.commonclasses.AppConstant;
import com.reekar.commonclasses.BaseActivity;
import com.reekar.commonclasses.CommonUtils;
import com.reekar.databinding.ActivityVerifySuccessBinding;
import com.reekar.fragment.PaymentOptionsFragment;

public class VerifySuccessActivity extends BaseActivity {

    private ActivityVerifySuccessBinding verifiSuccessBinding;

    private Context context;
    private String email;
//    private Boolean login=true;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        context =this;

        verifiSuccessBinding = DataBindingUtil.setContentView(this,R.layout.activity_verify_success);
        PreferencesManager.Companion.saveBooleanPreferences(context,AppConstant.IS_LOGIN,true);

        getIntentData();
        spanUsername();
        setListener();
    }

    private void spanUsername() {
        final SpannableString text = new SpannableString(email+",verified");
        text.setSpan(new ForegroundColorSpan(ContextCompat.getColor(context,R.color.yellow)), 0, email.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
        verifiSuccessBinding.usernameInOtp.setText(text);
    }
    private void getIntentData(){
        Intent intent = getIntent();
        if (intent!=null) {
            email = intent.getStringExtra(AppConstant.USER_EMAIL);
        }
    }
    @Override
    public void setToolbar() {

    }

    @Override
    public void setListener() {
        verifiSuccessBinding.gotoPayment.setOnClickListener(this);
    }

    @Override   
    public void onClick(View v) {

        switch (v.getId()){

            case R.id.goto_payment:
            {
                Intent intent = new Intent(this,MainActivity.class);
                intent.putExtra(AppConstant.FROM,AppConstant.VERIFY_ACTIVITY);
                startActivity(intent);
                finishAffinity();
                break;
            }

        }
    }
}
