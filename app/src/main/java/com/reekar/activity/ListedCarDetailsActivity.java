package com.reekar.activity;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;

import androidx.core.content.ContextCompat;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.GridLayoutManager;

import com.akanksha.commonclassutil.PreferencesManager;
import com.akanksha.commonclassutil.ToastUtil;
import com.google.gson.Gson;
import com.reekar.R;
import com.reekar.adapter.CarDetailsFeatureAdapter;
import com.reekar.adapter.CarFeatureAdapter;
import com.reekar.adapter.CarImageViewPagerAdapter;
import com.reekar.apiservices.ApiUtils;
import com.reekar.commonclasses.AppConstant;
import com.reekar.commonclasses.BaseActivity;
import com.reekar.commonclasses.CommonUtils;
import com.reekar.commonclasses.CurrencyJson;
import com.reekar.commonclasses.TimeUtil;
import com.reekar.databinding.ActivityListedCarDetailsBinding;
import com.reekar.interfaces.ApiResponseInterface;
import com.reekar.interfaces.OkCancelInterface;
import com.reekar.models.ApiRequest;
import com.reekar.models.ApiResponse;
import com.reekar.models.CarFeatureModel;
import com.reekar.models.CarImageModel;
import com.reekar.models.CarModel;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Response;

public class ListedCarDetailsActivity extends BaseActivity {

    private ActivityListedCarDetailsBinding listedCarDetailsBinding;
    private Context context;
    private String mId;

    private CarImageViewPagerAdapter imageViewPagerAdapter;
    private CarDetailsFeatureAdapter carDetailsFeatureAdapter;
    private List<CarFeatureModel> arlCarFeature = new ArrayList<>();
    private List<CarImageModel> arlCarImage = new ArrayList<>();
    private CarFeatureAdapter carFeatureAdapter;
    private List<String> arlFeature = new ArrayList<>();
    private ApiResponse apiResponse = new ApiResponse();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        listedCarDetailsBinding= DataBindingUtil.setContentView(this, R.layout.activity_listed_car_details);
        context=this;


        mGetIntent();

        setRecyclerAdapter();
        setToolbar();
        setListener();

        if (CommonUtils.isNetworkAvailable(context))
            callCarDetailsApi();
        else
            ToastUtil.Companion.showShortToast(context,getString(R.string.failed_internet));
    }

    private void mGetIntent() {
        if (getIntent() != null){
            if (getIntent().getStringExtra(AppConstant.ID) != null){
                mId = getIntent().getStringExtra(AppConstant.ID);
            }
        }
    }


    private void setRecyclerAdapter() {
        imageViewPagerAdapter = new CarImageViewPagerAdapter(context,arlCarImage, AppConstant.CAR_DETAIL_ACTIVITY);
        listedCarDetailsBinding.viewpagerCarDetails.setAdapter(imageViewPagerAdapter);
        listedCarDetailsBinding.carDetailsDotsIndicator.setupWithViewPager(listedCarDetailsBinding.viewpagerCarDetails);


        carDetailsFeatureAdapter=new CarDetailsFeatureAdapter(context,arlCarFeature);
        listedCarDetailsBinding.carDetailFeaturesRecyclerview.setHasFixedSize(true);
        listedCarDetailsBinding.carDetailFeaturesRecyclerview.setLayoutManager(new GridLayoutManager(context,2));
        listedCarDetailsBinding.carDetailFeaturesRecyclerview.setAdapter(carDetailsFeatureAdapter);
        listedCarDetailsBinding.carDetailFeaturesRecyclerview.setNestedScrollingEnabled(false);

        carFeatureAdapter =new CarFeatureAdapter(context,arlFeature,AppConstant.CAR_DETAIL_ACTIVITY);
        listedCarDetailsBinding.rvCarFeatures.setHasFixedSize(true);
        listedCarDetailsBinding.rvCarFeatures.setLayoutManager(new GridLayoutManager(context,4));
        listedCarDetailsBinding.rvCarFeatures.setAdapter(carFeatureAdapter);
        listedCarDetailsBinding.rvCarFeatures.setNestedScrollingEnabled(false);
    }
    @Override
    public void setToolbar() {
        listedCarDetailsBinding.toolbar.tvToolbarBarName.setText(R.string.listed_car_detail);
    }

    @Override
    public void setListener() {
        listedCarDetailsBinding.toolbar.custToolBckBtn.setOnClickListener(this);
        listedCarDetailsBinding.submit.setOnClickListener(this);
        listedCarDetailsBinding.tvEdit.setOnClickListener(this);
        listedCarDetailsBinding.tvDelete.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){

            case R.id.cust_tool_bck_btn:{
                finish();
                break;
            }
            case R.id.submit:{
                Intent intent = new Intent(context,MainActivity.class);
                startActivity(intent);
                break;
            }
            case R.id.tv_edit:{
                Intent intent = new Intent(context,CarInfromationActivity.class);
                intent.putExtra(AppConstant.DATA,new Gson().toJson(apiResponse));
                intent.putExtra(AppConstant.CAR_ID,mId);
                intent.putExtra(AppConstant.FROM,AppConstant.LISTED_CAR_DETAILS_ACTIVITY);
                startActivity(intent);
                break;
            }
            case R.id.tv_delete:{
                CommonUtils.showAlertDailog(context, getString(R.string.delete), getString(R.string.delete_message), getString(R.string.ok), getString(R.string.canle),
                        new OkCancelInterface() {
                            @Override
                            public void onOkClick() {
                                if (CommonUtils.isNetworkAvailable(context))
                                callCarDeleteApi();
                                else
                                    ToastUtil.Companion.showShortToast(context,getString(R.string.failed_internet));
                            }

                            @Override
                            public void onCancelClick() {

                            }
                        });
                break;
            }

        }
    }

    private void callCarDetailsApi(){
        CommonUtils.showProgressDialog(context);

        ApiRequest apiRequest = new ApiRequest();
        apiRequest.setCarId(mId);

        ApiUtils.callCarDetailsApi(context, apiRequest, new ApiResponseInterface() {
            @Override
            public void onResponse(Response<ApiResponse> response) {
                CommonUtils.hideProgressDialog();

                switch (response.code()){
                    case AppConstant.SUCCESS_CODE:
                        if (response.body() != null){
                            apiResponse = response.body();
                            if (response.body().getCar() != null){
                                setData(response.body().getCar());
                            }
                        }
                        break;
                    case AppConstant.UNAUTHORIZED_CODE:
                        CommonUtils.navigateToLogin(context);
                    default:
                        CommonUtils.showToast(context,getString(R.string.something_went_wrong));
                }
            }

            @Override
            public void onFailure() {
                CommonUtils.hideProgressDialog();
                CommonUtils.showToast(context,getString(R.string.something_went_wrong));
            }
        });
    }

    @SuppressLint("SetTextI18n")
    private void setData(CarModel cars) {
        listedCarDetailsBinding.llParent.setVisibility(View.VISIBLE);
        if (cars.getBrandDetail() != null) {
            String carName = cars.getBrandDetail().getName()+" "+cars.getModelDetail().getName();
//            listedCarDetailsBinding.tvBrand.setText(cars.getBrand());
            CommonUtils.spannableTextColor(context,carName,R.color.colorPrimary,cars.getBrandDetail().getName().length(),
                    carName.length(),listedCarDetailsBinding.tvBrand);
        }
//        String currency = CurrencyJson.updateCurrency(context)+" ";
//        if (cars.getCurrency() != null){
//
//            switch (cars.getCurrency()){
//                case AppConstant.USD:
//                    currency = "$";
//                    break;
//                case AppConstant.EURO:
//                    currency = "€";
//                    break;
//            }
//        }
        String currency = PreferencesManager.Companion.getStringPreferences(context,AppConstant.CURRENT_CURRENCY_SYMBOL);
        if (currency.isEmpty()){
            currency=PreferencesManager.Companion.getStringPreferences(context,AppConstant.CURRENT_CURRENCY);
        }


        if (cars.getPricePerDay() != null)
            listedCarDetailsBinding.tvPriceDay.setText(currency+" "+ cars.getPricePerDay());

        if (cars.getSecurityAmount() != null)
            listedCarDetailsBinding.tvSecurityAmount.setText(currency+" "+cars.getSecurityAmount());

        if (cars.getAvailableFromDate() != null) {
            listedCarDetailsBinding.tvAvailFrom.setText(getString(R.string.available_from)+": " + TimeUtil.UtcToLocal(cars.getAvailableFromDate(), "dd/MM/yyyy hh:mm a", context));
            CommonUtils.spannableTextColor(context,listedCarDetailsBinding.tvAvailFrom.getText().toString(), R.color.black, 0, 16, listedCarDetailsBinding.tvAvailFrom);
        }
        if (cars.getAvailableToDate()!=null) {
            listedCarDetailsBinding.tvAvailTo.setText(getString(R.string.available_to)+": " + TimeUtil.UtcToLocal(cars.getAvailableToDate(), "dd/MM/yyyy hh:mm a", context));
            CommonUtils.spannableTextColor(context,listedCarDetailsBinding.tvAvailTo.getText().toString(), R.color.black, 0, 14, listedCarDetailsBinding.tvAvailTo);
        }
        if (cars.getLicensed()==1){
            listedCarDetailsBinding.tvLicenseYn.setText(getString(R.string.yes));
        }
        if (cars.getLicensed()==0){
            listedCarDetailsBinding.tvLicenseYn.setText(getString(R.string.no));
        }


            /* if (cars.getModel() != null)
            listedCarDetailsBinding.tvModel.setText(cars.getModel());*/
        if (cars.getCarImages() != null && cars.getCarImages().size()>0){
            listedCarDetailsBinding.llViewpager.setVisibility(View.VISIBLE);

            arlCarImage.clear();
            arlCarImage.addAll(cars.getCarImages());
            imageViewPagerAdapter.notifyDataSetChanged();
        }
        else {
            listedCarDetailsBinding.llViewpager.setVisibility(View.GONE);
        }
        arlCarFeature.clear();
        if (cars.getVehicletypeDetail() != null && cars.getVehicletypeDetail().getName()!=null)
            setFeatureData(cars.getVehicletypeDetail().getName(),getString(R.string.vehicle_type),R.mipmap.car_type);

        if (cars.getEnginetypeDetail() != null && cars.getEnginetypeDetail().getName()!=null)
            setFeatureData(cars.getEnginetypeDetail().getName(),getString(R.string.engine_type),R.mipmap.engine);

        if (cars.getSeatDetail() != null && cars.getSeatDetail().getName()!=null)
            setFeatureData(cars.getSeatDetail().getName(),getString(R.string.seats)+" Persons",R.mipmap.seat_capacity);

        if (cars.getColorDetail() != null && cars.getColorDetail().getName()!=null)
            setFeatureData(cars.getColorDetail().getName(),getString(R.string.vehicle_color),R.mipmap.car_color);
        if (cars.getTransmissionDetail() != null && cars.getTransmissionDetail().getName()!=null)
            setFeatureData(cars.getTransmissionDetail().getName(),getString(R.string.transmission),R.mipmap.transmission);
        if (cars.getCruiseControlDetail() != null && cars.getCruiseControlDetail().getName()!=null)
            setFeatureData(cars.getCruiseControlDetail().getName(),getString(R.string.cruise_control),R.drawable.cruise_control);


        carDetailsFeatureAdapter.notifyDataSetChanged();
        if (cars.getFeatures() != null && cars.getFeatures().size()>0){
            listedCarDetailsBinding.tvTitleFeature.setVisibility(View.VISIBLE);

            arlFeature.addAll(cars.getFeatures());
            carFeatureAdapter.notifyDataSetChanged();
        }
        else {
            listedCarDetailsBinding.tvTitleFeature.setVisibility(View.GONE);
        }
        if (cars.getRejectReason() != null) {
//            holder.myListingCarItemBinding.tvCarReject.setText(arlCar.get(position).getRejectReason());
            String text = "Reason for rejection: "+ cars.getRejectReason();
            CommonUtils.spannableTextColor(context,text, R.color.black, 0, 21, listedCarDetailsBinding.tvCarReject);
        }
        switch (cars.getListingStatus()){
            case 1:
                listedCarDetailsBinding.tvStatus.setText(context.getString(R.string.approved));
                listedCarDetailsBinding.tvStatus.setBackground(ContextCompat.getDrawable(context,R.drawable.car_approved_shape));
                if (cars.isRented()){
                    listedCarDetailsBinding.llEdit.setVisibility(View.GONE);
                } else
                    listedCarDetailsBinding.llEdit.setVisibility(View.VISIBLE);
                break;
            case 2:
                listedCarDetailsBinding.tvCarReject.setVisibility(View.VISIBLE);
                listedCarDetailsBinding.tvStatus.setText(context.getString(R.string.rejected));
                listedCarDetailsBinding.tvStatus.setBackground(ContextCompat.getDrawable(context,R.drawable.car_reject_shape));
                break;
            case 0:
                listedCarDetailsBinding.tvStatus.setText(context.getString(R.string.pending));
                listedCarDetailsBinding.tvStatus.setBackground(ContextCompat.getDrawable(context,R.drawable.car_pending_shape));
                break;
        }

    }

    private void setFeatureData(String detail, String name, int image) {
        CarFeatureModel carFeatureModel = new CarFeatureModel();
        carFeatureModel.setName(name);
        carFeatureModel.setDetail(detail);
        carFeatureModel.setImage(image);
        arlCarFeature.add(carFeatureModel);
    }
    private void callCarDeleteApi(){
        CommonUtils.showProgressDialog(context);

        ApiRequest apiRequest = new ApiRequest();
        apiRequest.setCarId(mId);

        ApiUtils.callCarDeleteApi(context, apiRequest, new ApiResponseInterface() {
            @Override
            public void onResponse(Response<ApiResponse> response) {
                CommonUtils.hideProgressDialog();

                switch (response.code()){
                    case AppConstant.SUCCESS_CODE:
                        if (response.body() != null){
                            if (response.body().getMessage() != null){
                                ToastUtil.Companion.showShortToast(context,response.body().getMessage());
                                setResult(RESULT_OK);
                                finish();
                            }
                        }
                        break;
                    case AppConstant.UNAUTHORIZED_CODE:
                        CommonUtils.navigateToLogin(context);
                    default:
                        CommonUtils.showToast(context,getString(R.string.something_went_wrong));
                }
            }

            @Override
            public void onFailure() {
                CommonUtils.hideProgressDialog();
                CommonUtils.showToast(context,getString(R.string.something_went_wrong));
            }
        });
    }

}
