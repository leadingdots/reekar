package com.reekar.activity;

import android.os.Bundle;
import android.view.View;

import androidx.databinding.DataBindingUtil;

import com.reekar.R;
import com.reekar.adapter.TabViewPagerAdapter;
import com.reekar.commonclasses.BaseActivity;
import com.reekar.databinding.ActivityHowItWorksBinding;
import com.reekar.fragment.BookCarAppWorksFragment;
import com.reekar.fragment.ListCarAppWorksFragment;

import static androidx.fragment.app.FragmentStatePagerAdapter.BEHAVIOR_RESUME_ONLY_CURRENT_FRAGMENT;

public class HowItWorksActivity extends BaseActivity {

    private ActivityHowItWorksBinding binding;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this,R.layout.activity_how_it_works);

        setAdapter();
        setToolbar();
        setListener();
    }

    private void setAdapter() {
        TabViewPagerAdapter adapter = new  TabViewPagerAdapter(getSupportFragmentManager(),BEHAVIOR_RESUME_ONLY_CURRENT_FRAGMENT);

        adapter.addFrag(new BookCarAppWorksFragment(),getString(R.string.booking_a_car));
        adapter.addFrag(new ListCarAppWorksFragment(),getString(R.string.list_your_car));

        binding.vpAppWorking.setAdapter(adapter);
        binding.tabHeader.setupWithViewPager(binding.vpAppWorking);
    }

    @Override
    public void setToolbar() {
        binding.toolbar.tvToolbarBarName.setText(getString(R.string.how_it_works));
    }

    @Override
    public void setListener() {
        binding.toolbar.custToolBckBtn.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
        if (view.getId() == R.id.cust_tool_bck_btn){
            finish();
        }
    }
}
