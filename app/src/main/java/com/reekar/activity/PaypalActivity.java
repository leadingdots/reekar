package com.reekar.activity;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;

import com.akanksha.commonclassutil.ToastUtil;
import com.paypal.android.sdk.payments.PayPalConfiguration;
import com.paypal.android.sdk.payments.PayPalPayment;
import com.paypal.android.sdk.payments.PayPalPaymentDetails;
import com.paypal.android.sdk.payments.PayPalService;
import com.paypal.android.sdk.payments.PaymentActivity;
import com.paypal.android.sdk.payments.PaymentConfirmation;
import com.reekar.R;
import com.reekar.commonclasses.AppConstant;
import com.reekar.commonclasses.BaseActivity;
import com.reekar.databinding.ActivityPaypalBinding;
import com.reekar.paymentmethod.PayPalConfig;
import com.reekar.paymentmethod.PaymentDetailsActivity;

import org.json.JSONException;

import java.math.BigDecimal;

public class PaypalActivity extends BaseActivity {

    private ActivityPaypalBinding paypalBinding;
    private Context context;

    public static final int PAYPAL_REQUEST_CODE = 123;

    private static PayPalConfiguration payPalConfiguration =new PayPalConfiguration()
            .environment(PayPalConfiguration.ENVIRONMENT_SANDBOX)
            .clientId(PayPalConfig.PAYPAL_CLIENT_ID);

    private String amount="";
    private String grandTotalAmt;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        context=this;
        paypalBinding = DataBindingUtil.setContentView(this,R.layout.activity_paypal);

        getIntentData();

        setToolbar();
        setListener();
        setData();
    }

    private void getIntentData() {
        Intent intent =getIntent();
        if (intent!=null){
            grandTotalAmt= String.valueOf(intent.getDoubleExtra(AppConstant.GRAND_TOTAL,0));
        }
    }

    private void setData() {
//        paypalBinding.etPaypal.setText("$"+grandTotalAmt);

    }
    @Override
    public void setToolbar() {
       paypalBinding.paypalToolbar.tvToolbarBarName.setText(R.string.paypal);
    }

    @Override
    public void setListener() {
        paypalBinding.paypalToolbar.custToolBckBtn.setOnClickListener(this);
        paypalBinding.submitPaypalBtn.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {

        switch (v.getId()){

            case R.id.cust_tool_bck_btn:{
                finish();
                break;
            }
            case R.id.submit_paypal_btn:{
                finish();
                break;
            }

        }
    }

    private void processPayment(){
        amount=paypalBinding.etPaypal.getText().toString();
        PayPalPayment payPalPayment=new PayPalPayment(new BigDecimal(String.valueOf(amount)),"USD","Pay to Reekar",
        PayPalPayment.PAYMENT_INTENT_SALE);

        Intent intent =new Intent(context,PaymentActivity.class);
        intent.putExtra(PayPalService.EXTRA_PAYPAL_CONFIGURATION,payPalConfiguration);
        intent.putExtra(PaymentActivity.EXTRA_PAYMENT,payPalPayment);
        startActivityForResult(intent,PAYPAL_REQUEST_CODE);

    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        stopService(new Intent(context,PayPalService.class));
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode==PAYPAL_REQUEST_CODE){

            if (resultCode==RESULT_OK){
                PaymentConfirmation paymentConfirmation =data.getParcelableExtra(PaymentActivity.EXTRA_RESULT_CONFIRMATION);
                if (paymentConfirmation!=null){
                    try {

                        String paymentDetails= paymentConfirmation.toJSONObject().toString(4);
                        Intent intent =new Intent();
                        intent.putExtra(AppConstant.PAYMENT_DETAILS,paymentDetails);
                        setResult(RESULT_OK,intent);
                        finish();
//                        startActivity(new Intent(context, PaymentDetailsActivity.class)
//                        .putExtra(AppConstant.PAYMENT_DETAILS,paymentDetails)
//                        .putExtra(AppConstant.PAYMENT_AMOUNT,amount)
//                        );
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            }
            else if (requestCode==RESULT_CANCELED){
                ToastUtil.Companion.showShortToast(context,getString(R.string.cancel));
            }

        }

        if (resultCode==PaymentActivity.RESULT_EXTRAS_INVALID){
            ToastUtil.Companion.showShortToast(context,"Invalid");
        }

    }
}
