package com.reekar.activity;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;

import android.app.DatePickerDialog;
import android.bluetooth.BluetoothAssignedNumbers;
import android.content.Context;
import android.content.DialogInterface;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.View;
import android.widget.Button;
import android.widget.DatePicker;

import com.reekar.R;
import com.reekar.apiservices.ApiUtils;
import com.reekar.commonclasses.AppConstant;
import com.reekar.commonclasses.BaseActivity;
import com.reekar.commonclasses.CommonUtils;
import com.reekar.commonclasses.ErrorUtils;
import com.reekar.commonclasses.MonthYearPickerDialog;
import com.reekar.databinding.ActivityCreditCardBinding;
import com.reekar.interfaces.ApiResponseInterface;
import com.reekar.models.ApiRequest;
import com.reekar.models.ApiResponse;
import com.reekar.models.ErrorModel;
import com.reekar.translation.ChangeLanguage;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import retrofit2.Response;

public class CreditCardActivity extends BaseActivity {

    private ActivityCreditCardBinding creditCardBinding;

    private DatePickerDialog.OnDateSetListener mDateSetListener;

    private Context context;

    private String monthYearStr;
    SimpleDateFormat sdf = new SimpleDateFormat("MMM/yyyy");
    SimpleDateFormat input = new SimpleDateFormat("yyyy-MM-dd");

    private String cardnumber;
    private String cvv;
    private String nameOnCard;
    private String expdate;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        context=this;
        creditCardBinding = DataBindingUtil.setContentView(this,R.layout.activity_credit_card);

        setToolbar();
        setListener();
    }

    @Override
    public void setToolbar() {
        creditCardBinding.creditCardToolbar.tvToolbarBarName.setText(R.string.cc_avenue);
    }

    @Override
    public void setListener() {
        creditCardBinding.creditCardToolbar.custToolBckBtn.setOnClickListener(this);
        creditCardBinding.expiryDatePicker.setOnClickListener(this);
        creditCardBinding.submitCcDetailsBtn.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {

        switch (v.getId()){

            case R.id.cust_tool_bck_btn:
            {
                finish();
                break;
            }
            case R.id.expiry_date_picker:
            {
                datePicker();
                break;
            }
            case R.id.submit_cc_details_btn:
            {
                finish();
                //validation();
                break;
            }

        }
    }

    private void validation() {

        cardnumber =creditCardBinding.cardNumber.getText().toString();
        cvv =creditCardBinding.cardCvv.getText().toString();
        nameOnCard=creditCardBinding.cardCvv.getText().toString();
        expdate=creditCardBinding.expiryDatePicker.getText().toString();

        if (cardnumber.isEmpty()){
            creditCardBinding.cardNumber.setError(getString(R.string.enter_card_number));
            creditCardBinding.cardNumber.requestFocus();
        }
        else if (cardnumber.length()<16){
            creditCardBinding.cardNumber.setError(getString(R.string.enter_correct_card_number));
            creditCardBinding.cardNumber.requestFocus();
        }
        else if (expdate.isEmpty()){
            creditCardBinding.expiryDatePicker.setError(getString(R.string.enter_expiry_date));
            creditCardBinding.expiryDatePicker.requestFocus();
        }

        else if (cvv.isEmpty()){
            creditCardBinding.cardCvv.setError(getString(R.string.enter_cvv_number));
            creditCardBinding.cardCvv.requestFocus();
        }
        else if (cvv.length()<3){
            creditCardBinding.cardCvv.setError(getString(R.string.enter_correct_cvv_number));
            creditCardBinding.cardCvv.requestFocus();
        }
        else if (nameOnCard.isEmpty()){
            creditCardBinding.nameOnCard.setError(getString(R.string.enter_same_name_on_card));
            creditCardBinding.nameOnCard.requestFocus();
        }

        else {
           callApiAddPaymentMethod();
        }
    }

    public void datePicker(){
        MonthYearPickerDialog pickerDialog = new MonthYearPickerDialog();
        pickerDialog.setListener(new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker datePicker, int year, int month, int i2) {
                monthYearStr = year + "-" + (month + 1) + "-" + i2;
                creditCardBinding.expiryDatePicker.setText(formatMonthYear(monthYearStr));
            }
        });

        pickerDialog.show(getSupportFragmentManager(), "MonthYearPickerDialog");
    }

    String formatMonthYear(String str) {
        Date date = null;
        try {
            date = input.parse(str);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return sdf.format(date);
    }

    private void callApiAddPaymentMethod(){
        CommonUtils.showProgressDialog(context);
        ApiRequest apiRequest = new ApiRequest();
        apiRequest.setDevice_token(AppConstant.DEVICE_TOKEN);
        apiRequest.setType(AppConstant.CC);
        apiRequest.setCard_no(creditCardBinding.cardNumber.getText().toString().trim());
        apiRequest.setExp_date(CommonUtils.changeDateFormat(creditCardBinding.expiryDatePicker.getText().toString(),"MMM/yyyy","MM/yy"));
        apiRequest.setName_on_card(creditCardBinding.nameOnCard.getText().toString().trim());
        apiRequest.setCvv(creditCardBinding.cardCvv.getText().toString().trim());

        ApiUtils.callApiSavePaymentMethod(context, apiRequest, new ApiResponseInterface() {
            @Override
            public void onResponse(Response<ApiResponse> response) {
                CommonUtils.hideProgressDialog();
                if (response.code()==AppConstant.SUCCESS_CODE){

                    CommonUtils.showToast(context, ChangeLanguage.translate(context,response.body().getMessage()));
                    finish();
                }
                else if (response.code()==AppConstant.VALIDATION_ERROR_CODE){
                    parseError(response);
                }
                else {
                    CommonUtils.showToast(context,getString(R.string.something_went_wrong));
                }
            }

            @Override
            public void onFailure() {
                CommonUtils.hideProgressDialog();
            }
        });
    }

    private void parseError(Response<ApiResponse> response) {
        if (response.errorBody()!=null){
            try {
                ApiResponse error = ErrorUtils.parseError(response,context);
                if (error!=null && error.getError()!=null){
                    showError(error.getError());
                }
            }
            catch (Exception e){

            }
        }
    }

    private void showError(ErrorModel error) {
        if (error.getCardNo()!=null){
            creditCardBinding.cardNumber.setError(ChangeLanguage.translate(context,error.getCardNo().get(0)));
        }
        if (error.getNameOnCard()!=null){
            creditCardBinding.nameOnCard.setError(ChangeLanguage.translate(context,error.getNameOnCard().get(0)));
        }
        if (error.getCvv()!=null){
            creditCardBinding.cardCvv.setError(ChangeLanguage.translate(context,error.getCvv().get(0)));
        }

    }


}
