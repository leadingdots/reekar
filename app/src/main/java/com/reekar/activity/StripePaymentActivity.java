package com.reekar.activity;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import com.akanksha.commonclassutil.ToastUtil;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;
import com.reekar.R;
import com.reekar.apiservices.ApiUtils;
import com.reekar.commonclasses.AppConstant;
import com.reekar.commonclasses.BaseActivity;
import com.reekar.commonclasses.CommonUtils;
import com.reekar.databinding.ActivityStripePaymentBinding;
import com.reekar.interfaces.ApiResponseInterface;
import com.reekar.models.ApiRequest;
import com.reekar.models.ApiResponse;
import com.stripe.android.ApiResultCallback;
import com.stripe.android.PaymentIntentResult;
import com.stripe.android.Stripe;
import com.stripe.android.model.ConfirmPaymentIntentParams;
import com.stripe.android.model.PaymentIntent;
import com.stripe.android.model.PaymentMethodCreateParams;
import com.stripe.android.view.CardInputWidget;

import java.io.IOException;
import java.lang.ref.WeakReference;
import java.lang.reflect.Type;
import java.util.Map;
import java.util.Objects;
import java.util.TimeZone;

import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.MediaType;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

public class StripePaymentActivity extends BaseActivity {

    private ActivityStripePaymentBinding binding;
    private static final String BACKEND_URL = "http://192.168.1.97/reecar/api/v1/auth/Payment-Intent";

    private OkHttpClient httpClient = new OkHttpClient();
    private String paymentIntentClientSecret ="pi_1GLpZWDdXbCk904Uaeg0faQO_secret_spMrt2twRKfwn2QogEavy8wDR";
    private String stripePublishableKey;
    private Stripe stripe;

    private Context context;

    private String bookingId;
    private String paymentType;
    private double grandTotalUSD;
    private String txtId;

    private String carId;
    private String mPickDate;
    private String mDropDate;
    private String mPickuLocation;
    private int taxAmt =0;
    private String totalAmt;
    private String fname;
    private String lname;
    private String ccode;
    private String phoneNum;
    private String alternativeNum;
    private String address;
    private double pricePerDay;
    private double securityAmt;
    private double grandTotalAmt;
    private String email;
    private static String paymentId;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding= DataBindingUtil.setContentView(this,R.layout.activity_stripe_payment);
        context=this;

        getIntentData();
//        startCheckout();
        callStripePaymentApi();

        setToolbar();
        setListener();
    }
    private void getIntentData() {
        Intent intent =getIntent();
        if (intent!=null){
            if (intent.getStringExtra(AppConstant.BOOKING_FROM_DATE)!=null){
                mPickDate=intent.getStringExtra(AppConstant.BOOKING_FROM_DATE);
            }
            if (intent.getStringExtra(AppConstant.BOOKING_TO_DATE)!=null){
                mDropDate=intent.getStringExtra(AppConstant.BOOKING_TO_DATE);
            }
            if (intent.getStringExtra(AppConstant.PICKUP_LOCATION)!=null){
                mPickuLocation=intent.getStringExtra(AppConstant.PICKUP_LOCATION);
            }
            if (intent.getStringExtra(AppConstant.CAR_ID)!=null){
                carId=intent.getStringExtra(AppConstant.CAR_ID);
            }
            if (intent.getStringExtra(AppConstant.BILLING_FIRST_NAME)!=null){
                fname=intent.getStringExtra(AppConstant.BILLING_FIRST_NAME);
            }
            if (intent.getStringExtra(AppConstant.BILLING_LAST_NAME)!=null){
                lname=intent.getStringExtra(AppConstant.BILLING_LAST_NAME);
            }
            if (intent.getStringExtra(AppConstant.BILLING_MOBILE)!=null){
                phoneNum=intent.getStringExtra(AppConstant.BILLING_MOBILE);
            }
            if (intent.getStringExtra(AppConstant.BILLING_ALTERNATIVE_MOBILE)!=null){
                alternativeNum=intent.getStringExtra(AppConstant.BILLING_ALTERNATIVE_MOBILE);
            }
            if (intent.getStringExtra(AppConstant.BILLING_ADDRESS)!=null){
                address=intent.getStringExtra(AppConstant.BILLING_ADDRESS);
            }
//            if (intent.getStringExtra(AppConstant.PRICE_PER_DAY)!=null){
            pricePerDay= intent.getDoubleExtra(AppConstant.PRICE_PER_DAY,0);
//            }
//            if (intent.getStringExtra(AppConstant.SECURITY_AMOUNT)!=null){
            securityAmt= intent.getDoubleExtra(AppConstant.SECURITY_AMOUNT,0);
//            }
            if (intent.getStringExtra(AppConstant.BILLING_EMAIL)!=null){
                email=intent.getStringExtra(AppConstant.BILLING_EMAIL);
            }
            if (intent.getStringExtra(AppConstant.ID)!=null){
                bookingId=intent.getStringExtra(AppConstant.ID);
            }
            grandTotalAmt=intent.getDoubleExtra(AppConstant.GRAND_TOTAL,0);
            grandTotalUSD=intent.getDoubleExtra(AppConstant.GRAND_TOTAL_USD,0);
        }
    }
    @Override
    public void setToolbar() {
    binding.toolbar.tvToolbarBarName.setVisibility(View.INVISIBLE);
    }

    @Override
    public void setListener() {
        Button payButton = findViewById(R.id.payButton);
        payButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                CommonUtils.showProgressDialog(context);
                CardInputWidget cardInputWidget = findViewById(R.id.cardInputWidget);
                PaymentMethodCreateParams params = cardInputWidget.getPaymentMethodCreateParams();
                if (params != null) {
                    ConfirmPaymentIntentParams confirmParams = ConfirmPaymentIntentParams
                            .createWithPaymentMethodCreateParams(params, paymentIntentClientSecret);
                    stripe.confirmPayment(StripePaymentActivity.this, confirmParams);
                }
            }
        });
        binding.toolbar.custToolBckBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
    }
//    private void startCheckout() {
//        // Create a PaymentIntent by calling the sample server's /create-payment-intent endpoint.
//        MediaType mediaType = MediaType.get("application/json; charset=utf-8");
//        String json = "{"+ "\"currency\":\"usd\","+ "\"items\":["+ "{\"id\":\"photo_subscription\"}"+ "]"+ "}";
//        RequestBody body = RequestBody.create(json,mediaType);
//        Request request = new Request.Builder()
//                .url(BACKEND_URL) //create-payment-intent
//                .post(body)
//                .build();
//        httpClient.newCall(request)
//                .enqueue(new PayCallback(this));
//
//        //-----------------api call--------------------------
//
//        // Hook up the pay button to the card widget and stripe instance
//
//    }

    private void callStripePaymentApi(){
        CommonUtils.showProgressDialog(context);
        ApiRequest apiRequest =new ApiRequest();
        apiRequest.setAmount(String.valueOf(grandTotalAmt));

        ApiUtils.callStripePaymentApi(context, apiRequest, new ApiResponseInterface() {
            @Override
            public void onResponse(retrofit2.Response<ApiResponse> response) {
                CommonUtils.hideProgressDialog();

                if (response.code()==AppConstant.SUCCESS_CODE) {
                    if (response.body() != null) {

                        if (response.body().getPublishableKey() != null) {
                            stripePublishableKey = response.body().getPublishableKey();
                        }
                        if (response.body().getCsStripeKey() != null) {
                            paymentIntentClientSecret = response.body().getCsStripeKey();
                        }
                        // Configure the SDK with your Stripe publishable key so that it can make requests to the Stripe API
                        stripe = new Stripe(getApplicationContext(),stripePublishableKey);
                    }
                }
                else {
                    ToastUtil.Companion.showShortToast(context,getString(R.string.something_went_wrong));
                }
            }

            @Override
            public void onFailure() {
                CommonUtils.hideProgressDialog();
            }
        });
    }
    private void displayAlert(@NonNull String title,
                              @Nullable String message,
                              boolean restartDemo) {
        AlertDialog.Builder builder = new AlertDialog.Builder(this)
                .setTitle(title)
                .setMessage(message);
        if (restartDemo) {
            builder.setPositiveButton("Restart demo",
                    new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int index) {
                            CardInputWidget cardInputWidget = StripePaymentActivity.this.findViewById(R.id.cardInputWidget);
                            cardInputWidget.clear();
//                            StripePaymentActivity.this.startCheckout();
                        }
                    });
        } else {
            builder.setPositiveButton("Ok", null);
        }
        builder.create().show();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        // Handle the result of stripe.confirmPayment
        stripe.onPaymentResult(requestCode, data, new PaymentResultCallback(this));
    }

    private void onPaymentSuccess(@NonNull final Response response) throws IOException {
        CommonUtils.hideProgressDialog();
        Gson gson = new Gson();
        Type type = new TypeToken<Map<String, String>>(){}.getType();
        Map<String, String> responseMap = gson.fromJson(
                Objects.requireNonNull(response.body()).string(),
                type
        );

        // The response from the server includes the Stripe publishable key and
        // PaymentIntent details.
        // For added security, our sample app gets the publishable key from the server
        if (responseMap.get("publishableKey")!=null) {
            stripePublishableKey = responseMap.get("publishableKey");
        }
        if (responseMap.get("cs_key")!=null) {
            paymentIntentClientSecret = responseMap.get("cs_key");
        }//clientSecret

        // Configure the SDK with your Stripe publishable key so that it can make requests to the Stripe API
        stripe = new Stripe(
                getApplicationContext(),
                Objects.requireNonNull(stripePublishableKey)
        );
    }

    @Override
    public void onClick(View v) {

    }

    private static final class PayCallback implements Callback {
        @NonNull private final WeakReference<StripePaymentActivity> activityRef;

        PayCallback(@NonNull StripePaymentActivity activity) {
            activityRef = new WeakReference<>(activity);
        }

        @Override
        public void onFailure(@NonNull Call call, @NonNull final IOException e) {
            CommonUtils.hideProgressDialog();
            final StripePaymentActivity activity = activityRef.get();
            if (activity == null) {
                return;
            }

            activity.runOnUiThread(new Runnable() {
                                       @Override
                                       public void run() {
                                           Toast.makeText(activity, "Error: " + e.toString(), Toast.LENGTH_LONG).show();
                                       }
                                   }
            );
        }

        @Override
        public void onResponse(@NonNull Call call, @NonNull final Response response)
                throws IOException {
            final StripePaymentActivity activity = activityRef.get();
            if (activity == null) {
                return;
            }

            if (!response.isSuccessful()) {
                activity.runOnUiThread(new Runnable() {
                                           @Override
                                           public void run() {
                                               Toast.makeText(
                                                       activity, "Error: " + response.toString(), Toast.LENGTH_LONG
                                               ).show();
                                           }
                                       }
                );
            } else {
                activity.onPaymentSuccess(response);
            }
        }
    }

    private  final class PaymentResultCallback
            implements ApiResultCallback<PaymentIntentResult> {
        @NonNull private final WeakReference<StripePaymentActivity> activityRef;

        PaymentResultCallback(@NonNull StripePaymentActivity activity) {
            activityRef = new WeakReference<>(activity);
        }

        @Override
        public void onSuccess(@NonNull PaymentIntentResult result) {
            CommonUtils.hideProgressDialog();
            final StripePaymentActivity activity = activityRef.get();
            if (activity == null) {
                return;
            }
            PaymentIntent paymentIntent = result.getIntent();
            PaymentIntent.Status status = paymentIntent.getStatus();
            if (status == PaymentIntent.Status.Succeeded) {
                // Payment completed successfully
                Gson gson = new GsonBuilder().setPrettyPrinting().create();
                if (paymentIntent.getId()!=null)
                paymentId =paymentIntent.getId();
                paymentType="Stripe";
                callApiSuccessOrderApi();
//                Intent intent=new Intent(StripePaymentActivity.this,PaymentSuccessfulActivity.class);
//                intent.putExtra(AppConstant.ID,paymentId);
//                startActivity(intent);
                //activity.displayAlert("Payment completed",gson.toJson(paymentIntent),true);
            }
            else if (status == PaymentIntent.Status.RequiresPaymentMethod) {
                ToastUtil.Companion.showLongToast(context,getString(R.string.payment_failed_stripe));
                // Payment failed – allow retrying using a different payment method
//                activity.displayAlert("Payment failed",Objects.requireNonNull(paymentIntent.getLastPaymentError())
//                        .getMessage(),false);
            }
        }

        @Override
        public void onError(@NonNull Exception e) {
            CommonUtils.hideProgressDialog();
            final StripePaymentActivity activity = activityRef.get();
            if (activity == null) {
                return;
            }

            // Payment request failed – allow retrying using the same payment method
            activity.displayAlert("Error", e.toString(), false);
        }
    }

    private void callApiSuccessOrderApi(){
        CommonUtils.showProgressDialog(context);

        ApiRequest apiRequest =new ApiRequest();
        apiRequest.setBookingId(bookingId);
        apiRequest.setPaymentMethod(paymentType);
        apiRequest.setBankReferenceId("");
        apiRequest.setTransactionId(paymentId);
        apiRequest.setTimeZone(TimeZone.getDefault().getID());
        ApiUtils.callSuccessOrderApi(context, apiRequest, new ApiResponseInterface() {
            @Override
            public void onResponse(retrofit2.Response<ApiResponse> response) {
                CommonUtils.hideProgressDialog();
                if (response.code()== AppConstant.SUCCESS_CODE){
                    if (response.body()!=null){

                        if (response.body().getMessage()!=null)
                            ToastUtil.Companion.showShortToast(context,response.body().getMessage());

                        Intent intent = new Intent(context,PaymentSuccessfulActivity.class);
                        intent.putExtra(AppConstant.ID,response.body().getOrderId());
                        startActivity(intent);
                        finishAffinity();
                    }
                }
                else if (response.code()==AppConstant.VALIDATION_ERROR_CODE){

                }
                else if (response.code()==AppConstant.UNAUTHORIZED_CODE){
                    ToastUtil.Companion.showShortToast(context,getString(R.string.something_went_wrong));
                }
                else {
                    ToastUtil.Companion.showShortToast(context,getString(R.string.something_went_wrong));
                }
            }
            @Override
            public void onFailure() {
                CommonUtils.hideProgressDialog();
                ToastUtil.Companion.showShortToast(context,getString(R.string.something_went_wrong));
            }
        });
    }
}