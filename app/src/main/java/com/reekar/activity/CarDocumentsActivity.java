package com.reekar.activity;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.content.FileProvider;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.LinearLayoutManager;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;

import com.akanksha.commonclassutil.CheckPermission;
import com.akanksha.commonclassutil.ToastUtil;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.reekar.R;
import com.reekar.adapter.CarDocumentsAdapter;
import com.reekar.apiservices.ApiUtils;
import com.reekar.commonclasses.AppConstant;
import com.reekar.commonclasses.BaseActivity;
import com.reekar.commonclasses.CommonUtils;
import com.reekar.commonclasses.TakePictureUtils;
import com.reekar.databinding.ActivityCarDocumentsBinding;
import com.reekar.interfaces.ApiResponseInterface;
import com.reekar.interfaces.RecyclerviewItemClickInterface;
import com.reekar.models.ApiRequest;
import com.reekar.models.ApiResponse;
import com.reekar.models.CarDocModel;
import com.reekar.models.CarModel;
import com.reekar.models.UploadFileModel;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import okhttp3.MediaType;
import okhttp3.RequestBody;
import retrofit2.Response;

public class CarDocumentsActivity extends BaseActivity
{

    private ActivityCarDocumentsBinding carDocumentsBinding;
    private Context context;

    private ArrayList<UploadFileModel> uploadFileModelArrayList = new ArrayList<>();
    private CarDocumentsAdapter carDocumentsAdapter;
    private String image_="image";

    private File file;
    private String carId;

    private ApiResponse apiResponse = new ApiResponse();
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        carDocumentsBinding= DataBindingUtil.setContentView(this,R.layout.activity_car_documents);
        context=this;


        getIntentData();
//        setData();
        setCarDocumentsAdapter();
        setToolbar();
        setListener();
        if (CommonUtils.isNetworkAvailable(context)){
            callCarDetailsApi();
        }
        else
            ToastUtil.Companion.showShortToast(context,getString(R.string.failed_internet));
    }

    private void getIntentData(){
        Intent intent =getIntent();
        if (intent!=null){
            carId =intent.getStringExtra(AppConstant.CAR_ID);

            if (getIntent().getStringExtra(AppConstant.DATA) != null){
                Type type = new TypeToken<ApiResponse>() {}.getType();
                apiResponse = new Gson().fromJson(getIntent().getStringExtra(AppConstant.DATA),type);
            }
        }
    }
    private void setCarDocumentsAdapter(){
        carDocumentsAdapter = new CarDocumentsAdapter(context, uploadFileModelArrayList,carId,new RecyclerviewItemClickInterface() {
            @Override
            public void onItemClick(int position) {
                hideShow();
            }
        });
        carDocumentsBinding.recyclerview.setLayoutManager(new LinearLayoutManager(context));
        carDocumentsBinding.recyclerview.setAdapter(carDocumentsAdapter);
    }
    @Override
    public void setToolbar() {
        carDocumentsBinding.toolbarCarDocuments.tvToolbarBarName.setText(getString(R.string.car_documents));
    }

    @Override
    public void setListener() {
        carDocumentsBinding.toolbarCarDocuments.custToolBckBtn.setOnClickListener(this);
        carDocumentsBinding.addCarDocuments.setOnClickListener(this);
        carDocumentsBinding.submit.setOnClickListener(this);

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){

            case R.id.cust_tool_bck_btn:{
                finish();
                break;
            }
            case R.id.submit:{
                if (CommonUtils.isNetworkAvailable(context))
                callApiSaveCarDocs();
                else
                    ToastUtil.Companion.showShortToast(context,getString(R.string.failed_internet));
//                Intent intent =new Intent(context,CarRentalLocationActivity.class);
//                startActivity(intent);
                break;
            }
            case R.id.add_car_documents:{
                image_ = image_+System.currentTimeMillis();
                checkPermission();
                break;
            }
        }
    }


    private void checkPermission(){
        if (CheckPermission.Companion.checkIsMarshMallowVersion()){
            if (CheckPermission.Companion.checkCameraStoragePermission(context)){
                CommonUtils.showImageDailoge(context,image_,true);
            }
            else {
                CheckPermission.Companion.requestCameraStoragePermission((Activity) context);
            }
        }
        else {
            CommonUtils.showImageDailoge(context,image_,true);
        }
    }
    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);

        switch (requestCode){

            case CheckPermission.REQUEST_CODE_CAMERA_STORAGE_PERMISSION:{
                if (grantResults.length>0 && grantResults[0]== PackageManager.PERMISSION_GRANTED){
                    CommonUtils.showImageDailoge(context,image_,false);
                }
                break;
            }
        }
    }
    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode==Activity.RESULT_OK)
            switch (requestCode){

                case TakePictureUtils.TAKE_PICTURE:
                    setImage(data,requestCode);
                    break;
                case TakePictureUtils.PICK_GALLERY: {
                    if (data!=null) {
                        try {
                            InputStream inputStream = getContentResolver().openInputStream(data.getData());
                            FileOutputStream fileOutputStream = new FileOutputStream(new File(getExternalFilesDir("temp"), image_ + ".jpg"));
                            TakePictureUtils.copyStream(inputStream, fileOutputStream);
                            fileOutputStream.close();
                            inputStream.close();

                        } catch (FileNotFoundException e) {
                            e.printStackTrace();
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                    /*
                      check the file before getting from gallary
                     */
                        String fileType = getContentResolver().getType(data.getData());
                        if (fileType!= null && (fileType.equals(AppConstant.IMAGE_JPEG) ||
                                fileType.equals(AppConstant.IMAGE_JPG) ||
                                fileType.equals(AppConstant.IMAGE_PNG)))
                            setImage(data, requestCode);
                        else {
                            ToastUtil.Companion.showShortToast(context, getString(R.string.invalid_image));
                        }
                    }
                    break;
                }
                case TakePictureUtils.PICK_DOCUMENT: {
                    if (data!= null && data.getData()!= null)
                        setDocument(data, requestCode);
                    break;
                }
            }
    }

    private void setDocument(Intent data, int requestCode) {
        UploadFileModel uploadFileModel=new UploadFileModel();
        String filePath= TakePictureUtils.getFilePath(data.getData(),context);
        file =new File(filePath);
        uploadFileModel.setFile(file);
        uploadFileModel.setFiletype(getContentResolver().getType(data.getData()));
        uploadFileModel.setUri(data.getData());
        uploadFileModelArrayList.add(0,uploadFileModel);
        carDocumentsAdapter.notifyItemInserted(0);
        carDocumentsAdapter.notifyItemRangeChanged(0,uploadFileModelArrayList.size());
        hideShow();

    }

    private void setImage(Intent data, int requestCode) {
        UploadFileModel uploadFileModel =new UploadFileModel();
        File file =new File(getExternalFilesDir("temp"),image_+".jpg");
        Uri uri= FileProvider.getUriForFile(context,context.getApplicationContext().getPackageName()+".provider",file);

        uploadFileModel.setFile(file);
        uploadFileModel.setFiletype(getContentResolver().getType(uri));

        if (requestCode==TakePictureUtils.PICK_GALLERY)
            uploadFileModel.setUri(data.getData());
        else
            uploadFileModel.setUri(uri);
        uploadFileModelArrayList.add(0, uploadFileModel);
        carDocumentsAdapter.notifyItemInserted(0);
        carDocumentsAdapter.notifyItemRangeChanged(0,uploadFileModelArrayList.size());
        carDocumentsBinding.recyclerview.smoothScrollToPosition(0);
        hideShow();
    }

    private void hideShow() {
        if (uploadFileModelArrayList.size()==4){
            carDocumentsBinding.addCarDocuments.setVisibility(View.GONE);
        }
        else {
            carDocumentsBinding.addCarDocuments.setVisibility(View.VISIBLE);
        }
        if (uploadFileModelArrayList.size()>2){
            carDocumentsBinding.submit.setVisibility(View.VISIBLE);
        }
        else {
            carDocumentsBinding.submit.setVisibility(View.GONE);
        }
    }

    private void callApiSaveCarDocs(){
        CommonUtils.showProgressDialog(context);

        Map<String, RequestBody> map=new HashMap<>();
        map.put("car_id", ApiUtils.toRequestBody(carId));
        /* --- fro multiple file --- */
        for (int i=0; i<uploadFileModelArrayList.size();i++) {
            if (uploadFileModelArrayList.get(i).getFile()!=null) {
                RequestBody filebody = RequestBody.create(MediaType.parse(uploadFileModelArrayList.get(i).getFiletype()), uploadFileModelArrayList.get(i).getFile());
                map.put("docs["+i+"][file]\"; filename=\"" + uploadFileModelArrayList.get(i).getFile().getName(), filebody);
            }
        }
        ApiUtils.callSaveCarDocumentsApi(context, map, new ApiResponseInterface() {
            @Override
            public void onResponse(Response<ApiResponse> response) {
                CommonUtils.hideProgressDialog();
                if (response.code()==AppConstant.SUCCESS_CODE) {
                    if (response.body() != null) {
                        String msg = response.body().getMessage();
                        ToastUtil.Companion.showShortToast(context, msg);
                        Intent intent =new Intent(context,CarRentalLocationActivity.class);
                        intent.putExtra(AppConstant.CAR_ID,carId);
                        intent.putExtra(AppConstant.DATA,new Gson().toJson(apiResponse));
                        startActivity(intent);
                    }

                }
            }

            @Override
            public void onFailure() {
                CommonUtils.hideProgressDialog();

            }
        });
    }

    private void setData(){
        if (apiResponse != null){
            if (apiResponse.getCar() != null){
                CarModel cars = apiResponse.getCar();
                if (cars.getDocs() != null) {
                    for (String s:cars.getDocs()){
                        UploadFileModel uploadFileModel =new UploadFileModel();
                        uploadFileModel.setDocs(s);
                        uploadFileModel.setFiletype(CommonUtils.getMimeType(s));
                        uploadFileModelArrayList.add(uploadFileModel);
                    }
//                    carDocumentsAdapter.notifyDataSetChanged();
                }
            }
        }
    }

    private void callCarDetailsApi(){
        CommonUtils.showProgressDialog(context);

        ApiRequest apiRequest = new ApiRequest();
        apiRequest.setCarId(carId);

        ApiUtils.callCarDetailsApi(context, apiRequest, new ApiResponseInterface() {
            @Override
            public void onResponse(Response<ApiResponse> response) {
                CommonUtils.hideProgressDialog();

                switch (response.code()){
                    case AppConstant.SUCCESS_CODE:
                        if (response.body() != null){
                            if (response.body().getCar() != null){
                                if (response.body().getCar().getCarDocs() != null &&response.body().getCar().getCarDocs().size()>0){
                                    CarModel cars = response.body().getCar();
                                    if (cars.getCarDocs()!= null) {
                                        for (CarDocModel s:cars.getCarDocs()){
                                            UploadFileModel uploadFileModel =new UploadFileModel();
                                            uploadFileModel.setDocs(s.getDoc());
                                            uploadFileModel.setId(s.getId());

                                            uploadFileModel.setFiletype(CommonUtils.getMimeType(s.getDoc()));
                                            uploadFileModelArrayList.add(uploadFileModel);
                                        }
                                        hideShow();
                                        carDocumentsAdapter.notifyDataSetChanged();

                                    }
                                }
                            }
                        }
                        break;
                    case AppConstant.UNAUTHORIZED_CODE:
                        CommonUtils.navigateToLogin(context);
                    default:
                        CommonUtils.showToast(context,getString(R.string.something_went_wrong));
                }
            }

            @Override
            public void onFailure() {
                CommonUtils.hideProgressDialog();
                CommonUtils.showToast(context,getString(R.string.something_went_wrong));
            }
        });
    }



}
