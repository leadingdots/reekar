package com.reekar.activity;

import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;

import com.reekar.R;
import com.reekar.commonclasses.AppConstant;
import com.reekar.commonclasses.BaseActivity;
import com.reekar.databinding.ActivityPaymentSuccessfulBinding;

public class PaymentSuccessfulActivity extends BaseActivity {

    private ActivityPaymentSuccessfulBinding paymentSuccessfulBinding;
    private Context context;
    private String bookingId;

    private String notifyTitle,notifyBody,notifySubject;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        paymentSuccessfulBinding= DataBindingUtil.setContentView(this,R.layout.activity_payment_successful);
        context=this;

        getIntentData();
        setToolbar();
        setListener();
    }

    private void getIntentData() {
        Intent intent =getIntent();
        if (intent!=null){
            if (intent.getStringExtra(AppConstant.ID)!=null){
                bookingId=intent.getStringExtra(AppConstant.ID);
            }
        }
    }

    private void notification(){
        NotificationManager notif=(NotificationManager)getSystemService(Context.NOTIFICATION_SERVICE);
        Notification notify=new Notification.Builder
                (getApplicationContext()).setContentTitle(notifyTitle).setContentText(notifyBody).
                setContentTitle(notifySubject).setSmallIcon(R.mipmap.logo).build();

        Intent notificationIntent = new Intent(context, MainActivity.class);

        notificationIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP
                | Intent.FLAG_ACTIVITY_SINGLE_TOP);
        PendingIntent intent = PendingIntent.getActivity(context, 0,
                notificationIntent, 0);


        notify.flags |= Notification.FLAG_AUTO_CANCEL;
        notif.notify(0, notify);
    }
    @Override
    public void setToolbar() {
        paymentSuccessfulBinding.paymentSuccessToolbar.custToolBckBtn.setVisibility(View.GONE);
        paymentSuccessfulBinding.paymentSuccessToolbar.tvToolbarBarName.setText(R.string.payment_successful);
    }

    @Override
    public void setListener() {
        paymentSuccessfulBinding.gotoOrders.setOnClickListener(this);
    }
    @Override
    public void onClick(View v) {
        switch (v.getId()){

            case R.id.goto_orders:{
                startActivity(new Intent(context,MainActivity.class));
                Intent intent=new Intent(context,MyOrderDetailsActivity.class);
                intent.putExtra(AppConstant.ID,bookingId);
                startActivity(intent);
                finishAffinity();
                break;
            }
        }

    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        Intent intent =new Intent(context,MainActivity.class);
        startActivity(intent);
        finishAffinity();
    }
}
