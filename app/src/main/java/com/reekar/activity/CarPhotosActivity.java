package com.reekar.activity;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.content.FileProvider;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.GridLayoutManager;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;

import com.akanksha.commonclassutil.CheckPermission;
import com.akanksha.commonclassutil.ToastUtil;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.reekar.R;
import com.reekar.adapter.UploadCarImageAdapter;
import com.reekar.apiservices.ApiUtils;
import com.reekar.commonclasses.AppConstant;
import com.reekar.commonclasses.BaseActivity;
import com.reekar.commonclasses.CommonUtils;
import com.reekar.commonclasses.TakePictureUtils;
import com.reekar.databinding.ActivityCarPhotosBinding;
import com.reekar.interfaces.ApiResponseInterface;
import com.reekar.interfaces.RecyclerviewItemClickInterface;
import com.reekar.models.ApiRequest;
import com.reekar.models.ApiResponse;
import com.reekar.models.CarImageModel;
import com.reekar.models.CarModel;
import com.reekar.models.FeatureModel;
import com.reekar.models.SaveCarInfoApiRequest;
import com.reekar.models.UploadFileModel;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import okhttp3.MediaType;
import okhttp3.RequestBody;
import retrofit2.Response;

public class CarPhotosActivity extends BaseActivity {

    private ActivityCarPhotosBinding carPhotosBinding;

    private Context context;

    private String tempImage="tempImage";
    private File file;
    private String carId;
    private ApiResponse apiResponse = new ApiResponse();

    private ArrayList<UploadFileModel> uploadFileModelArrayList = new ArrayList<>();
    private UploadCarImageAdapter uploadCarImageAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        carPhotosBinding = DataBindingUtil.setContentView(this,R.layout.activity_car_photos);
        context =this;

        getIntentData();
        setAdapter();
//        setData();
        setToolbar();
        setListener();
        if (CommonUtils.isNetworkAvailable(context)){
            callCarDetailsApi();
        }
        else
            ToastUtil.Companion.showShortToast(context,getString(R.string.failed_internet));
    }

    private void setAdapter() {
        uploadCarImageAdapter = new UploadCarImageAdapter(context, uploadFileModelArrayList,carId, new RecyclerviewItemClickInterface() {
            @Override
            public void onItemClick(int position) {
                showButton();
            }
        });
//        carPhotosBinding.carphotoRecyclerview.setHasFixedSize(true);
        carPhotosBinding.carphotoRecyclerview.setLayoutManager(new GridLayoutManager(context,2));
        carPhotosBinding.carphotoRecyclerview.setAdapter(uploadCarImageAdapter);

    }

    private void getIntentData(){
        Intent intent =getIntent();
        if (intent!=null){
            carId =intent.getStringExtra(AppConstant.CAR_ID);

            if (getIntent().getStringExtra(AppConstant.DATA) != null){
                Type type = new TypeToken<ApiResponse>() {}.getType();
                apiResponse = new Gson().fromJson(getIntent().getStringExtra(AppConstant.DATA),type);
            }

        }
    }
    @Override
    public void setToolbar() {
        carPhotosBinding.toolbarCarPhotos.tvToolbarBarName.setText(R.string.car_photos);
    }

    @Override
    public void setListener() {
        carPhotosBinding.toolbarCarPhotos.custToolBckBtn.setOnClickListener(this);
        carPhotosBinding.addCarPhotosBtn.setOnClickListener(this);
        carPhotosBinding.nextBtn.setOnClickListener(this);

    }

    @Override
    public void onClick(View v) {

        switch (v.getId()){

            case R.id.cust_tool_bck_btn:{
                finish();
                break;
            }
            case R.id.add_car_photos_btn:{
                tempImage = tempImage+System.currentTimeMillis();
                checkPermission();
                break;
            }
            case R.id.next_btn:{
                if (CommonUtils.isNetworkAvailable(context))
                callApiSaveCarPhotos();
                else
                    ToastUtil.Companion.showShortToast(context,getString(R.string.failed_internet));
//                Intent intent=new Intent(context,CarDocumentsActivity.class);
//                startActivity(intent);
                break;
            }
        }
    }

    private void checkPermission(){
        if (CheckPermission.Companion.checkIsMarshMallowVersion()){
            if (CheckPermission.Companion.checkCameraStoragePermission(context)){
                CommonUtils.showImageDailoge(context,tempImage,false);
            }
            else {
                CheckPermission.Companion.requestCameraStoragePermission((Activity) context);
            }
        }
        else {
            CommonUtils.showImageDailoge(context,tempImage,false);
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);

        switch (requestCode){

            case CheckPermission.REQUEST_CODE_CAMERA_STORAGE_PERMISSION:{
                if (grantResults.length>0 && grantResults[0]== PackageManager.PERMISSION_GRANTED){
                    CommonUtils.showImageDailoge(context,tempImage,false);
                }
                break;
            }
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode==Activity.RESULT_OK)
        switch (requestCode){

            case TakePictureUtils.TAKE_PICTURE:

                setImage(data,requestCode);
                break;
            case TakePictureUtils.PICK_GALLERY: {
                if (data!=null) {
                    try {
                        InputStream inputStream = getContentResolver().openInputStream(data.getData());
                        FileOutputStream fileOutputStream = new FileOutputStream(new File(getExternalFilesDir("temp"), tempImage + ".jpg"));
                        TakePictureUtils.copyStream(inputStream, fileOutputStream);
                        fileOutputStream.close();
                        inputStream.close();

                    } catch (FileNotFoundException e) {
                        e.printStackTrace();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                    String fileType = getContentResolver().getType(data.getData());
                    if (fileType!=null && (fileType.equals(AppConstant.IMAGE_JPEG) ||
                            fileType.equals(AppConstant.IMAGE_JPG) ||
                            fileType.equals(AppConstant.IMAGE_PNG)))
                        setImage(data, requestCode);
                    else {
                        ToastUtil.Companion.showShortToast(context,getString(R.string.invalid_image));
                    }
                }
                break;
            }
        }
    }

    private void setImage(Intent data, int requestCode) {
        UploadFileModel uploadFileModel =new UploadFileModel();
        file =new File(getExternalFilesDir("temp"),tempImage+".jpg");
        uploadFileModel.setFile(file);
        Uri uri= FileProvider.getUriForFile(context, context.getApplicationContext().getPackageName()+".provider",file);
        if (requestCode==TakePictureUtils.PICK_GALLERY) {
            uploadFileModel.setUri(data.getData());
        }else
            uploadFileModel.setUri(uri);
        uploadFileModelArrayList.add(0, uploadFileModel);
//      uploadCarImageAdapter.notifyDataSetChanged();
        uploadCarImageAdapter.notifyItemInserted(0);
        uploadCarImageAdapter.notifyItemRangeChanged(0, uploadFileModelArrayList.size());
        carPhotosBinding.carphotoRecyclerview.smoothScrollToPosition(0);
        showButton();
    }
    private void showButton(){
        if (uploadFileModelArrayList.size()==6){
            carPhotosBinding.addCarPhotosBtn.setVisibility(View.GONE);
        }
        else {
            carPhotosBinding.addCarPhotosBtn.setVisibility(View.VISIBLE);
        }
        if (uploadFileModelArrayList.size()>=3){
            carPhotosBinding.nextBtn.setVisibility(View.VISIBLE);
        }
        else {
            carPhotosBinding.nextBtn.setVisibility(View.GONE);
        }
    }

    private void callApiSaveCarPhotos(){
        CommonUtils.showProgressDialog(context);

        Map<String, RequestBody> map=new HashMap<>();
        map.put("car_id",ApiUtils.toRequestBody(carId));

        /* --- fro multiple file --- */
        for (int i=0; i<uploadFileModelArrayList.size();i++) {
            if (uploadFileModelArrayList.get(i).getFile()!=null) {
                RequestBody filebody = RequestBody.create(MediaType.parse("image/jpg"), uploadFileModelArrayList.get(i).getFile());
                map.put("images["+i+"][image]\"; filename=\"" + uploadFileModelArrayList.get(i).getFile().getName(), filebody);
            }
//            if (uploadFileModelArrayList.get(i).getImages()!=null) {
//                RequestBody filebody = RequestBody.create(MediaType.parse("image/jpg"), uploadFileModelArrayList.get(i).getImages());
//                map.put("images["+i+"][image]\"; filename=\"" + uploadFileModelArrayList.get(i).getImages(), filebody);
//            }

        }
        ApiUtils.callSaveCarPhotosApi(context, map, new ApiResponseInterface() {
            @Override
            public void onResponse(Response<ApiResponse> response) {
                CommonUtils.hideProgressDialog();
                if (response.code()==AppConstant.SUCCESS_CODE) {
                    if (response.body() != null) {
                        String msg = response.body().getMessage();
                        ToastUtil.Companion.showShortToast(context, msg);
                        Intent intent = new Intent(context,CarDocumentsActivity.class);
                        intent.putExtra(AppConstant.CAR_ID,carId);
                        intent.putExtra(AppConstant.DATA,new Gson().toJson(apiResponse));
                        startActivity(intent);
                    }

                }
                else if (response.code()==AppConstant.UNAUTHORIZED_CODE){
                    CommonUtils.navigateToLogin(context);
                }
                else
                {
                    CommonUtils.showToast(context,getString(R.string.something_went_wrong));
                }
            }

            @Override
            public void onFailure() {
                CommonUtils.hideProgressDialog();
                CommonUtils.showToast(context,getString(R.string.something_went_wrong));

            }
        });
    }

    private void setData(){
        if (apiResponse != null){
            if (apiResponse.getCar() != null){
                CarModel cars = apiResponse.getCar();
                if (cars.getImages() != null) {
                    for (String s:cars.getImages()){
                        UploadFileModel uploadFileModel =new UploadFileModel();
                        uploadFileModel.setImages(s);
                        uploadFileModelArrayList.add(uploadFileModel);
                    }
                    showButton();
                    uploadCarImageAdapter.notifyDataSetChanged();

                }
            }
        }
    }

    private void callCarDetailsApi(){
        CommonUtils.showProgressDialog(context);

        ApiRequest apiRequest = new ApiRequest();
        apiRequest.setCarId(carId);

        ApiUtils.callCarDetailsApi(context, apiRequest, new ApiResponseInterface() {
            @Override
            public void onResponse(Response<ApiResponse> response) {
                CommonUtils.hideProgressDialog();

                switch (response.code()){
                    case AppConstant.SUCCESS_CODE:
                        if (response.body() != null){
                            if (response.body().getCar() != null){
                                if (response.body().getCar().getCarImages() != null &&response.body().getCar().getCarImages().size()>0){
                                    CarModel cars = response.body().getCar();
                                    if (cars.getCarImages() != null) {
                                        for (CarImageModel s:cars.getCarImages()){
                                            UploadFileModel uploadFileModel =new UploadFileModel();
                                            uploadFileModel.setImages(s.getImage());
                                            uploadFileModel.setId(String.valueOf(s.getId()));
                                            uploadFileModelArrayList.add(uploadFileModel);
                                        }
                                        showButton();
                                        uploadCarImageAdapter.notifyDataSetChanged();

                                    }
                                }
                            }
                        }
                        break;
                    case AppConstant.UNAUTHORIZED_CODE:
                        CommonUtils.navigateToLogin(context);
                    default:
                        CommonUtils.showToast(context,getString(R.string.something_went_wrong));
                }
            }

            @Override
            public void onFailure() {
                CommonUtils.hideProgressDialog();
                CommonUtils.showToast(context,getString(R.string.something_went_wrong));
            }
        });
    }


}
