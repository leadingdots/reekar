package com.reekar.activity;

import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.Toast;

import com.akanksha.commonclassutil.PreferencesManager;
import com.akanksha.commonclassutil.ToastUtil;
import com.goodiebag.pinview.Pinview;
import com.reekar.R;
import com.reekar.apiservices.APIInterface;
import com.reekar.apiservices.ApiUtils;
import com.reekar.commonclasses.AppConstant;
import com.reekar.commonclasses.BaseActivity;
import com.reekar.commonclasses.CommonUtils;
import com.reekar.commonclasses.ErrorUtils;
import com.reekar.databinding.ActivityOtpBinding;
import com.reekar.interfaces.ApiResponseInterface;
import com.reekar.models.ApiRequest;
import com.reekar.models.ApiResponse;
import com.reekar.models.ErrorModel;
import com.reekar.models.RegistrationModel;
import com.reekar.models.SignUpModel;
import com.reekar.translation.ChangeLanguage;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class OtpActivity extends BaseActivity {

    private ActivityOtpBinding otpBinding;
    private APIInterface mApiInterface;
    private Context mContext;

    /*
     fixed string to be send to api
     */
    private String action ="wcfm_ajax_controller";
    private String controller ="wcfm-memberships-registration";
    private String actionSignUppage ="wcfmvm_email_verification_code";

    private String fname;
    private String lname;
    private String email;
    private String ccode;
    private String mnumber;
    private String pass;
    private String cpass;
    private String otp;
    private String from;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mContext =this;
        setContentView(R.layout.activity_otp);

        otpBinding = DataBindingUtil.setContentView(this,R.layout.activity_otp);
        Pinview pinview1 = findViewById(R.id.pinview);
        pinview1.setPinViewEventListener(new Pinview.PinViewEventListener() {
            @Override
            public void onDataEntered(Pinview pinview, boolean fromUser) {
//                Toast.makeText(OtpActivity.this, pinview.getValue(), Toast.LENGTH_SHORT).show();
            }
        });




        getIntentData();
        setListener();
        setToolbar();
    }

    public void getIntentData() {
        Bundle extras = getIntent().getExtras();
        if (extras != null) {
            Intent intent = getIntent();
            email = intent.getStringExtra(AppConstant.USER_EMAIL);
            from = intent.getStringExtra(AppConstant.FROM);
            otpBinding.otpEmailTv.setText(email);
        }

    }

     /*
      Api Hit of otp verification
     */
    public void callApiOtpVerify(){
        CommonUtils.showProgressDialog(mContext);

        ApiRequest apiRequest = new ApiRequest();
        apiRequest.setEmail(email);
        apiRequest.setDevice_type(AppConstant.DEVICE_TYPE);
        apiRequest.setDevice_token(PreferencesManager.Companion.getStringPreferences(mContext, AppConstant.DEVICE_TOKEN));
        apiRequest.setOtp(otpBinding.pinview.getValue());

        ApiUtils.callApiOtpVerify(mContext, apiRequest, new ApiResponseInterface() {
            @Override
            public void onResponse(Response<ApiResponse> response) {
                CommonUtils.hideProgressDialog();
                if (response.code()==AppConstant.SUCCESS_CODE){
                    saveData(response);

                   if (from!=null) {
                       if (from.equals(AppConstant.FORGOT_ACTIVITY)) {
                           Intent intent1 = new Intent(mContext, ChangePasswordActivity.class);
                           intent1.putExtra(AppConstant.USER_EMAIL,email);
                           intent1.putExtra(AppConstant.OTP_ACTIVITY,AppConstant.OTP_ACTIVITY);
                           startActivity(intent1);
                       }
                   }
                   else {
                       Intent intent = new Intent(mContext,VerifySuccessActivity.class);
                       intent.putExtra(AppConstant.USER_EMAIL,email);
                       startActivity(intent);
                       finishAffinity();
                   }
                }
                else if (response.code()==AppConstant.VALIDATION_ERROR_CODE){
                    parseError(response);
                }
                else if (response.code()==AppConstant.SUCCESS_CODE_201){
                    CommonUtils.showToast(mContext,ChangeLanguage.translate(mContext,response.body().getMessage()));
                }
                else {
                  CommonUtils.showToast(mContext,getString(R.string.something_went_wrong));
                }
            }

            @Override
            public void onFailure() {
                CommonUtils.hideProgressDialog();
                ToastUtil.Companion.showShortToast(mContext,getString(R.string.something_went_wrong));

            }
        });
    }

    private void saveData(Response<ApiResponse> response) {
        if (response.body().getAccessCode()!=null)
        PreferencesManager.Companion.saveStringPreferences(mContext,AppConstant.ACCESS_CODE,response.body().getAccessCode());
//        PreferencesManager.Companion.saveStringPreferences(mContext,AppConstant.USER_EMAIL,email);
        if (response.body().getFirstNaame()!=null)
            PreferencesManager.Companion.saveStringPreferences(mContext,AppConstant.FIRST_NAME,response.body().getFirstNaame());
        if (response.body().getLastName()!=null)
            PreferencesManager.Companion.saveStringPreferences(mContext,AppConstant.LAST_NAME,response.body().getLastName());
    }

    public void parseError(Response<ApiResponse> response){

        if (response.errorBody()!=null){
            try {
                ApiResponse error = ErrorUtils.parseError(response,mContext);
                if (error!=null && error.getError()!=null){
                    showError(error.getError());
                }
            }
            catch (Exception e){

            }
        }
    }

    private void showError(ErrorModel error) {
        if (error.getOtp()!=null){
           CommonUtils.showToast(mContext, ChangeLanguage.translate(mContext,error.getOtp().get(0)));
        }
    }

    public void callApiResendOtp(){
        CommonUtils.showProgressDialog(mContext);

        ApiRequest apiRequest = new ApiRequest();
        apiRequest.setEmail(email);

        ApiUtils.callApiResendOtp(mContext, apiRequest, new ApiResponseInterface() {
            @Override
            public void onResponse(Response<ApiResponse> response) {
                CommonUtils.hideProgressDialog();
                if (response.code()==AppConstant.SUCCESS_CODE){
                    CommonUtils.showToast(mContext,ChangeLanguage.translate(mContext,response.body().getMessage()));
                }
                else if (response.code()==AppConstant.UNAUTHORIZED_CODE){
                    CommonUtils.showToast(mContext,ChangeLanguage.translate(mContext,response.body().getMessage()));
                }
                else if (response.code()==AppConstant.SUCCESS_CODE_201){
                    CommonUtils.showToast(mContext,ChangeLanguage.translate(mContext,response.body().getMessage()));
                }
                else {
                    CommonUtils.showToast(mContext,getString(R.string.something_went_wrong));

                }
            }

            @Override
            public void onFailure() {
                CommonUtils.hideProgressDialog();
            }
        });
    }

    @Override
    public void setToolbar() {

    }

    private void validationOTP(){

        otp = otpBinding.pinview.getValue();
        if (otp.isEmpty()){
            CommonUtils.showToast(mContext,getString(R.string.enter_otp));
            otpBinding.pinview.requestPinEntryFocus();
        }
        else if (otp.length()<4){
            CommonUtils.showToast(mContext,getString(R.string.enter_otp_4_digit));
            otpBinding.pinview.requestPinEntryFocus();
        }
        else {
            if(CommonUtils.isNetworkAvailable(mContext)){
                callApiOtpVerify();
            }
            else
                ToastUtil.Companion.showShortToast(mContext,getString(R.string.failed_internet));
        }
    }

    @Override
    public void setListener() {
        otpBinding.backBtn.setOnClickListener(this);
        otpBinding.submitOtp.setOnClickListener(this);
        otpBinding.resendCodeTv.setOnClickListener(this);
        otpBinding.chngeEmailTv.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {

        switch (v.getId()){

            case R.id.submit_otp:{
                validationOTP();
                break;
            }
            case R.id.back_btn:
            case R.id.chnge_email_tv: {
                finish();
                break;
            }
            case R.id.resend_code_tv:{
                if (!TextUtils.isEmpty(otpBinding.pinview.getValue())) {
                    otpBinding.pinview.setValue("");
                }
                if (CommonUtils.isNetworkAvailable(mContext))
                callApiResendOtp();
                else
                    ToastUtil.Companion.showShortToast(mContext,getString(R.string.failed_internet));
                break;
            }

        }
    }
}
// pinView Customize
//        Pinview pinview5 = findViewById(R.id.pinview5);
//        pinview5.setCursorShape(R.drawable.example_cursor);
//        pinview5.setCursorColor(Color.BLUE);
//        pinview5.setTextSize(12);
//        pinview5.setTextColor(Color.BLACK);
//        pinview5.showCursor(true);