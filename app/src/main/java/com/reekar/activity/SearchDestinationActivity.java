package com.reekar.activity;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Location;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;

import androidx.annotation.NonNull;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.LinearLayoutManager;

import com.akanksha.commonclassutil.CheckPermission;
import com.google.android.libraries.places.api.Places;
import com.google.android.libraries.places.api.model.Place;
import com.reekar.R;
import com.reekar.adapter.LocationSuggestionAdapter;
import com.reekar.commonclasses.AppConstant;
import com.reekar.commonclasses.BaseActivity;
import com.reekar.commonclasses.CommonUtils;
import com.reekar.databinding.ActivitySearchDestinationBinding;
import com.reekar.interfaces.OnLocationChangeInterface;
import com.reekar.interfaces.OnPlaceItemClick;
import com.reekar.location.GPSTrackerNew;

public class SearchDestinationActivity extends BaseActivity {

    private ActivitySearchDestinationBinding searchDestinationBinding;
    private Context context;

    private LocationSuggestionAdapter locationSuggestionAdapter;
    private GPSTrackerNew gpsTracker;
    private double lng=0.0,lat=0.0;
    private String mAddress;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        searchDestinationBinding= DataBindingUtil.setContentView(this,R.layout.activity_search_destination);
        context=this;

        initPlacePicker();
        setRecyclerAdapter();
        setToolbar();
        setListener();
    }

    private void initPlacePicker() {
        if(!Places.isInitialized()){
            Places.initialize(this,getString(R.string.google_maps_key));
        }
    }

    private void setRecyclerAdapter() {
        locationSuggestionAdapter=new LocationSuggestionAdapter(context, new OnPlaceItemClick() {
            @Override
            public void onItemClick(Place place) {
                if (place.getLatLng() != null)
                setData(place.getLatLng().latitude,place.getLatLng().longitude,place.getName());
            }
        });
        searchDestinationBinding.destinationSuggRecyclerview.setHasFixedSize(true);
        searchDestinationBinding.destinationSuggRecyclerview.setLayoutManager(new LinearLayoutManager(context));
        searchDestinationBinding.destinationSuggRecyclerview.setAdapter(locationSuggestionAdapter);
    }
    @Override
    public void setToolbar() {
        searchDestinationBinding.searchDestinationToolbar.tvToolbarBarName.setText(R.string.search_pickup);
    }

    @Override
    public void setListener() {
        searchDestinationBinding.searchDestinationToolbar.custToolBckBtn.setOnClickListener(this);
        searchDestinationBinding.pickMyLoc.setOnClickListener(this);


        searchDestinationBinding.seachDstination.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                locationSuggestionAdapter.getFilter().filter(charSequence.toString());
                locationSuggestionAdapter.notifyDataSetChanged();

                /*if (charSequence.isNullOrEmpty()) {
                    iv_remove.visibility = View.GONE
                    rv_address.visibility = View.GONE
                    mapFragment?.view?.visibility =View.VISIBLE
                }
                else {
                    rv_address.visibility = View.VISIBLE
                    iv_remove.visibility = View.VISIBLE
                    mapFragment?.view?.visibility =View.GONE
                }*/
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){

            case R.id.cust_tool_bck_btn:{
                finish();
                break;
            }
            case R.id.pick_my_loc:{
                if (CheckPermission.Companion.checkIsMarshMallowVersion()){
                    if (CheckPermission.Companion.checkLocationPermission(context)){
                        setCurrentLocation();
                    } else
                        CheckPermission.Companion.requestLocationPermission((Activity) context);
                } else
                    setCurrentLocation();
//                Intent intent=new Intent(context,CarRentalLocationActivity.class);
//                startActivity(intent);
                break;
            }
        }
    }

    private void setCurrentLocation() {

        searchDestinationBinding.progressCurrentLoc.setVisibility(View.VISIBLE);

        gpsTracker = new GPSTrackerNew(context, new OnLocationChangeInterface() {
            @Override
            public void onLocationChanged(Location location) {
                searchDestinationBinding.progressCurrentLoc.setVisibility(View.INVISIBLE);
                setData(location.getLatitude(),location.getLongitude(),CommonUtils.fetchAddress(context,location.getLatitude(),location.getLongitude()));
                gpsTracker.stopUsingGPS();
            }
        });
    }

    private void setData(double latitude, double longitude,String address) {
        lat = latitude;
        lng = longitude;
        mAddress = address;


        Intent intent = new Intent();
        intent.putExtra(AppConstant.LATITUDE,lat);
        intent.putExtra(AppConstant.LONGITUDE,lng);
        intent.putExtra(AppConstant.ADDRESS,mAddress);

        setResult(RESULT_OK,intent);
        finish();
    }


    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);

        switch (requestCode) {
            case CheckPermission.REQUEST_CODE_LOCATION_PERMISSION: {
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    setCurrentLocation();
                }
            }
        }
    }
}
