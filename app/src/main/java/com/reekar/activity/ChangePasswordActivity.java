package com.reekar.activity;

import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;

import com.akanksha.commonclassutil.PreferencesManager;
import com.akanksha.commonclassutil.ToastUtil;
import com.reekar.R;
import com.reekar.apiservices.ApiUtils;
import com.reekar.commonclasses.AppConstant;
import com.reekar.commonclasses.BaseActivity;
import com.reekar.commonclasses.CommonUtils;
import com.reekar.commonclasses.ErrorUtils;
import com.reekar.databinding.ActivityChangePasswordBinding;
import com.reekar.interfaces.ApiResponseInterface;
import com.reekar.models.ApiRequest;
import com.reekar.models.ApiResponse;
import com.reekar.models.ErrorModel;
import com.reekar.translation.ChangeLanguage;

import retrofit2.Response;

public class ChangePasswordActivity extends BaseActivity {

    private ActivityChangePasswordBinding changePasswordBinding;
    private Context context;

    private String pass,confirmPass;
    private String email;
    private String from;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        context=this;
        changePasswordBinding = DataBindingUtil.setContentView(this,R.layout.activity_change_password);

        getIntentData();
        setToolbar();
        setListener();
    }

    private void getIntentData() {

        Intent intent =getIntent();
        if (intent!=null){
            email =intent.getStringExtra(AppConstant.USER_EMAIL);
            from =intent.getStringExtra(AppConstant.OTP_ACTIVITY);
        }
    }

    @Override
    public void setToolbar() {
       changePasswordBinding.changePassToolbar.tvToolbarBarName.setText(R.string.change_pass);
    }

    @Override
    public void setListener() {
        changePasswordBinding.changePassToolbar.custToolBckBtn.setOnClickListener(this);
        changePasswordBinding.submitNewPass.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {

        switch (v.getId()){

            case R.id.cust_tool_bck_btn:{
                finish();
                break;
            }
            case R.id.submit_new_pass:{
                validationError();
                break;
            }
        }
    }
    private void openActivity(){
//        if (from!=null) {
//            if (from.equals(AppConstant.OTP_ACTIVITY)) {
//                finish();
//            }
//        }
    }

    private void validationError(){
        pass=changePasswordBinding.enterNewPass.getText().toString();
        confirmPass = changePasswordBinding.confrmNewPass.getText().toString();

        if (pass.isEmpty()){
            changePasswordBinding.enterNewPass.setError(getString(R.string.please_enter_pass));
            changePasswordBinding.enterNewPass.requestFocus();
        }
        else if (pass.length()<8){
            changePasswordBinding.enterNewPass.setError(getString(R.string.please_enter_correct_pass2));
            changePasswordBinding.enterNewPass.requestFocus();
        }
        else if (!pass.equals(confirmPass)){
            changePasswordBinding.confrmNewPass.setError(getString(R.string.passwrd_doent_match));
            changePasswordBinding.confrmNewPass.requestFocus();
        }
        else {
            if (CommonUtils.isNetworkAvailable(context))
            callApiChangePass();
            else
                ToastUtil.Companion.showShortToast(context,getString(R.string.failed_internet));
        }
    }

    private void callApiChangePass() {
        CommonUtils.showProgressDialog(context);

        ApiRequest apiRequest = new ApiRequest();
        apiRequest.setPassword(changePasswordBinding.enterNewPass.getText().toString());
        apiRequest.setPassword_confirmation(changePasswordBinding.confrmNewPass.getText().toString());
        apiRequest.setEmail(email);
        apiRequest.setDevice_token(AppConstant.REG_TOKEN);

        ApiUtils.callApiChangePass(context, apiRequest, new ApiResponseInterface() {
            @Override
            public void onResponse(Response<ApiResponse> response) {
                CommonUtils.hideProgressDialog();
                if (response.code()== AppConstant.SUCCESS_CODE){
                    CommonUtils.showToast(context, ChangeLanguage.translate(context,response.body().getMessage()));
                    PreferencesManager.Companion.saveBooleanPreferences(context,AppConstant.IS_LOGIN,true);
                    Intent intent=new Intent(context,MainActivity.class);
                    startActivity(intent);
                    finishAffinity();
                }
                else if (response.code()==AppConstant.VALIDATION_ERROR_CODE){
                    errorParse(response);
                }
                else if (response.code()==AppConstant.SUCCESS_CODE_201){
                    CommonUtils.showToast(context,ChangeLanguage.translate(context,response.body().getMessage()));
                }
                else {
//                    PreferencesManager.Companion.saveBooleanPreferences(context,AppConstant.IS_LOGIN,false);
                    CommonUtils.showToast(context,getString(R.string.something_went_wrong));

                }
            }

            @Override
            public void onFailure() {
                CommonUtils.hideProgressDialog();
                ToastUtil.Companion.showShortToast(context,getString(R.string.something_went_wrong));

            }
        });
    }

    private void errorParse(Response<ApiResponse> response) {
        if (response.errorBody()!=null){
            try {
                ApiResponse error = ErrorUtils.parseError(response,context);
                if (error!=null && error.getError()!=null){
                    showError(error.getError());
                }
            }
            catch (Exception e){

            }
        }
    }

    private void showError(ErrorModel error) {
        if (error.getPassword()!=null){
            changePasswordBinding.enterNewPass.setError(ChangeLanguage.translate(context,error.getPassword().get(0)));
        }
        if (error.getPasswordConfirmation()!=null){
            changePasswordBinding.confrmNewPass.setError(ChangeLanguage.translate(context,error.getPasswordConfirmation().get(0)));
        }
    }
}
