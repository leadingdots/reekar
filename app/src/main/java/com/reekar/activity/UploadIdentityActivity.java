package com.reekar.activity;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.FileProvider;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.LinearLayoutManager;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Bundle;
import android.provider.ContactsContract;
import android.text.TextUtils;
import android.view.View;

import com.akanksha.commonclassutil.CheckPermission;
import com.akanksha.commonclassutil.ToastUtil;
import com.google.gson.Gson;
import com.reekar.R;
import com.reekar.adapter.CarDocumentsAdapter;
import com.reekar.adapter.UploadIdentityAdapter;
import com.reekar.apiservices.ApiUtils;
import com.reekar.commonclasses.AppConstant;
import com.reekar.commonclasses.BaseActivity;
import com.reekar.commonclasses.CommonUtils;
import com.reekar.commonclasses.TakePictureUtils;
import com.reekar.databinding.ActivityUploadIdentityBinding;
import com.reekar.interfaces.ApiResponseInterface;
import com.reekar.interfaces.RecyclerviewItemClickInterface;
import com.reekar.models.ApiResponse;
import com.reekar.models.UploadFileModel;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import okhttp3.MediaType;
import okhttp3.RequestBody;
import retrofit2.Response;

import static com.reekar.commonclasses.TakePictureUtils.TAKE_PICTURE;

public class UploadIdentityActivity extends BaseActivity {

    private ActivityUploadIdentityBinding uploadIdentityBinding;

    private Context context;
    private String image_="image";
    private UploadIdentityAdapter uploadIdentityAdapter;
    private ArrayList<UploadFileModel> uploadFileModelArrayList = new ArrayList<>();

    private Uri uri;
    private File file;

    private int position=0;
    private String bookingId;

    private String carId;
    private String mPickDate;
    private String mDropDate;
    private String mPickuLocation;
    private String mPickTime;
    private String mDropTime;

    private String docName;
    private String totalAmt;
    private String fname;
    private String lname;
    private String ccode;
    private String phoneNum;
    private String alternativeNum;
    private String address;
    private double pricePerDay;
    private double securityAmt;
    private double grandTotalAmt;
    private String email;
    private double grandTotalUSD;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        uploadIdentityBinding= DataBindingUtil.setContentView(this,R.layout.activity_upload_identity);
        context=this;

        getIntentData();
        setToolbar();
        setListener();
        setCarDocumentsAdapter();
    }

    private void getIntentData() {
        Intent intent =getIntent();
        if (intent!=null){
            if (intent.getStringExtra(AppConstant.BOOKING_FROM_DATE)!=null){
                mPickDate=intent.getStringExtra(AppConstant.BOOKING_FROM_DATE);
            }
            if (intent.getStringExtra(AppConstant.BOOKING_FROM_TIME)!=null){
                mPickTime=intent.getStringExtra(AppConstant.BOOKING_FROM_TIME);
            }
            if (intent.getStringExtra(AppConstant.BOOKING_TO_DATE)!=null){
                mDropDate=intent.getStringExtra(AppConstant.BOOKING_TO_DATE);
            }
            if (intent.getStringExtra(AppConstant.BOOKING_TO_TIME)!=null){
                mDropTime=intent.getStringExtra(AppConstant.BOOKING_TO_TIME);
            }
            if (intent.getStringExtra(AppConstant.PICKUP_LOCATION)!=null){
                mPickuLocation=intent.getStringExtra(AppConstant.PICKUP_LOCATION);
            }
            if (intent.getStringExtra(AppConstant.CAR_ID)!=null){
                carId=intent.getStringExtra(AppConstant.CAR_ID);
            }
            if (intent.getStringExtra(AppConstant.BILLING_FIRST_NAME)!=null){
                fname=intent.getStringExtra(AppConstant.BILLING_FIRST_NAME);
            }
            if (intent.getStringExtra(AppConstant.BILLING_LAST_NAME)!=null){
                lname=intent.getStringExtra(AppConstant.BILLING_LAST_NAME);
            }
            if (intent.getStringExtra(AppConstant.BILLING_MOBILE)!=null){
                phoneNum=intent.getStringExtra(AppConstant.BILLING_MOBILE);
            }
            if (intent.getStringExtra(AppConstant.BILLING_ALTERNATIVE_MOBILE)!=null){
                alternativeNum=intent.getStringExtra(AppConstant.BILLING_ALTERNATIVE_MOBILE);
            }
            if (intent.getStringExtra(AppConstant.BILLING_ADDRESS)!=null){
                address=intent.getStringExtra(AppConstant.BILLING_ADDRESS);
            }
            pricePerDay= intent.getDoubleExtra(AppConstant.PRICE_PER_DAY,0);
            securityAmt= intent.getDoubleExtra(AppConstant.SECURITY_AMOUNT,0);
            if (intent.getStringExtra(AppConstant.BILLING_EMAIL)!=null){
                email=intent.getStringExtra(AppConstant.BILLING_EMAIL);
            }
            if (intent.getStringExtra(AppConstant.ID)!=null){
                bookingId=intent.getStringExtra(AppConstant.ID);
            }
            grandTotalAmt=intent.getDoubleExtra(AppConstant.GRAND_TOTAL,0);
            grandTotalUSD=intent.getDoubleExtra(AppConstant.GRAND_TOTAL_USD,0);
        }
    }

    private void setCarDocumentsAdapter(){
        uploadIdentityAdapter = new UploadIdentityAdapter(context, uploadFileModelArrayList,carId,new RecyclerviewItemClickInterface() {
            @Override
            public void onItemClick(int position) {
                hideShow();
            }
        });
        uploadIdentityBinding.recyclerview.setLayoutManager(new LinearLayoutManager(context));
        uploadIdentityBinding.recyclerview.setAdapter(uploadIdentityAdapter);
    }

    private void hideShow() {
        if (uploadFileModelArrayList.size()==2){
            uploadIdentityBinding.uploadIdFirst.setVisibility(View.GONE);
        }
        else {
            uploadIdentityBinding.uploadIdFirst.setVisibility(View.VISIBLE);
        }
        if (uploadFileModelArrayList.size()==2){
            uploadIdentityBinding.submit.setVisibility(View.VISIBLE);
        }
        else {
            uploadIdentityBinding.submit.setVisibility(View.GONE);
        }
    }

    @Override
    public void setToolbar() {
        uploadIdentityBinding.uploadIdToolbar.tvToolbarBarName.setText(R.string.upload_identity);


    }

    @Override
    public void setListener() {
        uploadIdentityBinding.uploadIdToolbar.custToolBckBtn.setOnClickListener(this);
        uploadIdentityBinding.uploadIdFirst.setOnClickListener(this);
        uploadIdentityBinding.uploadIdSecond.setOnClickListener(this);
        uploadIdentityBinding.submit.setOnClickListener(this);

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){

            case R.id.cust_tool_bck_btn:{
                finish();
                break;
            }
            case R.id.upload_id_first:{
                image_ = "image"+System.currentTimeMillis();
                checkPermission();
                break;
            }

            case R.id.submit:{
                if (file!=null){
                    if (CommonUtils.isNetworkAvailable(context))
                    callApiUploadId();
                    else
                        ToastUtil.Companion.showShortToast(context,getString(R.string.failed_internet));
                }
                else {
                    ToastUtil.Companion.showShortToast(context,getString(R.string.docuements_upload_msg));
                }
                break;
            }

        }
    }

    private void checkPermission(){
        if (CheckPermission.Companion.checkIsMarshMallowVersion()){
            if (CheckPermission.Companion.checkCameraStoragePermission(context)){
                CommonUtils.showImageDailoge(context,image_,true);
            }
            else {
                CheckPermission.Companion.requestCameraStoragePermission((Activity) context);
            }
        }
        else {
            CommonUtils.showImageDailoge(context,image_,true);
        }
    }
    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);

        switch (requestCode){

            case CheckPermission.REQUEST_CODE_CAMERA_STORAGE_PERMISSION:{
                if (grantResults.length>0 && grantResults[0]== PackageManager.PERMISSION_GRANTED){
                    CommonUtils.showImageDailoge(context,image_,false);
                }
                break;
            }
        }
    }
    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode==Activity.RESULT_OK)
            switch (requestCode){

                case TakePictureUtils.TAKE_PICTURE:
                    setImage(data,requestCode);
                    break;
                case TakePictureUtils.PICK_GALLERY: {
                    try {
                        InputStream inputStream = getContentResolver().openInputStream(data.getData());
                        FileOutputStream fileOutputStream = new FileOutputStream(new File(getExternalFilesDir("temp"), image_ + ".jpg"));
                        TakePictureUtils.copyStream(inputStream, fileOutputStream);
                        fileOutputStream.close();
                        inputStream.close();

                    } catch (FileNotFoundException e) {
                        e.printStackTrace();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                     /*
                      check the file before getting from gallary
                     */
                    String fileType = getContentResolver().getType(data.getData());
                    if (fileType != null && (fileType.equals(AppConstant.IMAGE_JPEG) ||
                            fileType.equals(AppConstant.IMAGE_JPG) ||
                            fileType.equals(AppConstant.IMAGE_PNG)))
                        setImage(data, requestCode);
                    else {
                        ToastUtil.Companion.showShortToast(context, getString(R.string.invalid_image));
                    }
                    break;
                   }

                   case TakePictureUtils.PICK_DOCUMENT: {
                        if (data!= null && data.getData()!= null)
                            setDocument(data, requestCode);
                        break;
                    }

            }
    }
    private void setDocument(Intent data, int requestCode) {
        UploadFileModel uploadFileModel=new UploadFileModel();
        String filePath= TakePictureUtils.getFilePath(data.getData(),context);
        file =new File(filePath);
        uploadFileModel.setFile(file);
        uploadFileModel.setFiletype(getContentResolver().getType(data.getData()));
        uploadFileModel.setUri(data.getData());
        uploadFileModelArrayList.add(0,uploadFileModel);
        uploadIdentityAdapter.notifyItemInserted(0);
        uploadIdentityAdapter.notifyItemRangeChanged(0,uploadFileModelArrayList.size());
        hideShow();

    }
    private void setImage(Intent data, int requestCode) {
        UploadFileModel uploadFileModel = new UploadFileModel();
         file= new File(getExternalFilesDir("temp"), image_ + ".jpg");
        String strFileName = file.getName();
        uri = FileProvider.getUriForFile(context, context.getApplicationContext().getPackageName() + ".provider", file);

        uploadFileModel.setFile(file);
        uploadFileModel.setFiletype(getContentResolver().getType(uri));

        if (requestCode==TakePictureUtils.PICK_GALLERY) {
            uploadFileModel.setUri(data.getData());
        }else
            uploadFileModel.setUri(uri);
        uploadFileModelArrayList.add(0, uploadFileModel);
        uploadIdentityAdapter.notifyItemInserted(0);
        uploadIdentityAdapter.notifyItemRangeChanged(0, uploadFileModelArrayList.size());
        uploadIdentityBinding.recyclerview.smoothScrollToPosition(0);
        hideShow();

//        uploadFileModel.setFile(file);
//        uploadFileModel.setFiletype(getContentResolver().getType(uri));

//        if (requestCode == TakePictureUtils.PICK_GALLERY) {
//            uploadIdentityBinding.tvIdDoducmentFirst.setText(strFileName);
//            uploadFileModel.setUri(data.getData());
//        } else if (requestCode == TAKE_PICTURE) {
//            uploadIdentityBinding.tvIdDoducmentFirst.setText(strFileName);
//            uploadFileModel.setUri(uri);
//        }
//        uploadFileModelArrayList.add(0, uploadFileModel);

    }


    private void callApiUploadId(){
        CommonUtils.showProgressDialog(context);

        Map<String, RequestBody> map=new HashMap<>();
        map.put("booking_id", ApiUtils.toRequestBody(bookingId));

        /* --- for multiple file --- */
        for (int i=0; i<uploadFileModelArrayList.size();i++) {
            if (uploadFileModelArrayList.get(i).getFile()!=null) {
                if (uploadFileModelArrayList.get(i).getFile()!=null) {
                    RequestBody filebody = RequestBody.create(MediaType.parse("image/jpg"), uploadFileModelArrayList.get(i).getFile());
                    map.put("file["+i+"]\"; filename=\"" + uploadFileModelArrayList.get(i).getFile().getName(), filebody);
                }
                /*RequestBody filebody = RequestBody.create(MediaType.parse(uploadFileModelArrayList.get(i).getFiletype()), uploadFileModelArrayList.get(i).getFile());
                map.put("file\"; filename=\"" + uploadFileModelArrayList.get(i).getFile().getName(), filebody);*/
            }
        }
        ApiUtils.callUploadIdApi(context, map, new ApiResponseInterface() {
            @Override
            public void onResponse(Response<ApiResponse> response) {
                CommonUtils.hideProgressDialog();
                if (response.code()==AppConstant.SUCCESS_CODE) {
                    if (response.body() != null) {
                        String msg = response.body().getMessage();
                        ToastUtil.Companion.showShortToast(context, msg);
                        sendData();
                    }

                }
            }

            @Override
            public void onFailure() {
                CommonUtils.hideProgressDialog();
                ToastUtil.Companion.showShortToast(context,getString(R.string.something_went_wrong));

            }
        });
    }

    private void sendData(){
        Intent intent = new Intent(context,PayMethodActivity.class);
        intent.putExtra(AppConstant.ID,bookingId);
        intent.putExtra(AppConstant.CAR_ID,carId);
        intent.putExtra(AppConstant.GRAND_TOTAL,grandTotalAmt);
        intent.putExtra(AppConstant.GRAND_TOTAL_USD,grandTotalUSD);

        startActivity(intent);
    }

}
