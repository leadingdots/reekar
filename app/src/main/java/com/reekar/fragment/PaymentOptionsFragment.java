package com.reekar.fragment;


import android.content.Context;
import android.content.Intent;
import android.os.Bundle;

import androidx.databinding.DataBindingUtil;
import androidx.viewpager.widget.ViewPager;

import android.os.Handler;
import android.util.DisplayMetrics;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CompoundButton;

import com.reekar.CrousleLayout.CarouselPagerAdapter;
import com.reekar.R;
import com.reekar.activity.CreditCardActivity;
import com.reekar.activity.MainActivity;
import com.reekar.adapter.PaymentAdapter;
import com.reekar.commonclasses.AppConstant;
import com.reekar.commonclasses.BaseFragment;
import com.reekar.databinding.FragmentPaymentOptionsBinding;

import java.util.Timer;
import java.util.TimerTask;


public class PaymentOptionsFragment extends BaseFragment {

    private FragmentPaymentOptionsBinding paymentOptionsBinding;
    private Context context;
    private PaymentAdapter paymentAdapter;
    int images[] = {R.mipmap.paypal,R.mipmap.stripe};

    private int currentPage = 0;
    private int curentPage;
    private Timer timer;
    final long DELAY_MS = 400;
    final long PERIOD_MS = 4000;
    //crousle layout
    public final static int LOOPS = 1000;
    public CarouselPagerAdapter adapter;
//    public ViewPager pager;
    public static int count = 3; //ViewPager items size
    /**
     * You shouldn't define first page = 0.
     * Let define firstpage = 'number viewpager size' to make endless carousel
     */
    public static int FIRST_PAGE = 0;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_payment_options, container, false);
        paymentOptionsBinding = DataBindingUtil.bind(view);



        slider();
//        Crousle();
        radioBtn();
        autoSlide();
        setListener();
        setToolbar();
        return view;
    }

    private void slider() {
        paymentAdapter = new PaymentAdapter(context,images, AppConstant.PAYMENT_FRAGMENT);
        paymentOptionsBinding.paymentViewpager.setAdapter(paymentAdapter);
//        paymentOptionsBinding.paymentViewpager.setClipToPadding(false);
//        paymentOptionsBinding.paymentViewpager.setPadding(50,0,50,0);
        paymentOptionsBinding.tabLayoutPaymentActvty.setupWithViewPager(paymentOptionsBinding.paymentViewpager);
    }

    @Override
    public void setToolbar() {
        ((MainActivity) context).setToolBar(true);
        ((MainActivity) context).mainBinding.appBarMain.toolbarTextview.setText(R.string.payment_method);
    }

    @Override
    public void setListener() {
        paymentOptionsBinding.paymentViewpager.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
                curentPage= paymentOptionsBinding.paymentViewpager.getCurrentItem();
            }

            @Override
            public void onPageSelected(int position) {

            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });
    }

    @Override
    public void onClick(View v) {

    }
    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        this.context =context;
    }
    @Override
    public void onResume() {
        super.onResume();
        setToolbar();

    }
    public void autoSlide(){
        /*After setting the adapter use the timer */
        final Handler handler = new Handler();
        final Runnable Update = new Runnable() {
            public void run() {
                if (currentPage == images.length) {
                    currentPage = 0;
                }
                paymentOptionsBinding.paymentViewpager.setCurrentItem(currentPage++, true);
            }
        };

        timer = new Timer(); // This will create a new Thread
        timer.schedule(new TimerTask() { // task to be scheduled
            @Override
            public void run() {
                handler.post(Update);
            }
        }, DELAY_MS, PERIOD_MS);
    }
    public void radioBtn(){
        paymentOptionsBinding.radioBtnDbCard.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    Intent intent = new Intent(context, CreditCardActivity.class);
                    startActivity(intent);
                }
            }
        });

        paymentOptionsBinding.radioBtnCcCard.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    Intent intent = new Intent(context, CreditCardActivity.class);
                    startActivity(intent);
                }
            }
        });
    }

    private void Crousle(){
        //set page margin between pages for viewpager
        DisplayMetrics metrics = new DisplayMetrics();
        getActivity().getWindowManager().getDefaultDisplay().getMetrics(metrics);
        int pageMargin = ((metrics.widthPixels / 8) * 2);
        paymentOptionsBinding.paymentViewpager.setPageMargin(-pageMargin);

        adapter = new CarouselPagerAdapter(context, getActivity().getSupportFragmentManager(),paymentOptionsBinding.paymentViewpager);
        paymentOptionsBinding.paymentViewpager.setAdapter(adapter);
        adapter.notifyDataSetChanged();

        paymentOptionsBinding.paymentViewpager.addOnPageChangeListener(adapter);
//        paymentOptionsBinding.tabLayoutPaymentActvty.setupWithViewPager(paymentOptionsBinding.paymentViewpager);

        // Set current item to the middle page so we can fling to both
        // directions left and right
        paymentOptionsBinding.paymentViewpager.setCurrentItem(FIRST_PAGE);
        paymentOptionsBinding.paymentViewpager.setOffscreenPageLimit(3);
    }

}
