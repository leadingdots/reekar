package com.reekar.fragment;


import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.LinearLayout;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.FragmentActivity;

import com.akanksha.commonclassutil.CheckValidation;
import com.akanksha.commonclassutil.PreferencesManager;
import com.akanksha.commonclassutil.ToastUtil;
import com.facebook.AccessToken;
import com.facebook.AccessTokenTracker;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.facebook.Profile;
import com.facebook.login.LoginManager;
import com.facebook.login.LoginResult;
import com.google.android.gms.auth.api.Auth;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.auth.api.signin.GoogleSignInResult;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.hbb20.CountryCodePicker;
import com.reekar.R;
import com.reekar.activity.ForgotPasswordActivity;
import com.reekar.activity.MainActivity;
import com.reekar.activity.SignUpActivity;
import com.reekar.apiservices.APIInterface;
import com.reekar.apiservices.ApiUtils;
import com.reekar.commonclasses.AppConstant;
import com.reekar.commonclasses.BaseFragment;
import com.reekar.commonclasses.CommonUtils;
import com.reekar.commonclasses.ErrorUtils;
import com.reekar.databinding.FragmentLoginBinding;
import com.reekar.interfaces.ApiResponseInterface;
import com.reekar.models.ApiRequest;
import com.reekar.models.ApiResponse;
import com.reekar.models.ErrorModel;
import com.reekar.translation.ChangeLanguage;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.Arrays;

import retrofit2.Response;

import static com.google.android.gms.common.api.GoogleApiClient.Builder;

public class LoginFragment extends BaseFragment implements GoogleApiClient.OnConnectionFailedListener {


    private FragmentLoginBinding loginBinding;
    private Context context;
    private APIInterface mApiInterface;
    private View view;

    private GoogleApiClient googleApiClient;
    private static final int RC_SIGN_IN = 1;

    private String fname;
    private String email;
    private String onlineImage;
    private String mNumber;
    private String cccode;

    private EditText input;
    private CountryCodePicker ccp;

    private static final String EMAIL = "email";
    private LinearLayout loginButton;
    private CallbackManager callbackManager;
    private AccessTokenTracker accessTokenTracker;
    private String mFrom;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_login, container, false);
        loginBinding = DataBindingUtil.bind(view);


        mGetIntent();
        setRememberMe();
        facebookLogin();
        signInGoogle();
        setToolbar();
        setListener();
        return view;
    }

    private void mGetIntent() {
        if (getArguments() != null){
            if (getArguments().getString(AppConstant.FROM) != null){
                mFrom = getArguments().getString(AppConstant.FROM);
            }
        }
    }

    private void setRememberMe() {
//        saveLogin =PreferencesManager.Companion.getStringPreferences()
        loginBinding.etEmailLogin.setText(PreferencesManager.Companion.getStringPreferences(context,AppConstant.SAVE_LOGIN_EMAIL));
        loginBinding.etPassLogin.setText(PreferencesManager.Companion.getStringPreferences(context, AppConstant.SAVE_LOGIN_PASS));

        if (!TextUtils.isEmpty(loginBinding.etEmailLogin.getText()))
            loginBinding.saveLoginCheckbox.setChecked(true);

    }

    @Override
    public void setToolbar() {
        ((MainActivity)context).setToolBar(loginBinding.navBtn);
    }

    @Override
    public void setListener() {
        loginBinding.gotoSignup.setOnClickListener(this);
        loginBinding.loginTv.setOnClickListener(this);
        loginBinding.iFogotpass.setOnClickListener(this);
        loginBinding.gmailLogin.setOnClickListener(this);
        loginBinding.facebookSigninBtn.setOnClickListener(this);

    }


    @Override
    public void onClick(View v) {
        switch (v.getId()){

            case R.id.goto_signup:
            {
                Intent intent = new Intent(getActivity(), SignUpActivity.class);
                startActivity(intent);
                break;
            }
            case  R.id.login_tv:
            {
                validation();
                break;
            }
            case R.id.gmail_login:{
                Intent intent = Auth.GoogleSignInApi.getSignInIntent(googleApiClient);
                startActivityForResult(intent,RC_SIGN_IN);
                break;

            }
            case R.id.i_fogotpass:{
                Intent intent = new Intent(context, ForgotPasswordActivity.class);
                startActivity(intent);
                break;
            }
            case R.id.facebook_signin_btn:{
                LoginManager.getInstance().logInWithReadPermissions((FragmentActivity) context, Arrays.asList("email", "public_profile"));
                break;
            }

        }
    }

    @Override
    public void onStart() {
        super.onStart();
    }

    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);
        this.context =context;
    }
    @Override
    public void onResume() {
        super.onResume();
        setToolbar();
    }

    @Override
    public void onPause() {
        super.onPause();
        googleApiClient.stopAutoManage((FragmentActivity) context);
        googleApiClient.disconnect();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        LoginManager.getInstance().logOut();
//         We stop the tracking before destroying the activity
        accessTokenTracker.stopTracking();
    }

    private void facebookLogin() {
        callbackManager = CallbackManager.Factory.create();
        loginButton = view.findViewById(R.id.facebook_signin_btn);

//        loginButton.setReadPermissions(Arrays.asList("email", "public_profile"));

        LoginManager.getInstance().registerCallback(callbackManager, new FacebookCallback<LoginResult>() {

            private Profile mProfile;
            @Override
            public void onSuccess(LoginResult loginResult) {
                AccessToken accessToken = loginResult.getAccessToken();
                useLoginInformation(accessToken);
            }
            @Override
            public void onCancel() {
                // App code
                CommonUtils.showToast(context,"Sign in cancel");
            }
            @Override
            public void onError(FacebookException exception) {
                // App code
            }
        });

        // Defining the AccessTokenTracker
        accessTokenTracker = new AccessTokenTracker() {
            // This method is invoked everytime access token changes
            @Override
            protected void onCurrentAccessTokenChanged(AccessToken oldAccessToken, AccessToken currentAccessToken) {
                useLoginInformation(currentAccessToken);

            }
        };


    }

    private void useLoginInformation(AccessToken accessToken) {

        GraphRequest request = GraphRequest.newMeRequest(accessToken, new GraphRequest.GraphJSONObjectCallback() {
            //On Completed is invoked once the GraphRequest is successful
            @Override
            public void onCompleted(JSONObject object, GraphResponse response) {
                try {
                    if (object!=null && response!=null) {
                        if (object.getString("name") != null)
                            fname = object.getString("name");
                        if (object.getString("email") != null)
                            email = object.getString("email");
                        if (object.getJSONObject("picture").getJSONObject("data").getString("url") != null)
                            onlineImage = object.getJSONObject("picture").getJSONObject("data").getString("url");

//                        alertBoxEditText();
                        if (CommonUtils.isNetworkAvailable(context))
                        callApiSocialLogin();
                        else
                            ToastUtil.Companion.showShortToast(context,getString(R.string.failed_internet));
                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        });
        // We set parameters to the GraphRequest using a Bundle.
        Bundle parameters = new Bundle();
        parameters.putString("fields", "id,name,email,picture.width(200)");
        request.setParameters(parameters);
        // Initiate the GraphRequest
        request.executeAsync();
    }
    private void alertBoxEditText() {
        final AlertDialog.Builder alert = new AlertDialog.Builder(getActivity());
        alert.setCancelable(false);
        final LayoutInflater inflater = this.getLayoutInflater();
        View dialogView = inflater.inflate(R.layout.custom_dailog, null);

//        alert.setTitle("Add mobile number");
//        alert.setMessage("Please enter mobile number");

    // Set an EditText view to get user input
//        final EditText input = new EditText(getActivity());
        alert.setView(dialogView);

        input = (EditText) dialogView.findViewById(R.id.et_add_number);
        ccp = (CountryCodePicker) dialogView.findViewById(R.id.country_code_picker);
//        input.setText("");

        alert.setPositiveButton(getString(R.string.proceed), new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int whichButton) {
                if (input.getText()!=null) {
                    cccode = ccp.getSelectedCountryCodeWithPlus().toString();
                    mNumber = input.getText().toString();
                    if (mNumber.isEmpty()) {
//                        input.setError(getString(R.string.please_enter_phonenumber));
//                        input.requestFocus();

                        ToastUtil.Companion.showShortToast(context, getString(R.string.enter_mobile_number));
                    } else if (input.getText().length()<4) {
//                        input.setError(getString(R.string.correct_mobile_number));
//                        input.requestFocus();
                        ToastUtil.Companion.showShortToast(context, getString(R.string.correct_mobile_number));

                    } else {
                        if (CommonUtils.isNetworkAvailable(context))
                            callApiSocialLogin();
                        else
                            ToastUtil.Companion.showShortToast(context, getString(R.string.failed_internet));
                    }
                }

            }
        });


        alert.show();
    }

    public void validation() {

        String email = loginBinding.etEmailLogin.getText().toString();
        String pass = loginBinding.etPassLogin.getText().toString();
        String emailPattern = "[a-zA-Z0-9._-]+@[a-z]+\\.+[a-z]+";


        if (email.isEmpty()) {
            loginBinding.etEmailLogin.setError(getString(R.string.please_enter_email));
            loginBinding.etEmailLogin.requestFocus();
        }

        else if (!email.matches(emailPattern)) {
            loginBinding.etEmailLogin.setError(getString(R.string.please_enter_correct_email));
            loginBinding.etEmailLogin.requestFocus();
        }

        else if (pass.isEmpty()) {
            loginBinding.etPassLogin.setError(getString(R.string.please_enter_correct_pass2));
            loginBinding.etPassLogin.requestFocus();
        }
        else if (pass.length()<8) {
            loginBinding.etPassLogin.setError(getString(R.string.please_enter_correct_pass));
            loginBinding.etPassLogin.requestFocus();

        }

        else {
//             CommonUtils.setFragment(new MainFragment(), false, getActivity(), R.id.container_main, AppConstant.MAIN_FRAGMENT);
            if (CommonUtils.isNetworkAvailable(context))
            callLoginApi();
            else
                ToastUtil.Companion.showShortToast(context,getString(R.string.failed_internet));

        }
    }

    /*
     google sign in
    */
    public void signInGoogle(){
        GoogleSignInOptions gso =  new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestEmail()
                .build();
        googleApiClient=new Builder(getActivity())
                .enableAutoManage(getActivity(), this)
                .addApi(Auth.GOOGLE_SIGN_IN_API,gso)
                .build();
    }
    private void handleSignInResult(GoogleSignInResult result){
        if(result.isSuccess()){
//            gotoProfile();
            if (result.getSignInAccount()!=null) {
                fname=result.getSignInAccount().getDisplayName();
                email= result.getSignInAccount().getEmail();

                if (result.getSignInAccount().getPhotoUrl()!=null){
                    onlineImage = result.getSignInAccount().getPhotoUrl().toString();
                    Log.e("gimage",onlineImage);
                }
//                alertBoxEditText();
                if (CommonUtils.isNetworkAvailable(context))
                callApiSocialLogin();
                else
                    ToastUtil.Companion.showShortToast(context,getString(R.string.failed_internet));


            }
//      PreferencesManager.Companion.saveBooleanPreferences(getActivity(), AppConstant.LOGINCHECK,true);
        }else{
            CommonUtils.showToast(context,"Sign in cancel");
        }

        if (googleApiClient.isConnected()) {
            Auth.GoogleSignInApi.signOut(googleApiClient);
            googleApiClient.disconnect();
        }
    }

    public void callLoginApi() {
        CommonUtils.showProgressDialog(context);

        ApiRequest apiRequest = new ApiRequest();
        apiRequest.setEmail(loginBinding.etEmailLogin.getText().toString());
        apiRequest.setPassword(loginBinding.etPassLogin.getText().toString());
        apiRequest.setMyip(CommonUtils.getMobileIPAddress());
        apiRequest.setDevice_type(AppConstant.DEVICE_TYPE);
        apiRequest.setDevice_token(PreferencesManager.Companion.getStringPreferences(context, AppConstant.DEVICE_TOKEN));

        ApiUtils.callApiLogin(context, apiRequest, new ApiResponseInterface() {
            @Override
            public void onResponse(Response<ApiResponse> response) {
                CommonUtils.hideProgressDialog();
                if (response.code() == AppConstant.SUCCESS_CODE) {
                    if (response.body() != null) {
                        String msg = response.body().getMessage();
                        saveData(response);


                        //CommonUtils.setFragment(new DashboardFragment(), false, getActivity(), R.id.container_main, AppConstant.DASHBOARD);
                        PreferencesManager.Companion.saveBooleanPreferences(context,AppConstant.IS_LOGIN,true);
                        if (loginBinding.saveLoginCheckbox.isChecked()){
                            PreferencesManager.Companion.saveStringPreferences(context,AppConstant.SAVE_LOGIN_EMAIL,loginBinding.etEmailLogin.getText().toString());
                            PreferencesManager.Companion.saveStringPreferences(context,AppConstant.SAVE_LOGIN_PASS,loginBinding.etPassLogin.getText().toString());
                        }
                        else {
                            PreferencesManager.Companion.clearPreference(context,AppConstant.SAVE_LOGIN_EMAIL);
                            PreferencesManager.Companion.clearPreference(context,AppConstant.SAVE_LOGIN_PASS);
                        }

                        CommonUtils.showToast(context, ChangeLanguage.translate(context,msg));

                        if (mFrom != null){
                            ((Activity)context).finish();
                        } else {
                            Intent intent = new Intent(context,MainActivity.class);
                            startActivity(intent);
                            ((Activity)context).finishAffinity();
                        }
                    }
                }
                else if (response.code() == AppConstant.VALIDATION_ERROR_CODE){
                    parseError(response);

                }
                else if (response.code() == AppConstant.UNAUTHORIZED_CODE){
                    parseError(response);
                }
                else if (response.code()==AppConstant.SUCCESS_CODE_201){
                    CommonUtils.showToast(context,ChangeLanguage.translate(context,response.body().getMessage()));
                }
                else {
                    CommonUtils.showToast(context,getString(R.string.something_went_wrong));

                }
            }

            @Override
            public void onFailure() {
                CommonUtils.hideProgressDialog();
                ToastUtil.Companion.showShortToast(context,getString(R.string.something_went_wrong));

            }
        });
    }

    public void parseError(Response<ApiResponse> response){

        if (response.errorBody()!=null){
            try {
                ApiResponse error = ErrorUtils.parseError(response,context);
                if (error!=null && error.getError()!=null){
                    showError(error.getError());
                }
            }
            catch (Exception e){

            }
        }
    }

    private void showError(ErrorModel error) {
        if (error.getEmail()!=null){
            loginBinding.etEmailLogin.setError(ChangeLanguage.translate(context,error.getEmail().get(0)));
        }
        if (error.getPassword()!=null){
            loginBinding.etPassLogin.setError(ChangeLanguage.translate(context,error.getPassword().get(0)));
        }
    }

    /**
     * This method is used to save data in shared preference
     */
    private void saveData(Response<ApiResponse> response) {
        PreferencesManager.Companion.saveBooleanPreferences(context,AppConstant.IS_LOGIN,true);
        if (response.body().getAccessCode() != null)
            PreferencesManager.Companion.saveStringPreferences(context,AppConstant.ACCESS_CODE,response.body().getAccessCode());
        if (response.body().getFirstNaame() != null)
            PreferencesManager.Companion.saveStringPreferences(context,AppConstant.FIRST_NAME,response.body().getFirstNaame());
        if (response.body().getLastName() != null)
            PreferencesManager.Companion.saveStringPreferences(context,AppConstant.LAST_NAME,response.body().getLastName());
        if (response.body().getUserImage() != null)
            PreferencesManager.Companion.saveStringPreferences(context,AppConstant.USER_IMAGE,response.body().getUserImage());
//        if (response.body().getUserCurrency()!=null)
//            PreferencesManager.Companion.saveStringPreferences(context,AppConstant.CURRENT_CURRENCY,response.body().getUserCurrency());
    }


    private void callApiSocialLogin() {
        CommonUtils.showProgressDialog(context);
        ApiRequest apiRequest = new ApiRequest();
        apiRequest.setImage(onlineImage);
        apiRequest.setName(fname);
        apiRequest.setEmail(email);
        apiRequest.setCountry_code(cccode);
        apiRequest.setMobile(mNumber);
        apiRequest.setMyip(CommonUtils.getMobileIPAddress());
        apiRequest.setDevice_type(AppConstant.DEVICE_TYPE);
        apiRequest.setDevice_token(PreferencesManager.Companion.getStringPreferences(context, AppConstant.DEVICE_TOKEN));

        ApiUtils.callApiSocialLogin(context, apiRequest, new ApiResponseInterface() {
            @Override
            public void onResponse(Response<ApiResponse> response) {
                CommonUtils.hideProgressDialog();
                if (response.code() == AppConstant.SUCCESS_CODE) {
                    String msg = response.body().getMessage();
                    if (response.body() != null) {
                        if (response.body().getHasMobile()==0){
                            alertBoxEditText();
                        }
                        if (response.body().getHasMobile()==1){
                            saveData(response);

                            //CommonUtils.setFragment(new DashboardFragment(), false, getActivity(), R.id.container_main, AppConstant.DASHBOARD);

                            CommonUtils.showToast(context, ChangeLanguage.translate(context, msg));

                            if (mFrom != null){
                                ((Activity)context).finish();
                            } else {
                                Intent intent = new Intent(context,MainActivity.class);
                                startActivity(intent);
                                ((Activity) context).finishAffinity();

                            }
                        }
                    }
                }
                else if (response.code() == AppConstant.VALIDATION_ERROR_CODE){
                    parseError(response);

                }
                else if (response.code() == AppConstant.UNAUTHORIZED_CODE){
                    parseError(response);
                }
                else if (response.code()==AppConstant.SUCCESS_CODE_201){
                    CommonUtils.showToast(context,response.body().getMessage());
                }
                else {
                    CommonUtils.showToast(context,getString(R.string.something_went_wrong));

                }
            }

            @Override
            public void onFailure() {
                CommonUtils.hideProgressDialog();
                ToastUtil.Companion.showShortToast(context,getString(R.string.something_went_wrong));

            }
        });
    }

    private void gotoProfile(){
        Intent intent=new Intent(getActivity(),MainActivity.class);
        startActivity(intent);

    }
    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        callbackManager.onActivityResult(requestCode, resultCode, data);
        if(requestCode==RC_SIGN_IN){
            GoogleSignInResult result = Auth.GoogleSignInApi.getSignInResultFromIntent(data);
            handleSignInResult(result);
        }
    }
    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {
        CommonUtils.showToast(context,getString(R.string.something_went_wrong));

    }

}
