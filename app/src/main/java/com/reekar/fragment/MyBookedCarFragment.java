package com.reekar.fragment;


import android.content.Context;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.akanksha.commonclassutil.ToastUtil;
import com.reekar.R;
import com.reekar.activity.MainActivity;
import com.reekar.adapter.MyBookedCarAdapter;
import com.reekar.apiservices.ApiUtils;
import com.reekar.commonclasses.AppConstant;
import com.reekar.commonclasses.BaseFragment;
import com.reekar.commonclasses.CommonUtils;
import com.reekar.commonclasses.EndlessRecyclerViewScrollListener;
import com.reekar.databinding.FragmentMyBookedCarBinding;
import com.reekar.interfaces.ApiResponseInterface;
import com.reekar.models.ApiRequest;
import com.reekar.models.ApiResponse;
import com.reekar.models.DataModel;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Response;


public class MyBookedCarFragment extends BaseFragment {

    private FragmentMyBookedCarBinding binding;
    private Context context;
    private MyBookedCarAdapter myBookedCarAdapter;

    private LinearLayoutManager layoutManager;
    private int currentPage = 1;
    private int totalPage = 1;
    private List<DataModel> arlCar = new ArrayList<>();


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
           View view =inflater.inflate(R.layout.fragment_my_booked_car, container, false);
           binding = DataBindingUtil.bind(view);


        setRecyclerViewAdapter();
        setToolbar();
        setListener();
        if (CommonUtils.isNetworkAvailable(context)){
            callApiMyBookedCar(true);
        }
        else
            ToastUtil.Companion.showShortToast(context,getString(R.string.failed_internet));
        return view;
    }

    private void setRecyclerViewAdapter() {
        myBookedCarAdapter = new MyBookedCarAdapter(context,arlCar);
        binding.rvCarList.setHasFixedSize(true);
        binding.rvCarList.setLayoutManager(new LinearLayoutManager(context));
        binding.rvCarList.setAdapter(myBookedCarAdapter);

    }
    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);
        this.context =context;
    }
    @Override
    public void onResume() {
        super.onResume();
        setToolbar();

    }

    @Override
    public void setToolbar() {
        ((MainActivity) context).setToolBar(true);
        ((MainActivity) context).mainBinding.appBarMain.toolbarTextview.setText(R.string.my_booked_car);
    }

    @Override
    public void setListener() {
        binding.swipeRefresh.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                if (CommonUtils.isNetworkAvailable(context)) {
                    currentPage = 1;
                    callApiMyBookedCar(false);
                }
                else
                    ToastUtil.Companion.showShortToast(context,getString(R.string.failed_internet));
            }
        });

    }

    @Override
    public void onClick(View v) {

    }

    private void callApiMyBookedCar(boolean showProgress){
        if (showProgress)
            CommonUtils.showProgressDialog(context);

        ApiRequest apiRequest = new ApiRequest();
        apiRequest.setPage(currentPage);
        apiRequest.setPaginate(1);

        CommonUtils.showProgressDialog(context);
        ApiUtils.callMyBookedCarApi(context, new ApiResponseInterface() {
            @Override
            public void onResponse(Response<ApiResponse> response) {
                CommonUtils.hideProgressDialog();
                binding.swipeRefresh.setRefreshing(false);
                myBookedCarAdapter.showProgress(false);
                switch (response.code()){
                    case AppConstant.SUCCESS_CODE:
                        if (currentPage == 1)
                            arlCar.clear();
                        if (response.body().getData() != null && response.body().getData().size()>0) {
                            binding.tvNoData.noDataParent.setVisibility(View.GONE);
                            arlCar.addAll(response.body().getData());
                            myBookedCarAdapter.notifyDataSetChanged();
                        }
                        else {
                            binding.tvNoData.noDataParent.setVisibility(View.VISIBLE);
                            binding.tvNoData.tvNoData.setText(getString(R.string.no_data_found));
                        }
                        currentPage++;
                        totalPage = response.body().getTotalPage();

                        break;
                    case AppConstant.UNAUTHORIZED_CODE:
                        CommonUtils.navigateToLogin(context);
                    default:
                        CommonUtils.showToast(context,getString(R.string.something_went_wrong));
                }
            }
            @Override
            public void onFailure() {
                binding.swipeRefresh.setRefreshing(false);
                myBookedCarAdapter.showProgress(false);
                CommonUtils.hideProgressDialog();
                CommonUtils.showToast(context,getString(R.string.something_went_wrong));
            }
        });
    }
}
