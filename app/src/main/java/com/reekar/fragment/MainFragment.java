package com.reekar.fragment;


import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.databinding.DataBindingUtil;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;

import com.akanksha.commonclassutil.CheckPermission;
import com.akanksha.commonclassutil.PreferencesManager;
import com.reekar.R;

import com.reekar.activity.CarInfromationActivity;
import com.reekar.activity.LanguageActivity;
import com.reekar.activity.MainActivity;
import com.reekar.activity.SearchCarActivity;
import com.reekar.commonclasses.AppConstant;
import com.reekar.commonclasses.BaseFragment;
import com.reekar.commonclasses.CommonUtils;
import com.reekar.databinding.FragmentMainBinding;
import com.reekar.interfaces.OnLocationChangeInterface;
import com.reekar.location.GPSTrackerNew;
import com.reekar.translation.ChangeLanguage;
import com.reekar.translation.Language;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Currency;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import static android.app.Activity.RESULT_OK;


public class MainFragment extends BaseFragment implements AdapterView.OnItemSelectedListener {

    private FragmentMainBinding fragmentMainBinding;

    private Context context;

    private String language = Language.ENGLISH;

    private GPSTrackerNew tracker;
    private String currentDate;
    private String currentTime;

    private String mAddress;
    private double lat;
    private double lng;

    private Boolean login;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_main, container, false);
        fragmentMainBinding = DataBindingUtil.bind(view);
        login  = PreferencesManager.Companion.getBooleanPreferences(context,AppConstant.IS_LOGIN);
//         currentTime = new SimpleDateFormat("hh:mm: a", Locale.getDefault()).format(new Date());



        getLastSelectedLanguage();
        setListener();
        setToolbar();
        return view;
    }

    @Override
    public void setToolbar() {
        ((MainActivity)context).setToolBar(fragmentMainBinding.navBtn);
    }

    @Override
    public void setListener() {
        fragmentMainBinding.bookCarBtn.setOnClickListener(this);
        fragmentMainBinding.listCarBtn.setOnClickListener(this);
        fragmentMainBinding.viewBtn.setOnClickListener(this);
        fragmentMainBinding.ivContact.setOnClickListener(this);
        fragmentMainBinding.ivLng.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){

            case R.id.book_car_btn:{
                if (login){
                    CommonUtils.setFragment(new DashboardFragment(),true,getActivity(),R.id.container_mainn, AppConstant.DASHBOARD);
                }
                else
                CommonUtils.setFragment(new BookCarFragment(),true,getActivity(),R.id.container_mainn, AppConstant.BOOK);
                break;
            }
            case R.id.list_car_btn:{
                if (login){
                    Intent intent =new Intent(context, CarInfromationActivity.class);
                    startActivity(intent);
//                    CommonUtils.setFragment(new DashboardFragment(),true,getActivity(),R.id.container_mainn, AppConstant.DASHBOARD);
                }
                else
                    CommonUtils.setFragment(new ListCarFragment(),true,getActivity(),R.id.container_mainn, AppConstant.LIST);
                break;
            }
            case R.id.view_btn:{
                if (CheckPermission.Companion.checkIsMarshMallowVersion()){
                    if (CheckPermission.Companion.checkLocationPermission(context)){
                        getLocation();
                    } else
                        CheckPermission.Companion.requestLocationPermission((Activity) context);
                }
                else
                    getLocation();
                break;
                //CommonUtils.setFragment(new DashboardFragment(),true,getActivity(),R.id.container_mainn, AppConstant.DASHBOARD);
            }
            case R.id.iv_contact:{
                CommonUtils.setFragment(new ContactFragment(),true,getActivity(),R.id.container_mainn, AppConstant.CONTACT_FRAGMENT);
                break;
            }
            case R.id.iv_lng:{
                Intent intent=new Intent(context, LanguageActivity.class);
                startActivity(intent);
                break;
            }
        }
    }


    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        switch (requestCode){
            case CheckPermission.REQUEST_CODE_LOCATION_PERMISSION:{
                if (grantResults.length>0 && grantResults[0] == PackageManager.PERMISSION_GRANTED){
                    getLocation();
                }
            }
        }
    }

    private void getLocation() {
//        if (isGpsEnabled)
//        CommonUtils.showProgressDialog(context);

         tracker = new GPSTrackerNew(context, new OnLocationChangeInterface() {
            @Override
            public void onLocationChanged(Location location) {
                CommonUtils.hideProgressDialog();
                if (location!=null) {
                    mAddress = CommonUtils.fetchAddress(context, location.getLatitude(), location.getLongitude());

                    lat = location.getLatitude();
                    lng = location.getLongitude();
                }

                    currentDate = new SimpleDateFormat("dd/MM/yyyy", Locale.getDefault()).format(new Date());
                    currentTime = new SimpleDateFormat("hh:mm a", Locale.getDefault()).format(new Date());


                    Intent intent = new Intent(context, SearchCarActivity.class);
                    intent.putExtra(AppConstant.FROM, AppConstant.MAIN_FRAGMENT);
                    intent.putExtra(AppConstant.PICKUP_DATE, currentDate);
                    intent.putExtra(AppConstant.PICKUP_TIME, currentTime);
                    intent.putExtra(AppConstant.ADDRESS, mAddress);
                    intent.putExtra(AppConstant.LATITUDE, lat);
                    intent.putExtra(AppConstant.LONGITUDE, lng);
                    startActivity(intent);

                    tracker.stopUsingGPS();


            }
        });

    }

    private void changeLanguage(String language){
        ChangeLanguage.setLocale(context,language);
    }

    private void getLastSelectedLanguage() {
        String currentLanguage = PreferencesManager.Companion.getStringPreferences(context, AppConstant.CURRENT_LANGUAGE);
        language = currentLanguage;
        switch (currentLanguage){
            case Language.ENGLISH:
                break;

            case Language.ARABIC:
                break;
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        this.context =context;
    }
    @Override
    public void onResume() {
        super.onResume();
        setToolbar();
        context.registerReceiver(mMessageReceiver, new IntentFilter(GPSTrackerNew.BROADCAST_ACTION));
    }

    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
        switch (view.getId()){
            case R.id.btn_submit:
                changeLanguage(language);
                break;
            case R.id.ll_english:
                language = Language.ENGLISH;
                break;
            case R.id.ll_arabic:
                language = Language.ARABIC;
                break;
        }
    }

    @Override
    public void onNothingSelected(AdapterView<?> parent) {

    }

    @Override
    public void onPause() {
        super.onPause();
        context.unregisterReceiver(mMessageReceiver);
    }

    private BroadcastReceiver mMessageReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            if (intent.getBooleanExtra("isGPSEnabled",false)){
                CommonUtils.showProgressDialog(context);
            }
        }
    };


}
