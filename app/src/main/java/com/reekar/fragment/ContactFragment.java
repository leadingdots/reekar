package com.reekar.fragment;


import android.Manifest;
import android.app.Activity;
import android.app.UiAutomation;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;

import android.provider.ContactsContract;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.akanksha.commonclassutil.CheckPermission;
import com.akanksha.commonclassutil.ToastUtil;
import com.reekar.R;
import com.reekar.activity.MainActivity;
import com.reekar.apiservices.ApiUtils;
import com.reekar.commonclasses.AppConstant;
import com.reekar.commonclasses.BaseFragment;
import com.reekar.commonclasses.CommonUtils;
import com.reekar.databinding.FragmentContactBinding;
import com.reekar.interfaces.ApiResponseInterface;
import com.reekar.models.ApiResponse;

import retrofit2.Response;


public class ContactFragment extends BaseFragment {

    private FragmentContactBinding contactBinding;
    private Context context;

    String number = "01865339665";
    String address = "1600 Pennsylvania Ave NW, Washington, D.C.  DC 20500, ABD";
    String email = "info@reekar.com";
    String whatsapp;
    String ccode;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_contact, container, false);
        contactBinding = DataBindingUtil.bind(view);

        if (CommonUtils.isNetworkAvailable(context))
            callContactUSApi();
        else
            ToastUtil.Companion.showShortToast(context,getString(R.string.failed_internet));

        setToolbar();
        setListener();
        return view;
    }

    @Override
    public void setToolbar() {
        ((MainActivity) context).setToolBar(true);
        ((MainActivity) context).mainBinding.appBarMain.toolbarTextview.setText(R.string.contact);
    }

    @Override
    public void setListener() {
        contactBinding.llWhatsapp.setOnClickListener(this);
        contactBinding.llEmail.setOnClickListener(this);
        contactBinding.llLocation.setOnClickListener(this);
        contactBinding.llPhNumber.setOnClickListener(this);

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {

            case R.id.ll_ph_number: {
                try {
                    if (CheckPermission.Companion.checkCallPhonePermission(context)) {
                        Uri uri = Uri.parse("tel:" + number);
                        Intent dial = new Intent(Intent.ACTION_DIAL);
                        dial.setData(uri);
                        startActivity(dial);
                    }
                    else {
                        CheckPermission.Companion.requestCallPhonePermission((Activity) context);
                    }

                }
                catch (Exception e) {
                    e.printStackTrace();
                }
                break;
            }
            case R.id.ll_email:{
                Intent intent=new Intent(Intent.ACTION_SEND);
                String[] recipients={email};
                intent.putExtra(Intent.EXTRA_EMAIL, recipients);
                intent.putExtra(Intent.EXTRA_SUBJECT,"Subject text here...");
                intent.putExtra(Intent.EXTRA_TEXT,"Body of the content here...");
                intent.putExtra(Intent.EXTRA_CC,"mailcc@gmail.com");
                intent.setType("text/html");
                intent.setPackage("com.google.android.gm");
                startActivity(Intent.createChooser(intent, "Send mail"));

//                Intent intent = new Intent(Intent.ACTION_MAIN);
//                intent.addCategory(Intent.CATEGORY_APP_EMAIL);
//                startActivity(Intent.createChooser(intent,email));
                break;
            }
            case R.id.ll_location:{
                Uri mapUri = Uri.parse("geo:0,0?q=" + Uri.encode(address));
                Intent mapIntent = new Intent(Intent.ACTION_VIEW, mapUri);
                mapIntent.setPackage("com.google.android.apps.maps");
                startActivity(mapIntent);
                break;
            }
            case R.id.ll_whatsapp:{
                String url = "https://api.whatsapp.com/send?phone="+number;
                Intent i = new Intent(Intent.ACTION_VIEW);
                i.setData(Uri.parse(url));
                startActivity(i);
                break;
            }
        }
    }
    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        this.context =context;
    }
    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);

        switch (requestCode){

            case CheckPermission.REQUEST_CODE_CALL_PERMISSION:{
                if (grantResults.length>0 && grantResults[0]== PackageManager.PERMISSION_GRANTED){
                    Uri uri = Uri.parse("tel:" + number);
                    Intent dial = new Intent(Intent.ACTION_DIAL);
                    dial.setData(uri);
                    startActivity(dial);
                }
                break;
            }
        }
    }

    private void callContactUSApi(){
        CommonUtils.showProgressDialog(context);
        ApiUtils.callContactApi(context, new ApiResponseInterface() {
            @Override
            public void onResponse(Response<ApiResponse> response) {
                CommonUtils.hideProgressDialog();
                if (response.code()== AppConstant.SUCCESS_CODE){
                    contactBinding.llParent.setVisibility(View.VISIBLE);
                    if (response.body().getContact().getEmail()!=null) {
                        contactBinding.tvEmail.setText(response.body().getContact().getEmail());
                        email=response.body().getContact().getEmail();
                    }
                    if (response.body().getContact().getCompanyFullAddress()!=null) {
                        contactBinding.tvAddress.setText(response.body().getContact().getCompanyFullAddress());
                        address=response.body().getContact().getCompanyFullAddress();
                    }
                    if (response.body().getContact().getCountryCode()!=null) {
                        ccode = response.body().getContact().getCountryCode();
                    }
                    if (response.body().getContact().getContactNo()!=null) {
                        contactBinding.tvContact.setText(ccode + " " + response.body().getContact().getContactNo());
                    }
//                    if (response.body().getContact().getAlternateContactNo()!=null)
//                        whatsapp=response.body().getContact().getAlternateContactNo();

                }
                else {
                    ToastUtil.Companion.showShortToast(context,getString(R.string.something_went_wrong));
                    contactBinding.llParent.setVisibility(View.GONE);

                }
            }

            @Override
            public void onFailure() {
                contactBinding.llParent.setVisibility(View.GONE);
                CommonUtils.hideProgressDialog();
                ToastUtil.Companion.showShortToast(context,getString(R.string.something_went_wrong));
            }
        });
    }
}
