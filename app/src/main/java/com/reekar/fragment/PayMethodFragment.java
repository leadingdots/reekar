package com.reekar.fragment;


import android.os.Bundle;

import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.reekar.R;
import com.reekar.commonclasses.BaseFragment;
import com.reekar.databinding.FragmentPayMethodBinding;

public class PayMethodFragment extends BaseFragment{

    private FragmentPayMethodBinding payMethodBinding;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_pay_method, container, false);
        payMethodBinding = DataBindingUtil.bind(view);

        setToolbar();
        setListener();
        return view;
    }

    @Override
    public void setToolbar() {

    }

    @Override
    public void setListener() {

    }

    @Override
    public void onClick(View v) {

    }
}
