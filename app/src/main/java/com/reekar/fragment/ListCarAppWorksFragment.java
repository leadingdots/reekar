package com.reekar.fragment;


import android.os.Bundle;

import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.reekar.R;
import com.reekar.commonclasses.BaseFragment;

/**
 * A simple {@link Fragment} subclass.
 */
public class ListCarAppWorksFragment extends BaseFragment {


    public ListCarAppWorksFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_list_car_app_works, container, false);
    }

    @Override
    public void setToolbar() {

    }

    @Override
    public void setListener() {

    }

    @Override
    public void onClick(View view) {

    }
}
