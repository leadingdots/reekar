package com.reekar.fragment;


import android.content.Context;
import android.os.Bundle;

import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.akanksha.commonclassutil.ToastUtil;
import com.reekar.R;
import com.reekar.activity.MainActivity;
import com.reekar.adapter.MyOrderListAdapter;
import com.reekar.apiservices.ApiUtils;
import com.reekar.commonclasses.AppConstant;
import com.reekar.commonclasses.BaseFragment;
import com.reekar.commonclasses.CommonUtils;
import com.reekar.commonclasses.EndlessRecyclerViewScrollListener;
import com.reekar.databinding.FragmentMyOrdersBinding;
import com.reekar.interfaces.ApiResponseInterface;
import com.reekar.models.ApiRequest;
import com.reekar.models.ApiResponse;
import com.reekar.models.DataModel;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Response;

public class MyOrdersFragment extends BaseFragment {

    private FragmentMyOrdersBinding binding;
    private Context context;
    private MyOrderListAdapter myOrderListAdapter;
    private LinearLayoutManager linearLayoutManager;
    private int currentPage = 1;
    private int totalPage = 1;
    private List<DataModel> arlOrder = new ArrayList<>();

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_my_orders, container, false);
        binding = DataBindingUtil.bind(view);

        setToolbar();
        setAdapter();

        setListener();
        if (CommonUtils.isNetworkAvailable(context)){
            callOrderListApi(true);
        }
        else
            ToastUtil.Companion.showShortToast(context,getString(R.string.failed_internet));
        return view;
    }

    private void setAdapter() {
        linearLayoutManager = new LinearLayoutManager(context);
        myOrderListAdapter = new MyOrderListAdapter(context,arlOrder);
        binding.myOrderlistRecyclerview.setHasFixedSize(true);
        binding.myOrderlistRecyclerview.setLayoutManager(linearLayoutManager);
        binding.myOrderlistRecyclerview.setAdapter(myOrderListAdapter);
    }

    @Override
    public void setToolbar() {
        ((MainActivity) context).setToolBar(true);
        ((MainActivity) context).mainBinding.appBarMain.toolbarTextview.setVisibility(View.VISIBLE);
        ((MainActivity) context).mainBinding.appBarMain.toolbarTextview.setText(R.string.my_order_list);
    }

    @Override
    public void setListener() {
        binding.swipeRefresh.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                if (CommonUtils.isNetworkAvailable(context)) {
                    currentPage = 1;
                    callOrderListApi(false);
                }
                else
                    ToastUtil.Companion.showShortToast(context,getString(R.string.failed_internet));
            }
        });

        binding.myOrderlistRecyclerview.addOnScrollListener(new EndlessRecyclerViewScrollListener(linearLayoutManager) {
            @Override
            public void onLoadMore(int page, int totalItemsCount) {
                if (CommonUtils.isNetworkAvailable(context)) {
                    if (totalPage >= currentPage) {
                        myOrderListAdapter.showProgress(true);

                        callOrderListApi(false);
                    }
                }
                else
                    ToastUtil.Companion.showShortToast(context,getString(R.string.failed_internet));
            }
        });
    }

    @Override
    public void onClick(View v) {

    }
    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        this.context =context;
    }

    @Override
    public void onResume() {
        super.onResume();
        ((MainActivity) context).setToolBar(true);
        ((MainActivity) context).mainBinding.appBarMain.toolbarTextview.setVisibility(View.VISIBLE);
        ((MainActivity) context).mainBinding.appBarMain.toolbarTextview.setText(R.string.my_order_list);
    }

    private void callOrderListApi(boolean showProgress){
        if (showProgress)
            CommonUtils.showProgressDialog(context);

        ApiRequest apiRequest = new ApiRequest();
        apiRequest.setPage(currentPage);
        apiRequest.setPaginate(1);

        ApiUtils.callOrderListApi(context, apiRequest, new ApiResponseInterface() {
            @Override
            public void onResponse(Response<ApiResponse> response) {
                CommonUtils.hideProgressDialog();
                binding.swipeRefresh.setRefreshing(false);
                myOrderListAdapter.showProgress(false);
                switch (response.code()) {
                    case AppConstant.SUCCESS_CODE:

                        if (currentPage == 1)
                            arlOrder.clear();

                        if (response.body().getData() != null && response.body().getData().size() > 0) {
                            binding.llNoData.noDataParent.setVisibility(View.GONE);
                            arlOrder.addAll(response.body().getData());
                            myOrderListAdapter.notifyDataSetChanged();
                        } else
                            binding.llNoData.noDataParent.setVisibility(View.VISIBLE);
                        currentPage++;
                        totalPage = response.body().getTotalPage();

                        break;
                    case AppConstant.UNAUTHORIZED_CODE:
                        CommonUtils.navigateToLogin(context);
                    default:
                        CommonUtils.showToast(context, getString(R.string.something_went_wrong));
                }
            }

            @Override
            public void onFailure() {
                binding.swipeRefresh.setRefreshing(false);
                myOrderListAdapter.showProgress(false);
                CommonUtils.hideProgressDialog();
                CommonUtils.showToast(context,getString(R.string.something_went_wrong));
            }
        });
    }
}
