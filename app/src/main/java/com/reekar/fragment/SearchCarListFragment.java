package com.reekar.fragment;


import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import androidx.annotation.NonNull;
import androidx.core.content.ContextCompat;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.akanksha.commonclassutil.ToastUtil;
import com.reekar.R;
import com.reekar.activity.CarDetailsActivity;
import com.reekar.adapter.CarSearchListAdapter;
import com.reekar.apiservices.ApiUtils;
import com.reekar.commonclasses.AppConstant;
import com.reekar.commonclasses.BaseFragment;
import com.reekar.commonclasses.CommonUtils;
import com.reekar.commonclasses.EndlessRecyclerViewScrollListener;
import com.reekar.commonclasses.TimeUtil;
import com.reekar.databinding.FragmentSearchCarListBinding;
import com.reekar.interfaces.ApiResponseInterface;
import com.reekar.interfaces.OnFilterClick;
import com.reekar.interfaces.OnSortItemClick;
import com.reekar.interfaces.RecyclerviewItemClickInterface;
import com.reekar.models.ApiRequest;
import com.reekar.models.ApiResponse;
import com.reekar.models.CarModel;
import com.reekar.models.DataModel;
import com.reekar.models.FilterModel;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Response;


public class SearchCarListFragment extends BaseFragment {

    private FragmentSearchCarListBinding listViewBinding;
    private CarSearchListAdapter carSearchListAdapter;

    private Context context;
    private LinearLayoutManager layoutManager;

    private String mAddress,pickupTime,pickupDate,dropTime,dropDate;
    private double lat,lng;
    private List<CarModel> arlCars = new ArrayList<>();
    private ArrayList<FilterModel> arlFilter = new ArrayList<>();
    private ArrayList<Integer> arlBrand = new ArrayList<>();
    private ArrayList<Integer> arlModel = new ArrayList<>();
    private ArrayList<Integer> arlVehicleType = new ArrayList<>();
    private ArrayList<Integer> arlEngineType = new ArrayList<>();
    private ArrayList<Integer> arlTransmission = new ArrayList<>();
    private ArrayList<Integer> arlColor = new ArrayList<>();
    private ArrayList<Integer> arlSeats = new ArrayList<>();

    private int ac = 0;
    private int nav = 0;
    private int ipod = 0;
    private int sunroof = 0;
    private int childSeat = 0;
    private int electricWindow = 0;
    private int heatedSeat = 0;
    private int panormaRoof = 0;
    private int gauge = 0;
    private int abs = 0;
    private int tractionControl = 0;
    private int audioControl = 0;
    private String currency;

    private int currentPage = 1;
    private int totalPage = 1;
    private String sort="";
    private String year;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_search_car_list, container, false);
        listViewBinding = DataBindingUtil.bind(view);

        mGetIntent();
        setToolbar();
        setAdapter();
        setListener();
        if (CommonUtils.isNetworkAvailable(context))
        callApis();
        else
            ToastUtil.Companion.showShortToast(context,getString(R.string.failed_internet));

        return view;
    }
    private void callApis() {
        callApiMasterData();
        callSearchCarApi(false);
    }
    private void mGetIntent() {
        if (getArguments()!= null){
            if (getArguments().getString(AppConstant.ADDRESS) != null)
                mAddress = getArguments().getString(AppConstant.ADDRESS);
            if (getArguments().getString(AppConstant.PICKUP_TIME) != null)
                pickupTime = getArguments().getString(AppConstant.PICKUP_TIME);
            if (getArguments().getString(AppConstant.PICKUP_DATE) != null)
                pickupDate = getArguments().getString(AppConstant.PICKUP_DATE);
            if (getArguments().getString(AppConstant.DROP_TIME) != null)
                dropTime = getArguments().getString(AppConstant.DROP_TIME);
            if (getArguments().getString(AppConstant.DROP_DATE) != null)
                dropDate = getArguments().getString(AppConstant.DROP_DATE);
            lat = getArguments().getDouble(AppConstant.LATITUDE,0.0);
            lng = getArguments().getDouble(AppConstant.LONGITUDE,0.0);
        }
    }

    private void setAdapter() {
        layoutManager = new LinearLayoutManager(context);
        carSearchListAdapter = new CarSearchListAdapter(context, arlCars, new RecyclerviewItemClickInterface() {
            @Override
            public void onItemClick(int position) {
                Intent intent =new Intent(context, CarDetailsActivity.class);
                intent.putExtra(AppConstant.CAR_ID,String.valueOf(arlCars.get(position).getId()));
                intent.putExtra(AppConstant.BOOKING_FROM_DATE,pickupDate);
                intent.putExtra(AppConstant.BOOKING_FROM_TIME,pickupTime);
                intent.putExtra(AppConstant.BOOKING_TO_DATE,dropDate);
                intent.putExtra(AppConstant.BOOKING_TO_TIME,dropTime);
                intent.putExtra(AppConstant.PICKUP_LOCATION,mAddress);
                startActivity(intent);
            }
        });
        listViewBinding.carListviewRecyclerview.setHasFixedSize(true);
        listViewBinding.carListviewRecyclerview.setLayoutManager(layoutManager);
        listViewBinding.carListviewRecyclerview.setAdapter(carSearchListAdapter);
    }

    @Override
    public void setToolbar() {

    }

    @Override
    public void setListener() {
        listViewBinding.sortByLl.setOnClickListener(this);
        listViewBinding.filterLl.setOnClickListener(this);

        listViewBinding.carListviewRecyclerview.addOnScrollListener(
                new EndlessRecyclerViewScrollListener(layoutManager) {
                    @Override
                    public void onLoadMore(int page, int totalItemsCount) {
                        if (CommonUtils.isNetworkAvailable(context)) {
                            if (totalPage >= currentPage) {
                                carSearchListAdapter.showProgress(true);

                                callSearchCarApi(true);
                            }
                        }
                        else
                            ToastUtil.Companion.showShortToast(context,getString(R.string.failed_internet));
                    }
                });

        listViewBinding.swipeRefresh.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                if (CommonUtils.isNetworkAvailable(context)) {
                    currentPage = 1;
                    callSearchCarApi(true);
                }
                else
                    ToastUtil.Companion.showShortToast(context,getString(R.string.failed_internet));
            }
        });
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){

            case R.id.sort_by_ll:{
                changeBottomButtonColor(listViewBinding.sortByLl);
                openSortBy();
                break;
            }
            case R.id.filter_ll:{
                changeBottomButtonColor(listViewBinding.filterLl);
                openFilter();
                break;
            }
//            case {
//
//            }
        }
    }
    private void openSortBy() {
        CommonUtils.showSortByDialog(context,sort, new OnSortItemClick() {
            @Override
            public void onItemClick(String data) {
                sort = data;
                currentPage = 1;
                if (CommonUtils.isNetworkAvailable(context))
                callSearchCarApi(false);
                else
                    ToastUtil.Companion.showShortToast(context,getString(R.string.failed_internet));
            }
        });
    }

    private void changeBottomButtonColor(LinearLayout linearLayout) {
        listViewBinding.sortByLl.setBackgroundColor(ContextCompat.getColor(context,R.color.white));
        listViewBinding.filterLl.setBackgroundColor(ContextCompat.getColor(context,R.color.white));
        linearLayout.setBackgroundColor(ContextCompat.getColor(context,R.color.grayish));
    }
    private void openFilter() {
        CommonUtils.showFilterDailoge(context, arlFilter, new OnFilterClick() {
            @Override
            public void onItemClick(ArrayList<FilterModel> data) {
                resetFilterData();

                for (FilterModel model : data){
                    if (model.getData() != null && model.getData().size()>0){
                        for (DataModel dataModel : model.getData()){
                            if (dataModel.isCheck()){
                                switch (model.getName()){
                                    case "Brand":
                                        arlBrand.add(dataModel.getId());
                                        break;
                                    case "Model":
                                        arlModel.add(dataModel.getId());
                                        break;
                                    case "Color":
                                        arlColor.add(dataModel.getId());
                                        break;
                                    case "Engine Type":
                                        arlEngineType.add(dataModel.getId());
                                        break;
                                    case "Transmission":
                                        arlTransmission.add(dataModel.getId());
                                        break;
                                    case "Vehicle Type":
                                        arlVehicleType.add(dataModel.getId());
                                        break;
                                    case "Seats":
                                        arlSeats.add(dataModel.getId());
                                        break;
                                    case "Year":
                                        year = dataModel.getName();
                                        break;
                                    case "Features":
                                        switch (dataModel.getName()){
                                            case "Air Condition (AC)":
                                                ac = 1;
                                                break;
                                            case "Navigation Screen (GPS)":
                                                nav = 1;
                                                break;
                                            case "Ipod Interface":
                                                ipod = 1;
                                                break;
                                            case "Sunroof":
                                                sunroof = 1;
                                                break;
                                            case "Child Seat":
                                                childSeat = 1;
                                                break;
                                            case "Electric Windows":
                                                electricWindow = 1;
                                                break;
                                            case "Heated Seats":
                                                heatedSeat = 1;
                                                break;
                                            case "Panorma Roof":
                                                panormaRoof = 1;
                                                break;
                                            case "PRM Gauge":
                                                gauge = 1;
                                                break;
                                            case "ABS":
                                                abs = 1;
                                                break;
                                            case "Traction Control":
                                                tractionControl = 1;
                                                break;
                                            case "Audio System":
                                                audioControl = 1;
                                                break;
                                        }
                                        break;
                                    case "Currency":
                                        currency = dataModel.getName();
                                        break;
                                }
                            }
                        }
                    }
                }

                currentPage = 1;
                if (CommonUtils.isNetworkAvailable(context))
                callSearchCarApi(false);
                else
                    ToastUtil.Companion.showShortToast(context,getString(R.string.failed_internet));
            }
        });

    }

    private void resetFilterData() {
        arlBrand.clear();
        arlModel.clear();
        arlColor.clear();
        arlEngineType.clear();
        arlTransmission.clear();
        arlVehicleType.clear();
        arlSeats.clear();

        ac = 0;
        nav = 0;
        ipod = 0;
        sunroof = 0;
        childSeat = 0;
        electricWindow = 0;
        heatedSeat = 0;
        panormaRoof = 0;
        gauge = 0;
        abs = 0;
        tractionControl = 0;
        audioControl = 0;
    }

    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);
        this.context =context;
    }

    @Override
    public void onResume() {
        super.onResume();
    }

    private void callApiMasterData() {
//        CommonUtils.showProgressDialog(mContext);
        ApiUtils.callApiMasterData(context, new ApiResponseInterface() {
            @Override
            public void onResponse(Response<ApiResponse> response) {
//                CommonUtils.hideProgressDialog();
                arlFilter.clear();
                if (response.body() != null && response.body().getFilter() != null && response.body().getFilter().size() >0)
                    arlFilter.addAll(response.body().getFilter());


                addFilterData("Features",context.getResources().getStringArray(R.array.options_feature));
//                addFilterData("Currency",context.getResources().getStringArray(R.array.currency));


                addFilterData("Year",TimeUtil.getYearData());

            }

            @Override
            public void onFailure() {
//                CommonUtils.hideProgressDialog();
            }
        });
    }

    private void addFilterData(String features, String[] stringArray) {
        FilterModel model = new FilterModel();
        model.setName(features);

//        String[] features = getResources().getStringArray(R.array.options_feature);
        List<DataModel> arlFeatures = new ArrayList<>();
        for (String data : stringArray){
            DataModel dataModel = new DataModel();
            dataModel.setName(data);

            arlFeatures.add(dataModel);
        }
        model.setData(arlFeatures);

        arlFilter.add(model);
    }

    private void addFilterData(String features, List<Integer> stringArray) {
        FilterModel model = new FilterModel();
        model.setName(features);

//        String[] features = getResources().getStringArray(R.array.options_feature);
        List<DataModel> arlFeatures = new ArrayList<>();
        for (int data : stringArray){
            DataModel dataModel = new DataModel();
            dataModel.setName(String.valueOf(data));

            arlFeatures.add(dataModel);
        }
        model.setData(arlFeatures);

        arlFilter.add(model);
    }


    private void callSearchCarApi(boolean isRefresh) {
        if (!isRefresh)
            CommonUtils.showProgressDialog(context);

        ApiRequest apiRequest = new ApiRequest();
        apiRequest.setPaginate(1);
        apiRequest.setPage(currentPage);
        apiRequest.setPickupAddress(mAddress);
        apiRequest.setPickupLat(String.valueOf(lat));
        apiRequest.setPickupLong(String.valueOf(lng));
        apiRequest.setPickupDate(TimeUtil.localDateToGMT(pickupDate+" "+pickupTime,"dd/MM/yyyy hh:mm a",context));
//        apiRequest.setPickupTime(TimeUtil.localDateToGMT(pickupTime,"hh:mm a",mContext));
        apiRequest.setDropDate(TimeUtil.localDateToGMT(dropDate+" "+dropTime,"dd/MM/yyyy hh:mm a",context));
//        apiRequest.setDropTime(TimeUtil.localDateToGMT(dropTime,"hh:mm a",mContext));

        apiRequest.setSortBy(sort);
        apiRequest.setBrandId(arlBrand);
        apiRequest.setModelId(arlModel);
        apiRequest.setColorId(arlColor);
        apiRequest.setEngineTypeId(arlEngineType);
        apiRequest.setTransmissionId(arlTransmission);
        apiRequest.setVehicleTypeId(arlVehicleType);
        apiRequest.setSeatId(arlSeats);

        apiRequest.setAc(ac);
        apiRequest.setGps(nav);
        apiRequest.setIpodInterface(ipod);
        apiRequest.setSunroof(sunroof);
        apiRequest.setChildSeat(childSeat);
        apiRequest.setElectricWindows(electricWindow);
        apiRequest.setHeatedSeat(heatedSeat);
        apiRequest.setPanormaRoof(panormaRoof);
        apiRequest.setPrmGuage(gauge);
        apiRequest.setAbs(abs);
        apiRequest.setTractionControl(tractionControl);
        apiRequest.setAudioSystem(audioControl);
        apiRequest.setYear(year);
        apiRequest.setCurrency(currency);

        ApiUtils.callSearchCarApi(context, apiRequest, new ApiResponseInterface() {
            @Override
            public void onResponse(Response<ApiResponse> response) {
                CommonUtils.hideProgressDialog();
                listViewBinding.swipeRefresh.setRefreshing(false);
                carSearchListAdapter.showProgress(false);
                switch (response.code()){
                    case AppConstant.SUCCESS_CODE:
                        if (response.body() != null){
                            if (currentPage == 1)
                                arlCars.clear();
                            if (response.body().getCars() != null && response.body().getCars().size()>0){
                                listViewBinding.llNoData.noDataParent.setVisibility(View.GONE);
                                arlCars.addAll(response.body().getCars());
                            } else {
                                listViewBinding.llNoData.noDataParent.setVisibility(View.VISIBLE);
                            }

                            carSearchListAdapter.notifyDataSetChanged();
                            currentPage++;
                            totalPage = response.body().getTotalPage();
                        }
                        break;
                    case AppConstant.UNAUTHORIZED_CODE:
                        CommonUtils.navigateToLogin(context);
                    default:
                        CommonUtils.showToast(context,context.getString(R.string.something_went_wrong));
                }

            }

            @Override
            public void onFailure() {
                carSearchListAdapter.showProgress(false);
                CommonUtils.hideProgressDialog();
                listViewBinding.swipeRefresh.setRefreshing(false);
                CommonUtils.showToast(context,context.getString(R.string.something_went_wrong));
            }
        });
    }

//    private List<Integer> getYearData() {
//        List<Integer> arlYear = new ArrayList<>();
//        int currentYear = Calendar.getInstance().get(Calendar.YEAR);
//
//        int lastYear = currentYear - 15;
//
//        LogUtil.Companion.errorLog("year", String.valueOf(currentYear));
//        LogUtil.Companion.errorLog("lastYear", String.valueOf(lastYear));
//
//        for (int i=currentYear;i>=lastYear;i--){
//            arlYear.add(i);
//        }
//
//        return arlYear;
//    }
}
