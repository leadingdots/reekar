package com.reekar.fragment;


import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;

import com.akanksha.commonclassutil.PreferencesManager;
import com.akanksha.commonclassutil.ToastUtil;
import com.bumptech.glide.Glide;
import com.reekar.R;
import com.reekar.activity.MainActivity;
import com.reekar.activity.ProfileEditActivity;
import com.reekar.apiservices.ApiUtils;
import com.reekar.commonclasses.AppConstant;
import com.reekar.commonclasses.BaseFragment;
import com.reekar.commonclasses.CommonUtils;
import com.reekar.databinding.FragmentProfileBinding;
import com.reekar.interfaces.ApiResponseInterface;
import com.reekar.models.ApiRequest;
import com.reekar.models.ApiResponse;
import com.reekar.translation.ChangeLanguage;
import com.reekar.translation.Language;

import retrofit2.Response;

public class ProfileFragment extends BaseFragment {

    private FragmentProfileBinding profileBinding;

    private Context context;
    private String uri;

    private String id;
    private String fname;
    private String lname;
    private String ccode;
    private String phoneNum;
    private String image;
    private String email;
    private String address;
    private String imageUrl;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view= inflater.inflate(R.layout.fragment_profile, container, false);
        profileBinding = DataBindingUtil.bind(view);
//        if (PreferencesManager.Companion.getStringPreferences(context,AppConstant.USER_IMAGE) != null)
//            Glide.with(context)
//                    .load(PreferencesManager.Companion.getStringPreferences(context,AppConstant.USER_IMAGE))
//                    .into(profileBinding.profileImage);


        setToolbar();
        initLang();
        setListener();
        if (CommonUtils.isNetworkAvailable(context)){
            callApiProfile();
        }
        else
            ToastUtil.Companion.showShortToast(context,getString(R.string.failed_internet));


        return view;
    }

    private void initLang() {
        switch (PreferencesManager.Companion.getStringPreferences(context,AppConstant.CURRENT_LANGUAGE)){
            case Language.ENGLISH:
                profileBinding.spLang.setSelection(0);
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR1) {
                    profileBinding.spLang.setLayoutDirection(View.LAYOUT_DIRECTION_LTR);
                    profileBinding.spLang.setTextDirection(View.TEXT_DIRECTION_LTR);
                }
                break;
            case Language.ARABIC:
                profileBinding.spLang.setSelection(1);
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR1) {
                    profileBinding.spLang.setLayoutDirection(View.LAYOUT_DIRECTION_RTL);
                    profileBinding.spLang.setTextDirection(View.TEXT_DIRECTION_RTL);
                }
                break;
        }
    }

    @Override
    public void setToolbar() {
        ((MainActivity) context).setToolBar(true);
        ((MainActivity) context).mainBinding.appBarMain.toolbarTextview.setText(R.string.profile);
    }

    @Override
    public void setListener() {
        profileBinding.editProfile.setOnClickListener(this);

        profileBinding.spLang.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int position, long l) {
                String curLang = Language.ENGLISH;
                if (position == 0) {
                    curLang = Language.ENGLISH;
                }
                else {
                    curLang = Language.ARABIC;
                }
                if (!curLang.equals(PreferencesManager.Companion.getStringPreferences(context,AppConstant.CURRENT_LANGUAGE)))
                    ChangeLanguage.setLocale(context,curLang);


            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){

            case R.id.edit_profile:{
                startActivityForResult(new Intent(context, ProfileEditActivity.class)
                        .putExtra(AppConstant.FIRST_NAME,fname)
                        .putExtra(AppConstant.LAST_NAME,lname)
                        .putExtra(AppConstant.ADDRESS,profileBinding.address.getText().toString())
                        .putExtra(AppConstant.USER_NUMBER,profileBinding.phoneNumber.getText().toString())
                        .putExtra(AppConstant.COUNTRY_CODE,ccode)
                        .putExtra(AppConstant.IMAGE_URI,imageUrl+"/"+image),2);
                break;
            }
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode==2){
            if (data != null) {
                String fname = data.getStringExtra(AppConstant.FIRST_NAME);
                String lname = data.getStringExtra(AppConstant.LAST_NAME);
                String number = data.getStringExtra(AppConstant.USER_NUMBER);
                String address = data.getStringExtra(AppConstant.ADDRESS);
                String ccode =data.getStringExtra(AppConstant.COUNTRY_CODE);
                if (data.getStringExtra(AppConstant.IMAGE_URI)!=null) {
                    uri = (data.getStringExtra(AppConstant.IMAGE_URI));
                    PreferencesManager.Companion.saveStringPreferences(context,AppConstant.USER_IMAGE,uri);
                }
                if (fname!=null && lname!=null)
                profileBinding.tvUserName.setText(fname+lname);
                if (ccode!=null)
                profileBinding.countryCode.setText(ccode);
                if (number!=null)
                profileBinding.phoneNumber.setText(number);
                if (address!=null)
                profileBinding.address.setText(address);
                if (uri!=null)
                    Glide.with(context)
                            .load(uri)
                            .placeholder(R.drawable.ic_user)
                            .into(profileBinding.profileImage);
                PreferencesManager.Companion.saveStringPreferences(context,AppConstant.USER_IMAGE,uri);
                PreferencesManager.Companion.saveStringPreferences(context,AppConstant.FIRST_NAME,fname);
                if (lname!=null)
                    PreferencesManager.Companion.saveStringPreferences(context,AppConstant.LAST_NAME,lname);
            }
        }
    }

    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);
        this.context =context;
    }

    @Override
    public void onResume() {
        super.onResume();
        ((MainActivity) context).setToolBar(true);
        ((MainActivity) context).mainBinding.appBarMain.toolbarTextview.setText(R.string.profile);

        if (CommonUtils.isNetworkAvailable(context))
            callApiProfile();
        else
            ToastUtil.Companion.showShortToast(context,getString(R.string.failed_internet));
    }

    private void callApiProfile(){
        CommonUtils.showProgressDialog(context);

        ApiRequest apiRequest =new ApiRequest();
//        apiRequest(PreferencesManager.Companion.getStringPreferences(context,AppConstant.ACCESS_CODE));

        ApiUtils.callApiProfile(context, apiRequest, new ApiResponseInterface() {
            @Override
            public void onResponse(Response<ApiResponse> response) {
                CommonUtils.hideProgressDialog();
                if (response!=null)
                    if (response.code()==AppConstant.SUCCESS_CODE){
                        if (response.body()!=null){

                            profileBinding.profileLl.setVisibility(View.VISIBLE);

                            if (response.body().getProfile().getId()!=null)
                                id = response.body().getProfile().getId().toString();
                            if (response.body().getProfile().getFirstName()!=null)
                                fname = response.body().getProfile().getFirstName();
                            if (response.body().getProfile().getLastName()!=null)
                                lname = response.body().getProfile().getLastName();
                            if (response.body().getProfile().getEmail()!=null)
                                email = response.body().getProfile().getEmail();
                            if (response.body().getProfile().getCountryCode()!=null)
                                ccode = response.body().getProfile().getCountryCode();
                            if (response.body().getProfile().getMobile()!=null)
                                phoneNum = response.body().getProfile().getMobile();
                            if (response.body().getProfile().getAddress()!=null) {
                                address = response.body().getProfile().getAddress().toString();
                                profileBinding.llAddressParent.setVisibility(View.VISIBLE);
                            }
                            else {
                                profileBinding.llAddressParent.setVisibility(View.GONE);
                            }
                            if (response.body().getUserImageBaseUrl()!=null)
                                imageUrl = response.body().getUserImageBaseUrl();
                            if (response.body().getProfile().getProfilePic()!=null)
                                image = response.body().getProfile().getProfilePic().toString();

                            PreferencesManager.Companion.saveStringPreferences(context,AppConstant.USER_IMAGE,imageUrl+"/"+image);
//                          PreferencesManager.Companion.saveStringPreferences(context,AppConstant.USER_ID,id);

                            if (lname==null){
                                profileBinding.tvUserName.setText(fname);

                            }
                            else {
                                profileBinding.tvUserName.setText(fname+" "+lname);
                            }
                            if (ccode!=null)
                            profileBinding.countryCode.setText(ccode);
                            if (phoneNum!=null)
                            profileBinding.phoneNumber.setText(phoneNum);
                            if (email!=null)
                            profileBinding.userEmail.setText(email);
                            if (address!=null)
                            profileBinding.address.setText(address);

                            Glide.with(context)
                                    .load(imageUrl+"/"+image)
                                    .placeholder(R.drawable.ic_user)
                                    .into(profileBinding.profileImage);

                            PreferencesManager.Companion.saveStringPreferences(context,AppConstant.FIRST_NAME,fname);
                            if (lname!=null)
                                PreferencesManager.Companion.saveStringPreferences(context,AppConstant.LAST_NAME,lname);
                        }
                    }
                    else if (response.code()==AppConstant.UNAUTHORIZED_CODE){
                        CommonUtils.navigateToLogin(context);
                    }
                    else {

                    }

            }

            @Override
            public void onFailure() {
                CommonUtils.hideProgressDialog();
                ToastUtil.Companion.showShortToast(context,getString(R.string.something_went_wrong));

            }
        });
    }
}
