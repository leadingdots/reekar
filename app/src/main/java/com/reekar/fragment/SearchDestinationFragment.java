package com.reekar.fragment;

import android.content.Context;
import android.net.Uri;
import android.os.Bundle;

import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.reekar.R;
import com.reekar.commonclasses.BaseFragment;
import com.reekar.databinding.FragmentHomeBinding;
import com.reekar.databinding.FragmentSearchDestinationBinding;


public class SearchDestinationFragment extends BaseFragment {


    private FragmentSearchDestinationBinding searchDestinationBinding;
    private Context context;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_search_destination, container, false);
        searchDestinationBinding = DataBindingUtil.bind(view);

        setListener();
        setToolbar();
        return view;
    }


    @Override
    public void setToolbar() {

    }

    @Override
    public void setListener() {

    }

    @Override
    public void onClick(View v) {

    }
    @Override
    public void onResume() {
        super.onResume();
        setToolbar();

    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        this.context =context;
    }

}
