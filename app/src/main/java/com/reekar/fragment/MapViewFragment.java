package com.reekar.fragment;


import android.content.Context;
import android.content.Intent;
import android.location.Location;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;

import com.akanksha.commonclassutil.LogUtil;
import com.akanksha.commonclassutil.ToastUtil;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.libraries.places.api.Places;
import com.reekar.R;
import com.reekar.activity.CarDetailsActivity;
import com.reekar.apiservices.ApiUtils;
import com.reekar.commonclasses.AppConstant;
import com.reekar.commonclasses.BaseFragment;
import com.reekar.commonclasses.CommonUtils;
import com.reekar.commonclasses.TimeUtil;
import com.reekar.interfaces.ApiResponseInterface;
import com.reekar.models.ApiRequest;
import com.reekar.models.ApiResponse;
import com.reekar.models.CarModel;
import com.reekar.models.LoginModel;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import retrofit2.Response;

public class MapViewFragment extends BaseFragment implements OnMapReadyCallback, GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener, GoogleMap.OnMarkerClickListener {

    private Context context;
    private GoogleMap mMap;
    private List<CarModel> arlCars = new ArrayList<>();
    private String mAddress,pickupTime,pickupDate,dropTime,dropDate;
    private double lat,lng;


    private String carId;
    Location mLastLocation;
    Marker myLocatMarker;
    GoogleApiClient mGoogleApiClient;
    private Map<Marker, Integer> markersMap = new HashMap<Marker, Integer>();

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_map_view, container, false);

        Places.initialize(context,getString(R.string.google_maps_key));


        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        SupportMapFragment mapFragment = (SupportMapFragment) getChildFragmentManager().findFragmentById(R.id.map);
        buildGoogleApiClient();
        assert mapFragment != null;
        mapFragment.getMapAsync(this);

        mGetIntent();
        if (CommonUtils.isNetworkAvailable(context))
            callSearchCarApi();
        else
            ToastUtil.Companion.showShortToast(context,getString(R.string.failed_internet));
        return view;
    }

    private void mGetIntent() {
        if (getArguments()!= null){
            if (getArguments().getString(AppConstant.ADDRESS) != null)
                mAddress = getArguments().getString(AppConstant.ADDRESS);
            if (getArguments().getString(AppConstant.PICKUP_TIME) != null)
                pickupTime = getArguments().getString(AppConstant.PICKUP_TIME);
            if (getArguments().getString(AppConstant.PICKUP_DATE) != null)
                pickupDate = getArguments().getString(AppConstant.PICKUP_DATE);
            if (getArguments().getString(AppConstant.DROP_TIME) != null)
                dropTime = getArguments().getString(AppConstant.DROP_TIME);
            if (getArguments().getString(AppConstant.DROP_DATE) != null)
                dropDate = getArguments().getString(AppConstant.DROP_DATE);
            lat = getArguments().getDouble(AppConstant.LATITUDE,0.0);
            lng = getArguments().getDouble(AppConstant.LONGITUDE,0.0);
        }
    }

    @Override
    public void setToolbar() {

    }

    @Override
    public void setListener() {

    }

    @Override
    public void onClick(View v) {

    }
    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);
        this.context =context;
    }


    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;
        mGoogleApiClient.connect();
        // Add a marker in Location
        LatLng latLng = new LatLng(lat,lng);
        /*mMap.moveCamera(CameraUpdateFactory.newLatLng(latLng));
        mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(latLng, 10.0f));*/
        CameraPosition cameraPosition = new CameraPosition.Builder()
                .target(latLng)
                .zoom(8.0f)
                .build();
        CameraUpdate update = CameraUpdateFactory.newCameraPosition(cameraPosition);
        mMap.animateCamera(update);

        mMap.setOnMarkerClickListener(this);
    }
    protected synchronized void buildGoogleApiClient() {
        mGoogleApiClient = new GoogleApiClient.Builder(context)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .addApi(LocationServices.API)
                .build();
    }
    @Override
    public void onConnected(Bundle arg0) {
        mLastLocation = LocationServices.FusedLocationApi.getLastLocation(mGoogleApiClient);
        if (mLastLocation != null) {
            double lng = mLastLocation.getLongitude();
            double lat = mLastLocation.getLatitude();

            if (myLocatMarker != null) {
                myLocatMarker.remove();
            }
            setMapMarker();
        }
    }

    @Override
    public void onConnectionSuspended(int i) {

    }
    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {

    }

    @Override
    public void onResume() {
        super.onResume();
    }

    private void callSearchCarApi() {
        CommonUtils.showProgressDialog(context);

        ApiRequest apiRequest = new ApiRequest();
        apiRequest.setPickupAddress(mAddress);
        apiRequest.setPickupLat(String.valueOf(lat));
        apiRequest.setPickupLong(String.valueOf(lng));
        apiRequest.setPickupDate(TimeUtil.localDateToGMT(pickupDate+" "+pickupTime,"dd/MM/yyyy hh:mm a",context));
        apiRequest.setDropDate(TimeUtil.localDateToGMT(dropDate+" "+dropTime,"dd/MM/yyyy hh:mm a",context));

        ApiUtils.callSearchCarApi(context, apiRequest, new ApiResponseInterface() {
            @Override
            public void onResponse(Response<ApiResponse> response) {
                CommonUtils.hideProgressDialog();

                switch (response.code()){
                    case AppConstant.SUCCESS_CODE: {
                        if (response.body() != null) {
                            if (response.body().getCarId()!=null){
                                carId=response.body().getCarId();
                            }
                            if (response.body().getCars() != null && response.body().getCars().size() > 0) {
                                arlCars.clear();
                                arlCars.addAll(response.body().getCars());
                                setMapMarker();
                            }
                            else {
                                CommonUtils.showToast(context,getString(R.string.there_is_no_car));
                            }

                        }
                        break;
                    }
                    //case AppConstant.UNAUTHORIZED_CODE:{
//                        CommonUtils.navigateToLogin(context);
//                    }
                    default:
                        CommonUtils.showToast(context,getString(R.string.something_went_wrong));
                }

            }

            @Override
            public void onFailure() {
                CommonUtils.hideProgressDialog();
                CommonUtils.showToast(context,getString(R.string.something_went_wrong));
            }
        });
    }

    private void setMapMarker() {

        for (CarModel carModel : arlCars){
            double carLat = Double.parseDouble(carModel.getCarLocationLat());
            double carLng = Double.parseDouble(carModel.getCarLocationLong());
            LatLng latLng = new LatLng(carLat,carLng);
            if (mMap!=null) {
                Marker marker = mMap.addMarker(new MarkerOptions().position(latLng)
                        .icon(BitmapDescriptorFactory.fromResource(R.mipmap.icons_map_view_midium)));

                markersMap.put(marker,carModel.getId());
            }


        }
    }


    @Override
    public boolean onMarkerClick(Marker marker) {

        //LatLng position = marker.getPosition();
        //String idSelected= marker.getId();


        LogUtil.Companion.errorLog("car id ",String.valueOf(markersMap.get(marker)));
        Intent intent =new Intent(context,CarDetailsActivity.class);
        intent.putExtra(AppConstant.CAR_ID,String.valueOf(markersMap.get(marker)));
        startActivity(intent);
        return false;
    }
}
