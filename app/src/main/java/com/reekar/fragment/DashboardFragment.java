package com.reekar.fragment;


import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;

import com.akanksha.commonclassutil.LogUtil;
import com.akanksha.commonclassutil.PreferencesManager;
import com.akanksha.commonclassutil.ToastUtil;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.reekar.R;
import com.reekar.activity.MainActivity;
import com.reekar.activity.SearchCarActivity;
import com.reekar.activity.SearchDestinationActivity;
import com.reekar.commonclasses.AppConstant;
import com.reekar.commonclasses.BaseFragment;
import com.reekar.commonclasses.CommonUtils;
import com.reekar.commonclasses.TimeUtil;
import com.reekar.databinding.FragmentDashboardBinding;
import com.reekar.models.ApiResponse;
import com.reekar.models.CurrencyDataModel;

import java.io.IOException;
import java.io.InputStream;
import java.lang.reflect.Type;


public class DashboardFragment extends BaseFragment {

    private FragmentDashboardBinding dashboardBinding;
    private Context context;
    private String mAddress;
    private double lat=0.0,lng = 0.0;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_dashboard, container, false);
        dashboardBinding = DataBindingUtil.bind(view);


//        CommonUtils.translate(dashboardBinding.tvLocation.getText().toString(),"hi",dashboardBinding.tvLocation);
        setToolbar();
        setListener();
        return view;
    }
    private boolean validation(){
        String mPickDate = dashboardBinding.tvPickupDate.getText().toString();
        String mDropDate = dashboardBinding.tvDropDate.getText().toString();
        String mPickTime = dashboardBinding.tvPickupTime.getText().toString();
        String mDropTime = dashboardBinding.tvDropTime.getText().toString();


        if (mAddress==null){
            dashboardBinding.tvLocation.requestFocus();
            ToastUtil.Companion.showLongToast(context, getString(R.string.error_select_location));
            return false;
        }
        else if (TextUtils.isEmpty(mPickTime) && TextUtils.isEmpty(mPickDate)){
            ToastUtil.Companion.showLongToast(context, getString(R.string.error_pickup_time));
            return false;
        }
        else if (mPickTime.isEmpty()) {
            ToastUtil.Companion.showLongToast(context, getString(R.string.error_pickup_time));
            return false;
        }
        else if (!TimeUtil.isPastTime(mPickDate, mPickTime)){
            ToastUtil.Companion.showLongToast(context, getString(R.string.you_cant_select_past_time));
            return false;
        }
        else if (mPickDate.isEmpty()){
            ToastUtil.Companion.showLongToast(context, getString(R.string.error_pickup_date));
            return false;
        }
        else if (mDropTime.isEmpty()){
            ToastUtil.Companion.showLongToast(context, getString(R.string.error_drop_time));
            return false;
        }
        else if (mDropDate.isEmpty()){
            ToastUtil.Companion.showLongToast(context, getString(R.string.error_drop_date));
            return false;
        }
        else if (!TimeUtil.CheckDates(mPickDate, mDropDate)){
            ToastUtil.Companion.showLongToast(context,getString(R.string.pick_date_should_be_greater));
            return false;
        }
        else if (mPickTime.equals(mDropTime) && mPickDate.equals(mDropDate)){
            ToastUtil.Companion.showLongToast(context, getString(R.string.error_difffrent_times));
            return false;
        }
        else if (!TimeUtil.isTimeGreater(mPickDate, mPickTime, mDropDate, mDropTime)){
            ToastUtil.Companion.showLongToast(context,getString(R.string.drop_time_should_be_greater));
            return false;
        }
        else if (!TimeUtil.isPastTime(mDropDate, mDropTime)) {
            ToastUtil.Companion.showLongToast(context, getString(R.string.you_cant_select_past_time));
            return false;
        }

        else {
            return true;
        }

    }


    @Override
    public void setToolbar() {
        ((MainActivity)context).setToolBar(true);
    }

    @Override
    public void setListener() {
        dashboardBinding.tvPickupTime.setOnClickListener(this);
        dashboardBinding.tvPickupDate.setOnClickListener(this);
        dashboardBinding.tvDropDate.setOnClickListener(this);
        dashboardBinding.tvDropTime.setOnClickListener(this);
        dashboardBinding.searchCarBtn.setOnClickListener(this);
        dashboardBinding.tvLocation.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {

        switch (v.getId()){

            case R.id.tv_pickup_date:{
                TimeUtil.datePicker(context,dashboardBinding.tvPickupDate,"dd/MM/yyyy");
                break;
            }
            case R.id.tv_pickup_time:{
                TimeUtil.timePicker(context,dashboardBinding.tvPickupTime,"hh:mm a");
                break;
            }
            case R.id.tv_drop_date:{
                TimeUtil.datePicker(context,dashboardBinding.tvDropDate,"dd/MM/yyyy");
                break;
            }
            case R.id.tv_drop_time:{
                TimeUtil.timePicker(context,dashboardBinding.tvDropTime,"hh:mm a");
                break;
            }
            case R.id.search_car_btn:{
                if(validation()){
                    Intent intent=new Intent(context, SearchCarActivity.class);
                    intent.putExtra(AppConstant.LATITUDE,lat);
                    intent.putExtra(AppConstant.LONGITUDE,lng);
                    intent.putExtra(AppConstant.ADDRESS,mAddress);
                    intent.putExtra(AppConstant.PICKUP_TIME,dashboardBinding.tvPickupTime.getText().toString());
                    intent.putExtra(AppConstant.DROP_TIME,dashboardBinding.tvDropTime.getText().toString());
                    intent.putExtra(AppConstant.PICKUP_DATE,dashboardBinding.tvPickupDate.getText().toString());
                    intent.putExtra(AppConstant.DROP_DATE,dashboardBinding.tvDropDate.getText().toString());
                    startActivity(intent);
                }

                break;
            }
            case R.id.tv_location:{
                Intent intent=new Intent(context, SearchDestinationActivity.class);
                startActivityForResult(intent,1);
                break;
            }
        }
    }
    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);
        this.context =context;
    }
    @Override
    public void onResume() {
        super.onResume();
        setToolbar();

    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        if (resultCode == Activity.RESULT_OK){
            if (requestCode == 1) {
                if (data != null) {
                    lat = data.getDoubleExtra(AppConstant.LATITUDE, 0.0);
                    lng = data.getDoubleExtra(AppConstant.LONGITUDE, 0.0);
                    mAddress = data.getStringExtra(AppConstant.ADDRESS);

                    dashboardBinding.tvLocation.setText(mAddress);


                    String country = CommonUtils.fetchCountry(context,lat,lng);
                    String json = loadData("countries.json");
                    fetchCurrency(country,json);
                }
            }
        }
    }

    private void fetchCurrency(String country, String json) {
        String currency="";

        Type type = new TypeToken<CurrencyDataModel>() {
        }.getType();

        CurrencyDataModel currencyData = new Gson().fromJson(json, type);
        for (CurrencyDataModel.CountryModel model : currencyData.getCountry()){
            if(country.equalsIgnoreCase(model.getCountryName())){
                currency = model.getCurrencyCode();
                break;
            }
        }
        LogUtil.Companion.errorLog("currency",currency);
    }

    public String loadData(String inFile) {
        String tContents = "";

        try {
            InputStream stream = context.getAssets().open(inFile);

            int size = stream.available();
            byte[] buffer = new byte[size];
            stream.read(buffer);
            stream.close();
            tContents = new String(buffer);
        } catch (IOException e) {
            // Handle exceptions here
        }

        return tContents;

    }
}
