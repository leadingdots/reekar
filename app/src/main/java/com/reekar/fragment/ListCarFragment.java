package com.reekar.fragment;


import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.location.Location;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.databinding.DataBindingUtil;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.akanksha.commonclassutil.CheckPermission;
import com.akanksha.commonclassutil.PreferencesManager;
import com.reekar.R;

import com.reekar.activity.MainActivity;
import com.reekar.activity.SearchCarActivity;
import com.reekar.activity.SignUpActivity;
import com.reekar.commonclasses.BaseFragment;
import com.reekar.commonclasses.CommonUtils;
import com.reekar.commonclasses.AppConstant;
import com.reekar.databinding.FragmentListCarBinding;
import com.reekar.interfaces.OnLocationChangeInterface;
import com.reekar.location.GPSTrackerNew;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;


public class ListCarFragment extends BaseFragment {


    private FragmentListCarBinding listCarBinding;

    private Context context;

    private GPSTrackerNew tracker;
    private String currentDate;
    private String currentTime;

    private String mAddress;
    private double lat;
    private double lng;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_list_car, container, false);
        listCarBinding = DataBindingUtil.bind(view);


        setToolbar();
        setListener();
        return view;
    }

    @Override
    public void setToolbar() {
        ((MainActivity)context).setToolBar(listCarBinding.navBtn);
    }

    @Override
    public void setListener() {
        listCarBinding.signInTv.setOnClickListener(this);
        listCarBinding.skipTv.setOnClickListener(this);
        listCarBinding.gotoSignup.setOnClickListener(this);
        listCarBinding.viewBtn.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){

            case R.id.sign_in_tv:{
                CommonUtils.setFragment(new LoginFragment(),true,getActivity(),R.id.container_mainn, AppConstant.LOGIN);
                break;
            }
            case R.id.skip_tv:{
//                CommonUtils.setFragment(new MainFragment(),true,getActivity(),R.id.container_main, AppConstant.LOGIN);
//                PreferencesManager.Companion.saveBooleanPreferences(context,AppConstant.IS_SKIP,true);
                Intent intent = new Intent(getActivity(),MainActivity.class);
                intent.putExtra(AppConstant.IS_SKIP,AppConstant.IS_SKIP);
                startActivity(intent);
                break;
            }
            case R.id.goto_signup:{
                Intent intent = new Intent(getActivity(), SignUpActivity.class);
                startActivity(intent);
                break;
            }
            case R.id.view_btn:{
                if (CheckPermission.Companion.checkIsMarshMallowVersion()){
                    if (CheckPermission.Companion.checkLocationPermission(context)){
                        getLocation();
                    } else
                        CheckPermission.Companion.requestLocationPermission((Activity) context);
                } else
                    getLocation();
                break;
//                CommonUtils.setFragment(new DashboardFragment(),true,getActivity(),R.id.container_mainn, AppConstant.DASHBOARD);
            }
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        switch (requestCode){
            case CheckPermission.REQUEST_CODE_LOCATION_PERMISSION:{
                if (grantResults.length>0 && grantResults[0] == PackageManager.PERMISSION_GRANTED){
                    getLocation();
                }
            }
        }
    }

    private void getLocation() {
        CommonUtils.showProgressDialog(context);
        tracker = new GPSTrackerNew(context, new OnLocationChangeInterface() {
            @Override
            public void onLocationChanged(Location location) {
                CommonUtils.hideProgressDialog();
                if (location!=null) {
                    mAddress = CommonUtils.fetchAddress(context, location.getLatitude(), location.getLongitude());

                    lat = location.getLatitude();
                    lng = location.getLongitude();
                }

                    currentDate = new SimpleDateFormat("dd/MM/yyyy", Locale.getDefault()).format(new Date());
                    currentTime = new SimpleDateFormat("hh:mm a", Locale.getDefault()).format(new Date());


                    Intent intent = new Intent(context, SearchCarActivity.class);
                    intent.putExtra(AppConstant.FROM, AppConstant.LISTCAR_FRAGMENT);
                    intent.putExtra(AppConstant.PICKUP_DATE, currentDate);
                    intent.putExtra(AppConstant.PICKUP_TIME, currentTime);
                    intent.putExtra(AppConstant.ADDRESS, mAddress);
                    intent.putExtra(AppConstant.LATITUDE, lat);
                    intent.putExtra(AppConstant.LONGITUDE, lng);
                    startActivity(intent);

                    tracker.stopUsingGPS();
                }


        });
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        this.context =context;
    }
    @Override
    public void onResume() {
        super.onResume();
        setToolbar();
        context.registerReceiver(mMessageReceiver, new IntentFilter(GPSTrackerNew.BROADCAST_ACTION));

    }

    @Override
    public void onPause() {
        super.onPause();
        context.unregisterReceiver(mMessageReceiver);
    }

    private BroadcastReceiver mMessageReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            if (intent.getBooleanExtra("isGPSEnabled",false)){
                CommonUtils.showProgressDialog(context);
            }
        }
    };

}
