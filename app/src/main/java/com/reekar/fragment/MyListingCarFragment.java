package com.reekar.fragment;


import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.app.SharedElementCallback;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.akanksha.commonclassutil.ToastUtil;
import com.paypal.android.sdk.db;
import com.reekar.R;
import com.reekar.activity.ListedCarDetailsActivity;
import com.reekar.activity.MainActivity;
import com.reekar.adapter.MyListingCarAdapter;
import com.reekar.apiservices.ApiUtils;
import com.reekar.commonclasses.AppConstant;
import com.reekar.commonclasses.BaseFragment;
import com.reekar.commonclasses.CommonUtils;
import com.reekar.commonclasses.EndlessRecyclerViewScrollListener;
import com.reekar.databinding.FragmentMyListingCarBinding;
import com.reekar.interfaces.ApiResponseInterface;
import com.reekar.interfaces.RecyclerviewItemClickInterface;
import com.reekar.models.ApiRequest;
import com.reekar.models.ApiResponse;
import com.reekar.models.CarModel;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Response;


public class MyListingCarFragment extends BaseFragment {

    private FragmentMyListingCarBinding binding;

    private MyListingCarAdapter myListingCarAdapter;
    private LinearLayoutManager layoutManager;
    private Context context;
    private int currentPage = 1;
    private int totalPage = 1;
    private List<CarModel> arlCar = new ArrayList<>();

    private int pos;

    boolean allowRefresh;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_my_listing_car, container, false);

        binding = DataBindingUtil.bind(view);


        setAdapter();
        setListener();
        setToolbar();

        if (CommonUtils.isNetworkAvailable(context)){
            callListCarApi(true);
        }
        else
            ToastUtil.Companion.showShortToast(context,getString(R.string.failed_internet));
        return view;
    }

    private void setAdapter() {
        layoutManager = new LinearLayoutManager(context);
        myListingCarAdapter = new MyListingCarAdapter(context, arlCar, new RecyclerviewItemClickInterface() {
            @Override
            public void onItemClick(int position) {
                pos =position;
                Intent intent = new Intent(context, ListedCarDetailsActivity.class);
                intent.putExtra(AppConstant.ID,String.valueOf(arlCar.get(position).getId()));
                startActivityForResult(intent,2);
            }
        });
        binding.rvCarList.setHasFixedSize(true);
        binding.rvCarList.setLayoutManager(layoutManager);
        binding.rvCarList.setAdapter(myListingCarAdapter);

    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode== Activity.RESULT_OK){
            if (requestCode==2){
                    arlCar.remove(pos);
                    myListingCarAdapter.notifyItemRemoved(pos);
                    myListingCarAdapter.notifyItemRangeChanged(pos, arlCar.size());

            }
        }
    }

    @Override
    public void setToolbar() {
        ((MainActivity) context).setToolBar(true);
        ((MainActivity) context).mainBinding.appBarMain.toolbarTextview.setText(R.string.my_listing);
    }

    @Override
    public void setListener() {

        binding.swipeRefresh.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                if (CommonUtils.isNetworkAvailable(context)) {
                    currentPage = 1;
                    callListCarApi(false);
                }
                else
                    ToastUtil.Companion.showShortToast(context,getString(R.string.failed_internet));
            }
        });

        binding.rvCarList.addOnScrollListener(new EndlessRecyclerViewScrollListener(layoutManager) {
            @Override
            public void onLoadMore(int page, int totalItemsCount) {
                if (CommonUtils.isNetworkAvailable(context)) {
                    if (totalPage >= currentPage) {
                        myListingCarAdapter.showProgress(true);

                        callListCarApi(false);
                    }
                }
                else
                    ToastUtil.Companion.showShortToast(context,getString(R.string.failed_internet));
            }
        });
    }


    @Override
    public void onClick(View v) {

    }
    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);
        this.context =context;
    }
    @Override
    public void onResume() {
        super.onResume();
        setToolbar();

    }

    private void callListCarApi(boolean showProgress){
        if (showProgress)
            CommonUtils.showProgressDialog(context);

        ApiRequest apiRequest = new ApiRequest();
        apiRequest.setPage(currentPage);
        apiRequest.setPaginate(1);
        ApiUtils.callListCarApi(context, apiRequest, new ApiResponseInterface() {
            @Override
            public void onResponse(Response<ApiResponse> response) {
                CommonUtils.hideProgressDialog();
                binding.swipeRefresh.setRefreshing(false);
                myListingCarAdapter.showProgress(false);
                switch (response.code()){
                    case AppConstant.SUCCESS_CODE:

                        if (currentPage == 1)
                            arlCar.clear();

                        if (response.body().getCars() != null && response.body().getCars().size()>0) {
                            binding.llNoData.noDataParent.setVisibility(View.GONE);
                            arlCar.addAll(response.body().getCars());
                        } else {
                            binding.llNoData.noDataParent.setVisibility(View.VISIBLE);
                        }


                        myListingCarAdapter.notifyDataSetChanged();
                        currentPage++;
                        totalPage = response.body().getTotalPage();

                        break;
                    case AppConstant.UNAUTHORIZED_CODE:
                        CommonUtils.navigateToLogin(context);
                    default:
                        CommonUtils.showToast(context,getString(R.string.something_went_wrong));
                }
            }

            @Override
            public void onFailure() {
                binding.swipeRefresh.setRefreshing(false);
                myListingCarAdapter.showProgress(false);
                CommonUtils.hideProgressDialog();
                CommonUtils.showToast(context,getString(R.string.something_went_wrong));
            }
        });
    }
}
