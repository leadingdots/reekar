package com.reekar.paymentmethod;

import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;

import com.reekar.R;
import com.reekar.commonclasses.AppConstant;
import com.reekar.commonclasses.BaseActivity;
import com.reekar.databinding.ActivityPaymentDetailsBinding;

import org.json.JSONException;
import org.json.JSONObject;

public class PaymentDetailsActivity extends BaseActivity {

    private ActivityPaymentDetailsBinding binding;
    private Context context;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        context=this;
        binding = DataBindingUtil.setContentView(this,R.layout.activity_payment_details);


//        getIntentData();
        setToolbar();
        setListener();
    }

    private void getIntentData() {
        Intent intent =getIntent();
        if (intent!=null){
            try {
                JSONObject jsonObject=new JSONObject(intent.getStringExtra(AppConstant.PAYMENT_DETAILS));
                showDetails(jsonObject.getJSONObject("response"),intent.getStringExtra(AppConstant.PAYMENT_AMOUNT));
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    private void showDetails(JSONObject response, String paymentAmount) {
        try {
            binding.tvTxtId.setText(response.getString("id"));
            binding.tvTxtStatus.setText(response.getString("status"));
            binding.tvTxtAmount.setText(response.getString(String.format("$%s",paymentAmount)));

        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void setToolbar() {
//        binding.toolbar.tvToolbarBarName.setText("Payment Details");
    }

    @Override
    public void setListener() {
//        binding.toolbar.custToolBckBtn.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){

//            case R.id.cust_tool_bck_btn:{
//                finish();
//                break;
//            }
        }
    }
}
