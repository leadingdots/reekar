package com.reekar.translation;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.util.DisplayMetrics;
import android.util.Log;

import com.akanksha.commonclassutil.PreferencesManager;
import com.reekar.activity.MainActivity;
import com.reekar.commonclasses.AppConstant;

import java.util.Locale;

/**
 * Author:    Akanksha Paul
 * Created:   11/2/20
 **/
public class ChangeLanguage {
    private static String result = "";

    public static void setLocale(Context context,String localeName) {
        String currentLanguage = PreferencesManager.Companion.getStringPreferences(context, AppConstant.CURRENT_LANGUAGE);
       // if (!localeName.equals(currentLanguage)) {
            changeLocale(context,localeName);

            PreferencesManager.Companion.saveStringPreferences(context,AppConstant.CURRENT_LANGUAGE,localeName);
            Intent refresh = new Intent(context, MainActivity.class);
//            refresh.putExtra(currentLang, localeName);
            context.startActivity(refresh);
            ((Activity)context).finishAffinity();
        //}
    }

    public static void changeLocale(Context context, String localeName) {
        Locale myLocale = new Locale(localeName);
        Resources res = context.getResources();
        DisplayMetrics dm = res.getDisplayMetrics();
        Configuration conf = res.getConfiguration();
        conf.locale = myLocale;
        res.updateConfiguration(conf, dm);
    }

    public static String translate(Context context, final String textToTranslate) {

        /*try {
            TranslateAPI translateAPI = new TranslateAPI(
                    Language.AUTO_DETECT,   //Source Language
                    PreferencesManager.Companion.getStringPreferences(context, AppConstant.CURRENT_LANGUAGE),         //Target Language
                    textToTranslate);           //Query Text

            translateAPI.setTranslateListener(new TranslateAPI.TranslateListener() {
                @Override
                public void onSuccess(String translatedText) {
                    result = translatedText;
                }

                @Override
                public void onFailure(String ErrorText) {
                    Log.d("translate", "onFailure: "+ErrorText);
                }
            });
        }
        catch(Exception e) {
            e.printStackTrace();
        }*/

        return textToTranslate;
    }
}
