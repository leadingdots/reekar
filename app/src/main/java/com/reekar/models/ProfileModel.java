package com.reekar.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class ProfileModel {

@SerializedName("id")
@Expose
private Integer id;
@SerializedName("first_name")
@Expose
private String firstName;
@SerializedName("last_name")
@Expose
private String lastName;
@SerializedName("country_code")
@Expose
private String countryCode;
@SerializedName("mobile")
@Expose
private String mobile;
@SerializedName("profile_pic")
@Expose
private Object profilePic;
@SerializedName("email")
@Expose
private String email;
@SerializedName("address")
@Expose
private Object address;

public Integer getId() {
return id;
}

public void setId(Integer id) {
this.id = id;
}

public String getFirstName() {
return firstName;
}

public void setFirstName(String firstName) {
this.firstName = firstName;
}

public String getLastName() {
return lastName;
}

public void setLastName(String lastName) {
this.lastName = lastName;
}

public String getCountryCode() {
return countryCode;
}

public void setCountryCode(String countryCode) {
this.countryCode = countryCode;
}

public String getMobile() {
return mobile;
}

public void setMobile(String mobile) {
this.mobile = mobile;
}

public Object getProfilePic() {
return profilePic;
}

public void setProfilePic(Object profilePic) {
this.profilePic = profilePic;
}

public String getEmail() {
return email;
}

public void setEmail(String email) {
this.email = email;
}

public Object getAddress() {
return address;
}

public void setAddress(Object address) {
this.address = address;
}

}