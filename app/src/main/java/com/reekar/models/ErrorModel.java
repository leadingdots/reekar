package com.reekar.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class ErrorModel {
    @SerializedName("email")
    @Expose
    private List<String> email = null;
    @SerializedName("password")
    @Expose
    private List<String> password = null;
    @SerializedName("device_type")
    @Expose
    private List<String> deviceType = null;

    //register error
    @SerializedName("first_name")
    @Expose
    private List<String> firstName = null;
    @SerializedName("last_name")
    @Expose
    private List<String> lastName = null;

    @SerializedName("country_code")
    @Expose
    private List<String> countryCode = null;
    @SerializedName("mobile")
    @Expose
    private List<String> mobile = null;

    @SerializedName("password_confirmation")
    @Expose
    private List<String> passwordConfirmation = null;

    //payment method
    @SerializedName("type")
    @Expose
    private List<String> type = null;


    @SerializedName("otp")
    @Expose
    private List<String> otp = null;

    //cc payment method
    @SerializedName("card_no")
    @Expose
    private List<String> cardNo = null;
    @SerializedName("exp_date")
    @Expose
    private List<String> expDate = null;
    @SerializedName("cvv")
    @Expose
    private List<String> cvv = null;
    @SerializedName("name_on_card")
    @Expose
    private List<String> nameOnCard = null;


    @SerializedName("address")
    @Expose
    private List<String> address = null;

    @SerializedName("image")
    @Expose
    private List<String> image = null;

    /*-- error save car availability--*/
    @SerializedName("available_from_date")
    @Expose
    private List<String> availableFromDate = null;
    @SerializedName("available_from_time")
    @Expose
    private List<String> availableFromTime = null;
    @SerializedName("available_to_date")
    @Expose
    private List<String> availableToDate = null;
    @SerializedName("available_to_time")
    @Expose
    private List<String> availableToTime = null;


    /*----- save order -----*/
    @SerializedName("currency")
    @Expose
    private List<String> currency = null;
    @SerializedName("total_amount")
    @Expose
    private List<String> totalAmount = null;
    @SerializedName("tax_amount")
    @Expose
    private List<String> taxAmount = null;
    @SerializedName("grand_total")
    @Expose
    private List<String> grandTotal = null;
    @SerializedName("billing_first_name")
    @Expose
    private List<String> billingFirstName = null;
    @SerializedName("billing_last_name")
    @Expose
    private List<String> billingLastName = null;
    @SerializedName("billing_email")
    @Expose
    private List<String> billingEmail = null;
    @SerializedName("billing_mobile")
    @Expose
    private List<String> billingMobile = null;
    @SerializedName("billing_alternate_mobile")
    @Expose
    private List<String> billingAlternateMobile = null;
    @SerializedName("billing_address")
    @Expose
    private List<String> billingAddress = null;
    @SerializedName("pickup_location")
    @Expose
    private List<String> pickupLocation = null;


    //-----sohail 26 june 20
    @SerializedName("brand_id")
    @Expose
    private List<String> brandId = null;
    @SerializedName("model_id")
    @Expose
    private List<String> modelId = null;
    @SerializedName("vehicle_type_id")
    @Expose
    private List<String> vehicleTypeId = null;
    @SerializedName("engine_type_id")
    @Expose
    private List<String> engineTypeId = null;
    @SerializedName("transmission_id")
    @Expose
    private List<String> transmissionId = null;
    @SerializedName("color_id")
    @Expose
    private List<String> colorId = null;
    @SerializedName("seat_id")
    @Expose
    private List<String> seatId = null;
    @SerializedName("year")
    @Expose
    private List<String> year = null;
    @SerializedName("price_per_day")
    @Expose
    private List<String> pricePerDay = null;
    @SerializedName("ac")
    @Expose
    private List<String> ac = null;
    @SerializedName("gps")
    @Expose
    private List<String> gps = null;
    @SerializedName("ipod_interface")
    @Expose
    private List<String> ipodInterface = null;
    @SerializedName("sunroof")
    @Expose
    private List<String> sunroof = null;
    @SerializedName("child_seat")
    @Expose
    private List<String> childSeat = null;
    @SerializedName("electric_windows")
    @Expose
    private List<String> electricWindows = null;
    @SerializedName("heated_seat")
    @Expose
    private List<String> heatedSeat = null;
    @SerializedName("panorma_roof")
    @Expose
    private List<String> panormaRoof = null;
    @SerializedName("prm_gauge")
    @Expose
    private List<String> prmGauge = null;
    @SerializedName("abs")
    @Expose
    private List<String> abs = null;
    @SerializedName("traction_control")
    @Expose
    private List<String> tractionControl = null;
    @SerializedName("audio_system")
    @Expose
    private List<String> audioSystem = null;
    @SerializedName("licensed")
    @Expose
    private List<String> licensed = null;
    @SerializedName("cruise_control")
    @Expose
    private List<String> cruiseControl = null;


    public List<String> getBrandId() {
        return brandId;
    }

    public void setBrandId(List<String> brandId) {
        this.brandId = brandId;
    }

    public List<String> getModelId() {
        return modelId;
    }

    public void setModelId(List<String> modelId) {
        this.modelId = modelId;
    }

    public List<String> getVehicleTypeId() {
        return vehicleTypeId;
    }

    public void setVehicleTypeId(List<String> vehicleTypeId) {
        this.vehicleTypeId = vehicleTypeId;
    }

    public List<String> getEngineTypeId() {
        return engineTypeId;
    }

    public void setEngineTypeId(List<String> engineTypeId) {
        this.engineTypeId = engineTypeId;
    }

    public List<String> getTransmissionId() {
        return transmissionId;
    }

    public void setTransmissionId(List<String> transmissionId) {
        this.transmissionId = transmissionId;
    }

    public List<String> getColorId() {
        return colorId;
    }

    public void setColorId(List<String> colorId) {
        this.colorId = colorId;
    }

    public List<String> getSeatId() {
        return seatId;
    }

    public void setSeatId(List<String> seatId) {
        this.seatId = seatId;
    }

    public List<String> getYear() {
        return year;
    }

    public void setYear(List<String> year) {
        this.year = year;
    }

    public List<String> getPricePerDay() {
        return pricePerDay;
    }

    public void setPricePerDay(List<String> pricePerDay) {
        this.pricePerDay = pricePerDay;
    }

    public List<String> getAc() {
        return ac;
    }

    public void setAc(List<String> ac) {
        this.ac = ac;
    }

    public List<String> getGps() {
        return gps;
    }

    public void setGps(List<String> gps) {
        this.gps = gps;
    }

    public List<String> getIpodInterface() {
        return ipodInterface;
    }

    public void setIpodInterface(List<String> ipodInterface) {
        this.ipodInterface = ipodInterface;
    }

    public List<String> getSunroof() {
        return sunroof;
    }

    public void setSunroof(List<String> sunroof) {
        this.sunroof = sunroof;
    }

    public List<String> getChildSeat() {
        return childSeat;
    }

    public void setChildSeat(List<String> childSeat) {
        this.childSeat = childSeat;
    }

    public List<String> getElectricWindows() {
        return electricWindows;
    }

    public void setElectricWindows(List<String> electricWindows) {
        this.electricWindows = electricWindows;
    }

    public List<String> getHeatedSeat() {
        return heatedSeat;
    }

    public void setHeatedSeat(List<String> heatedSeat) {
        this.heatedSeat = heatedSeat;
    }

    public List<String> getPanormaRoof() {
        return panormaRoof;
    }

    public void setPanormaRoof(List<String> panormaRoof) {
        this.panormaRoof = panormaRoof;
    }

    public List<String> getPrmGauge() {
        return prmGauge;
    }

    public void setPrmGauge(List<String> prmGauge) {
        this.prmGauge = prmGauge;
    }

    public List<String> getAbs() {
        return abs;
    }

    public void setAbs(List<String> abs) {
        this.abs = abs;
    }

    public List<String> getTractionControl() {
        return tractionControl;
    }

    public void setTractionControl(List<String> tractionControl) {
        this.tractionControl = tractionControl;
    }

    public List<String> getAudioSystem() {
        return audioSystem;
    }

    public void setAudioSystem(List<String> audioSystem) {
        this.audioSystem = audioSystem;
    }

    public List<String> getLicensed() {
        return licensed;
    }

    public void setLicensed(List<String> licensed) {
        this.licensed = licensed;
    }

    public List<String> getCruiseControl() {
        return cruiseControl;
    }

    public void setCruiseControl(List<String> cruiseControl) {
        this.cruiseControl = cruiseControl;
    }

    public List<String> getCurrency() {
        return currency;
    }

    public void setCurrency(List<String> currency) {
        this.currency = currency;
    }

    public List<String> getTotalAmount() {
        return totalAmount;
    }

    public void setTotalAmount(List<String> totalAmount) {
        this.totalAmount = totalAmount;
    }

    public List<String> getTaxAmount() {
        return taxAmount;
    }

    public void setTaxAmount(List<String> taxAmount) {
        this.taxAmount = taxAmount;
    }

    public List<String> getGrandTotal() {
        return grandTotal;
    }

    public void setGrandTotal(List<String> grandTotal) {
        this.grandTotal = grandTotal;
    }

    public List<String> getBillingFirstName() {
        return billingFirstName;
    }

    public void setBillingFirstName(List<String> billingFirstName) {
        this.billingFirstName = billingFirstName;
    }

    public List<String> getBillingLastName() {
        return billingLastName;
    }

    public void setBillingLastName(List<String> billingLastName) {
        this.billingLastName = billingLastName;
    }

    public List<String> getBillingEmail() {
        return billingEmail;
    }

    public void setBillingEmail(List<String> billingEmail) {
        this.billingEmail = billingEmail;
    }

    public List<String> getBillingMobile() {
        return billingMobile;
    }

    public void setBillingMobile(List<String> billingMobile) {
        this.billingMobile = billingMobile;
    }

    public List<String> getBillingAlternateMobile() {
        return billingAlternateMobile;
    }

    public void setBillingAlternateMobile(List<String> billingAlternateMobile) {
        this.billingAlternateMobile = billingAlternateMobile;
    }

    public List<String> getBillingAddress() {
        return billingAddress;
    }

    public void setBillingAddress(List<String> billingAddress) {
        this.billingAddress = billingAddress;
    }

    public List<String> getPickupLocation() {
        return pickupLocation;
    }

    public void setPickupLocation(List<String> pickupLocation) {
        this.pickupLocation = pickupLocation;
    }

    public List<String> getAvailableFromDate() {
        return availableFromDate;
    }

    public void setAvailableFromDate(List<String> availableFromDate) {
        this.availableFromDate = availableFromDate;
    }

    public List<String> getAvailableFromTime() {
        return availableFromTime;
    }

    public void setAvailableFromTime(List<String> availableFromTime) {
        this.availableFromTime = availableFromTime;
    }

    public List<String> getAvailableToDate() {
        return availableToDate;
    }

    public void setAvailableToDate(List<String> availableToDate) {
        this.availableToDate = availableToDate;
    }

    public List<String> getAvailableToTime() {
        return availableToTime;
    }

    public void setAvailableToTime(List<String> availableToTime) {
        this.availableToTime = availableToTime;
    }



    public List<String> getImage() {
        return image;
    }

    public void setImage(List<String> image) {
        this.image = image;
    }

    public List<String> getAddress() {
        return address;
    }

    public void setAddress(List<String> address) {
        this.address = address;
    }

    public List<String> getEmail() {
        return email;
    }

    public void setEmail(List<String> email) {
        this.email = email;
    }

    public List<String> getPassword() {
        return password;
    }

    public void setPassword(List<String> password) {
        this.password = password;
    }

    public List<String> getDeviceType() {
        return deviceType;
    }

    public void setDeviceType(List<String> deviceType) {
        this.deviceType = deviceType;
    }


    public List<String> getFirstName() {
        return firstName;
    }

    public void setFirstName(List<String> firstName) {
        this.firstName = firstName;
    }

    public List<String> getLastName() {
        return lastName;
    }

    public void setLastName(List<String> lastName) {
        this.lastName = lastName;
    }

    public List<String> getCountryCode() {
        return countryCode;
    }

    public void setCountryCode(List<String> countryCode) {
        this.countryCode = countryCode;
    }

    public List<String> getMobile() {
        return mobile;
    }

    public void setMobile(List<String> mobile) {
        this.mobile = mobile;
    }

    public List<String> getPasswordConfirmation() {
        return passwordConfirmation;
    }

    public void setPasswordConfirmation(List<String> passwordConfirmation) {
        this.passwordConfirmation = passwordConfirmation;
    }

    public List<String> getType() {
        return type;
    }

    public void setType(List<String> type) {
        this.type = type;
    }

    public List<String> getOtp() {
        return otp;
    }

    public void setOtp(List<String> otp) {
        this.otp = otp;
    }

    public List<String> getCardNo() {
        return cardNo;
    }

    public void setCardNo(List<String> cardNo) {
        this.cardNo = cardNo;
    }

    public List<String> getExpDate() {
        return expDate;
    }

    public void setExpDate(List<String> expDate) {
        this.expDate = expDate;
    }

    public List<String> getCvv() {
        return cvv;
    }

    public void setCvv(List<String> cvv) {
        this.cvv = cvv;
    }

    public List<String> getNameOnCard() {
        return nameOnCard;
    }

    public void setNameOnCard(List<String> nameOnCard) {
        this.nameOnCard = nameOnCard;
    }
}
