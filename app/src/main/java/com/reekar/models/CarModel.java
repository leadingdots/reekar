package com.reekar.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Author:    Akanksha Paul
 * Created:   22/1/20
 **/
public class CarModel {
    @SerializedName("id")
    @Expose
    private int id;
    @SerializedName("brand_id")
    @Expose
    private int brandId;
    @SerializedName("name")
    @Expose
    private String name;
    @SerializedName("year")
    @Expose
    private String year;
    @SerializedName("car_location")
    @Expose
    private String carLocation;
    @SerializedName("available_from_date")
    @Expose
    private String availableFromDate;
    @SerializedName("available_from_time")
    @Expose
    private String availableFromTime;
    @SerializedName("available_to_date")
    @Expose
    private String availableToDate;
    @SerializedName("available_to_time")
    @Expose
    private String availableToTime;
    @SerializedName("currency")
    @Expose
    private String currency;
    @SerializedName("price_per_day")
    @Expose
    private Double pricePerDay;
    @SerializedName("total_price")
    @Expose
    private Double totalPrice;
    @SerializedName("car_location_lat")
    @Expose
    private String carLocationLat;
    @SerializedName("car_location_long")
    @Expose
    private String carLocationLong;
    @SerializedName("distance")
    @Expose
    private Double distance;
    @SerializedName("brand")
    @Expose
    private String brand;
    @SerializedName("model")
    @Expose
    private String model;
    @SerializedName("color")
    @Expose
    private String color;
    @SerializedName("enginetype")
    @Expose
    private String enginetype;
    @SerializedName("seat")
    @Expose
    private String seat;
    @SerializedName("transmission")
    @Expose
    private String transmission;
    @SerializedName("vehicletype")
    @Expose
    private String vehicletype;
    @SerializedName("features")
    @Expose
    private List<String> features = null;

    //new fields added
    @SerializedName("mileage")
    @Expose
    private String mileage;
    @SerializedName("chassis_number")
    @Expose
    private String chassisNumber;
    @SerializedName("cruise_control")
    @Expose
    private int cruiseControl;
    @SerializedName("licensed")
    @Expose
    private int licensed;


    @SerializedName("image")
    @Expose
    private String image;
    @SerializedName("images")
    @Expose
    private List<String> images = null;

    @SerializedName("docs")
    @Expose
    private List<String> docs = null;

    public List<CarImageModel> getCarImages() {
        return carImages;
    }

    public void setCarImages(List<CarImageModel> carImages) {
        this.carImages = carImages;
    }

    public List<CarDocModel> getCarDocs() {
        return carDocs;
    }

    public void setCarDocs(List<CarDocModel> carDocs) {
        this.carDocs = carDocs;
    }

    @SerializedName("car_images")
    @Expose
    private List<CarImageModel> carImages = null;
    @SerializedName("car_docs")
    @Expose
    private List<CarDocModel> carDocs = null;


    @SerializedName("reject_reason")
    @Expose
    private String rejectReason;
    @SerializedName("listing_status")
    @Expose
    private int listingStatus;
    @SerializedName("security_amount")
    @Expose
    private Double securityAmount;
    @SerializedName("is_rented")
    @Expose
    private boolean isRented;


    @SerializedName("brand_detail")
    @Expose
    private BrandModel brandDetail;
    @SerializedName("model_detail")
    @Expose
    private BrandModel modelDetail;
    @SerializedName("color_detail")
    @Expose
    private BrandModel colorDetail;
    @SerializedName("seat_detail")
    @Expose
    private BrandModel seatDetail;
    @SerializedName("enginetype_detail")
    @Expose
    private BrandModel enginetypeDetail;
    @SerializedName("transmission_detail")
    @Expose
    private BrandModel transmissionDetail;
    @SerializedName("vehicletype_detail")
    @Expose
    private BrandModel vehicletypeDetail;

    @SerializedName("mileage_detail")
    @Expose
    private BrandModel mileageDetail;
    @SerializedName("chassis_number_detail")
    @Expose
    private BrandModel chassisNumberDetail;
    @SerializedName("cruise_control_detail")
    @Expose
    private BrandModel cruiseControlDetail;

    @SerializedName("licensed_detail")
    @Expose
    private BrandModel licensedDetail;

    public BrandModel getMileageDetail() {
        return mileageDetail;
    }

    public void setMileageDetail(BrandModel mileageDetail) {
        this.mileageDetail = mileageDetail;
    }

    public BrandModel getChassisNumberDetail() {
        return chassisNumberDetail;
    }

    public void setChassisNumberDetail(BrandModel chassisNumberDetail) {
        this.chassisNumberDetail = chassisNumberDetail;
    }

    public BrandModel getCruiseControlDetail() {
        return cruiseControlDetail;
    }

    public void setCruiseControlDetail(BrandModel cruiseControlDetail) {
        this.cruiseControlDetail = cruiseControlDetail;
    }

    public BrandModel getLicensedDetail() {
        return licensedDetail;
    }

    public void setLicensedDetail(BrandModel licensedDetail) {
        this.licensedDetail = licensedDetail;
    }

    public String getMileage() {
        return mileage;
    }

    public void setMileage(String mileage) {
        this.mileage = mileage;
    }

    public String getChassisNumber() {
        return chassisNumber;
    }

    public void setChassisNumber(String chassisNumber) {
        this.chassisNumber = chassisNumber;
    }

    public int getCruiseControl() {
        return cruiseControl;
    }

    public void setCruiseControl(int cruiseControl) {
        this.cruiseControl = cruiseControl;
    }

    public int getLicensed() {
        return licensed;
    }

    public void setLicensed(int licensed) {
        this.licensed = licensed;
    }

    public String getBrand() {
        return brand;
    }

    public void setBrand(String brand) {
        this.brand = brand;
    }

    public String getModel() {
        return model;
    }

    public void setModel(String model) {
        this.model = model;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public String getEnginetype() {
        return enginetype;
    }

    public void setEnginetype(String enginetype) {
        this.enginetype = enginetype;
    }

    public String getSeat() {
        return seat;
    }

    public void setSeat(String seat) {
        this.seat = seat;
    }

    public String getTransmission() {
        return transmission;
    }

    public void setTransmission(String transmission) {
        this.transmission = transmission;
    }

    public String getVehicletype() {
        return vehicletype;
    }

    public void setVehicletype(String vehicletype) {
        this.vehicletype = vehicletype;
    }

    public BrandModel getBrandDetail() {
        return brandDetail;
    }

    public void setBrandDetail(BrandModel brandDetail) {
        this.brandDetail = brandDetail;
    }

    public BrandModel getModelDetail() {
        return modelDetail;
    }

    public void setModelDetail(BrandModel modelDetail) {
        this.modelDetail = modelDetail;
    }

    public BrandModel getColorDetail() {
        return colorDetail;
    }

    public void setColorDetail(BrandModel colorDetail) {
        this.colorDetail = colorDetail;
    }

    public BrandModel getSeatDetail() {
        return seatDetail;
    }

    public void setSeatDetail(BrandModel seatDetail) {
        this.seatDetail = seatDetail;
    }

    public BrandModel getEnginetypeDetail() {
        return enginetypeDetail;
    }

    public void setEnginetypeDetail(BrandModel enginetypeDetail) {
        this.enginetypeDetail = enginetypeDetail;
    }

    public BrandModel getTransmissionDetail() {
        return transmissionDetail;
    }

    public void setTransmissionDetail(BrandModel transmissionDetail) {
        this.transmissionDetail = transmissionDetail;
    }

    public BrandModel getVehicletypeDetail() {
        return vehicletypeDetail;
    }

    public void setVehicletypeDetail(BrandModel vehicletypeDetail) {
        this.vehicletypeDetail = vehicletypeDetail;
    }

    public Double getSecurityAmount() {
        return securityAmount;
    }

    public void setSecurityAmount(Double securityAmount) {
        this.securityAmount = securityAmount;
    }

    public boolean isRented() {
        return isRented;
    }

    public void setRented(boolean rented) {
        isRented = rented;
    }

    public String getRejectReason() {
        return rejectReason;
    }

    public void setRejectReason(String rejectReason) {
        this.rejectReason = rejectReason;
    }

    public int getListingStatus() {
        return listingStatus;
    }

    public void setListingStatus(int listingStatus) {
        this.listingStatus = listingStatus;
    }



    public List<String> getImages() {
        return images;
    }

    public List<String> getDocs() {
        return docs;
    }

    public void setDocs(List<String> docs) {
        this.docs = docs;
    }

    public void setImages(List<String> images) {
        this.images = images;
    }

    public String getYear() {
        return year;
    }

    public void setYear(String year) {
        this.year = year;
    }

    public String getCarLocation() {
        return carLocation;
    }

    public void setCarLocation(String carLocation) {
        this.carLocation = carLocation;
    }

    public String getAvailableFromDate() {
        return availableFromDate;
    }

    public void setAvailableFromDate(String availableFromDate) {
        this.availableFromDate = availableFromDate;
    }

    public String getAvailableFromTime() {
        return availableFromTime;
    }

    public void setAvailableFromTime(String availableFromTime) {
        this.availableFromTime = availableFromTime;
    }

    public String getAvailableToDate() {
        return availableToDate;
    }

    public void setAvailableToDate(String availableToDate) {
        this.availableToDate = availableToDate;
    }

    public String getAvailableToTime() {
        return availableToTime;
    }

    public void setAvailableToTime(String availableToTime) {
        this.availableToTime = availableToTime;
    }

    public String getCurrency() {
        return currency;
    }

    public void setCurrency(String currency) {
        this.currency = currency;
    }

    public Double getPricePerDay() {
        return pricePerDay;
    }

    public void setPricePerDay(Double pricePerDay) {
        this.pricePerDay = pricePerDay;
    }

    public Double getTotalPrice() {
        return totalPrice;
    }

    public void setTotalPrice(Double totalPrice) {
        this.totalPrice = totalPrice;
    }

    public String getCarLocationLat() {
        return carLocationLat;
    }

    public void setCarLocationLat(String carLocationLat) {
        this.carLocationLat = carLocationLat;
    }

    public String getCarLocationLong() {
        return carLocationLong;
    }

    public void setCarLocationLong(String carLocationLong) {
        this.carLocationLong = carLocationLong;
    }

    public Double getDistance() {
        return distance;
    }

    public void setDistance(Double distance) {
        this.distance = distance;
    }


    public List<String> getFeatures() {
        return features;
    }

    public void setFeatures(List<String> features) {
        this.features = features;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getBrandId() {
        return brandId;
    }

    public void setBrandId(int brandId) {
        this.brandId = brandId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }


}
