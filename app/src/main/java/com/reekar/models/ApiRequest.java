package com.reekar.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class ApiRequest {

    @SerializedName("email")
    @Expose
    private String email;
    @SerializedName("password")
    @Expose
    private String password;
    @SerializedName("device_type")
    @Expose
    private String device_type;
    @SerializedName("device_token")
    @Expose
    private String device_token;

    @SerializedName("myip")
    @Expose
    private String myip;

    //register request
    @SerializedName("first_name")
    @Expose
    private String first_name;
    @SerializedName("last_name")
    @Expose
    private String last_name;
    @SerializedName("country_code")
    @Expose
    private String country_code;
    @SerializedName("mobile")
    @Expose
    private String mobile;
    @SerializedName("password_confirmation")
    @Expose
    private String password_confirmation;

    @SerializedName("otp")
    @Expose
    private String otp;

    //payment method save
    @SerializedName("payment_id")
    @Expose
    private String payment_id;
    @SerializedName("type")
    @Expose
    private String type;
    @SerializedName("iban")
    @Expose
    private String iban;
    @SerializedName("bank_name")
    @Expose
    private String bank_name;
    @SerializedName("bank_swift_code")
    @Expose
    private String bank_swift_code;
    @SerializedName("card_no")
    @Expose
    private String card_no;
    @SerializedName("exp_date")
    @Expose
    private String exp_date;
    @SerializedName("cvv")
    @Expose
    private String cvv;
    @SerializedName("name_on_card")
    @Expose
    private String name_on_card;

    @SerializedName("pickup_address")
    @Expose
    private String pickupAddress;
    @SerializedName("pickup_time")
    @Expose
    private String pickupTime;
    @SerializedName("pickup_date")
    @Expose
    private String pickupDate;
    @SerializedName("drop_time")
    @Expose
    private String dropTime;
    @SerializedName("drop_date")
    @Expose
    private String dropDate;
    @SerializedName("pickup_lat")
    @Expose
    private String pickupLat;
    @SerializedName("pickup_long")
    @Expose
    private String pickupLong;
    @SerializedName("sort_by")
    @Expose
    private String sortBy;
    @SerializedName("paginate")
    @Expose
    private int paginate;
    @SerializedName("page")
    @Expose
    private int page;

    @SerializedName("name")
    @Expose
    private String name;;
    @SerializedName("image")
    @Expose
    private String image;
    @SerializedName("brand_id")
    @Expose
    private List<Integer> brandId;
    @SerializedName("model_id")
    @Expose
    private List<Integer> modelId;

    @SerializedName("vehicle_type_id")
    @Expose
    private List<Integer> vehicleTypeId;

    @SerializedName("seat_id")
    @Expose
    private List<Integer> seatId;

    @SerializedName("engine_type_id")
    @Expose
    private List<Integer> engineTypeId;

    @SerializedName("transmission_id")
    @Expose
    private List<Integer> transmissionId;

    @SerializedName("color_id")
    @Expose
    private List<Integer> colorId;

    @SerializedName("ac")
    @Expose
    private int ac;

    @SerializedName("gps")
    @Expose
    private int gps;

    @SerializedName("ipod_interface")
    @Expose
    private int ipodInterface;

    @SerializedName("sunroof")
    @Expose
    private int sunroof;

    @SerializedName("child_seat")
    @Expose
    private int childSeat;

    @SerializedName("electric_windows")
    @Expose
    private int electricWindows;

    @SerializedName("heated_seat")
    @Expose
    private int heatedSeat;
    @SerializedName("panorma_roof")
    @Expose
    private int panormaRoof;
    @SerializedName("prm_guage")
    @Expose
    private int prmGuage;
    @SerializedName("abs")
    @Expose
    private int abs;
    @SerializedName("traction_control")
    @Expose
    private int tractionControl;
    @SerializedName("audio_system")
    @Expose
    private int audioSystem;
    @SerializedName("year")
    @Expose
    private String year;
    @SerializedName("currency")
    @Expose
    private String currency;

    @SerializedName("car_id")
    @Expose
    private String carId;

    @SerializedName("address")
    @Expose
    private String address;
    @SerializedName("order_id")
    @Expose
    private String orderId;

    /*---Place Order ---*/
    @SerializedName("booking_from_date")
    @Expose
    private String bookingFromDate;
    @SerializedName("booking_to_date")
    @Expose
    private String bookingToDate;

    @SerializedName("total_amount")
    @Expose
    private String totalAmount;
    @SerializedName("security_amount")
    @Expose
    private String securityAmount;
    @SerializedName("tax_amount")
    @Expose
    private String taxAmount;
    @SerializedName("grand_total")
    @Expose
    private String grandTotal;
    @SerializedName("billing_first_name")
    @Expose
    private String billingFirstName;
    @SerializedName("billing_last_name")
    @Expose
    private String billingLastName;
    @SerializedName("billing_email")
    @Expose
    private String billingEmail;
    @SerializedName("billing_mobile")
    @Expose
    private String billingMobile;
    @SerializedName("billing_alternate_mobile")
    @Expose
    private String billingAlternateMobile;
    @SerializedName("billing_address")
    @Expose
    private String billingAddress;
    @SerializedName("pickup_location")
    @Expose
    private String pickupLocation;

    /*---------CAR INFO SAVE ----*/
    @SerializedName("price_per_day")
    @Expose
    private String pricePerDay;
    @SerializedName("total_price")
    @Expose
    private String totalPrice;

    /*--success order ---*/
    @SerializedName("booking_id")
    @Expose
    private String bookingId;
    @SerializedName("payment_method")
    @Expose
    private String paymentMethod;
    @SerializedName("bank_reference_id")
    @Expose
    private String bankReferenceId;
    @SerializedName("transaction_id")
    @Expose
    private String transactionId;

    @SerializedName("time_zone")
    @Expose
    private String timeZone;

    @SerializedName("content_for")
    @Expose
    private String contentFor;


    /*-----Delete Documents -----*/
    @SerializedName("file_type")
    @Expose
    private String fileType;
    @SerializedName("file_id")
    @Expose
    private String fileId;

    @SerializedName("amount")
    @Expose
    private String amount;

    @SerializedName("payment_method_nonce")
    @Expose
    private String paymentMethodNonce;
    @SerializedName("device_data")
    @Expose
    private String deviceData;

    public String getAmount() {
        return amount;
    }

    public String getPaymentMethodNonce() {
        return paymentMethodNonce;
    }

    public String getDeviceData() {
        return deviceData;
    }

    public void setDeviceData(String deviceData) {
        this.deviceData = deviceData;
    }

    public void setPaymentMethodNonce(String paymentMethodNonce) {
        this.paymentMethodNonce = paymentMethodNonce;
    }

    public void setAmount(String amount) {
        this.amount = amount;
    }

    public String getTimeZone() {
        return timeZone;
    }

    public void setTimeZone(String timeZone) {
        this.timeZone = timeZone;
    }

    public String getContentFor() {
        return contentFor;
    }

    public void setContentFor(String contentFor) {
        this.contentFor = contentFor;
    }

    public String getFileType() {
        return fileType;
    }

    public void setFileType(String fileType) {
        this.fileType = fileType;
    }

    public String getFileId() {
        return fileId;
    }

    public void setFileId(String fileId) {
        this.fileId = fileId;
    }

    public String getBookingId() {
        return bookingId;
    }

    public void setBookingId(String bookingId) {
        this.bookingId = bookingId;
    }

    public String getPaymentMethod() {
        return paymentMethod;
    }

    public void setPaymentMethod(String paymentMethod) {
        this.paymentMethod = paymentMethod;
    }

    public String getBankReferenceId() {
        return bankReferenceId;
    }

    public void setBankReferenceId(String bankReferenceId) {
        this.bankReferenceId = bankReferenceId;
    }

    public String getTransactionId() {
        return transactionId;
    }

    public void setTransactionId(String transactionId) {
        this.transactionId = transactionId;
    }

    public String getPricePerDay() {
        return pricePerDay;
    }

    public void setPricePerDay(String pricePerDay) {
        this.pricePerDay = pricePerDay;
    }

    public String getTotalPrice() {
        return totalPrice;
    }

    public void setTotalPrice(String totalPrice) {
        this.totalPrice = totalPrice;
    }

    public String getBookingFromDate() {
        return bookingFromDate;
    }

    public void setBookingFromDate(String bookingFromDate) {
        this.bookingFromDate = bookingFromDate;
    }

    public String getMyip() {
        return myip;
    }

    public void setMyip(String myip) {
        this.myip = myip;
    }

    public String getBookingToDate() {
        return bookingToDate;
    }

    public void setBookingToDate(String bookingToDate) {
        this.bookingToDate = bookingToDate;
    }

    public String getTotalAmount() {
        return totalAmount;
    }

    public void setTotalAmount(String totalAmount) {
        this.totalAmount = totalAmount;
    }

    public String getSecurityAmount() {
        return securityAmount;
    }

    public void setSecurityAmount(String securityAmount) {
        this.securityAmount = securityAmount;
    }

    public String getTaxAmount() {
        return taxAmount;
    }

    public void setTaxAmount(String taxAmount) {
        this.taxAmount = taxAmount;
    }

    public String getGrandTotal() {
        return grandTotal;
    }

    public void setGrandTotal(String grandTotal) {
        this.grandTotal = grandTotal;
    }

    public String getBillingFirstName() {
        return billingFirstName;
    }

    public void setBillingFirstName(String billingFirstName) {
        this.billingFirstName = billingFirstName;
    }

    public String getBillingLastName() {
        return billingLastName;
    }

    public void setBillingLastName(String billingLastName) {
        this.billingLastName = billingLastName;
    }

    public String getBillingEmail() {
        return billingEmail;
    }

    public void setBillingEmail(String billingEmail) {
        this.billingEmail = billingEmail;
    }

    public String getBillingMobile() {
        return billingMobile;
    }

    public void setBillingMobile(String billingMobile) {
        this.billingMobile = billingMobile;
    }

    public String getBillingAlternateMobile() {
        return billingAlternateMobile;
    }

    public void setBillingAlternateMobile(String billingAlternateMobile) {
        this.billingAlternateMobile = billingAlternateMobile;
    }

    public String getBillingAddress() {
        return billingAddress;
    }

    public void setBillingAddress(String billingAddress) {
        this.billingAddress = billingAddress;
    }

    public String getPickupLocation() {
        return pickupLocation;
    }

    public void setPickupLocation(String pickupLocation) {
        this.pickupLocation = pickupLocation;
    }

    public String getOrderId() {
        return orderId;
    }

    public void setOrderId(String orderId) {
        this.orderId = orderId;
    }


    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public int getPage() {
        return page;
    }

    public void setPage(int page) {
        this.page = page;
    }

    public String getCurrency() {
        return currency;
    }

    public void setCurrency(String currency) {
        this.currency = currency;
    }

    public List<Integer> getSeatId() {
        return seatId;
    }

    public void setSeatId(List<Integer> seatId) {
        this.seatId = seatId;
    }

    public String getCarId() {
        return carId;
    }

    public void setCarId(String carId) {
        this.carId = carId;
    }

    public String getYear() {
        return year;
    }

    public void setYear(String year) {
        this.year = year;
    }

    public List<Integer> getModelId() {
        return modelId;
    }

    public void setModelId(List<Integer> modelId) {
        this.modelId = modelId;
    }

    public List<Integer> getVehicleTypeId() {
        return vehicleTypeId;
    }

    public void setVehicleTypeId(List<Integer> vehicleTypeId) {
        this.vehicleTypeId = vehicleTypeId;
    }

    public List<Integer> getEngineTypeId() {
        return engineTypeId;
    }

    public void setEngineTypeId(List<Integer> engineTypeId) {
        this.engineTypeId = engineTypeId;
    }

    public List<Integer> getTransmissionId() {
        return transmissionId;
    }

    public void setTransmissionId(List<Integer> transmissionId) {
        this.transmissionId = transmissionId;
    }

    public List<Integer> getColorId() {
        return colorId;
    }

    public void setColorId(List<Integer> colorId) {
        this.colorId = colorId;
    }

    public int getAc() {
        return ac;
    }

    public void setAc(int ac) {
        this.ac = ac;
    }

    public int getGps() {
        return gps;
    }

    public void setGps(int gps) {
        this.gps = gps;
    }

    public int getIpodInterface() {
        return ipodInterface;
    }

    public void setIpodInterface(int ipodInterface) {
        this.ipodInterface = ipodInterface;
    }

    public int getSunroof() {
        return sunroof;
    }

    public void setSunroof(int sunroof) {
        this.sunroof = sunroof;
    }

    public int getChildSeat() {
        return childSeat;
    }

    public void setChildSeat(int childSeat) {
        this.childSeat = childSeat;
    }

    public int getElectricWindows() {
        return electricWindows;
    }

    public void setElectricWindows(int electricWindows) {
        this.electricWindows = electricWindows;
    }

    public int getHeatedSeat() {
        return heatedSeat;
    }

    public void setHeatedSeat(int heatedSeat) {
        this.heatedSeat = heatedSeat;
    }

    public int getPanormaRoof() {
        return panormaRoof;
    }

    public void setPanormaRoof(int panormaRoof) {
        this.panormaRoof = panormaRoof;
    }

    public int getPrmGuage() {
        return prmGuage;
    }

    public void setPrmGuage(int prmGuage) {
        this.prmGuage = prmGuage;
    }

    public int getAbs() {
        return abs;
    }

    public void setAbs(int abs) {
        this.abs = abs;
    }

    public int getTractionControl() {
        return tractionControl;
    }

    public void setTractionControl(int tractionControl) {
        this.tractionControl = tractionControl;
    }

    public int getAudioSystem() {
        return audioSystem;
    }

    public void setAudioSystem(int audioSystem) {
        this.audioSystem = audioSystem;
    }

    public List<Integer> getBrandId() {
        return brandId;
    }

    public void setBrandId(List<Integer> brandId) {
        this.brandId = brandId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public int getPaginate() {
        return paginate;
    }

    public void setPaginate(int paginate) {
        this.paginate = paginate;
    }

    public String getSortBy() {
        return sortBy;
    }

    public void setSortBy(String sortBy) {
        this.sortBy = sortBy;
    }

    public String getDropTime() {
        return dropTime;
    }

    public void setDropTime(String dropTime) {
        this.dropTime = dropTime;
    }

    public String getDropDate() {
        return dropDate;
    }

    public void setDropDate(String dropDate) {
        this.dropDate = dropDate;
    }

    public String getPickupAddress() {
        return pickupAddress;
    }

    public void setPickupAddress(String pickupAddress) {
        this.pickupAddress = pickupAddress;
    }

    public String getPickupTime() {
        return pickupTime;
    }

    public void setPickupTime(String pickupTime) {
        this.pickupTime = pickupTime;
    }

    public String getPickupDate() {
        return pickupDate;
    }

    public void setPickupDate(String pickupDate) {
        this.pickupDate = pickupDate;
    }

    public String getPickupLat() {
        return pickupLat;
    }

    public void setPickupLat(String pickupLat) {
        this.pickupLat = pickupLat;
    }

    public String getPickupLong() {
        return pickupLong;
    }

    public void setPickupLong(String pickupLong) {
        this.pickupLong = pickupLong;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getDevice_type() {
        return device_type;
    }

    public void setDevice_type(String device_type) {
        this.device_type = device_type;
    }

    public String getDevice_token() {
        return device_token;
    }

    public void setDevice_token(String device_token) {
        this.device_token = device_token;
    }

    //register request

    public String getFirst_name() {
        return first_name;
    }

    public void setFirst_name(String first_name) {
        this.first_name = first_name;
    }

    public String getLast_name() {
        return last_name;
    }

    public void setLast_name(String last_name) {
        this.last_name = last_name;
    }

    public String getCountry_code() {
        return country_code;
    }

    public void setCountry_code(String country_code) {
        this.country_code = country_code;
    }

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    public String getPassword_confirmation() {
        return password_confirmation;
    }

    public void setPassword_confirmation(String password_confirmation) {
        this.password_confirmation = password_confirmation;
    }

    public String getOtp() {
        return otp;
    }

    public void setOtp(String otp) {
        this.otp = otp;
    }

    public String getPayment_id() {
        return payment_id;
    }

    public void setPayment_id(String payment_id) {
        this.payment_id = payment_id;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getIban() {
        return iban;
    }

    public void setIban(String iban) {
        this.iban = iban;
    }

    public String getBank_name() {
        return bank_name;
    }

    public void setBank_name(String bank_name) {
        this.bank_name = bank_name;
    }

    public String getBank_swift_code() {
        return bank_swift_code;
    }

    public void setBank_swift_code(String bank_swift_code) {
        this.bank_swift_code = bank_swift_code;
    }

    public String getCard_no() {
        return card_no;
    }

    public void setCard_no(String card_no) {
        this.card_no = card_no;
    }

    public String getExp_date() {
        return exp_date;
    }

    public void setExp_date(String exp_date) {
        this.exp_date = exp_date;
    }

    public String getCvv() {
        return cvv;
    }

    public void setCvv(String cvv) {
        this.cvv = cvv;
    }

    public String getName_on_card() {
        return name_on_card;
    }

    public void setName_on_card(String name_on_card) {
        this.name_on_card = name_on_card;
    }
}
