package com.reekar.models;

import androidx.annotation.NonNull;

/**
 * Author:    Akanksha Paul
 * Created:   22/1/20
 **/
public class PlaceAutocompleteModel {
    public CharSequence placeId;
    public CharSequence address, area;

    public CharSequence getPlaceId() {
        return placeId;
    }

    public void setPlaceId(CharSequence placeId) {
        this.placeId = placeId;
    }

    public CharSequence getAddress() {
        return address;
    }

    public void setAddress(CharSequence address) {
        this.address = address;
    }

    public CharSequence getArea() {
        return area;
    }

    public void setArea(CharSequence area) {
        this.area = area;
    }

    @NonNull
    @Override
    public String toString() {
        return area.toString();

    }
}
