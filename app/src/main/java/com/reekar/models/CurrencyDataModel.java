package com.reekar.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class CurrencyDataModel {
    @SerializedName("country")
    @Expose
    private List<CountryModel> country = null;

    public List<CountryModel> getCountry() {
        return country;
    }

    public void setCountry(List<CountryModel> country) {
        this.country = country;
    }


    public class CountryModel {
        @SerializedName("countryCode")
        @Expose
        private String countryCode;
        @SerializedName("countryName")
        @Expose
        private String countryName;
        @SerializedName("currencyCode")
        @Expose
        private String currencyCode;
        @SerializedName("population")
        @Expose
        private String population;
        @SerializedName("capital")
        @Expose
        private String capital;
        @SerializedName("continentName")
        @Expose
        private String continentName;

        public String getCountryCode() {
            return countryCode;
        }

        public void setCountryCode(String countryCode) {
            this.countryCode = countryCode;
        }

        public String getCountryName() {
            return countryName;
        }

        public void setCountryName(String countryName) {
            this.countryName = countryName;
        }

        public String getCurrencyCode() {
            return currencyCode;
        }

        public void setCurrencyCode(String currencyCode) {
            this.currencyCode = currencyCode;
        }

        public String getPopulation() {
            return population;
        }

        public void setPopulation(String population) {
            this.population = population;
        }

        public String getCapital() {
            return capital;
        }

        public void setCapital(String capital) {
            this.capital = capital;
        }

        public String getContinentName() {
            return continentName;
        }

        public void setContinentName(String continentName) {
            this.continentName = continentName;
        }
    }
}
