package com.reekar.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class SaveCarInfoApiRequest {

    @SerializedName("car_id")
    @Expose
    private String carId;
    @SerializedName("brand_id")
    @Expose
    private int brandId;
    @SerializedName("model_id")
    @Expose
    private int modelId;
    @SerializedName("vehicle_type_id")
    @Expose
    private int vehicleTypeId;
    @SerializedName("engine_type_id")
    @Expose
    private int engineTypeId;
    @SerializedName("transmission_id")
    @Expose
    private int transmissionId;

    @SerializedName("seat_id")
    @Expose
    private int seatId;

    /*--- save car photos ---*/
    @SerializedName("images[0][image]")
    @Expose
    private String images0Image;
    @SerializedName("images[1][image]")
    @Expose
    private String images1Image;
    @SerializedName("images[2][image]")
    @Expose
    private String images2Image;

    public String getImages0Image() {
        return images0Image;
    }

    /*--- save car location --*/

    @SerializedName("car_location")
    @Expose
    private String carLocation;
    @SerializedName("car_location_lat")
    @Expose
    private String carLocationLat;
    @SerializedName("car_location_long")
    @Expose
    private String carLocationLong;

    /*-- save car availablity ---*/
    @SerializedName("available_from_date")
    @Expose
    private String availableFromDate;
    @SerializedName("available_from_time")
    @Expose
    private String availableFromTime;
    @SerializedName("available_to_date")
    @Expose
    private String availableToDate;
    @SerializedName("available_to_time")
    @Expose
    private String availableToTime;



    @SerializedName("color_id")
    @Expose
    private int colorId;
    @SerializedName("year")
    @Expose
    private String year;
    @SerializedName("currency")
    @Expose
    private String currency;
    @SerializedName("price_per_day")
    @Expose
    private String pricePerDay;
    @SerializedName("total_price")
    @Expose
    private String totalPrice;
    @SerializedName("security_amount")
    @Expose
    private String securityAmount;
    @SerializedName("ac")
    @Expose
    private int ac;
    @SerializedName("gps")
    @Expose
    private int gps;
    @SerializedName("ipod_interface")
    @Expose
    private int ipodInterface;
    @SerializedName("sunroof")
    @Expose
    private int sunroof;
    @SerializedName("child_seat")
    @Expose
    private int childSeat;
    @SerializedName("electric_windows")
    @Expose
    private int electricWindows;
    @SerializedName("heated_seat")
    @Expose
    private int heatedSeat;
    @SerializedName("panorma_roof")
    @Expose
    private int panormaRoof;
    @SerializedName("prm_gauge")
    @Expose
    private int prmGauge;
    @SerializedName("abs")
    @Expose
    private int abs;
    @SerializedName("traction_control")
    @Expose
    private int tractionControl;
    @SerializedName("audio_system")
    @Expose
    private int audioSystem;

    //new field added
    @SerializedName("mileage")
    @Expose
    private String mileage;
    @SerializedName("chassis_number")
    @Expose
    private String chassisNumber;
    @SerializedName("cruise_control")
    @Expose
    private int cruiseControl;

    @SerializedName("licensed")
    @Expose
    private int licensed;

    public int getLicensed() {
        return licensed;
    }

    public void setLicensed(int licensed) {
        this.licensed = licensed;
    }

    public String getMileage() {
        return mileage;
    }

    public void setMileage(String mileage) {
        this.mileage = mileage;
    }

    public String getChassisNumber() {
        return chassisNumber;
    }

    public void setChassisNumber(String chassisNumber) {
        this.chassisNumber = chassisNumber;
    }

    public int getCruiseControl() {
        return cruiseControl;
    }

    public void setCruiseControl(int cruiseControl) {
        this.cruiseControl = cruiseControl;
    }

    public String getAvailableFromDate() {
        return availableFromDate;
    }

    public void setAvailableFromDate(String availableFromDate) {
        this.availableFromDate = availableFromDate;
    }

    public String getAvailableFromTime() {
        return availableFromTime;
    }

    public void setAvailableFromTime(String availableFromTime) {
        this.availableFromTime = availableFromTime;
    }

    public String getAvailableToDate() {
        return availableToDate;
    }

    public void setAvailableToDate(String availableToDate) {
        this.availableToDate = availableToDate;
    }

    public String getAvailableToTime() {
        return availableToTime;
    }

    public void setAvailableToTime(String availableToTime) {
        this.availableToTime = availableToTime;
    }


    public void setImages0Image(String images0Image) {
        this.images0Image = images0Image;
    }

    public String getImages1Image() {
        return images1Image;
    }

    public void setImages1Image(String images1Image) {
        this.images1Image = images1Image;
    }

    public String getImages2Image() {
        return images2Image;
    }

    public void setImages2Image(String images2Image) {
        this.images2Image = images2Image;
    }

    public String getCarLocation() {
        return carLocation;
    }

    public void setCarLocation(String carLocation) {
        this.carLocation = carLocation;
    }

    public String getCarLocationLat() {
        return carLocationLat;
    }

    public void setCarLocationLat(String carLocationLat) {
        this.carLocationLat = carLocationLat;
    }

    public String getCarLocationLong() {
        return carLocationLong;
    }

    public void setCarLocationLong(String carLocationLong) {
        this.carLocationLong = carLocationLong;
    }

    public int getSeatId() {
        return seatId;
    }

    public void setSeatId(int seatId) {
        this.seatId = seatId;
    }

    public String getCarId() {
        return carId;
    }

    public void setCarId(String carId) {
        this.carId = carId;
    }

    public int getBrandId() {
        return brandId;
    }

    public void setBrandId(int brandId) {
        this.brandId = brandId;
    }

    public int getModelId() {
        return modelId;
    }

    public void setModelId(int modelId) {
        this.modelId = modelId;
    }

    public int getVehicleTypeId() {
        return vehicleTypeId;
    }

    public void setVehicleTypeId(int vehicleTypeId) {
        this.vehicleTypeId = vehicleTypeId;
    }

    public int getEngineTypeId() {
        return engineTypeId;
    }

    public void setEngineTypeId(int engineTypeId) {
        this.engineTypeId = engineTypeId;
    }

    public int getTransmissionId() {
        return transmissionId;
    }

    public void setTransmissionId(int transmissionId) {
        this.transmissionId = transmissionId;
    }



    public String getYear() {
        return year;
    }

    public void setYear(String year) {
        this.year = year;
    }

    public String getCurrency() {
        return currency;
    }

    public void setCurrency(String currency) {
        this.currency = currency;
    }

    public String getPricePerDay() {
        return pricePerDay;
    }

    public void setPricePerDay(String pricePerDay) {
        this.pricePerDay = pricePerDay;
    }

    public String getTotalPrice() {
        return totalPrice;
    }

    public void setTotalPrice(String totalPrice) {
        this.totalPrice = totalPrice;
    }

    public String getSecurityAmount() {
        return securityAmount;
    }

    public void setSecurityAmount(String securityAmount) {
        this.securityAmount = securityAmount;
    }

    public int getAc() {
        return ac;
    }

    public void setAc(int ac) {
        this.ac = ac;
    }

    public int getGps() {
        return gps;
    }

    public void setGps(int gps) {
        this.gps = gps;
    }

    public int getIpodInterface() {
        return ipodInterface;
    }

    public void setIpodInterface(int ipodInterface) {
        this.ipodInterface = ipodInterface;
    }

    public int getSunroof() {
        return sunroof;
    }

    public void setSunroof(int sunroof) {
        this.sunroof = sunroof;
    }

    public int getChildSeat() {
        return childSeat;
    }

    public void setChildSeat(int childSeat) {
        this.childSeat = childSeat;
    }

    public int getElectricWindows() {
        return electricWindows;
    }

    public void setElectricWindows(int electricWindows) {
        this.electricWindows = electricWindows;
    }

    public int getHeatedSeat() {
        return heatedSeat;
    }

    public void setHeatedSeat(int heatedSeat) {
        this.heatedSeat = heatedSeat;
    }

    public int getPanormaRoof() {
        return panormaRoof;
    }

    public void setPanormaRoof(int panormaRoof) {
        this.panormaRoof = panormaRoof;
    }

    public int getPrmGauge() {
        return prmGauge;
    }

    public void setPrmGauge(int prmGauge) {
        this.prmGauge = prmGauge;
    }

    public int getAbs() {
        return abs;
    }

    public void setAbs(int abs) {
        this.abs = abs;
    }

    public int getTractionControl() {
        return tractionControl;
    }

    public void setTractionControl(int tractionControl) {
        this.tractionControl = tractionControl;
    }

    public int getAudioSystem() {
        return audioSystem;
    }

    public void setAudioSystem(int audioSystem) {
        this.audioSystem = audioSystem;
    }

    public int getColorId() {
        return colorId;
    }

    public void setColorId(int colorId) {
        this.colorId = colorId;
    }



}
