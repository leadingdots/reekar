package com.reekar.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class LoginModel {

@SerializedName("message")
@Expose
private String message;
@SerializedName("access_code")
@Expose
private String accessCode;

    @SerializedName("errors")
    @Expose
    private List<ErrorModel> errors = null;


public String getMessage() {
return message;
}

public void setMessage(String message) {
this.message = message;
}

public String getAccessCode() {
return accessCode;
}

public void setAccessCode(String accessCode) {
this.accessCode = accessCode;
}

}