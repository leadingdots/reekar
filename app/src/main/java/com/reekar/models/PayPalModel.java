package com.reekar.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class PayPalModel {

    @SerializedName("mClientMetadataId")
    @Expose
    private String mClientMetadataId;
    @SerializedName("mEmail")
    @Expose
    private String mEmail;
    @SerializedName("mFirstName")
    @Expose
    private String mFirstName;
    @SerializedName("mLastName")
    @Expose
    private String mLastName;
    @SerializedName("mPayerId")
    @Expose
    private String mPayerId;
    @SerializedName("mPhone")
    @Expose
    private String mPhone;

    @SerializedName("mDefault")
    @Expose
    private Boolean mDefault;
    @SerializedName("mDescription")
    @Expose
    private String mDescription;
    @SerializedName("mNonce")
    @Expose
    private String mNonce;

    public String getmClientMetadataId() {
        return mClientMetadataId;
    }

    public void setmClientMetadataId(String mClientMetadataId) {
        this.mClientMetadataId = mClientMetadataId;
    }

    public String getmEmail() {
        return mEmail;
    }

    public void setmEmail(String mEmail) {
        this.mEmail = mEmail;
    }

    public String getmFirstName() {
        return mFirstName;
    }

    public void setmFirstName(String mFirstName) {
        this.mFirstName = mFirstName;
    }

    public String getmLastName() {
        return mLastName;
    }

    public void setmLastName(String mLastName) {
        this.mLastName = mLastName;
    }

    public String getmPayerId() {
        return mPayerId;
    }

    public void setmPayerId(String mPayerId) {
        this.mPayerId = mPayerId;
    }

    public String getmPhone() {
        return mPhone;
    }

    public void setmPhone(String mPhone) {
        this.mPhone = mPhone;
    }

    public Boolean getmDefault() {
        return mDefault;
    }

    public void setmDefault(Boolean mDefault) {
        this.mDefault = mDefault;
    }

    public String getmDescription() {
        return mDescription;
    }

    public void setmDescription(String mDescription) {
        this.mDescription = mDescription;
    }

    public String getmNonce() {
        return mNonce;
    }

    public void setmNonce(String mNonce) {
        this.mNonce = mNonce;
    }
}
