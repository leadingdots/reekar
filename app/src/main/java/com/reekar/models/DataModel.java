package com.reekar.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Author:    Akanksha Paul
 * Created:   22/1/20
 **/
public class DataModel {
    @SerializedName("id")
    @Expose
    private int id;
    @SerializedName("name")
    @Expose
    private String name;
    @SerializedName("models")
    @Expose
    private List<CarModel> models = null;

    private boolean check;
    @SerializedName("car_id")
    @Expose
    private int carId;
    @SerializedName("booking_id")
    @Expose
    private String bookingId;
    @SerializedName("booking_from_date")
    @Expose
    private String bookingFromDate;
    @SerializedName("booking_to_date")
    @Expose
    private String bookingToDate;
    @SerializedName("grand_total")
    @Expose
    private Double grandTotal;
    @SerializedName("payment_method")
    @Expose
    private String paymentMethod;
    @SerializedName("updated_at")
    @Expose
    private String updatedAt;
    @SerializedName("brand")
    @Expose
    private String brand;
    @SerializedName("model")
    @Expose
    private String model;
    @SerializedName("vehicletype")
    @Expose
    private String vehicletype;
    @SerializedName("image")
    @Expose
    private String image;
    @SerializedName("enginetype")
    @Expose
    private String engineType;
    @SerializedName("brand_id")
    @Expose
    private Integer brandId;

    @SerializedName("billing_first_name")
    @Expose
    private String billing_first_name;

    @SerializedName("billing_last_name")
    @Expose
    private String billing_last_name;

    /*-- my booked car --*/

    public String getBilling_first_name() {
        return billing_first_name;
    }

    public void setBilling_first_name(String billing_first_name) {
        this.billing_first_name = billing_first_name;
    }

    public String getBilling_last_name() {
        return billing_last_name;
    }

    public void setBilling_last_name(String billing_last_name) {
        this.billing_last_name = billing_last_name;
    }

    public Integer getBrandId() {
        return brandId;
    }

    public void setBrandId(Integer brandId) {
        this.brandId = brandId;
    }

    public String getEngineType() {
        return engineType;
    }

    public void setEngineType(String engineType) {
        this.engineType = engineType;
    }

    public int getCarId() {
        return carId;
    }

    public void setCarId(int carId) {
        this.carId = carId;
    }

    public String getBookingId() {
        return bookingId;
    }

    public void setBookingId(String bookingId) {
        this.bookingId = bookingId;
    }

    public String getBookingFromDate() {
        return bookingFromDate;
    }

    public void setBookingFromDate(String bookingFromDate) {
        this.bookingFromDate = bookingFromDate;
    }

    public String getBookingToDate() {
        return bookingToDate;
    }

    public void setBookingToDate(String bookingToDate) {
        this.bookingToDate = bookingToDate;
    }

    public Double getGrandTotal() {
        return grandTotal;
    }

    public void setGrandTotal(Double grandTotal) {
        this.grandTotal = grandTotal;
    }

    public String getPaymentMethod() {
        return paymentMethod;
    }

    public void setPaymentMethod(String paymentMethod) {
        this.paymentMethod = paymentMethod;
    }

    public String getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(String updatedAt) {
        this.updatedAt = updatedAt;
    }

    public String getBrand() {
        return brand;
    }

    public void setBrand(String brand) {
        this.brand = brand;
    }

    public String getModel() {
        return model;
    }

    public void setModel(String model) {
        this.model = model;
    }

    public String getVehicletype() {
        return vehicletype;
    }

    public void setVehicletype(String vehicletype) {
        this.vehicletype = vehicletype;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public boolean isCheck() {
        return check;
    }

    public void setCheck(boolean check) {
        this.check = check;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<CarModel> getModels() {
        return models;
    }

    public void setModels(List<CarModel> models) {
        this.models = models;
    }
}
