package com.reekar.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class ApiResponse {

    @SerializedName("message")
    @Expose
    private String message;
    @SerializedName("access_code")
    @Expose
    private String accessCode;
    @SerializedName("first_name")
    @Expose
    private String firstNaame;
    @SerializedName("last_name")
    @Expose
    private String lastName;
    @SerializedName("user_image")
    @Expose
    private String userImage;
    @SerializedName("content")
    @Expose
    private String content;

    @SerializedName("user_currency")
    @Expose
    private String userCurrency;

    @SerializedName("geoplugin_currencySymbol")
    @Expose
    private String userCurrencySymbol;

    //error
    @SerializedName("error")
    @Expose
    private ErrorModel error;
    @SerializedName("filter")
    @Expose
    private List<FilterModel> filter;
    @SerializedName("cars")
    @Expose
    private List<CarModel> cars = null;
    @SerializedName("current_page")
    @Expose
    private int currentPage;
    @SerializedName("total_page")
    @Expose
    private int totalPage;
    @SerializedName("car")
    @Expose
    private CarModel car;
    @SerializedName("order")
    @Expose
    private OrderModel order;

    /*-- profile ----*/
    @SerializedName("profile")
    @Expose
    private ProfileModel profile;
    @SerializedName("user_image_base_url")
    @Expose
    private String userImageBaseUrl;

    @SerializedName("image")
    @Expose
    private String image;

    @SerializedName("car_id")
    @Expose
    private String carId;
    @SerializedName("data")
    @Expose
    private List<DataModel> data = null;

    /*---- save order response  ---*/
    @SerializedName("booking_id")
    @Expose
    private String bookingId;

    @SerializedName("order_id")
    @Expose
    private String orderId;

    @SerializedName("grand_total_usd")
    @Expose
    private double grandTotalUsd;

    @SerializedName("cs_key")
    @Expose
    private String csStripeKey;

    @SerializedName("cp_key")
    @Expose
    private String publishableKey;


    @SerializedName("result")
    @Expose
    private CsKey result;

    @SerializedName("has_mobile")
    @Expose
    private int hasMobile;

    @SerializedName("contact")
    @Expose
    private Contact contact;


    public String getUserCurrencySymbol() {
        return userCurrencySymbol;
    }

    public void setUserCurrencySymbol(String userCurrencySymbol) {
        this.userCurrencySymbol = userCurrencySymbol;
    }

    public Contact getContact() {
        return contact;
    }

    public void setContact(Contact contact) {
        this.contact = contact;
    }

    public int getHasMobile() {
        return hasMobile;
    }

    public void setHasMobile(int hasMobile) {
        this.hasMobile = hasMobile;
    }

    public CsKey getResult() {
        return result;
    }

    public void setResult(CsKey result) {
        this.result = result;
    }

    public String getCsStripeKey() {
        return csStripeKey;
    }

    public void setCsStripeKey(String csStripeKey) {
        this.csStripeKey = csStripeKey;
    }

    public String getPublishableKey() {
        return publishableKey;
    }

    public void setPublishableKey(String publishableKey) {
        this.publishableKey = publishableKey;
    }

    public double getGrandTotalUsd() {
        return grandTotalUsd;
    }

    public void setGrandTotalUsd(double grandTotalUsd) {
        this.grandTotalUsd = grandTotalUsd;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getOrderId() {
        return orderId;
    }

    public void setOrderId(String orderId) {
        this.orderId = orderId;
    }

    public String getBookingId() {
        return bookingId;
    }

    public void setBookingId(String bookingId) {
        this.bookingId = bookingId;
    }

    public OrderModel getOrder() {
        return order;
    }

    public void setOrder(OrderModel order) {
        this.order = order;
    }

    public List<DataModel> getData() {
        return data;
    }

    public void setData(List<DataModel> data) {
        this.data = data;
    }

    public String getCarId() {
        return carId;
    }

    public void setCarId(String carId) {
        this.carId = carId;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public ProfileModel getProfile() {
        return profile;
    }

    public void setProfile(ProfileModel profile) {
        this.profile = profile;
    }

    public String getUserImageBaseUrl() {
        return userImageBaseUrl;
    }

    public void setUserImageBaseUrl(String userImageBaseUrl) {
        this.userImageBaseUrl = userImageBaseUrl;
    }


    public String getUserCurrency() {
        return userCurrency;
    }

    public void setUserCurrency(String userCurrency) {
        this.userCurrency = userCurrency;
    }

    public CarModel getCar() {
        return car;
    }

    public void setCar(CarModel car) {
        this.car = car;
    }

    public List<CarModel> getCars() {
        return cars;
    }

    public void setCars(List<CarModel> cars) {
        this.cars = cars;
    }

    public int getCurrentPage() {
        return currentPage;
    }

    public void setCurrentPage(int currentPage) {
        this.currentPage = currentPage;
    }

    public int getTotalPage() {
        return totalPage;
    }

    public void setTotalPage(int totalPage) {
        this.totalPage = totalPage;
    }

    public List<FilterModel> getFilter() {
        return filter;
    }

    public void setFilter(List<FilterModel> filter) {
        this.filter = filter;
    }

    public String getFirstNaame() {
        return firstNaame;
    }

    public void setFirstNaame(String firstNaame) {
        this.firstNaame = firstNaame;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getUserImage() {
        return userImage;
    }

    public void setUserImage(String userImage) {
        this.userImage = userImage;
    }

    public ErrorModel getError() {
        return error;
    }

    public void setError(ErrorModel error) {
        this.error = error;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getAccessCode() {
        return accessCode;
    }

    public void setAccessCode(String accessCode) {
        this.accessCode = accessCode;
    }


}
