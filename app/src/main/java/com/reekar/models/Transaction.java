package com.reekar.models;

import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Transaction {

    @SerializedName("id")
    @Expose
    private String id;
    @SerializedName("status")
    @Expose
    private String status;
    @SerializedName("type")
    @Expose
    private String type;
    @SerializedName("currencyIsoCode")
    @Expose
    private String currencyIsoCode;
    @SerializedName("amount")
    @Expose
    private String amount;
    @SerializedName("merchantAccountId")
    @Expose
    private String merchantAccountId;
    @SerializedName("subMerchantAccountId")
    @Expose
    private Object subMerchantAccountId;
    @SerializedName("masterMerchantAccountId")
    @Expose
    private Object masterMerchantAccountId;
    @SerializedName("orderId")
    @Expose
    private Object orderId;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getCurrencyIsoCode() {
        return currencyIsoCode;
    }

    public void setCurrencyIsoCode(String currencyIsoCode) {
        this.currencyIsoCode = currencyIsoCode;
    }

    public String getAmount() {
        return amount;
    }

    public void setAmount(String amount) {
        this.amount = amount;
    }

    public String getMerchantAccountId() {
        return merchantAccountId;
    }

    public void setMerchantAccountId(String merchantAccountId) {
        this.merchantAccountId = merchantAccountId;
    }

    public Object getSubMerchantAccountId() {
        return subMerchantAccountId;
    }

    public void setSubMerchantAccountId(Object subMerchantAccountId) {
        this.subMerchantAccountId = subMerchantAccountId;
    }

    public Object getMasterMerchantAccountId() {
        return masterMerchantAccountId;
    }

    public void setMasterMerchantAccountId(Object masterMerchantAccountId) {
        this.masterMerchantAccountId = masterMerchantAccountId;
    }

    public Object getOrderId() {
        return orderId;
    }

    public void setOrderId(Object orderId) {
        this.orderId = orderId;
    }
}
