package com.reekar.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Author:    Akanksha Paul
 * Created:   1/2/20
 **/
public class OrderModel {
    @SerializedName("id")
    @Expose
    private Integer id;
    @SerializedName("car_id")
    @Expose
    private Integer carId;
    @SerializedName("booking_id")
    @Expose
    private String bookingId;
    @SerializedName("booking_from_date")
    @Expose
    private String bookingFromDate;
    @SerializedName("booking_to_date")
    @Expose
    private String bookingToDate;
    @SerializedName("total_amount")
    @Expose
    private double totalAmount;
    @SerializedName("security_amount")
    @Expose
    private double securityAmount;
    @SerializedName("tax_amount")
    @Expose
    private double taxAmount;
    @SerializedName("grand_total")
    @Expose
    private double grandTotal;
    @SerializedName("payment_method")
    @Expose
    private String paymentMethod;
    @SerializedName("bank_reference_id")
    @Expose
    private String bankReferenceId;
    @SerializedName("transaction_id")
    @Expose
    private String transactionId;
    @SerializedName("billing_first_name")
    @Expose
    private String billingFirstName;
    @SerializedName("billing_last_name")
    @Expose
    private String billingLastName;
    @SerializedName("billing_email")
    @Expose
    private String billingEmail;
    @SerializedName("billing_mobile")
    @Expose
    private String billingMobile;
    @SerializedName("billing_alternate_mobile")
    @Expose
    private String billingAlternateMobile;
    @SerializedName("billing_address")
    @Expose
    private String billingAddress;
    @SerializedName("pickup_location")
    @Expose
    private String pickupLocation;
    @SerializedName("order_status")
    @Expose
    private Integer orderStatus;
    @SerializedName("updated_at")
    @Expose
    private String updatedAt;
    @SerializedName("brand")
    @Expose
    private String brand;
    @SerializedName("model")
    @Expose
    private String model;
    @SerializedName("color")
    @Expose
    private String color;
    @SerializedName("vehicletype")
    @Expose
    private String vehicletype;
    @SerializedName("enginetype")
    @Expose
    private String enginetype;
    @SerializedName("image")
    @Expose
    private String image;
    @SerializedName("owner_name")
    @Expose
    private String ownerName;
    @SerializedName("contact_no")
    @Expose
    private String contactNo;

    @SerializedName("invoice")
    @Expose
    private String invoice;

    public String getInvoice() {
        return invoice;
    }

    public void setInvoice(String invoice) {
        this.invoice = invoice;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getCarId() {
        return carId;
    }

    public void setCarId(Integer carId) {
        this.carId = carId;
    }

    public String getBookingId() {
        return bookingId;
    }

    public void setBookingId(String bookingId) {
        this.bookingId = bookingId;
    }

    public String getBookingFromDate() {
        return bookingFromDate;
    }

    public void setBookingFromDate(String bookingFromDate) {
        this.bookingFromDate = bookingFromDate;
    }

    public String getBookingToDate() {
        return bookingToDate;
    }

    public void setBookingToDate(String bookingToDate) {
        this.bookingToDate = bookingToDate;
    }

    public double getTotalAmount() {
        return totalAmount;
    }

    public void setTotalAmount(double totalAmount) {
        this.totalAmount = totalAmount;
    }

    public double getSecurityAmount() {
        return securityAmount;
    }

    public void setSecurityAmount(double securityAmount) {
        this.securityAmount = securityAmount;
    }

    public double getTaxAmount() {
        return taxAmount;
    }

    public void setTaxAmount(double taxAmount) {
        this.taxAmount = taxAmount;
    }

    public double getGrandTotal() {
        return grandTotal;
    }

    public void setGrandTotal(double grandTotal) {
        this.grandTotal = grandTotal;
    }

    public String getPaymentMethod() {
        return paymentMethod;
    }

    public void setPaymentMethod(String paymentMethod) {
        this.paymentMethod = paymentMethod;
    }

    public String getBankReferenceId() {
        return bankReferenceId;
    }

    public void setBankReferenceId(String bankReferenceId) {
        this.bankReferenceId = bankReferenceId;
    }

    public String getTransactionId() {
        return transactionId;
    }

    public void setTransactionId(String transactionId) {
        this.transactionId = transactionId;
    }

    public String getBillingFirstName() {
        return billingFirstName;
    }

    public void setBillingFirstName(String billingFirstName) {
        this.billingFirstName = billingFirstName;
    }

    public String getBillingLastName() {
        return billingLastName;
    }

    public void setBillingLastName(String billingLastName) {
        this.billingLastName = billingLastName;
    }

    public String getBillingEmail() {
        return billingEmail;
    }

    public void setBillingEmail(String billingEmail) {
        this.billingEmail = billingEmail;
    }

    public String getBillingMobile() {
        return billingMobile;
    }

    public void setBillingMobile(String billingMobile) {
        this.billingMobile = billingMobile;
    }

    public String getBillingAlternateMobile() {
        return billingAlternateMobile;
    }

    public void setBillingAlternateMobile(String billingAlternateMobile) {
        this.billingAlternateMobile = billingAlternateMobile;
    }

    public String getBillingAddress() {
        return billingAddress;
    }

    public void setBillingAddress(String billingAddress) {
        this.billingAddress = billingAddress;
    }

    public String getPickupLocation() {
        return pickupLocation;
    }

    public void setPickupLocation(String pickupLocation) {
        this.pickupLocation = pickupLocation;
    }

    public Integer getOrderStatus() {
        return orderStatus;
    }

    public void setOrderStatus(Integer orderStatus) {
        this.orderStatus = orderStatus;
    }

    public String getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(String updatedAt) {
        this.updatedAt = updatedAt;
    }

    public String getBrand() {
        return brand;
    }

    public void setBrand(String brand) {
        this.brand = brand;
    }

    public String getModel() {
        return model;
    }

    public void setModel(String model) {
        this.model = model;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public String getVehicletype() {
        return vehicletype;
    }

    public void setVehicletype(String vehicletype) {
        this.vehicletype = vehicletype;
    }

    public String getEnginetype() {
        return enginetype;
    }

    public void setEnginetype(String enginetype) {
        this.enginetype = enginetype;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getOwnerName() {
        return ownerName;
    }

    public void setOwnerName(String ownerName) {
        this.ownerName = ownerName;
    }

    public String getContactNo() {
        return contactNo;
    }

    public void setContactNo(String contactNo) {
        this.contactNo = contactNo;
    }
}
