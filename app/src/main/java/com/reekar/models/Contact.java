package com.reekar.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Contact {


    //contact us page
    @SerializedName("company_name")
    @Expose
    private String companyName;
    @SerializedName("logo")
    @Expose
    private Object logo;
    @SerializedName("fav_icon")
    @Expose
    private Object favIcon;
    @SerializedName("contact_person")
    @Expose
    private String contactPerson;
    @SerializedName("country_code")
    @Expose
    private String countryCode;
    @SerializedName("contact_no")
    @Expose
    private String contactNo;
    @SerializedName("alternate_contact_no")
    @Expose
    private String alternateContactNo;
    @SerializedName("email")
    @Expose
    private String email;
    @SerializedName("company_full_address")
    @Expose
    private String companyFullAddress;
    //-------------------------------


    public String getCompanyName() {
        return companyName;
    }

    public void setCompanyName(String companyName) {
        this.companyName = companyName;
    }

    public Object getLogo() {
        return logo;
    }

    public void setLogo(Object logo) {
        this.logo = logo;
    }

    public Object getFavIcon() {
        return favIcon;
    }

    public void setFavIcon(Object favIcon) {
        this.favIcon = favIcon;
    }

    public String getContactPerson() {
        return contactPerson;
    }

    public void setContactPerson(String contactPerson) {
        this.contactPerson = contactPerson;
    }

    public String getCountryCode() {
        return countryCode;
    }

    public void setCountryCode(String countryCode) {
        this.countryCode = countryCode;
    }

    public String getContactNo() {
        return contactNo;
    }

    public void setContactNo(String contactNo) {
        this.contactNo = contactNo;
    }

    public String getAlternateContactNo() {
        return alternateContactNo;
    }

    public void setAlternateContactNo(String alternateContactNo) {
        this.alternateContactNo = alternateContactNo;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getCompanyFullAddress() {
        return companyFullAddress;
    }

    public void setCompanyFullAddress(String companyFullAddress) {
        this.companyFullAddress = companyFullAddress;
    }
}
