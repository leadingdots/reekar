package com.reekar.interfaces;


import com.reekar.models.ApiResponse;

import retrofit2.Response;


public interface ApiResponseInterface {
    void onResponse(Response<ApiResponse> response);
    void onFailure();


}