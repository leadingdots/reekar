package com.reekar.interfaces;

/**
 * Author:    Akanksha Paul
 * Created:   22/1/20
 **/
public interface OnSortItemClick {
    void onItemClick(String data);
}
