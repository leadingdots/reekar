package com.reekar.interfaces;

import com.reekar.models.FilterModel;

import java.util.ArrayList;

/**
 * Author:    Akanksha Paul
 * Created:   22/1/20
 **/
public interface OnFilterClick {
    void onItemClick(ArrayList<FilterModel> data);
}
