package com.reekar.interfaces;

import com.google.android.libraries.places.api.model.Place;

/**
 * Author:    Akanksha Paul
 * Created:   22/1/20
 **/
public interface OnPlaceItemClick {
    void onItemClick(Place place);
}
