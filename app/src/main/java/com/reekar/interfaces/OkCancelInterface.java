package com.reekar.interfaces;

public interface OkCancelInterface {
    void onOkClick();
    void onCancelClick();
}
