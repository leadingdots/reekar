package com.reekar.interfaces;

public interface RecyclerviewItemClickInterface {
    void onItemClick(int position);
}
