package com.reekar.interfaces;

import android.location.Location;

/**
 * Author:    Akanksha Paul
 * Created:   22/1/20
 **/
public interface OnLocationChangeInterface {
    void onLocationChanged(Location location);
}
