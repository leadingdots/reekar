package com.reekar.fcm;

import android.app.Activity;
import android.app.NotificationManager;
import android.content.Context;
import android.content.Intent;

import com.google.gson.Gson;
import com.reekar.activity.MainActivity;

class HandleClickNotification {
    private static final String BROADCAST_ACTION = "com.permaconn.notification_broadcast";
    static final int BROADCAST_ID = 1;
    private Intent intent;
    private Activity currentActivity;

    Intent sendData(FcmModel model, Context applicationContext, NotificationManager notificationManager, int id) {

        intent = new Intent(applicationContext, MainActivity.class);
        if (intent != null) {
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK|Intent.FLAG_ACTIVITY_SINGLE_TOP);
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
//            intent.putExtra(AppConstant.NOTIFICATION, new Gson().toJson(model));
            intent.setAction(Long.toString(System.currentTimeMillis()));
        }

        return intent;
    }

    private void sendBroadcast(Context applicationContext, FcmModel model, NotificationManager notificationManager, int id) {
//        LogUtils.errorLog("call screen", "broadcast");
        Intent broadcastIntent = new Intent(BROADCAST_ACTION);

        //put whatever data you want to send, if any
//        broadcastIntent.putExtra("notification", new Gson().toJson(model));

        //send broadcast
        applicationContext.sendBroadcast(broadcastIntent);

        if (notificationManager != null)
            cancelNotification(notificationManager,id);
    }

    private static void cancelNotification(NotificationManager notificationManager, int id){
        if (notificationManager != null)
            notificationManager.cancel(id);
    }
}
