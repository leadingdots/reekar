package com.reekar.fcm;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

import com.akanksha.commonclassutil.LogUtil;

public class NotificationBroadcast extends BroadcastReceiver {
    private static  final String TAG = "NotificationBroadcast";
    @Override
    public void onReceive(Context context, Intent intent) {
        try
        {
                String data = intent.getStringExtra("data"); // data is a key specified to intent while sending broadcast
                LogUtil.Companion.errorLog(TAG, "data=="+data);

        }
        catch (Exception ex)
        {
            ex.printStackTrace();
        }
    }
}
