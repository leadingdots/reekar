package com.reekar.fcm;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.TaskStackBuilder;
import android.content.Context;
import android.content.Intent;
import android.os.Build;

import androidx.annotation.NonNull;
import androidx.core.app.NotificationCompat;

import com.akanksha.commonclassutil.LogUtil;
import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;
import com.google.gson.Gson;
import com.reekar.R;
import com.reekar.activity.MainActivity;

import java.util.Random;


public class MyFirebaseMessagingService extends FirebaseMessagingService {

    private static final String TAG = MyFirebaseMessagingService.class.getSimpleName();
    private PendingIntent notifyPendingIntent;
    private FcmModel model= new FcmModel();
    private int id;
    private NotificationManager notificationManager;
    private String body;
    private String title;

   /* @Override
    public void onNewToken(@NonNull String s) {
        super.onNewToken(s);
    }*/

    @Override
    public void onMessageReceived(@NonNull RemoteMessage remoteMessage) {
        super.onMessageReceived(remoteMessage);

        id++;
        Random rand = new Random();
        id = rand.nextInt(2000);
        LogUtil.Companion.errorLog(TAG, "From: " + remoteMessage.getFrom());
        LogUtil.Companion.errorLog(TAG, "id: " + id);
        LogUtil.Companion.errorLog(TAG, "msg: " + new Gson().toJson(remoteMessage.getData()));
        LogUtil.Companion.errorLog(TAG, "msg: " + new Gson().toJson(remoteMessage.getNotification()));
        LogUtil.Companion.errorLog(TAG, "msg: " + new Gson().toJson(remoteMessage));

        if (remoteMessage.getNotification() == null)
            return;

        body = remoteMessage.getNotification().getBody();
        title = remoteMessage.getNotification().getTitle();

        handleNotificationClick(remoteMessage);


        LogUtil.Companion.errorLog(TAG,"notification sent");
        LogUtil.Companion.errorLog(TAG,new Gson().toJson(remoteMessage.getNotification()));
        LogUtil.Companion.errorLog(TAG,"getClickAction "+new Gson().toJson(remoteMessage.getNotification().getClickAction()));
        LogUtil.Companion.errorLog(TAG,new Gson().toJson(remoteMessage.getData()));

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            sendOreoNotification(remoteMessage);
        } else {
            sendNotification(remoteMessage);
        }
    }

    /**
     * This method is used to handle notification click
     * @param remoteMessage fcm message
     */
    private void handleNotificationClick(RemoteMessage remoteMessage) {
        Intent intent = new Intent(this, MainActivity.class);
        if (intent != null) {
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK|Intent.FLAG_ACTIVITY_SINGLE_TOP);
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            intent.setAction(Intent.ACTION_MAIN);
            intent.addCategory(Intent.CATEGORY_LAUNCHER);
        }
//        HandleClickNotification handleClickNotification = new HandleClickNotification();
//        Intent notifyIntent = handleClickNotification.sendData(model, getApplicationContext(),notificationManager,id);
        if (intent != null) {
            notifyPendingIntent =
                    PendingIntent.getActivity(this, id, intent,
                            PendingIntent.FLAG_CANCEL_CURRENT|PendingIntent.FLAG_UPDATE_CURRENT|
                                    PendingIntent.FLAG_ONE_SHOT);



        }

/*

        TaskStackBuilder stackBuilder = TaskStackBuilder.create(this);
        stackBuilder.addNextIntentWithParentStack(intent);
// Get the PendingIntent containing the entire back stack
        notifyPendingIntent =
                stackBuilder.getPendingIntent(id, PendingIntent.FLAG_UPDATE_CURRENT);
        LogUtil.Companion.errorLog(TAG,"intent sent");
*/

    }

    /**
     * This method is used to send notification below API level 26 (Android 8)
     * @param remoteMessage fcm message
     */
    private void sendNotification(RemoteMessage remoteMessage) {
//        String channelId = getString(R.string.default_notification_channel_id);
        String channelId = "General";

        // send message when app is in foreground
        notificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);

        NotificationCompat.Builder notificationBuilder = new NotificationCompat.Builder(this,channelId);

           /* NotificationCompat.BigTextStyle bigTextStyle = new NotificationCompat.BigTextStyle();
            bigTextStyle.setBigContentTitle(title);
            bigTextStyle.bigText(body);

            notificationBuilder.setStyle(bigTextStyle);*/
        notificationBuilder
//                    .setDefaults(Notification.DEFAULT_VIBRATE|Notification.DEFAULT_LIGHTS)
                .setVibrate(new long[]{100, 200/*, 300, 400, 500, 400*/, 300, 200, 400})
                .setWhen(System.currentTimeMillis())
                .setSmallIcon(R.mipmap.ic_launcher)
                .setPriority(NotificationManager.IMPORTANCE_DEFAULT)
                .setBadgeIconType(NotificationCompat.BADGE_ICON_SMALL)
                .setContentIntent(notifyPendingIntent)
                .setContentTitle(title)
                .setContentText(body)
                .setAutoCancel(true);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            notificationBuilder.setSmallIcon(R.mipmap.ic_launcher);
            notificationBuilder.setColor(getResources().getColor(R.color.colorPrimary));
        } else {
            notificationBuilder.setSmallIcon(R.mipmap.ic_launcher);
        }
        notificationBuilder.setStyle(new NotificationCompat.BigTextStyle().bigText(body));

//            if (notificationManager != null)
        notificationManager.notify(id, notificationBuilder.build());

    }


    /**
     * This method is used to send notification from and above API level 26 (Android 8)
     * @param remoteMessage fcm message
     */
    private void sendOreoNotification(RemoteMessage remoteMessage) {
        // send message when app is in foreground
        OreoNotification oreoNotification;
        oreoNotification = new OreoNotification(this);
        Notification.Builder builder = oreoNotification.getOreoNotification(title, body, notifyPendingIntent);

        notificationManager = oreoNotification.getManager();
        int i = id;
        oreoNotification.getManager().notify(i, builder.build());

    }
}