package com.reekar.location

import android.app.Activity
import android.app.AlertDialog
import android.app.Service
import android.content.Context
import android.content.Intent
import android.location.Location
import android.location.LocationManager
import android.os.Bundle
import android.os.IBinder
import android.provider.Settings
import com.google.android.gms.common.ConnectionResult
import com.google.android.gms.common.api.GoogleApiClient
import com.google.android.gms.location.LocationListener
import com.google.android.gms.location.LocationRequest
import com.google.android.gms.location.LocationServices
import com.reekar.R
import com.reekar.commonclasses.AppConstant
import com.reekar.commonclasses.BaseFragment
import com.reekar.interfaces.OnLocationChangeInterface


open class GPSTrackerNew(
        private val mContext: Context,
        private val onLocationChangeInterface: OnLocationChangeInterface
) : Service(), LocationListener,
        GoogleApiClient.ConnectionCallbacks,
        GoogleApiClient.OnConnectionFailedListener {

    // flag for GPS statu
    private var isGPSEnabled = false

    // flag for network status
    private var isNetworkEnabled = false

    // flag for GPS status
    private var canGetLocation = false

    private var location: Location? = null // location
    private var latitude: Double = 0.toDouble() // latitude
    private var longitude: Double = 0.toDouble() // longitude

    private val mLocationRequest = LocationRequest()
    private var mGoogleApiClient : GoogleApiClient? = null
    // Declaring a Location Manager
    private var locationManager: LocationManager? = null

    init {
        getLocation()
    }

    private fun getLocation(): Location? {
        try {
            locationManager = mContext.getSystemService(Context.LOCATION_SERVICE) as LocationManager

            // getting GPS status
            isGPSEnabled = locationManager!!.isProviderEnabled(LocationManager.GPS_PROVIDER)

            // getting network status
            isNetworkEnabled = locationManager!!
                    .isProviderEnabled(LocationManager.NETWORK_PROVIDER)

            try {
                BaseFragment.isGpsEnabled = true
            }
            catch (e:Exception ){
                e.printStackTrace()
            }
//            if (!isGPSEnabled) {
                if (!isGPSEnabled){

                    try {
                        BaseFragment.isGpsEnabled = false
                    }
                    catch (e:Exception ){
                        e.printStackTrace()
                    }
                    showSettingsAlert()


                }
                // no network provider is enabled
//            }
        else {
                this.canGetLocation = true


                createLocationRequest()

                mGoogleApiClient = GoogleApiClient.Builder(mContext)
                        .addApi(LocationServices.API)
                        .addConnectionCallbacks(this)
                        .addOnConnectionFailedListener(this)
                        .build()
                mGoogleApiClient?.connect()
            }

        } catch (e: Exception) {
            e.printStackTrace()
        }
        val broadcastIntent = Intent(BROADCAST_ACTION)
        broadcastIntent.putExtra("isGPSEnabled", isGPSEnabled)
        mContext.sendBroadcast(broadcastIntent)
        return location
    }

    private fun createLocationRequest() {
        mLocationRequest.interval =
                INTERVAL
        mLocationRequest.fastestInterval =
                FASTEST_INTERVAL
        mLocationRequest.smallestDisplacement =
                SMALLEST_DISPLACEMENT
        mLocationRequest.priority = LocationRequest.PRIORITY_HIGH_ACCURACY
    }

    /**
     * Stop using GPS listener
     * Calling this function will stop using GPS in your app
     */

    fun stopUsingGPS() {
        if (mGoogleApiClient != null) {
            if (mGoogleApiClient?.isConnected!!)
                LocationServices.FusedLocationApi.removeLocationUpdates(mGoogleApiClient,this)
        }
    }

    /**
     * Function to get latitude
     */

    fun getLatitude(): Double {
        if (location != null) {
            latitude = location!!.latitude
        }

        // return latitude
        return latitude
    }

    /**
     * Function to get longitude
     */

    fun getLongitude(): Double {
        if (location != null) {
            longitude = location!!.longitude
        }

        // return longitude
        return longitude
    }

    /**
     * Function to check GPS/wifi enabled
     * @return boolean
     */

    fun canGetLocation(): Boolean {
        return this.canGetLocation
    }

    /**
     * Function to show settings alert dialog
     * On pressing Settings button will lauch Settings Options
     */

    private fun showSettingsAlert() {
        val alertDialog = AlertDialog.Builder(mContext, R.style.AlertDialogStyleRed)

        // Setting Dialog Title
        alertDialog.setTitle("GPS not enabled")

        // Setting Dialog Message
        alertDialog.setMessage("GPS is not enabled. Do you want to go to settings menu?")

        // On pressing Settings button
        alertDialog.setPositiveButton("Settings") { dialog, which ->
            val intent = Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS)
            //mContext.startActivity(intent)

            (mContext as Activity).startActivityForResult(intent, AppConstant.REQUEST_CODE_GPS)
        }

        // on pressing cancel button
        alertDialog.setNegativeButton("Cancel") { dialog, which -> dialog.cancel() }

        // Showing Alert Message
        alertDialog.show()
    }

    override fun onLocationChanged(location: Location) {
//        LogUtil.errorLog("Location", "onLocationChanged")
        latitude = location.latitude
        longitude = location.longitude
        //        getLocation();
        onLocationChangeInterface.onLocationChanged(location)
    }

    /* override fun onProviderDisabled(provider: String) {}

     override fun onProviderEnabled(provider: String) {}

     override fun onStatusChanged(provider: String, status: Int, extras: Bundle) {}
 */
    override fun onBind(arg0: Intent): IBinder? {
        return null
    }

    companion object {

        const val INTERVAL:Long =  1000 * 5//10 sec
        const val FASTEST_INTERVAL:Long = 1000 * 2//2 sec
        const val SMALLEST_DISPLACEMENT =  0.01F
        const val BROADCAST_ACTION = "broadcast_gps"
    }

    override fun onConnected(p0: Bundle?) {
//        LogUtil.errorLog("onConnected","onConnected")
        startLocationUpdates()
    }

    private fun startLocationUpdates() {
        try {
            LocationServices.FusedLocationApi.requestLocationUpdates(mGoogleApiClient, mLocationRequest, this)
        } catch (e:SecurityException){
            e.printStackTrace()
        }
    }

    override fun onConnectionSuspended(p0: Int) {
//        LogUtil.errorLog("onConnectionSuspended","onConnectionSuspended")
    }

    override fun onConnectionFailed(p0: ConnectionResult) {
//        LogUtil.errorLog("onConnectionFailed","onConnectionFailed")
    }
}