package com.reekar.apiservices;

import com.reekar.models.ApiRequest;
import com.reekar.models.ApiResponse;
import com.reekar.models.SaveCarInfoApiRequest;

import java.util.Map;

import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.PartMap;

public interface APIInterface {
     @POST("auth/login")
    Call<ApiResponse> callLoginApi (@Body ApiRequest apiRequest);


    @POST("auth/register")
    Call<ApiResponse> callRegisterApi (@Body ApiRequest apiRequest);


    @POST("auth/check-email")
    Call<ApiResponse> callApiEmailCheck (@Body ApiRequest apiRequest);

    @POST("auth/verify-otp")
    Call<ApiResponse> callApiOtpVerify (@Body ApiRequest apiRequest);

    @POST("auth/resend-otp")
    Call<ApiResponse> callApiResendOtp (@Body ApiRequest apiRequest);

    @POST("auth/change-password")
    Call<ApiResponse> callApiChangePass (@Body ApiRequest apiRequest);

    @POST("auth/save-payment-method")
    Call<ApiResponse> callApiSavePaymentMethod (@Body ApiRequest apiRequest);

    @POST("auth/logout")
    Call<ApiResponse> callApiLogout(@Body ApiRequest apiRequest);

    @POST("auth/social-register")
    Call<ApiResponse> callApiSocialLogin(@Body ApiRequest apiRequest);

    @POST("auth/profile")
    Call<ApiResponse> callApiProfile(@Body ApiRequest apiRequest);

    @POST("auth/update-profile")
    @Multipart
    Call<ApiResponse> callApiUpdateProfile(@PartMap Map <String, RequestBody> map);

    /*----------------------------- Car list -----------------------------*/

    @POST("auth/get-master-data")
    Call<ApiResponse> callApiMasterData();

//    @POST("cars/term-and-service-and-policy")
//    Call<ApiResponse> callStaticContentApi (@Body ApiRequest apiRequest);

    @POST("/static/content")
    Call<ApiResponse> callStaticContentApi (@Body ApiRequest apiRequest);


    @POST("cars/search-cars")
    Call<ApiResponse> callSearchCarApi(@Body ApiRequest apiRequest);

    @POST("cars/car-details")
    Call<ApiResponse> callSearchCarDetailsApi(@Body ApiRequest apiRequest);

    /*----------------------------car listing-------------------------------*/
    @POST("auth/save-car-info")
    Call<ApiResponse> callSaveCarInfoApi(@Body SaveCarInfoApiRequest apiRequest);

    @POST("auth/upload-car-images")
    @Multipart
    Call<ApiResponse> callSaveCarPhotosApi(@PartMap Map <String, RequestBody> map);

    @POST("auth/upload-car-docs")
    @Multipart
    Call<ApiResponse> callSaveCarDocsApi(@PartMap Map <String, RequestBody> map);

    @POST("auth/save-car-location")
    Call<ApiResponse> callSaveCarLocationApi(@Body SaveCarInfoApiRequest apiRequest);

    @POST("auth/save-car-availability")
    Call<ApiResponse> callSaveCarAvailableApi(@Body SaveCarInfoApiRequest apiRequest);
    /*-- list a car complete --*/

    @POST("auth/list-cars")
    Call<ApiResponse> callListCarApi(@Body ApiRequest apiRequest);

    @POST("auth/car-details")
    Call<ApiResponse> callCarDetailsApi(@Body ApiRequest apiRequest);

    @POST("auth/delete-car")
    Call<ApiResponse> callCarDeleteApi(@Body ApiRequest apiRequest);

    @POST("orders/list-my-car-booking")
    Call<ApiResponse> callMyBookedCarApi();

    @POST("orders/details")
    Call<ApiResponse> callMyBookedCarDetailsApi();

    /*----------------------------car order-------------------------------*/
    @POST("orders/list")
    Call<ApiResponse> callOrderListApi(@Body ApiRequest apiRequest);

    @POST("orders/details")
    Call<ApiResponse> callOrderDetailApi(@Body ApiRequest apiRequest);

   /* ------------------------- Place order ------------------------------*/
   @POST("orders/upload-identity-doc")
   @Multipart
   Call<ApiResponse> callUploadIdApi(@PartMap Map <String, RequestBody> map);

   @POST("orders/save-order")
   Call<ApiResponse> callPlaceOrderApi(@Body ApiRequest apiRequest);

   @POST("orders/success-order")
   Call<ApiResponse> callSuccessOrderApi(@Body ApiRequest apiRequest);

   /*--------------------------Delete Docs & Delete images ----------------*/
   @POST("auth/delete-car-file")
   Call<ApiResponse> callDeleteDocsApi(@Body ApiRequest apiRequest);

   /*--------------------------Delete Docs & Delete images ----------------*/
   @POST("auth/delete-car-file")
   Call<ApiResponse> callPrivacyPolicyApi();
    /*--------------------------stripe payment ----------------*/
    @POST("auth/Payment-Intent")
    Call<ApiResponse> callStripePaymentApi(@Body ApiRequest apiRequest);

    @POST("auth/Braintree-Payment")
    Call<ApiResponse> callBraintreePaymentApi();

    @POST("auth/Braintree-Payment-Save")
    Call<ApiResponse> callBraintreePaypalPaymentApi(@Body ApiRequest apiRequest);

    @POST("auth/contact-us")
    Call<ApiResponse> callContactApi();

    @POST("cars/get-current-currency")
    Call<ApiResponse> callCurrencyApi(@Body ApiRequest apiRequest);
}