package com.reekar.apiservices;

import android.content.Context;
import android.util.Log;

import com.akanksha.commonclassutil.PreferencesManager;
import com.reekar.commonclasses.AppConstant;
import com.reekar.interfaces.ApiResponseInterface;
import com.reekar.models.ApiRequest;
import com.reekar.models.ApiResponse;
import com.reekar.models.SaveCarInfoApiRequest;

import java.util.Map;

import okhttp3.MediaType;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ApiUtils {

    public static void callApiLogin(final Context mContext, ApiRequest apiRequest, final ApiResponseInterface apiResponseInterface){

        Call<ApiResponse> call = APIClient.getClient(mContext).create(APIInterface.class).callLoginApi(apiRequest);
        call.enqueue(new Callback<ApiResponse>() {
            @Override
            public void onResponse(Call<ApiResponse> call, Response<ApiResponse> response) {
                if (response.code() == AppConstant.SUCCESS_CODE) {
                    if (response.body() != null) {
                        if (response.body().getAccessCode()!= null)
                            PreferencesManager.Companion.saveStringPreferences(mContext, AppConstant.ACCESS_CODE,response.body().getAccessCode());
                    }

                }
                apiResponseInterface.onResponse(response);
            }

            @Override
            public void onFailure(Call<ApiResponse> call, Throwable t) {
                apiResponseInterface.onFailure();
            }
        });
    }

    public static void callRegisterApi(final Context mContext, final ApiRequest apiRequest, final ApiResponseInterface apiResponseInterface){

        Call<ApiResponse> call = APIClient.getClient(mContext).create(APIInterface.class).callRegisterApi(apiRequest);
        call.enqueue(new Callback<ApiResponse>() {
            @Override
            public void onResponse(Call<ApiResponse> call, Response<ApiResponse> response) {

                apiResponseInterface.onResponse(response);
            }

            @Override
            public void onFailure(Call<ApiResponse> call, Throwable t) {
                apiResponseInterface.onFailure();
            }
        });
    }
    public static void callApiEmailCheck(final Context mContext,final ApiRequest apiRequest, final ApiResponseInterface apiResponseInterface){

        Call<ApiResponse> call = APIClient.getClient(mContext).create(APIInterface.class).callApiEmailCheck(apiRequest);
        call.enqueue(new Callback<ApiResponse>() {
            @Override
            public void onResponse(Call<ApiResponse> call, Response<ApiResponse> response) {
                apiResponseInterface.onResponse(response);
            }

            @Override
            public void onFailure(Call<ApiResponse> call, Throwable t) {
                apiResponseInterface.onFailure();
            }
        });
    }
    public static void callApiOtpVerify(final Context mContext,final ApiRequest apiRequest, final ApiResponseInterface apiResponseInterface){

        Call<ApiResponse> call = APIClient.getClient(mContext).create(APIInterface.class).callApiOtpVerify(apiRequest);
        call.enqueue(new Callback<ApiResponse>() {
            @Override
            public void onResponse(Call<ApiResponse> call, Response<ApiResponse> response) {
                apiResponseInterface.onResponse(response);
            }

            @Override
            public void onFailure(Call<ApiResponse> call, Throwable t) {
                apiResponseInterface.onFailure();
            }
        });
    }
    public static void callApiResendOtp(final Context mContext,final ApiRequest apiRequest, final ApiResponseInterface apiResponseInterface){

        Call<ApiResponse> call = APIClient.getClient(mContext).create(APIInterface.class).callApiResendOtp(apiRequest);
        call.enqueue(new Callback<ApiResponse>() {
            @Override
            public void onResponse(Call<ApiResponse> call, Response<ApiResponse> response) {
                apiResponseInterface.onResponse(response);
            }

            @Override
            public void onFailure(Call<ApiResponse> call, Throwable t) {
                apiResponseInterface.onFailure();
            }
        });
    }
    public static void callApiChangePass(final Context mContext,final ApiRequest apiRequest, final ApiResponseInterface apiResponseInterface){

        Call<ApiResponse> call = APIClient.getClient(mContext).create(APIInterface.class).callApiChangePass(apiRequest);
        call.enqueue(new Callback<ApiResponse>() {
            @Override
            public void onResponse(Call<ApiResponse> call, Response<ApiResponse> response) {
                apiResponseInterface.onResponse(response);
            }

            @Override
            public void onFailure(Call<ApiResponse> call, Throwable t) {
                apiResponseInterface.onFailure();
            }
        });
    }

    public static void callApiSavePaymentMethod(final Context mContext,final ApiRequest apiRequest, final ApiResponseInterface apiResponseInterface){

        Call<ApiResponse> call = APIClient.getClient(mContext).create(APIInterface.class).callApiSavePaymentMethod(apiRequest);
        call.enqueue(new Callback<ApiResponse>() {
            @Override
            public void onResponse(Call<ApiResponse> call, Response<ApiResponse> response) {
                apiResponseInterface.onResponse(response);
            }

            @Override
            public void onFailure(Call<ApiResponse> call, Throwable t) {
                apiResponseInterface.onFailure();
            }
        });
    }

    public static void callApiLogout(final Context mContext,final ApiRequest apiRequest, final ApiResponseInterface apiResponseInterface){

        Call<ApiResponse> call = APIClient.getClient(mContext).create(APIInterface.class).callApiLogout(apiRequest);
        call.enqueue(new Callback<ApiResponse>() {
            @Override
            public void onResponse(Call<ApiResponse> call, Response<ApiResponse> response) {
                apiResponseInterface.onResponse(response);
            }

            @Override
            public void onFailure(Call<ApiResponse> call, Throwable t) {
                apiResponseInterface.onFailure();
            }
        });
    }
    public static void callApiSocialLogin(final Context mContext,final ApiRequest apiRequest, final ApiResponseInterface apiResponseInterface){

        Call<ApiResponse> call = APIClient.getClient(mContext).create(APIInterface.class).callApiSocialLogin(apiRequest);
        call.enqueue(new Callback<ApiResponse>() {
            @Override
            public void onResponse(Call<ApiResponse> call, Response<ApiResponse> response) {
                apiResponseInterface.onResponse(response);
            }

            @Override
            public void onFailure(Call<ApiResponse> call, Throwable t) {
                apiResponseInterface.onFailure();
            }
        });
    }

    public static void callApiProfile(final Context mContext,final ApiRequest apiRequest, final ApiResponseInterface apiResponseInterface){

        Call<ApiResponse> call = APIClient.getClient(mContext).create(APIInterface.class).callApiProfile(apiRequest);
        call.enqueue(new Callback<ApiResponse>() {
            @Override
            public void onResponse(Call<ApiResponse> call, Response<ApiResponse> response) {
                apiResponseInterface.onResponse(response);
            }

            @Override
            public void onFailure(Call<ApiResponse> call, Throwable t) {
                apiResponseInterface.onFailure();
            }
        });
    }

    public static void callApiUpdateProfile(final Context mContext,final Map<String, RequestBody> apiRequest, final ApiResponseInterface apiResponseInterface){

        Call<ApiResponse> call = APIClient.getClient(mContext).create(APIInterface.class).callApiUpdateProfile(apiRequest);
        call.enqueue(new Callback<ApiResponse>() {
            @Override
            public void onResponse(Call<ApiResponse> call, Response<ApiResponse> response) {
                apiResponseInterface.onResponse(response);
            }

            @Override
            public void onFailure(Call<ApiResponse> call, Throwable t) {
                apiResponseInterface.onFailure();
            }
        });
    }

    public static RequestBody toRequestBody(String str){
        return  RequestBody.create(MediaType.parse("text/plain"), str);
    }
//    public static void callApiUpdateProfilePicture(final Context mContext, final Map<String, RequestBody> apiRequest, final ApiResponseInterface apiResponseInterface){
//
//        Call<ApiResponse> call = APIClient.getClient(mContext).create(APIInterface.class).callApiUpdateProfilePicture(apiRequest);
//        call.enqueue(new Callback<ApiResponse>() {
//            @Override
//            public void onResponse(Call<ApiResponse> call, Response<ApiResponse> response) {
//                apiResponseInterface.onResponse(response);
//            }
//
//            @Override
//            public void onFailure(Call<ApiResponse> call, Throwable t) {
//                apiResponseInterface.onFailure();
//            }
//        });
//    }


    /*----------------------------- Car list -----------------------------*/
    public static void callApiMasterData(final Context mContext, final ApiResponseInterface apiResponseInterface){

        Call<ApiResponse> call = APIClient.getClient(mContext).create(APIInterface.class).callApiMasterData();
        call.enqueue(new Callback<ApiResponse>() {
            @Override
            public void onResponse(Call<ApiResponse> call, Response<ApiResponse> response) {
                apiResponseInterface.onResponse(response);
            }

            @Override
            public void onFailure(Call<ApiResponse> call, Throwable t) {
                apiResponseInterface.onFailure();
            }
        });
    }
    public static void callSearchCarApi(final Context mContext,final ApiRequest apiRequest, final ApiResponseInterface apiResponseInterface){

        Call<ApiResponse> call = APIClient.getClient(mContext).create(APIInterface.class).callSearchCarApi(apiRequest);
        call.enqueue(new Callback<ApiResponse>() {
            @Override
            public void onResponse(Call<ApiResponse> call, Response<ApiResponse> response) {
                apiResponseInterface.onResponse(response);
            }

            @Override
            public void onFailure(Call<ApiResponse> call, Throwable t) {
                apiResponseInterface.onFailure();
            }
        });
    }
    public static void callSearchCarDetailsApi(final Context mContext, final ApiRequest apiRequest, final ApiResponseInterface apiResponseInterface){

        Call<ApiResponse> call = APIClient.getClient(mContext).create(APIInterface.class).callSearchCarDetailsApi(apiRequest);
        call.enqueue(new Callback<ApiResponse>() {
            @Override
            public void onResponse(Call<ApiResponse> call, Response<ApiResponse> response) {
                apiResponseInterface.onResponse(response);
            }

            @Override
            public void onFailure(Call<ApiResponse> call, Throwable t) {
                apiResponseInterface.onFailure();
            }
        });
    }
    /*-----------------------------Save Car Info -----------------------------*/
    public static void callSaveCarInfoApi(final Context mContext, final SaveCarInfoApiRequest apiRequest, final ApiResponseInterface apiResponseInterface){

        Call<ApiResponse> call = APIClient.getClient(mContext).create(APIInterface.class).callSaveCarInfoApi(apiRequest);
        call.enqueue(new Callback<ApiResponse>() {
            @Override
            public void onResponse(Call<ApiResponse> call, Response<ApiResponse> response) {
                apiResponseInterface.onResponse(response);
            }

            @Override
            public void onFailure(Call<ApiResponse> call, Throwable t) {
                apiResponseInterface.onFailure();
            }
        });
    }
    public static void callSaveCarPhotosApi(final Context mContext,final Map<String, RequestBody> apiRequest, final ApiResponseInterface apiResponseInterface){

        Call<ApiResponse> call = APIClient.getClientPhotos(mContext).create(APIInterface.class).callSaveCarPhotosApi(apiRequest);
        call.enqueue(new Callback<ApiResponse>() {
            @Override
            public void onResponse(Call<ApiResponse> call, Response<ApiResponse> response) {
                apiResponseInterface.onResponse(response);
            }

            @Override
            public void onFailure(Call<ApiResponse> call, Throwable t) {
                apiResponseInterface.onFailure();
                Log.e("err",t.getMessage());
            }
        });
    }

    public static void callSaveCarDocumentsApi(final Context mContext,final Map<String, RequestBody> apiRequest, final ApiResponseInterface apiResponseInterface){

        Call<ApiResponse> call = APIClient.getClientPhotos(mContext).create(APIInterface.class).callSaveCarDocsApi(apiRequest);
        call.enqueue(new Callback<ApiResponse>() {
            @Override
            public void onResponse(Call<ApiResponse> call, Response<ApiResponse> response) {
                apiResponseInterface.onResponse(response);
            }

            @Override
            public void onFailure(Call<ApiResponse> call, Throwable t) {
                apiResponseInterface.onFailure();
            }
        });
    }

    public static void callSaveCarAvailableApi(final Context mContext,final SaveCarInfoApiRequest apiRequest, final ApiResponseInterface apiResponseInterface){

        Call<ApiResponse> call = APIClient.getClient(mContext).create(APIInterface.class).callSaveCarAvailableApi(apiRequest);
        call.enqueue(new Callback<ApiResponse>() {
            @Override
            public void onResponse(Call<ApiResponse> call, Response<ApiResponse> response) {
                apiResponseInterface.onResponse(response);
            }

            @Override
            public void onFailure(Call<ApiResponse> call, Throwable t) {
                apiResponseInterface.onFailure();
            }
        });
    }

    public static void callSaveCarLocationApi(final Context mContext, final SaveCarInfoApiRequest apiRequest, final ApiResponseInterface apiResponseInterface){

        Call<ApiResponse> call = APIClient.getClient(mContext).create(APIInterface.class).callSaveCarLocationApi(apiRequest);
        call.enqueue(new Callback<ApiResponse>() {
            @Override
            public void onResponse(Call<ApiResponse> call, Response<ApiResponse> response) {
                apiResponseInterface.onResponse(response);
            }

            @Override
            public void onFailure(Call<ApiResponse> call, Throwable t) {
                apiResponseInterface.onFailure();
            }
        });
    }
    public static void callListCarApi(final Context mContext, final ApiRequest apiRequest, final ApiResponseInterface apiResponseInterface){

        Call<ApiResponse> call = APIClient.getClient(mContext).create(APIInterface.class).callListCarApi(apiRequest);
        call.enqueue(new Callback<ApiResponse>() {
            @Override
            public void onResponse(Call<ApiResponse> call, Response<ApiResponse> response) {
                apiResponseInterface.onResponse(response);
            }

            @Override
            public void onFailure(Call<ApiResponse> call, Throwable t) {
                apiResponseInterface.onFailure();
            }
        });
    }

    public static void callCarDetailsApi(final Context mContext,final ApiRequest apiRequest, final ApiResponseInterface apiResponseInterface){

        Call<ApiResponse> call = APIClient.getClient(mContext).create(APIInterface.class).callCarDetailsApi(apiRequest);
        call.enqueue(new Callback<ApiResponse>() {
            @Override
            public void onResponse(Call<ApiResponse> call, Response<ApiResponse> response) {
                apiResponseInterface.onResponse(response);
            }

            @Override
            public void onFailure(Call<ApiResponse> call, Throwable t) {
                apiResponseInterface.onFailure();
            }
        });
    }

    public static void callCarDeleteApi(final Context mContext,final ApiRequest apiRequest, final ApiResponseInterface apiResponseInterface){

        Call<ApiResponse> call = APIClient.getClient(mContext).create(APIInterface.class).callCarDeleteApi(apiRequest);
        call.enqueue(new Callback<ApiResponse>() {
            @Override
            public void onResponse(Call<ApiResponse> call, Response<ApiResponse> response) {
                apiResponseInterface.onResponse(response);
            }

            @Override
            public void onFailure(Call<ApiResponse> call, Throwable t) {
                apiResponseInterface.onFailure();
            }
        });
    }


    public static void callMyBookedCarApi(final Context mContext, final ApiResponseInterface apiResponseInterface){

        Call<ApiResponse> call = APIClient.getClient(mContext).create(APIInterface.class).callMyBookedCarApi();
        call.enqueue(new Callback<ApiResponse>() {
            @Override
            public void onResponse(Call<ApiResponse> call, Response<ApiResponse> response) {
                apiResponseInterface.onResponse(response);
            }

            @Override
            public void onFailure(Call<ApiResponse> call, Throwable t) {
                apiResponseInterface.onFailure();
            }
        });
    }

    public static void callMyBookedCarDetailsApi(final Context mContext, final ApiResponseInterface apiResponseInterface){

        Call<ApiResponse> call = APIClient.getClient(mContext).create(APIInterface.class).callMyBookedCarDetailsApi();
        call.enqueue(new Callback<ApiResponse>() {
            @Override
            public void onResponse(Call<ApiResponse> call, Response<ApiResponse> response) {
                apiResponseInterface.onResponse(response);
            }

            @Override
            public void onFailure(Call<ApiResponse> call, Throwable t) {
                apiResponseInterface.onFailure();
            }
        });
    }

    /*----------------------------car order-------------------------------*/

    public static void callOrderListApi(final Context mContext,final ApiRequest apiRequest, final ApiResponseInterface apiResponseInterface){

        Call<ApiResponse> call = APIClient.getClient(mContext).create(APIInterface.class).callOrderListApi(apiRequest);
        call.enqueue(new Callback<ApiResponse>() {
            @Override
            public void onResponse(Call<ApiResponse> call, Response<ApiResponse> response) {
                apiResponseInterface.onResponse(response);
            }

            @Override
            public void onFailure(Call<ApiResponse> call, Throwable t) {
                apiResponseInterface.onFailure();
            }
        });
    }

    public static void callOrderDetailApi(final Context mContext,final ApiRequest apiRequest, final ApiResponseInterface apiResponseInterface){

        Call<ApiResponse> call = APIClient.getClient(mContext).create(APIInterface.class).callOrderDetailApi(apiRequest);
        call.enqueue(new Callback<ApiResponse>() {
            @Override
            public void onResponse(Call<ApiResponse> call, Response<ApiResponse> response) {
                apiResponseInterface.onResponse(response);
            }

            @Override
            public void onFailure(Call<ApiResponse> call, Throwable t) {
                apiResponseInterface.onFailure();
            }
        });
    }

    /*-----------------Place Order Api --------------------------------*/
    public static void callUploadIdApi(final Context mContext,final Map<String, RequestBody> apiRequest, final ApiResponseInterface apiResponseInterface){

        Call<ApiResponse> call = APIClient.getClientPhotos(mContext).create(APIInterface.class).callUploadIdApi(apiRequest);
        call.enqueue(new Callback<ApiResponse>() {
            @Override
            public void onResponse(Call<ApiResponse> call, Response<ApiResponse> response) {
                apiResponseInterface.onResponse(response);
            }

            @Override
            public void onFailure(Call<ApiResponse> call, Throwable t) {
                apiResponseInterface.onFailure();
            }
        });
    }

    public static void callPlaceOrderApi(final Context mContext,final ApiRequest apiRequest, final ApiResponseInterface apiResponseInterface){

        Call<ApiResponse> call = APIClient.getClient(mContext).create(APIInterface.class).callPlaceOrderApi(apiRequest);
        call.enqueue(new Callback<ApiResponse>() {
            @Override
            public void onResponse(Call<ApiResponse> call, Response<ApiResponse> response) {
                apiResponseInterface.onResponse(response);
            }

            @Override
            public void onFailure(Call<ApiResponse> call, Throwable t) {
                apiResponseInterface.onFailure();
            }
        });
    }

    public static void callSuccessOrderApi(final Context mContext,final ApiRequest apiRequest, final ApiResponseInterface apiResponseInterface){

        Call<ApiResponse> call = APIClient.getClient(mContext).create(APIInterface.class).callSuccessOrderApi(apiRequest);
        call.enqueue(new Callback<ApiResponse>() {
            @Override
            public void onResponse(Call<ApiResponse> call, Response<ApiResponse> response) {
                apiResponseInterface.onResponse(response);
            }

            @Override
            public void onFailure(Call<ApiResponse> call, Throwable t) {
                apiResponseInterface.onFailure();
            }
        });
    }
    /*---------------delete car documents--------------------------------*/
    public static void callDeleteDocsApi(final Context mContext,final ApiRequest apiRequest, final ApiResponseInterface apiResponseInterface){

        Call<ApiResponse> call = APIClient.getClient(mContext).create(APIInterface.class).callDeleteDocsApi(apiRequest);
        call.enqueue(new Callback<ApiResponse>() {
            @Override
            public void onResponse(Call<ApiResponse> call, Response<ApiResponse> response) {
                apiResponseInterface.onResponse(response);
            }

            @Override
            public void onFailure(Call<ApiResponse> call, Throwable t) {
                apiResponseInterface.onFailure();
            }
        });
    }

    /*---------------delete car documents--------------------------------*/
    public static void callStaticContentApi(final Context mContext,ApiRequest apiRequest, final ApiResponseInterface apiResponseInterface){

        Call<ApiResponse> call = APIClient.getClient(mContext).create(APIInterface.class).callStaticContentApi(apiRequest);
        call.enqueue(new Callback<ApiResponse>() {
            @Override
            public void onResponse(Call<ApiResponse> call, Response<ApiResponse> response) {
                apiResponseInterface.onResponse(response);
            }

            @Override
            public void onFailure(Call<ApiResponse> call, Throwable t) {
                apiResponseInterface.onFailure();
            }
        });
    }

    /*---------------stripe payment--------------------------------*/
    public static void callStripePaymentApi(final Context mContext,ApiRequest apiRequest, final ApiResponseInterface apiResponseInterface){

        Call<ApiResponse> call = APIClient.getClient(mContext).create(APIInterface.class).callStripePaymentApi(apiRequest);
        call.enqueue(new Callback<ApiResponse>() {
            @Override
            public void onResponse(Call<ApiResponse> call, Response<ApiResponse> response) {
                apiResponseInterface.onResponse(response);
            }

            @Override
            public void onFailure(Call<ApiResponse> call, Throwable t) {
                apiResponseInterface.onFailure();
            }
        });
    }

    /*---------------Braintree payment key--------------------------------*/
    public static void callBraintreePaymentApi(final Context mContext,final ApiResponseInterface apiResponseInterface){

        Call<ApiResponse> call = APIClient.getClient(mContext).create(APIInterface.class).callBraintreePaymentApi();
        call.enqueue(new Callback<ApiResponse>() {
            @Override
            public void onResponse(Call<ApiResponse> call, Response<ApiResponse> response) {
                apiResponseInterface.onResponse(response);
            }

            @Override
            public void onFailure(Call<ApiResponse> call, Throwable t) {
                apiResponseInterface.onFailure();
            }
        });
    }

    public static void callBraintreePaypalPaymentApi(final Context mContext,final ApiRequest apiRequest,final ApiResponseInterface apiResponseInterface){

        Call<ApiResponse> call = APIClient.getClient(mContext).create(APIInterface.class).callBraintreePaypalPaymentApi(apiRequest);
        call.enqueue(new Callback<ApiResponse>() {
            @Override
            public void onResponse(Call<ApiResponse> call, Response<ApiResponse> response) {
                apiResponseInterface.onResponse(response);
            }

            @Override
            public void onFailure(Call<ApiResponse> call, Throwable t) {
                apiResponseInterface.onFailure();
                Log.e("err",t.getMessage());
            }
        });
    }

    public static void callContactApi(final Context mContext,final ApiResponseInterface apiResponseInterface){

        Call<ApiResponse> call = APIClient.getClient(mContext).create(APIInterface.class).callContactApi();
        call.enqueue(new Callback<ApiResponse>() {
            @Override
            public void onResponse(Call<ApiResponse> call, Response<ApiResponse> response) {
                apiResponseInterface.onResponse(response);
            }

            @Override
            public void onFailure(Call<ApiResponse> call, Throwable t) {
                apiResponseInterface.onFailure();
                Log.e("err",t.getMessage());
            }
        });
    }

    public static void callCurrencyApi(final Context mContext,ApiRequest apiRequest,final ApiResponseInterface apiResponseInterface){

        Call<ApiResponse> call = APIClient.getClient(mContext).create(APIInterface.class).callCurrencyApi(apiRequest);
        call.enqueue(new Callback<ApiResponse>() {
            @Override
            public void onResponse(Call<ApiResponse> call, Response<ApiResponse> response) {
                apiResponseInterface.onResponse(response);
            }

            @Override
            public void onFailure(Call<ApiResponse> call, Throwable t) {
                apiResponseInterface.onFailure();
                Log.e("err",t.getMessage());
            }
        });
    }
}
