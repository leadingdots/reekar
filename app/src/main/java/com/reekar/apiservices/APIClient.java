package com.reekar.apiservices;


import android.content.Context;
import android.preference.PreferenceManager;
import android.text.TextUtils;

import androidx.annotation.NonNull;

import com.akanksha.commonclassutil.PreferencesManager;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.reekar.commonclasses.AppConstant;
import com.reekar.commonclasses.CommonUtils;

import java.io.IOException;
import java.util.concurrent.TimeUnit;

import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class APIClient {

    private static String auth;

    public static Retrofit getClient(final Context mContext) {

        final String currency =PreferencesManager.Companion.getStringPreferences(mContext,AppConstant.CURRENT_CURRENCY);

        if (!TextUtils.isEmpty(PreferencesManager.Companion.getStringPreferences(mContext, AppConstant.ACCESS_CODE)))
            auth = "Bearer "+ PreferencesManager.Companion.getStringPreferences(mContext, AppConstant.ACCESS_CODE);
        HttpLoggingInterceptor interceptor = new HttpLoggingInterceptor();
        interceptor.setLevel(HttpLoggingInterceptor.Level.BODY);
       // ========================================
        OkHttpClient.Builder httpClient = new OkHttpClient.Builder();
        httpClient.connectTimeout(60, TimeUnit.SECONDS);
        httpClient.readTimeout(60, TimeUnit.SECONDS);
        httpClient.addInterceptor(new Interceptor() {
            @Override
            public okhttp3.Response intercept(@NonNull Interceptor.Chain chain) throws IOException {
                Request original = chain.request();

                Request.Builder requestBuilder = original.newBuilder()
                        .header("Content-Type", "application/json")
                        .header("Accept", "application/json")
//                        .addHeader(AppConstant.CURRENT_CURRENCY, PreferencesManager.Companion.getStringPreferences(mContext,AppConstant.CURRENT_CURRENCY))
                        .method(original.method(), original.body());
                if (!TextUtils.isEmpty(auth)){
                    requestBuilder.addHeader("Authorization",auth);
                }
                if (!TextUtils.isEmpty(currency)){
                    requestBuilder.addHeader(AppConstant.CURRENT_CURRENCY,currency);
                }
                else{
                    requestBuilder.addHeader(AppConstant.CURRENT_CURRENCY,"AED");
                }

                Request request = requestBuilder.build();


                return chain.proceed
                        (request);
            }
        });
        httpClient.addInterceptor(interceptor); // show the log
        OkHttpClient client = httpClient.build();
        Gson gson = new GsonBuilder()
                .setLenient()
                .create();
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(AppConstant.BASE_URL)
                .addConverterFactory(GsonConverterFactory.create(gson))
                .client(client)
                .build();
        retrofit.create(APIInterface.class);
        return retrofit;
    }

    //--------------api -------------------
    public static Retrofit getClientPhotos(final Context mContext) {

        final String currency =PreferencesManager.Companion.getStringPreferences(mContext,AppConstant.CURRENT_CURRENCY);

        if (!TextUtils.isEmpty(PreferencesManager.Companion.getStringPreferences(mContext, AppConstant.ACCESS_CODE)))
            auth = "Bearer "+ PreferencesManager.Companion.getStringPreferences(mContext, AppConstant.ACCESS_CODE);
        HttpLoggingInterceptor interceptor = new HttpLoggingInterceptor();
        interceptor.setLevel(HttpLoggingInterceptor.Level.BODY);
        // ========================================
        OkHttpClient.Builder httpClient = new OkHttpClient.Builder();
        httpClient.connectTimeout(120, TimeUnit.SECONDS);
        httpClient.readTimeout(120, TimeUnit.SECONDS);
        httpClient.addInterceptor(new Interceptor() {
            @Override
            public okhttp3.Response intercept(@NonNull Interceptor.Chain chain) throws IOException {
                Request original = chain.request();

                Request.Builder requestBuilder = original.newBuilder()
                        .header("Content-Type", "application/json")
                        .header("Accept", "application/json")
//                        .addHeader(AppConstant.CURRENT_CURRENCY, PreferencesManager.Companion.getStringPreferences(mContext,AppConstant.CURRENT_CURRENCY))
                        .method(original.method(), original.body());
                if (!TextUtils.isEmpty(auth)){
                    requestBuilder.addHeader("Authorization",auth);
                }
                if (!TextUtils.isEmpty(currency)){
                    requestBuilder.addHeader(AppConstant.CURRENT_CURRENCY,currency);
                }
                else{
                    requestBuilder.addHeader(AppConstant.CURRENT_CURRENCY,"AED");
                }

                Request request = requestBuilder.build();


                return chain.proceed
                        (request);
            }
        });
        httpClient.addInterceptor(interceptor); // show the log
        OkHttpClient client = httpClient.build();
        Gson gson = new GsonBuilder()
                .setLenient()
                .create();
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(AppConstant.BASE_URL)
                .addConverterFactory(GsonConverterFactory.create(gson))
                .client(client)
                .build();
        retrofit.create(APIInterface.class);
        return retrofit;
    }

}
