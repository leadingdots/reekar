package com.reekar.adapter;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.RecyclerView;

import com.akanksha.commonclassutil.PreferencesManager;
import com.bumptech.glide.Glide;
import com.reekar.R;
import com.reekar.activity.MyOrderDetailsActivity;
import com.reekar.commonclasses.AppConstant;
import com.reekar.commonclasses.CommonUtils;
import com.reekar.commonclasses.CurrencyJson;
import com.reekar.commonclasses.TimeUtil;
import com.reekar.databinding.MyOrderlistItemBinding;
import com.reekar.models.DataModel;

import java.util.List;

public class MyOrderListAdapter extends RecyclerView.Adapter<MyOrderListAdapter.ViewHolder> {


    private Context context;
    private boolean isLoading;
    private List<DataModel> arlOrder;

    public MyOrderListAdapter(Context context, List<DataModel> arlOrder) {
        this.context=context;
        this.arlOrder = arlOrder;
    }

    @NonNull
    @Override
    public MyOrderListAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
        View view = layoutInflater.inflate(R.layout.my_orderlist_item,parent,false);

        return new MyOrderListAdapter.ViewHolder(view);
    }

    @SuppressLint("SetTextI18n")
    @NonNull


    @Override
    public void onBindViewHolder(@NonNull MyOrderListAdapter.ViewHolder holder, final int position) {
        String currency = PreferencesManager.Companion.getStringPreferences(context,AppConstant.CURRENT_CURRENCY_SYMBOL);
        if (currency.isEmpty()){
            currency=PreferencesManager.Companion.getStringPreferences(context,AppConstant.CURRENT_CURRENCY);
        }

        if (position == arlOrder.size()-1)
            holder.myOrderlistItemBinding.progressCircular.setVisibility(isLoading? View.VISIBLE:View.GONE);
        else
            holder.myOrderlistItemBinding.progressCircular.setVisibility(View.GONE);

        if (arlOrder.get(position).getBookingId() != null)
            holder.myOrderlistItemBinding.tvBookingId.setText(context.getString(R.string.booking_id_)+arlOrder.get(position).getBookingId());

        if (arlOrder.get(position).getBrand() != null)
            holder.myOrderlistItemBinding.tvCar.setText(context.getString(R.string.car_)+arlOrder.get(position).getBrand()+" "+arlOrder.get(position).getModel());

        if (arlOrder.get(position).getBookingFromDate() != null)
            holder.myOrderlistItemBinding.tvRentedFromDate.setText(context.getString(R.string.rented_from_)+ TimeUtil.UtcToLocal(arlOrder.get(position).getBookingFromDate(),
                    "dd MMM yyyy",context));

        if (arlOrder.get(position).getBookingToDate() != null)
            holder.myOrderlistItemBinding.tvRentedTillDate.setText(context.getString(R.string.rented_till_)+ TimeUtil.UtcToLocal(arlOrder.get(position).getBookingToDate(),
                    "dd MMM yyyy",context));

        if (arlOrder.get(position).getUpdatedAt() != null)
            holder.myOrderlistItemBinding.tvDate.setText(context.getString(R.string.date_)+ TimeUtil.UtcToLocal(arlOrder.get(position).getUpdatedAt(),
                    "dd MMM yyyy",context));

        if (arlOrder.get(position).getEngineType() != null)
            holder.myOrderlistItemBinding.tvVariant.setText(context.getString(R.string.variant_)+ arlOrder.get(position).getEngineType());

//        if (arlOrder.get(position).getGrandTotal() != null)
        holder.myOrderlistItemBinding.tvAmount.setText(context.getString(R.string.amount_paid_)+ currency +" " + arlOrder.get(position).getGrandTotal());

        Glide.with(context).load(arlOrder.get(position).getImage()).placeholder(R.mipmap.placeholder_car)
                .into(holder.myOrderlistItemBinding.ivCar);

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(context, MyOrderDetailsActivity.class);
                intent.putExtra(AppConstant.ID,String.valueOf(arlOrder.get(position).getId()));
                context.startActivity(intent);
            }
        });


        spanableText(holder);

    }

    private void spanableText(ViewHolder holder) {
        CommonUtils.spannableTextColor(context,holder.myOrderlistItemBinding.tvBookingId.getText().toString(),R.color.black,
                0,context.getString(R.string.booking_id_).length(),holder.myOrderlistItemBinding.tvBookingId);

        CommonUtils.spannableTextColor(context,holder.myOrderlistItemBinding.tvCar.getText().toString(),R.color.black,
                0,context.getString(R.string.car_).length(),holder.myOrderlistItemBinding.tvCar);

        CommonUtils.spannableTextColor(context,holder.myOrderlistItemBinding.tvRentedFromDate.getText().toString(),R.color.black,
                0,context.getString(R.string.rented_from_).length(),holder.myOrderlistItemBinding.tvRentedFromDate);
        CommonUtils.spannableTextColor(context,holder.myOrderlistItemBinding.tvAmount.getText().toString(),R.color.black,
                0,context.getString(R.string.amount_paid_).length(),holder.myOrderlistItemBinding.tvAmount);

        CommonUtils.spannableTextColor(context,holder.myOrderlistItemBinding.tvDate.getText().toString(),R.color.black,
                0,context.getString(R.string.date_).length(),holder.myOrderlistItemBinding.tvDate);
        CommonUtils.spannableTextColor(context,holder.myOrderlistItemBinding.tvVariant.getText().toString(),R.color.black,
                0,context.getString(R.string.variant_).length(),holder.myOrderlistItemBinding.tvVariant);
        CommonUtils.spannableTextColor(context,holder.myOrderlistItemBinding.tvRentedTillDate.getText().toString(),R.color.black,
                0,context.getString(R.string.rented_till_).length(),holder.myOrderlistItemBinding.tvRentedTillDate);

    }

    @Override
    public int getItemCount() {
        return arlOrder.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder{

        MyOrderlistItemBinding myOrderlistItemBinding;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);

            myOrderlistItemBinding= DataBindingUtil.bind(itemView);
        }
    }

    public void showProgress(boolean isLoading){
        this.isLoading = isLoading;
    }
}