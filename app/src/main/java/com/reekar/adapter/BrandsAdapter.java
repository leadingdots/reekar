package com.reekar.adapter;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.RecyclerView;

import com.akanksha.commonclassutil.ToastUtil;
import com.reekar.R;
import com.reekar.databinding.BrandsItemBinding;
import com.reekar.interfaces.RecyclerviewItemClickInterface;
import com.reekar.models.DataModel;
import com.reekar.models.FilterModel;

import java.util.ArrayList;
import java.util.List;

public class BrandsAdapter extends RecyclerView.Adapter<BrandsAdapter.ViewHolder> {

    private ArrayList<DataModel> list;
//    private static List<DataModel> dataModelList = new ArrayList<>();
    private Context context;
    private String pos;
    private boolean checked;
    private RecyclerviewItemClickInterface recyclerviewItemClickInterface;

    public BrandsAdapter(Context context, ArrayList<DataModel> list, RecyclerviewItemClickInterface recyclerviewItemClickInterface) {
        this.context = context;
        this.list = list;
        this.recyclerviewItemClickInterface=recyclerviewItemClickInterface;
    }
    @NonNull
    @Override
    public BrandsAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
        View view = layoutInflater.inflate(R.layout.brands_item,parent,false);
        BrandsAdapter.ViewHolder viewHolder =  new BrandsAdapter.ViewHolder(view);
        return viewHolder;
    }

    @NonNull


    @Override
    public void onBindViewHolder(@NonNull final BrandsAdapter.ViewHolder holder, final int position) {

        holder.brandsItemBinding.tvBrandName.setText(list.get(position).getName());

        holder.brandsItemBinding.llParent.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                recyclerviewItemClickInterface.onItemClick(position);
            }
        });
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder{

        BrandsItemBinding brandsItemBinding;
        public ViewHolder(@NonNull View itemView) {
            super(itemView);

            brandsItemBinding=DataBindingUtil.bind(itemView);

        }
    }

    public void filterList(ArrayList<DataModel> filteredList) {
        list = filteredList;
        notifyDataSetChanged();
    }

}