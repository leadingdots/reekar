package com.reekar.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.RecyclerView;

import com.reekar.R;
import com.reekar.databinding.CarFeaturesItemBinding;
import com.reekar.databinding.CarRentFeaturesItemBinding;
import com.reekar.interfaces.RecyclerviewItemClickInterface;
import com.reekar.models.FeatureModel;

import java.util.List;

public class CarRentingFeatureAdapter extends RecyclerView.Adapter<CarRentingFeatureAdapter.ViewHolder> {

    private List<FeatureModel> featurelist;
    private Context context;
    String checkposition;
    private boolean checked;

    private String selectedPosition;
    private RecyclerviewItemClickInterface recyclerviewItemClickInterface;

    public CarRentingFeatureAdapter(Context context, List<FeatureModel> featurelist,RecyclerviewItemClickInterface recyclerviewItemClickInterface) {
        this.context = context;
        this.featurelist=featurelist;
        this.recyclerviewItemClickInterface=recyclerviewItemClickInterface;

    }
    @NonNull
    @Override
    public CarRentingFeatureAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
        View view = layoutInflater.inflate(R.layout.car_rent_features_item,parent,false);
        CarRentingFeatureAdapter.ViewHolder viewHolder =  new CarRentingFeatureAdapter.ViewHolder(view);
        return viewHolder;
    }

    @NonNull


    @Override
    public void onBindViewHolder(@NonNull final CarRentingFeatureAdapter.ViewHolder holder, final int position) {

//        featurelist.get(position).setChecked(false);
        holder.carRentFeaturesItemBinding.featureName.setText(featurelist.get(position).getName());

        if (featurelist.get(position).isChecked())
            holder.carRentFeaturesItemBinding.check.setVisibility(View.VISIBLE);
        else
            holder.carRentFeaturesItemBinding.check.setVisibility(View.INVISIBLE);

        holder.carRentFeaturesItemBinding.llFeature.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!featurelist.get(position).isChecked()){
                    holder.carRentFeaturesItemBinding.check.setVisibility(View.VISIBLE);
                    featurelist.get(position).setChecked(true);
                }
                else {
                    holder.carRentFeaturesItemBinding.check.setVisibility(View.INVISIBLE);
                    featurelist.get(position).setChecked(false);
                }

                recyclerviewItemClickInterface.onItemClick(position);

            }
        });


    }

    private void checkedItem(ViewHolder holder) {
                if (!checked) {
                    holder.carRentFeaturesItemBinding.check.setVisibility(View.VISIBLE);
                    checked = true;
                }
                else {
                    holder.carRentFeaturesItemBinding.check.setVisibility(View.INVISIBLE);
                    checked = false;
                }
    }

    @Override
    public int getItemCount() {
        return featurelist.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder{

        CarRentFeaturesItemBinding carRentFeaturesItemBinding;
        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            carRentFeaturesItemBinding= DataBindingUtil.bind(itemView);
        }
    }
}