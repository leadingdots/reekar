package com.reekar.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;

import androidx.annotation.NonNull;
import androidx.viewpager.widget.PagerAdapter;
import androidx.viewpager.widget.ViewPager;

import com.bumptech.glide.Glide;
import com.reekar.R;
import com.reekar.models.CarImageModel;

import java.util.List;

public class CarImageViewPagerAdapter extends PagerAdapter implements ViewPager.OnPageChangeListener{

    private Context context;
    private List<CarImageModel>images;
    private String from;

    LayoutInflater layoutInflater;
    private LinearLayout linearLeayout;


    public CarImageViewPagerAdapter(Context context, List<CarImageModel> images, String from) {
        this.context = context;
        this.images = images;
        this.from = from;

        layoutInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public int getCount() {
        return images.size();
    }

    @Override
    public boolean isViewFromObject(@NonNull View view, @NonNull Object object) {
        return view == object;
    }


    @NonNull
    @Override
    public Object instantiateItem(@NonNull ViewGroup container, final int position) {
        View itemView = layoutInflater.inflate(R.layout.car_details_image_item, container, false);

        ImageView imageView = (ImageView) itemView.findViewById(R.id.car_details_image);

        Glide.with(context).load(images.get(position).getImage()).placeholder(R.mipmap.placeholder_car).into(imageView);
        container.addView(itemView);

        imageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

            }
        });

        return itemView;
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        container.removeView((LinearLayout) object);

    }


    @Override
    public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

    }

    @Override
    public void onPageSelected(int position) {

    }

    @Override
    public void onPageScrollStateChanged(int state) {

    }
}