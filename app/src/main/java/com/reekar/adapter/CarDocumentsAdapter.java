package com.reekar.adapter;

import android.content.Context;
import android.os.Build;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.PopupMenu;

import androidx.annotation.NonNull;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.RecyclerView;

import com.akanksha.commonclassutil.ToastUtil;
import com.bumptech.glide.Glide;
import com.google.android.gms.common.api.Api;
import com.reekar.R;
import com.reekar.apiservices.ApiUtils;
import com.reekar.commonclasses.AppConstant;
import com.reekar.commonclasses.CommonUtils;
import com.reekar.databinding.DocumentsUplaodItemBinding;
import com.reekar.interfaces.ApiResponseInterface;
import com.reekar.interfaces.RecyclerviewItemClickInterface;
import com.reekar.models.ApiRequest;
import com.reekar.models.ApiResponse;
import com.reekar.models.UploadFileModel;

import java.net.URI;
import java.net.URISyntaxException;
import java.nio.file.Paths;
import java.util.ArrayList;

import retrofit2.Response;

public class CarDocumentsAdapter  extends RecyclerView.Adapter<CarDocumentsAdapter.ViewHolder> {


    private Context context;
    private  String carId;
    private ArrayList<UploadFileModel> uploadFileModelArrayList;

    private RecyclerviewItemClickInterface recyclerviewItemClickInterface;

    public CarDocumentsAdapter(Context context, ArrayList<UploadFileModel> uploadFileModelArrayList, String carId, RecyclerviewItemClickInterface recyclerviewItemClickInterface) {

        this.context = context;
        this.uploadFileModelArrayList = uploadFileModelArrayList;
        this.recyclerviewItemClickInterface=recyclerviewItemClickInterface;
        this.carId=carId;
    }

    @NonNull
    @Override
    public CarDocumentsAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
        View view = layoutInflater.inflate(R.layout.documents_uplaod_item, parent, false);

        CarDocumentsAdapter.ViewHolder viewHolder = new CarDocumentsAdapter.ViewHolder(view);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull final ViewHolder holder, final int position) {

        if (uploadFileModelArrayList.get(position).getFile()!=null && uploadFileModelArrayList.get(position).getFile().getName()!=null)
            holder.documentsUplaodItemBinding.docName.setText(uploadFileModelArrayList.get(position).getFile().getName());
        else {
            try {
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                    holder.documentsUplaodItemBinding.docName.setText( Paths.get(new URI(uploadFileModelArrayList.get(position).getDocs()).getPath()).getFileName().toString());
                }
                else {
                    holder.documentsUplaodItemBinding.docName.setText(uploadFileModelArrayList.get(position).getDocs().substring(uploadFileModelArrayList.get(position).getDocs().lastIndexOf("/")+1));
                }
            } catch (URISyntaxException e) {
                e.printStackTrace();
            }
        }
            setFileTypeImage(holder,position);


        holder.documentsUplaodItemBinding.deleteDoc.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                deleteDocuments(holder,position);
            }
        });

    }
    private void deleteDocuments(ViewHolder holder, final int position) {

        if (uploadFileModelArrayList.get(position).getId()!=null){
            if (CommonUtils.isNetworkAvailable(context))
             callDeleteDocsApi(position);
            else
                ToastUtil.Companion.showShortToast(context,context.getString(R.string.failed_internet));
        }
            else {
                uploadFileModelArrayList.remove(position);
                notifyItemRemoved(position);
                notifyItemRangeChanged(position, uploadFileModelArrayList.size());
                recyclerviewItemClickInterface.onItemClick(position);

        }
    }

    private void callDeleteDocsApi(final int position) {
        CommonUtils.showProgressDialog(context);

        ApiRequest apiRequest =new ApiRequest();
        apiRequest.setCarId(carId);
        apiRequest.setFileId(uploadFileModelArrayList.get(position).getId());
        apiRequest.setFileType(AppConstant.DELETE_DOC_TYPE);

        ApiUtils.callDeleteDocsApi(context, apiRequest, new ApiResponseInterface() {
            @Override
            public void onResponse(Response<ApiResponse> response) {
                CommonUtils.hideProgressDialog();
                if (response.code()==AppConstant.SUCCESS_CODE){
                    if (response.body()!=null){
                        if (response.body().getMessage()!=null)
                        ToastUtil.Companion.showShortToast(context,response.body().getMessage());

                        uploadFileModelArrayList.remove(position);
                        notifyItemRemoved(position);
                        notifyItemRangeChanged(position, uploadFileModelArrayList.size());
                        recyclerviewItemClickInterface.onItemClick(position);
                    }
                }
                else if (response.code()==AppConstant.UNAUTHORIZED_CODE){
                    CommonUtils.navigateToLogin(context);
                }
                else {
                    ToastUtil.Companion.showShortToast(context,context.getString(R.string.something_went_wrong));
                }
            }

            @Override
            public void onFailure() {
                CommonUtils.hideProgressDialog();
                ToastUtil.Companion.showShortToast(context,context.getString(R.string.something_went_wrong));
            }
        });
    }

    private void setFileTypeImage(ViewHolder holder, int position) {
        String file = uploadFileModelArrayList.get(position).getFiletype();
        if (file!=null)
        switch (file){
            case AppConstant.PDF:{
                holder.documentsUplaodItemBinding.docImage.setImageResource(R.drawable.ic_pdf);
                holder.documentsUplaodItemBinding.fileType.setText(R.string.pdf_doc);
                break;
            }
            case AppConstant.MS_WORD_DOC:
            case AppConstant.MS_WORD:{
                holder.documentsUplaodItemBinding.docImage.setImageResource(R.drawable.ic_word);
                holder.documentsUplaodItemBinding.fileType.setText(R.string.word_doc);
                break;
            }
            case AppConstant.IMAGE_JPEG:
            case AppConstant.IMAGE_JPG:
            {
                holder.documentsUplaodItemBinding.docImage.setImageResource(R.drawable.ic_image_black);
                holder.documentsUplaodItemBinding.fileType.setText(R.string.image_jpeg);
                break;
            }
            case AppConstant.IMAGE_PNG:
            {
                holder.documentsUplaodItemBinding.docImage.setImageResource(R.drawable.ic_image_black);
                holder.documentsUplaodItemBinding.fileType.setText(R.string.image_png);
                break;
            }

        }
    }


    @Override
    public int getItemCount() {
        return uploadFileModelArrayList.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder {

        DocumentsUplaodItemBinding documentsUplaodItemBinding;

        ViewHolder(@NonNull View itemView) {
            super(itemView);

            documentsUplaodItemBinding = DataBindingUtil.bind(itemView);
        }
    }
}