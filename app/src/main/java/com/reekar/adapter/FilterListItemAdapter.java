package com.reekar.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CompoundButton;

import androidx.annotation.NonNull;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.RecyclerView;

import com.akanksha.commonclassutil.LogUtil;
import com.reekar.R;
import com.reekar.databinding.FilterListItemBinding;
import com.reekar.models.DataModel;

import java.util.List;

public class FilterListItemAdapter extends RecyclerView.Adapter<FilterListItemAdapter.ViewHolder> {



//    private List<FilterSubListModel> filter;
    private List<DataModel> filter;
    private Context context;
    private String pos;

    public FilterListItemAdapter(Context context, List<DataModel> filter) {
        this.context = context;
        this.filter = filter;

    }
    @NonNull
    @Override
    public FilterListItemAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
        View view = layoutInflater.inflate(R.layout.filter_list_item,parent,false);
        FilterListItemAdapter.ViewHolder viewHolder =  new FilterListItemAdapter.ViewHolder(view);
        return viewHolder;
    }

    @NonNull


    @Override
    public void onBindViewHolder(@NonNull final FilterListItemAdapter.ViewHolder holder, final int position) {

        LogUtil.Companion.errorLog("position", String.valueOf(position));
        holder.filterListItemBinding.filterSublistTv.setText(filter.get(position).getName());
        holder.filterListItemBinding.checkBox.setChecked(filter.get(position).isCheck());

        holder.filterListItemBinding.checkBox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                filter.get(holder.getAdapterPosition()).setCheck(isChecked);


            }
        });
    }

    @Override
    public int getItemCount() {
        return filter.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder{

        FilterListItemBinding filterListItemBinding;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            filterListItemBinding= DataBindingUtil.bind(itemView);
        }
    }
}