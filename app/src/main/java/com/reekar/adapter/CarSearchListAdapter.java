package com.reekar.adapter;

import android.annotation.SuppressLint;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.akanksha.commonclassutil.PreferencesManager;
import com.bumptech.glide.Glide;
import com.reekar.R;
import com.reekar.commonclasses.AppConstant;
import com.reekar.commonclasses.CurrencyJson;
import com.reekar.databinding.SearchCarListItemBinding;
import com.reekar.interfaces.RecyclerviewItemClickInterface;
import com.reekar.models.CarModel;

import java.util.List;

public class CarSearchListAdapter extends RecyclerView.Adapter<CarSearchListAdapter.ViewHolder> {


    private Context context;
    private List<CarModel> arlCars;
    private boolean isLoading = false;

    private RecyclerviewItemClickInterface recyclerviewItemClickInterface;

    public CarSearchListAdapter(Context context, List<CarModel> arlCars, RecyclerviewItemClickInterface recyclerviewItemClickInterface) {
        this.arlCars = arlCars;
        this.context=context;
        this.recyclerviewItemClickInterface=recyclerviewItemClickInterface;

    }

    @NonNull
    @Override
    public CarSearchListAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
        View view = layoutInflater.inflate(R.layout.search_car_list_item,parent,false);


        CarSearchListAdapter.ViewHolder viewHolder =  new CarSearchListAdapter.ViewHolder(view);
        return viewHolder;
    }

    @SuppressLint({"SetTextI18n", "DefaultLocale"})
    @Override
    public void onBindViewHolder(@NonNull CarSearchListAdapter.ViewHolder holder, final int position) {
        if (position == arlCars.size()-1)
            holder.listviewFragmentItemBinding.progressCircular.setVisibility(isLoading? View.VISIBLE:View.GONE);
        else
            holder.listviewFragmentItemBinding.progressCircular.setVisibility(View.GONE);

        String  carName = arlCars.get(position).getBrand() +" "+arlCars.get(position).getModel() +" ("+
                arlCars.get(position).getEnginetype()+")";
        holder.listviewFragmentItemBinding.tvCarName.setText(carName);

        Glide.with(context).load(arlCars.get(position).getImage())
                .placeholder(R.mipmap.placeholder_car)
                .into(holder.listviewFragmentItemBinding.ivCar);
//        String currency = CurrencyJson.updateCurrency(context)+" ";
        String currency = PreferencesManager.Companion.getStringPreferences(context,AppConstant.CURRENT_CURRENCY_SYMBOL);
        if (currency.isEmpty()){
            currency=PreferencesManager.Companion.getStringPreferences(context,AppConstant.CURRENT_CURRENCY);
        }

        if (arlCars.get(position).getPricePerDay() != null)
            holder.listviewFragmentItemBinding.tvPriceDay.setText(context.getString(R.string.daily_price)+currency+" "+
                    arlCars.get(position).getPricePerDay());

        if (arlCars.get(position).getTotalPrice() != null)
            holder.listviewFragmentItemBinding.tvTotalPrice.setText(context.getString(R.string.total_price_)+currency+" "+arlCars.get(position).getTotalPrice());

        if (arlCars.get(position).getTransmission() != null)    
            holder.listviewFragmentItemBinding.tvTransmission.setText(context.getString(R.string.transmission_)+
                    arlCars.get(position).getTransmission());
        if (arlCars.get(position).getDistance() != null) {
            holder.listviewFragmentItemBinding.tvDistance.setText(context.getString(R.string.distance_)+
                    String.format("%.2f", arlCars.get(position).getDistance())+context.getString(R.string.km));
        }

        setAdapter(holder.listviewFragmentItemBinding.carFeaturesRecyclerview,arlCars.get(position).getFeatures());
        holder.listviewFragmentItemBinding.viewCarDetailsLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                recyclerviewItemClickInterface.onItemClick(position);
//                notifyDataSetChanged();
//                Intent intent = new Intent(context, CarDetailsActivity.class);
//                intent.putExtra(AppConstant.ID,String.valueOf(arlCars.get(position).getId()));
//                context.startActivity(intent);
            }
        });


    }

    public void showProgress(boolean isLoading){
        this.isLoading = isLoading;
    }

    private void setAdapter(RecyclerView carFeaturesRecyclerview, List<String> features) {
        CarFeatureAdapter carFeatureAdapter =new CarFeatureAdapter(context,features,AppConstant.CAR_LIST);
        carFeaturesRecyclerview.setHasFixedSize(true);
        carFeaturesRecyclerview.setLayoutManager(new GridLayoutManager(context,8));
        carFeaturesRecyclerview.setAdapter(carFeatureAdapter);
    }

    @Override
    public int getItemCount() {
        return arlCars.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder{

        SearchCarListItemBinding listviewFragmentItemBinding;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);

            listviewFragmentItemBinding=DataBindingUtil.bind(itemView);
        }
    }
}