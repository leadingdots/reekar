package com.reekar.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.RecyclerView;

import com.reekar.R;
import com.reekar.databinding.CarDetailPageFeatureItemBinding;
import com.reekar.models.CarFeatureModel;

import java.util.List;

public class CarDetailsFeatureAdapter extends RecyclerView.Adapter<CarDetailsFeatureAdapter.ViewHolder> {

    private Context context;
    private List<CarFeatureModel> arlCarFeature;

    public CarDetailsFeatureAdapter(Context context, List<CarFeatureModel> arlCarFeature) {
        this.context = context;
        this.arlCarFeature = arlCarFeature;
    }

    @NonNull
    @Override
    public CarDetailsFeatureAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
        View view = layoutInflater.inflate(R.layout.car_detail_page_feature_item,parent,false);
        CarDetailsFeatureAdapter.ViewHolder viewHolder =  new CarDetailsFeatureAdapter.ViewHolder(view);
        return viewHolder;
    }

    @NonNull
    @Override
    public void onBindViewHolder(@NonNull CarDetailsFeatureAdapter.ViewHolder holder, int position) {
        holder.carDetailPageFeatureItemBinding.ivFeature.setImageResource(arlCarFeature.get(position).getImage());
        holder.carDetailPageFeatureItemBinding.tvFeatureName.setText(arlCarFeature.get(position).getName());

        if (arlCarFeature.get(position).getDetail() != null)
            holder.carDetailPageFeatureItemBinding.tvFeatureDetail.setText(arlCarFeature.get(position).getDetail());
    }

    @Override
    public int getItemCount() {
        return arlCarFeature.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder{

        CarDetailPageFeatureItemBinding carDetailPageFeatureItemBinding;
        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            carDetailPageFeatureItemBinding= DataBindingUtil.bind(itemView);
        }
    }
}