package com.reekar.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.core.content.ContextCompat;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.RecyclerView;

import com.reekar.R;
import com.reekar.commonclasses.AppConstant;
import com.reekar.databinding.CarFeaturesItemBinding;

import java.util.List;

public class CarFeatureAdapter extends RecyclerView.Adapter<CarFeatureAdapter.ViewHolder> {

    private List<String> features;
    private Context context;
    private String mFrom;

    public CarFeatureAdapter(Context context, List<String> features,String mFrom) {
        this.context = context;
        this.features = features;
        this.mFrom = mFrom;
    }
    @NonNull
    @Override
    public CarFeatureAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
        View view = layoutInflater.inflate(R.layout.car_features_item,parent,false);
        return new CarFeatureAdapter.ViewHolder(view);
    }

    @NonNull
    @Override
    public void onBindViewHolder(@NonNull CarFeatureAdapter.ViewHolder holder, int position) {
        if (mFrom.equalsIgnoreCase(AppConstant.CAR_DETAIL_ACTIVITY)) {
            holder.carFeaturesItemBinding.tvFeature.setVisibility(View.VISIBLE);
            holder.carFeaturesItemBinding.llParent.setBackground(ContextCompat.getDrawable(context,R.drawable.square_shape));
        }
        else {
            holder.carFeaturesItemBinding.tvFeature.setVisibility(View.GONE);
            holder.carFeaturesItemBinding.llParent.setBackgroundColor(ContextCompat.getColor(context,R.color.puretransparent));
        }


        if (features.get(position) != null){
            switch (features.get(position)){
                case AppConstant.AC:
                    holder.carFeaturesItemBinding.ivFeature.setImageResource(R.mipmap.ac);
                    holder.carFeaturesItemBinding.tvFeature.setText(R.string.ac);
                    break;
                case AppConstant.GPS:
                    holder.carFeaturesItemBinding.ivFeature.setImageResource(R.mipmap.gps);
                    holder.carFeaturesItemBinding.tvFeature.setText(R.string.gps);
                    break;
                case AppConstant.IPOD_INTERFACE:
                    holder.carFeaturesItemBinding.ivFeature.setImageResource(R.mipmap.ipod);
                    holder.carFeaturesItemBinding.tvFeature.setText(R.string.ipod_interface);
                    break;
                case AppConstant.SUNROOF:
                    holder.carFeaturesItemBinding.ivFeature.setImageResource(R.mipmap.sun_roof);
                    holder.carFeaturesItemBinding.tvFeature.setText(R.string.sunroof);
                    break;
                case AppConstant.CHILD_SEAT:
                    holder.carFeaturesItemBinding.ivFeature.setImageResource(R.mipmap.child_seat);
                    holder.carFeaturesItemBinding.tvFeature.setText(R.string.child_seat);
                    break;
                case AppConstant.ELECTRIC_WINDOW:
                    holder.carFeaturesItemBinding.ivFeature.setImageResource(R.mipmap.electric_window);
                    holder.carFeaturesItemBinding.tvFeature.setText(R.string.electric_windows);
                    break;
                case AppConstant.HEATED_SEAT:
                    holder.carFeaturesItemBinding.ivFeature.setImageResource(R.mipmap.heated_seats);
                    holder.carFeaturesItemBinding.tvFeature.setText(R.string.heated_seat);
                    break;
                case AppConstant.GAUGE:
                    holder.carFeaturesItemBinding.ivFeature.setImageResource(R.mipmap.gauge);
                    holder.carFeaturesItemBinding.tvFeature.setText(R.string.prm_gauge);
                    break;
                case AppConstant.ABS:
                    holder.carFeaturesItemBinding.ivFeature.setImageResource(R.mipmap.abs);
                    holder.carFeaturesItemBinding.tvFeature.setText(R.string.abs);
                    break;
                case AppConstant.TRACTION_CONTROL:
                    holder.carFeaturesItemBinding.ivFeature.setImageResource(R.mipmap.traction_control);
                    holder.carFeaturesItemBinding.tvFeature.setText(R.string.traction_control);
                    break;
                case AppConstant.AUDIO_SYSTEM:
                    holder.carFeaturesItemBinding.ivFeature.setImageResource(R.mipmap.audio_system);
                    holder.carFeaturesItemBinding.tvFeature.setText(R.string.audio_system);
                    break;
                case AppConstant.PANORMA_ROOF:
                    holder.carFeaturesItemBinding.ivFeature.setImageResource(R.mipmap.sun_roof);
                    holder.carFeaturesItemBinding.tvFeature.setText(R.string.panorma_roof);
                    break;
                case AppConstant.CRUISE_CONTROL:
                    holder.carFeaturesItemBinding.ivFeature.setImageResource(R.drawable.cruise_control);
                    holder.carFeaturesItemBinding.tvFeature.setText(R.string.cruise_control);
                    break;
            }
        }
//        holder.carFeaturesItemBinding.ivFeature.setImageResource(images[position]);
    }

    @Override
    public int getItemCount() {
        return features.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder{

        CarFeaturesItemBinding carFeaturesItemBinding;
        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            carFeaturesItemBinding=DataBindingUtil.bind(itemView);
        }
    }
}