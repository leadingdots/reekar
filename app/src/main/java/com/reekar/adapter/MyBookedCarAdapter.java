package com.reekar.adapter;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.core.content.ContextCompat;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.reekar.R;
import com.reekar.activity.ListedCarDetailsActivity;
import com.reekar.activity.MyBookedCarDetailsActivity;
import com.reekar.commonclasses.AppConstant;
import com.reekar.commonclasses.CommonUtils;
import com.reekar.commonclasses.TimeUtil;
import com.reekar.databinding.MyBookedCarItemBinding;
import com.reekar.models.CarModel;
import com.reekar.models.DataModel;

import java.util.List;

public class MyBookedCarAdapter extends RecyclerView.Adapter<MyBookedCarAdapter.ViewHolder> {


    private Context context;
    private List<DataModel> arlCar;

    private boolean isLoading = false;

    public MyBookedCarAdapter(Context context, List<DataModel> arlCar) {
        this.context = context;
        this.arlCar=arlCar;
    }

    @NonNull
    @Override
    public MyBookedCarAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.my_booked_car_item,parent,false);
        MyBookedCarAdapter.ViewHolder viewHolder =  new MyBookedCarAdapter.ViewHolder(view);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull MyBookedCarAdapter.ViewHolder holder, final int position) {

        if (position == arlCar.size()-1)
            holder.myBookedCarItemBinding.progressCircular.setVisibility(isLoading? View.VISIBLE:View.GONE);
        else
            holder.myBookedCarItemBinding.progressCircular.setVisibility(View.GONE);
        if (arlCar.get(position).getImage() != null)
            Glide.with(context).load(arlCar.get(position).getImage()).placeholder(R.mipmap.placeholder_car)
                    .into(holder.myBookedCarItemBinding.ivCar);
        if (arlCar.get(position).getBrand() != null){
            holder.myBookedCarItemBinding.tvCarBrand.setText(arlCar.get(position).getBrand()+" "+arlCar.get(position).getModel());
        }
        if (arlCar.get(position).getBilling_first_name()!=null &&  arlCar.get(position).getBilling_last_name()!=null) {
            holder.myBookedCarItemBinding.tvCustomerName.setText(context.getString(R.string.booked_by_) + arlCar.get(position).getBilling_first_name()+" "+arlCar.get(position).getBilling_last_name());
        }
        else {
            holder.myBookedCarItemBinding.tvCustomerName.setText(context.getString(R.string.booked_by_) + arlCar.get(position).getBilling_first_name());
        }
        if (arlCar.get(position).getBookingFromDate()!=null){
            holder.myBookedCarItemBinding.tvBookedFrom.setText(context.getString(R.string.booked_from_)+ TimeUtil.UtcToLocal(arlCar.get(position).getBookingFromDate(),"dd/MM/yyyy hh:mm a",context));
        }
        if (arlCar.get(position).getBookingToDate()!=null){
            holder.myBookedCarItemBinding.tvBookeTill.setText(context.getString(R.string.booked_til_)+TimeUtil.UtcToLocal(arlCar.get(position).getBookingToDate(),"dd/MM/yyyy hh:mm a",context));
        }
        CommonUtils.spannableTextColor(context,holder.myBookedCarItemBinding.tvCarModel.getText().toString(),
                R.color.black,0,6,holder.myBookedCarItemBinding.tvCarModel);
        CommonUtils.spannableTextColor(context,holder.myBookedCarItemBinding.tvCustomerName.getText().toString(),
                R.color.black,0,context.getString(R.string.booked_by_).length(),holder.myBookedCarItemBinding.tvCustomerName);
        CommonUtils.spannableTextColor(context,holder.myBookedCarItemBinding.tvBookedFrom.getText().toString(),
                R.color.black,0,context.getString(R.string.booked_from_).length(),holder.myBookedCarItemBinding.tvBookedFrom);
        CommonUtils.spannableTextColor(context,holder.myBookedCarItemBinding.tvBookeTill.getText().toString(),
                R.color.black,0,context.getString(R.string.booked_til_).length(),holder.myBookedCarItemBinding.tvBookeTill);
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(context, MyBookedCarDetailsActivity.class);
                intent.putExtra(AppConstant.ID,String.valueOf(arlCar.get(position).getId()));
                context.startActivity(intent);
            }
        });
    }

    public void showProgress(boolean isLoading){
        this.isLoading = isLoading;
    }

    @Override
    public int getItemCount() {
        return arlCar.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder{

        MyBookedCarItemBinding myBookedCarItemBinding;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);

            myBookedCarItemBinding= DataBindingUtil.bind(itemView);
        }
    }
}
