package com.reekar.adapter;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.PopupMenu;

import androidx.annotation.NonNull;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.RecyclerView;

import com.akanksha.commonclassutil.ToastUtil;
import com.bumptech.glide.Glide;
import com.reekar.R;
import com.reekar.apiservices.ApiUtils;
import com.reekar.commonclasses.AppConstant;
import com.reekar.commonclasses.CommonUtils;
import com.reekar.databinding.CarPhotosItemBinding;
import com.reekar.interfaces.ApiResponseInterface;
import com.reekar.interfaces.RecyclerviewItemClickInterface;
import com.reekar.models.ApiRequest;
import com.reekar.models.ApiResponse;
import com.reekar.models.UploadFileModel;

import java.util.ArrayList;

import retrofit2.Response;

public class UploadCarImageAdapter extends RecyclerView.Adapter<UploadCarImageAdapter.ViewHolder> {


    private Context context;
    private String carId;
    private ArrayList<UploadFileModel> uploadFileModelArrayList;

    private RecyclerviewItemClickInterface recyclerviewItemClickInterface;

    public UploadCarImageAdapter(Context context, ArrayList<UploadFileModel> uploadFileModelArrayList, String carId, RecyclerviewItemClickInterface recyclerviewItemClickInterface) {

        this.context = context;
        this.uploadFileModelArrayList = uploadFileModelArrayList;
        this.recyclerviewItemClickInterface=recyclerviewItemClickInterface;
        this.carId=carId;
    }

    @NonNull
    @Override
    public UploadCarImageAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
        View view = layoutInflater.inflate(R.layout.car_photos_item, parent, false);

        UploadCarImageAdapter.ViewHolder viewHolder = new UploadCarImageAdapter.ViewHolder(view);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull final UploadCarImageAdapter.ViewHolder holder, final int position) {

        if (uploadFileModelArrayList.get(position).getUri()!=null)
        holder.carPhotosItemBinding.carPhoto.setImageURI(uploadFileModelArrayList.get(position).getUri());
        if (uploadFileModelArrayList.get(position).getImages()!=null)
            Glide.with(context).load(uploadFileModelArrayList.get(position).getImages())
            .placeholder(R.mipmap.placeholder_car)
            .into(holder.carPhotosItemBinding.carPhoto);

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                deleteImage(holder,position);
            }
        });

    }

    private void deleteImage(final ViewHolder holder, final int position) {
        //Creating the instance of PopupMenu
        PopupMenu popup = new PopupMenu(context,holder.carPhotosItemBinding.carPhoto);
        //Inflating the Popup using xml file
        popup.getMenuInflater().inflate(R.menu.popup_menu, popup.getMenu());
        //registering popup with OnMenuItemClickListener
        popup.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
            public boolean onMenuItemClick(MenuItem item) {
        //  Toast.makeText(context,"You Clicked : " + item.getTitle(), Toast.LENGTH_SHORT).show();
            if (uploadFileModelArrayList.get(position).getId()!=null){
                callDeleteDocsApi(position);
            }
            else {
                    uploadFileModelArrayList.remove(position);
                    notifyItemRemoved(position);
                    notifyItemRangeChanged(position, uploadFileModelArrayList.size());
                    recyclerviewItemClickInterface.onItemClick(position);

            }
                return true;
            }
        });
        popup.show();//showing popup menu
    }

    private void callDeleteDocsApi(final int position) {
        CommonUtils.showProgressDialog(context);

        ApiRequest apiRequest =new ApiRequest();
        apiRequest.setCarId(carId);
        apiRequest.setFileId(uploadFileModelArrayList.get(position).getId());
        apiRequest.setFileType(AppConstant.DELETE_IMAGE_TYPE);

        ApiUtils.callDeleteDocsApi(context, apiRequest, new ApiResponseInterface() {
            @Override
            public void onResponse(Response<ApiResponse> response) {
                CommonUtils.hideProgressDialog();
                if (response.code()==AppConstant.SUCCESS_CODE){
                    if (response.body()!=null){
                        if (response.body().getMessage()!=null)
                            ToastUtil.Companion.showShortToast(context,response.body().getMessage());

                        uploadFileModelArrayList.remove(position);
                        notifyItemRemoved(position);
                        notifyItemRangeChanged(position, uploadFileModelArrayList.size());
                        recyclerviewItemClickInterface.onItemClick(position);
                    }
                }
                else if (response.code()==AppConstant.UNAUTHORIZED_CODE){
                    CommonUtils.navigateToLogin(context);
                }
                else {
                    ToastUtil.Companion.showShortToast(context,context.getString(R.string.something_went_wrong));

                }
            }

            @Override
            public void onFailure() {
                CommonUtils.hideProgressDialog();
                ToastUtil.Companion.showShortToast(context,context.getString(R.string.something_went_wrong));

            }
        });
    }

    @Override
    public int getItemCount() {
//        Log.e("uploadFileModelArrayList", String.valueOf(uploadFileModelArrayList.size()));
       return uploadFileModelArrayList.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder {

        CarPhotosItemBinding carPhotosItemBinding;

        ViewHolder(@NonNull View itemView) {
            super(itemView);

            carPhotosItemBinding = DataBindingUtil.bind(itemView);
        }
    }
}