package com.reekar.adapter;

import android.content.Context;
import android.graphics.Typeface;
import android.text.style.StyleSpan;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;

import androidx.annotation.NonNull;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.android.gms.tasks.Tasks;
import com.google.android.libraries.places.api.Places;
import com.google.android.libraries.places.api.model.AutocompletePrediction;
import com.google.android.libraries.places.api.model.AutocompleteSessionToken;
import com.google.android.libraries.places.api.model.Place;
import com.google.android.libraries.places.api.net.FetchPlaceRequest;
import com.google.android.libraries.places.api.net.FetchPlaceResponse;
import com.google.android.libraries.places.api.net.FindAutocompletePredictionsRequest;
import com.google.android.libraries.places.api.net.FindAutocompletePredictionsResponse;
import com.google.android.libraries.places.api.net.PlacesClient;
import com.reekar.R;
import com.reekar.databinding.CarDetailPageFeatureItemBinding;
import com.reekar.databinding.LocationPickerItemBinding;
import com.reekar.interfaces.OnPlaceItemClick;
import com.reekar.models.PlaceAutocompleteModel;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;

public class LocationSuggestionAdapter extends RecyclerView.Adapter<LocationSuggestionAdapter.ViewHolder> implements Filterable {

    private final PlacesClient placesClient;
    private Context context;
    private OnPlaceItemClick onPlaceItemClick;
    private List<PlaceAutocompleteModel> arlPlace = new ArrayList<>();

    private static StyleSpan STYLE_BOLD = new StyleSpan(Typeface.BOLD);
    private static StyleSpan STYLE_NORMAL = new StyleSpan(Typeface.NORMAL);
    public LocationSuggestionAdapter(Context context, OnPlaceItemClick onPlaceItemClick) {
        this.context = context;
        this.onPlaceItemClick = onPlaceItemClick;
//        this.images = images;

        placesClient = Places.createClient(context);
    }
    @NonNull
    @Override
    public LocationSuggestionAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
        View view = layoutInflater.inflate(R.layout.location_picker_item,parent,false);
        LocationSuggestionAdapter.ViewHolder viewHolder =  new LocationSuggestionAdapter.ViewHolder(view);
        return viewHolder;
    }

    @NonNull
    @Override
    public void onBindViewHolder(@NonNull LocationSuggestionAdapter.ViewHolder holder, final int position) {
//        holder.carDetailsFeatureItemBinding.feature.setImageResource(images[position]);
        holder.locationPickerItemBinding.tvLocation.setText(arlPlace.get(position).address);
        holder.locationPickerItemBinding.tvAddress.setText(arlPlace.get(position).area);
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String placeId = arlPlace.get(position).placeId.toString();
                List<Place.Field> placeFields = Arrays.asList(Place.Field.ID, Place.Field.NAME, Place.Field.LAT_LNG,
                        Place.Field.ADDRESS);
                FetchPlaceRequest request = FetchPlaceRequest.builder(placeId, placeFields).build();


                placesClient.fetchPlace(request).addOnSuccessListener(new OnSuccessListener<FetchPlaceResponse>() {
                    @Override
                    public void onSuccess(FetchPlaceResponse fetchPlaceResponse) {
                        onPlaceItemClick.onItemClick(fetchPlaceResponse.getPlace());
                    }
                }).addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {
                        e.printStackTrace();
                    }
                });
            }
        });
    }

    @Override
    public int getItemCount() {
//        return images.length;
        return arlPlace.size();
    }

    @Override
    public Filter getFilter() {
        return new Filter() {
            @Override
            protected FilterResults performFiltering(CharSequence charSequence) {
                FilterResults results = new FilterResults();
                if (charSequence != null){
                    arlPlace = getPredictions(charSequence);

                    if (!arlPlace.isEmpty()) {
                        results.values = arlPlace;
                        results.count = arlPlace.size();
                    }
                }
                return results;
            }

            @Override
            protected void publishResults(CharSequence charSequence, FilterResults filterResults) {

            }
        };
    }

    private List<PlaceAutocompleteModel> getPredictions(CharSequence charSequence) {
        ArrayList<PlaceAutocompleteModel> resultList = new ArrayList<>();
        AutocompleteSessionToken token = AutocompleteSessionToken.newInstance();
        FindAutocompletePredictionsRequest request = FindAutocompletePredictionsRequest.builder()
                .setSessionToken(token)
                .setQuery(charSequence.toString())
                .build();

        Task<FindAutocompletePredictionsResponse> autocompletePredictions = placesClient.findAutocompletePredictions(request);;
        try {
            Tasks.await(autocompletePredictions,60, TimeUnit.SECONDS);
        } catch (ExecutionException e) {
            e.printStackTrace();
        } catch (InterruptedException e) {
            e.printStackTrace();
        } catch (TimeoutException e) {
            e.printStackTrace();
        }


        if (autocompletePredictions.isSuccessful()){
            FindAutocompletePredictionsResponse findAutocompletePredictionsResponse = autocompletePredictions.getResult();
            if (findAutocompletePredictionsResponse != null){
                for (AutocompletePrediction prediction : findAutocompletePredictionsResponse.getAutocompletePredictions()){

                    PlaceAutocompleteModel placeAutocomplete = new PlaceAutocompleteModel();
                    placeAutocomplete.placeId = prediction.getPlaceId();
                    placeAutocomplete.address = prediction.getPrimaryText(STYLE_NORMAL).toString();
                    placeAutocomplete.area = prediction.getFullText(STYLE_BOLD).toString();

//                    println("place "+placeAutocomplete.area+" "+placeAutocomplete.address)
                    resultList.add(placeAutocomplete);
                }
            }
        }

        return resultList;
    }

    public class ViewHolder extends RecyclerView.ViewHolder{

        LocationPickerItemBinding locationPickerItemBinding;
        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            locationPickerItemBinding= DataBindingUtil.bind(itemView);
        }
    }
}