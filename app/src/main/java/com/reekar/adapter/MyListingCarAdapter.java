package com.reekar.adapter;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.core.content.ContextCompat;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.reekar.R;
import com.reekar.activity.ListedCarDetailsActivity;
import com.reekar.commonclasses.AppConstant;
import com.reekar.commonclasses.CommonUtils;
import com.reekar.databinding.MyListingCarItemBinding;
import com.reekar.interfaces.RecyclerviewItemClickInterface;
import com.reekar.models.CarModel;

import java.util.List;

public class MyListingCarAdapter extends RecyclerView.Adapter<MyListingCarAdapter.ViewHolder> {


    private Context context;
    private List<CarModel> arlCar;
    private boolean isLoading = false;
    private RecyclerviewItemClickInterface recyclerviewItemClickInterface;
    public MyListingCarAdapter(Context context, List<CarModel> arlCar, RecyclerviewItemClickInterface recyclerviewItemClickInterface) {

        this.arlCar = arlCar;
        this.context=context;
        this.recyclerviewItemClickInterface=recyclerviewItemClickInterface;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
        View view = layoutInflater.inflate(R.layout.my_listing_car_item,parent,false);


        MyListingCarAdapter.ViewHolder viewHolder =  new MyListingCarAdapter.ViewHolder(view);
        return viewHolder;
    }

    @SuppressLint("SetTextI18n")
    @NonNull


    @Override
    public void onBindViewHolder(@NonNull MyListingCarAdapter.ViewHolder holder, final int position) {
        if (position == arlCar.size()-1)
            holder.myListingCarItemBinding.progressCircular.setVisibility(isLoading? View.VISIBLE:View.GONE);
        else
            holder.myListingCarItemBinding.progressCircular.setVisibility(View.GONE);
//        if (arlCar.get(position).getImage() != null)
            Glide.with(context).load(arlCar.get(position).getImage()).placeholder(R.mipmap.placeholder_car)
                    .into(holder.myListingCarItemBinding.ivCar);

        if (arlCar.get(position).getBrand() != null){
            holder.myListingCarItemBinding.tvCarBrand.setText(arlCar.get(position).getBrand()+" "+arlCar.get(position).getModel());
        }
        if (arlCar.get(position).getYear() != null)
            holder.myListingCarItemBinding.tvYear.setText(context.getString(R.string.year)+": "+arlCar.get(position).getYear());

        if (arlCar.get(position).getEnginetype() != null)
            holder.myListingCarItemBinding.tvModalName.setText(context.getString(R.string.engine_type)+": "+arlCar.get(position).getEnginetype());

        if (arlCar.get(position).getRejectReason() != null) {
//            holder.myListingCarItemBinding.tvCarReject.setText(arlCar.get(position).getRejectReason());
            String text = context.getString(R.string.reason_for_rejection_)+ arlCar.get(position).getRejectReason();
            CommonUtils.spannableTextColor(context,text, R.color.black, 0,context.getString(R.string.reason_for_rejection_).length() , holder.myListingCarItemBinding.tvCarReject);
        }

        holder.myListingCarItemBinding.tvCarReject.setVisibility(View.GONE);
        switch (arlCar.get(position).getListingStatus()){
            case 1:
                holder.myListingCarItemBinding.tvCarStatus.setText(context.getString(R.string.approved));
                holder.myListingCarItemBinding.tvCarStatus.setBackground(ContextCompat.getDrawable(context,R.drawable.car_approved_shape));
                break;
            case 2:
                holder.myListingCarItemBinding.tvCarReject.setVisibility(View.VISIBLE);
                holder.myListingCarItemBinding.tvCarStatus.setText(context.getString(R.string.rejected));
                holder.myListingCarItemBinding.tvCarStatus.setBackground(ContextCompat.getDrawable(context,R.drawable.car_reject_shape));
                break;
            case 0:
                holder.myListingCarItemBinding.tvCarStatus.setText(context.getString(R.string.pending));
                holder.myListingCarItemBinding.tvCarStatus.setBackground(ContextCompat.getDrawable(context,R.drawable.car_pending_shape));
                break;
        }
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                recyclerviewItemClickInterface.onItemClick(position);
//                Intent intent = new Intent(context, ListedCarDetailsActivity.class);
//                intent.putExtra(AppConstant.ID,String.valueOf(arlCar.get(position).getId()));
//                context.startActivity(intent);
            }
        });

    }

    public void showProgress(boolean isLoading){
        this.isLoading = isLoading;
    }

    @Override
    public int getItemCount() {
        return arlCar.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder{

        MyListingCarItemBinding myListingCarItemBinding;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);

            myListingCarItemBinding= DataBindingUtil.bind(itemView);
        }
    }
}