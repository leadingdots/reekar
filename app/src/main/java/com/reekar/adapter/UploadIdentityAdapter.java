package com.reekar.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import androidx.annotation.NonNull;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.RecyclerView;
import com.reekar.R;
import com.reekar.databinding.UploadIdItemBinding;
import com.reekar.interfaces.RecyclerviewItemClickInterface;
import com.reekar.models.UploadFileModel;


import java.util.ArrayList;


public class UploadIdentityAdapter extends RecyclerView.Adapter<UploadIdentityAdapter.ViewHolder> {


    private Context context;
    private String carId;
    private ArrayList<UploadFileModel> uploadFileModelArrayList;

    private RecyclerviewItemClickInterface recyclerviewItemClickInterface;

    public UploadIdentityAdapter(Context context, ArrayList<UploadFileModel> uploadFileModelArrayList, String carId, RecyclerviewItemClickInterface recyclerviewItemClickInterface) {

        this.context = context;
        this.uploadFileModelArrayList = uploadFileModelArrayList;
        this.recyclerviewItemClickInterface=recyclerviewItemClickInterface;
        this.carId=carId;
    }

    @NonNull
    @Override
    public UploadIdentityAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
        View view = layoutInflater.inflate(R.layout.upload_id_item, parent, false);

        UploadIdentityAdapter.ViewHolder viewHolder = new UploadIdentityAdapter.ViewHolder(view);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull final UploadIdentityAdapter.ViewHolder holder, final int position) {

        if (uploadFileModelArrayList.get(position).getFile()!=null && uploadFileModelArrayList.get(position).getFile().getName()!=null)
            holder.uploadIdItemBinding.tvIdDoducmentName.setText(uploadFileModelArrayList.get(position).getFile().getName());
        else {

        }

    }

    @Override
    public int getItemCount() {
//        Log.e("uploadFileModelArrayList", String.valueOf(uploadFileModelArrayList.size()));
        return uploadFileModelArrayList.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder {

        UploadIdItemBinding uploadIdItemBinding;

        ViewHolder(@NonNull View itemView) {
            super(itemView);

            uploadIdItemBinding = DataBindingUtil.bind(itemView);
        }
    }
}