package com.reekar.adapter;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;

import androidx.viewpager.widget.PagerAdapter;
import androidx.viewpager.widget.ViewPager;

import com.akanksha.commonclassutil.PreferencesManager;
import com.reekar.R;
import com.reekar.activity.BankActivity;
import com.reekar.activity.CreditCardActivity;
import com.reekar.activity.PaypalActivity;
import com.reekar.commonclasses.AppConstant;

public class PaymentAdapter extends PagerAdapter implements ViewPager.OnPageChangeListener{

//    public final static float BIG_SCALE = 1.0f;
//    public final static float SMALL_SCALE = 0.7f;
//    public final static float DIFF_SCALE = BIG_SCALE - SMALL_SCALE;

    private Context context;
    private int images[];
    private String from;

    LayoutInflater layoutInflater;
    private LinearLayout linearLeayout;


    public PaymentAdapter(Context context, int images[],String from) {
        this.context = context;
        this.images = images;
        this.from = from;

        layoutInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public int getCount() {
        return images.length;
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view == ((LinearLayout) object);
    }


    @Override
    public Object instantiateItem(ViewGroup container, final int position) {
        View itemView = layoutInflater.inflate(R.layout.payment_adapter_item, container, false);

        ImageView imageView = (ImageView) itemView.findViewById(R.id.imageViewPayment);
        imageView.setImageResource(images[position]);
//        linearLeayout = (LinearLayout) itemView.findViewById(R.id.payment_crousle);
        container.addView(itemView);

        imageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                switch (position){

                    case 0: {
                        PreferencesManager.Companion.saveStringPreferences(context,AppConstant.PAYPAL,"Paypal");
                        Intent intent = new Intent(context, PaypalActivity.class);
                        context.startActivity(intent);
                        break;
                    }
                    case 1:{
                        PreferencesManager.Companion.saveStringPreferences(context,AppConstant.CC,"CC");
                        Intent intent = new Intent(context, CreditCardActivity.class);
                        context.startActivity(intent);
                        break;
                    }
                    case 2:{
                        PreferencesManager.Companion.saveStringPreferences(context,AppConstant.IBAN,"IBAN");
                        Intent intent = new Intent(context, BankActivity.class);
                        context.startActivity(intent);
                        break;
                    }

                }
//                Toast.makeText(context, "you clicked image " + (position + 1), Toast.LENGTH_LONG).show();
            }
        });

        return itemView;
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        container.removeView((LinearLayout) object);
//        super.destroyItem(container, position, object);

    }


    @Override
    public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

    }

    @Override
    public void onPageSelected(int position) {

    }

    @Override
    public void onPageScrollStateChanged(int state) {

    }
}