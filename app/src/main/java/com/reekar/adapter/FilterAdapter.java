package com.reekar.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.core.content.ContextCompat;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.RecyclerView;

import com.reekar.R;
import com.reekar.databinding.FilterItemBinding;
import com.reekar.interfaces.RecyclerviewItemClickInterface;
import com.reekar.models.FilterModel;

import java.util.ArrayList;

public class FilterAdapter extends RecyclerView.Adapter<FilterAdapter.ViewHolder> {

    private ArrayList<FilterModel> filter;

    private int selectedPosition=0;

    private Context context;
    private RecyclerviewItemClickInterface recyclerviewItemClickInterface;

    public FilterAdapter(Context context, ArrayList<FilterModel> filter, RecyclerviewItemClickInterface recyclerviewItemClickInterface) {
        this.context = context;
        this.filter = filter;
        this.recyclerviewItemClickInterface=recyclerviewItemClickInterface;
    }
    @NonNull
    @Override
    public FilterAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
        View view = layoutInflater.inflate(R.layout.filter_item,parent,false);
        FilterAdapter.ViewHolder viewHolder =  new FilterAdapter.ViewHolder(view);
        return viewHolder;
    }

    @NonNull


    @Override
    public void onBindViewHolder(@NonNull final FilterAdapter.ViewHolder holder, final int position) {

        holder.filterItemBinding.filterListTv.setText(filter.get(position).getName());
        holder.filterItemBinding.filterListLl.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                selectedPosition=position;
                recyclerviewItemClickInterface.onItemClick(position);
                notifyDataSetChanged();

            }
        });

        if (selectedPosition==position){
            changeView(holder,holder.filterItemBinding.filterListTv,R.color.colorPrimary,R.color.white);
        }
        else {
            changeView(holder,holder.filterItemBinding.filterListTv,R.color.white,R.color.black);
        }
    }

    private void changeView(ViewHolder holder, TextView textView, int textBackground, int textColor) {
        holder.filterItemBinding.filterListTv.setBackgroundColor(ContextCompat.getColor(context,textBackground));
        holder.filterItemBinding.filterListTv.setTextColor(ContextCompat.getColor(context,textColor));


    }


    @Override
    public int getItemCount() {
        return filter.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder{

        private FilterItemBinding filterItemBinding;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            filterItemBinding= DataBindingUtil.bind(itemView);
        }
    }
}